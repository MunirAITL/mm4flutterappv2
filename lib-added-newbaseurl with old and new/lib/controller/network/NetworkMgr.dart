//  https://github.com/flutterchina/dio
import 'dart:convert';
import 'dart:io';

import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/view_model/rx/UploadProgController.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;
import 'package:http_parser/http_parser.dart';
import 'package:json_string/json_string.dart';
import 'package:mime_type/mime_type.dart';
import 'package:path_provider/path_provider.dart';
import '../../Mixin.dart';
import 'CookieMgr.dart';
import 'ModelMgr.dart';

//typedef mapValue = Function(Map<String, dynamic>);

enum NetEnum { Available, NotAvailable, ServerDown }

enum ReqType {
  Get,
  Post,
  Put,
  Delete,
  Head,
  Patch,
  Copy,
  Options,
  Link,
  UnLink,
  Purge,
  Lock,
  UnLock,
  PropFind,
  View,
}

class NetworkMgr with Mixin {
  final httpCacheReqDays = 7;
  static final NetworkMgr shared = NetworkMgr._internal();
  factory NetworkMgr() {
    return shared;
  }
  NetworkMgr._internal();

  addToken(Dio dio, String url, String accessToken, String refreshToken) {
    return InterceptorsWrapper(
      onRequest: (request, handler) {
        if (accessToken != null && accessToken != '')
          request.headers['Authorization'] = 'Bearer $accessToken';
        return handler.next(request);
      },
      onError: (err, handler) async {
        if (err.response?.statusCode == 401) {
          try {
            await dio
                .post(url, data: jsonEncode({"refreshToken": refreshToken}))
                .then((value) async {
              if (value?.statusCode == 201) {
                //get new tokens ...
                print("acces token" + accessToken);
                print("refresh token" + refreshToken);
                //set bearer
                err.requestOptions.headers["Authorization"] =
                    "Bearer " + accessToken;
                //create request with new access token
                final opts = new Options(
                    method: err.requestOptions.method,
                    headers: err.requestOptions.headers);
                final cloneReq = await dio.request(err.requestOptions.path,
                    options: opts,
                    data: err.requestOptions.data,
                    queryParameters: err.requestOptions.queryParameters);

                return handler.resolve(cloneReq);
              }
              return err;
            });
            return dio;
          } catch (err, st) {
            print(err.toString());
          }
        }
      },
    );

    //dio.options.baseUrl = Server.BASE_URL;
  }

  Future<T> req<T, K>(
      {context,
      url,
      param,
      reqType = ReqType.Post,
      isLoading = true,
      isCookie = false,
      headers,
      UploadProgController progController}) async {
    final NetEnum netEnum = await hasNetwork();
    if (netEnum == NetEnum.Available) {
      try {
        if (isLoading) {
          startLoading();
        }
        myLog("ws::" + reqType.toString() + ": " + url);
        myLog(json.encode(param));

        final accessToken = await PrefMgr.shared.getPrefStr("accessToken");
        final refreshToken = await PrefMgr.shared.getPrefStr("refreshToken");

        var cookieJar = await CookieMgr().getCookiee();
        final c = await cookieJar.loadForRequest(Uri.parse(Server.BASE_URL));
        BaseOptions options = new BaseOptions(
            baseUrl: Server.BASE_URL,
            //receiveDataWhenStatusError: true,
            connectTimeout: 60 * 1000, // 10 seconds
            receiveTimeout: 30 * 1000, // 10 seconds
            headers: {
              "Accept": "application/json",
              "Content-Type": "application/json",
              "Cookie":
                  c.length > 0 ? (c.first.name + "=" + c.first.value) : "",
              "Authorization": "Bearer $accessToken",
              "refreshToken": refreshToken,
            });

        var _dio = new Dio(options);
        if (isCookie) {
          cookieJar.saveFromResponse(Uri.parse(Server.BASE_URL), c);
          if (accessToken != null && refreshToken != null) {
            _dio.interceptors.addAll([
              CookieManager(cookieJar),
              addToken(_dio, url, accessToken, refreshToken),
            ]);
          } else {
            _dio.interceptors.add(CookieManager(cookieJar));
          }
        }

        /*CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.addAll([
          CookieManager(cj),
          //addToken(_dio, url, accessToken, refreshToken),
        ]);*/

        // ]);
        /*if (isCookie) {
          cj.saveFromResponse(
              Uri.parse(url), await cj.loadForRequest(Uri.parse(url)));
          log(cj.loadForRequest(Uri.parse(url)).toString());
        }*/

        /*_dio.interceptors
            .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
          // Do something before request is sent
          options.headers["Authorization"] = "Bearer ";
          return options;
        }));*/

        var response;
        if (reqType == ReqType.Post)
          response = await _dio.post(
            url,
            data: param,
            onSendProgress: (int sent, int total) {
              final progress = sent / total;
              if (progController != null)
                progController.progress.value = progress;
              //print('progress: $progress ($sent/$total)');
            },
            onReceiveProgress: (int sent, int total) {
              //final progress = sent / total;
              //progController.progress.value = progress;
              //print('progress: $progress ($sent/$total)');
            },
            //options: buildCacheOptions(Duration(days: httpCacheReqDays)),
          );
        else if (reqType == ReqType.Put)
          response = await _dio.put(
            url,
            data: param,
            onSendProgress: (int sent, int total) {
              final progress = sent / total;
              if (progController != null)
                progController.progress.value = progress;
              //print('progress: $progress ($sent/$total)');
            },
            //options: buildCacheOptions(Duration(days: httpCacheReqDays)),
          );
        else if (reqType == ReqType.Delete)
          response = await _dio.delete(url);
        else if (reqType == ReqType.Get)
          response = await _dio.get(
            url,
            queryParameters: param ?? {},
            //options: buildCacheOptions(Duration(days: httpCacheReqDays)),
          );
        else if (reqType == ReqType.Head)
          response = await _dio.head(url);
        else if (reqType == ReqType.Patch) response = await _dio.patch(url);
        _dio.close();

        if (isCookie) {
          print(await cookieJar.loadForRequest(Uri.parse(Server.BASE_URL)));
        }

        final jsonString = JsonString(json.encode(response.data));

        myLog(jsonString.source);
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          //final m = FromJsonModel.typeOf<T>();
          //print((m as T)(mapRes));
          //.fromJson(mapRes);
          return ModelMgr().fromJson(mapRes);

          //final c = BaseModel.typeOf<T>().toString();
          //print(t);
          //final m = c.fromJson(mapRes);
          //return m as T;
          //return t;
        } else {
          return null;
        }
      } on DioError catch (e) {
        try {
          if (e.response.statusCode == HttpStatus.notAcceptable) {
            //  406 = not acceptable client error -> expired oauth2
            //showToast(context: context, msg: "Sorry, your access token has been expired, please re-login", which: 0);
            //Future.delayed(Duration(seconds: 3), () {});
            //StateProvider().notify(ObserverState.STATE_LOGOUT, null);
          }
        } catch (e) {}

        if (e.type == DioErrorType.connectTimeout) {
          myLog("******************** NETWORK ERROR - CONNECT TIMEOUT " + url);
        } else if (e.type == DioErrorType.receiveTimeout) {
          myLog("******************** NETWORK ERROR - RECEIVE TIMEOUT " + url);
        } else {
          myLog("******************** NETWORK ERROR " + url);
        }
        myLog(json.encode(param));
        myLog(e.toString());
        myLog(
            "***************************************************************");
        if (isLoading) {
          stopLoading();
        }
      }
    } else if (netEnum == NetEnum.NotAvailable) {
      showSnake(context, "Not connected!");
    } else if (netEnum == NetEnum.ServerDown) {
      showSnake(context, "Sorry, web server is down please try later. thanks");
    }
    return null;
  }

  //  files only
  Future<T> uploadFiles<T, K>(
      {context,
      url,
      List<File> files,
      isLoading = true,
      UploadProgController progController}) async {
    try {
      final NetEnum netEnum = await hasNetwork();
      if (netEnum == NetEnum.Available) {
        if (isLoading) {
          startLoading();
        }
        myLog("ws::uploadFiles:: files only  ==================" + url);
        var formData = FormData();
        for (var file in files) {
          //final String mimeStr = lookupMimeType(file.path);
          //var fileType = mimeStr.split('/');
          //myLog(fileType[0]);
          //myLog('file type ${mimeStr}, ${fileType}');
          //final String fileName = file.path.split('/').last;
          //myLog(MediaType(fileType[0], mimeStr));
          /*formData.files.addAll(
            [
              MapEntry(
                  "file",
                  await MultipartFile.fromFile(file.path,
                      filename: basename(file.path),
                      contentType: MediaType(fileType[0], mimeStr)))
            ],
          );*/

          String mimeType = mime(file.path);
          String mimee = mimeType.split('/')[0];
          String type = mimeType.split('/')[1];

          formData = FormData.fromMap({
            "files": [
              await MultipartFile.fromFile(
                file.path,
                filename: file.path,
                contentType: MediaType(mimee, type),
              )
            ],
          });
        }

        final accessToken = await PrefMgr.shared.getPrefStr("accessToken");
        final refreshToken = await PrefMgr.shared.getPrefStr("refreshToken");

        BaseOptions options = new BaseOptions(
            baseUrl: Server.BASE_URL,
            //receiveDataWhenStatusError: true,
            connectTimeout: 60 * 1000, // 10 seconds
            receiveTimeout: 30 * 1000, // 10 seconds
            headers: {
              "Accept": "application/json",
              "Content-Type": "application/json",
              "Authorization": "Bearer $accessToken",
              "refreshToken": refreshToken,
            });

        var _dio = new Dio(options);
        _dio.options.headers = {
          'Content-type': 'multipart/form-data',
        };
        CookieJar cj = await CookieMgr().getCookiee();
        _dio.interceptors.add(CookieManager(cj));
        cj.saveFromResponse(
            Uri.parse(url), await cj.loadForRequest(Uri.parse(url)));
        print(cj.loadForRequest(Uri.parse(url)));
        final response = await _dio.post(
          url,
          data: formData,
          onSendProgress: (int sent, int total) {
            final progress = sent / total;
            if (progController != null)
              progController.progress.value = progress;
            //print('progress: $progress ($sent/$total)');
          },
        );
        _dio.close();
        myLog(response.data.toString());
        if (isLoading) {
          stopLoading();
        }
        if (response.statusCode == HttpStatus.ok) {
          final mapRes = response.data;
          return ModelMgr().fromJson(mapRes);
        } else {
          return null;
        }
      } else if (netEnum == NetEnum.NotAvailable) {
        showSnake(context, "Not connected!");
      } else if (netEnum == NetEnum.ServerDown) {
        showSnake(
            context, "Sorry, web server is down please try later. thanks");
      }
    } catch (e) {
      myLog("******************** NETWORK ERROR " + url);
      myLog(e.toString());
      myLog("***************************************************************");
      if (isLoading) {
        stopLoading();
      }
    }
  }

  downloadFile({
    BuildContext context,
    String url,
    isLoading = true,
    Function(File) callback,
  }) async {
    try {
      final NetEnum netEnum = await hasNetwork();
      if (netEnum == NetEnum.Available) {
        if (isLoading) {
          startLoading();
        }
        var response = await get(Uri.parse(url));
        if (response.statusCode == 200) {
          myLog("ws::downloadFile ==================" + url);
          final String fileName = url.split('/').last;
          final dir = "/HT/";
          Directory directory;
          if (Platform.isIOS) {
            directory = await getApplicationDocumentsDirectory();
          } else {
            directory = await getApplicationDocumentsDirectory();
          }

          directory = Directory(directory.path + dir);
          if (!await directory.exists()) {
            await directory.create(recursive: true);
          }
          if (await directory.exists()) {
            String fullPath = directory.path + fileName;
            File file = File(fullPath);
            var raf = file.openSync(mode: FileMode.write);
            raf.writeFromSync(response.bodyBytes);
            await raf.close();
            callback(file);
          }
        }

        if (isLoading) {
          stopLoading();
        }
      } else if (netEnum == NetEnum.NotAvailable) {
        showSnake(context, "Not connected!");
      } else if (netEnum == NetEnum.ServerDown) {
        showSnake(
            context, "Sorry, web server is down please try later. thanks");
      }
    } catch (e) {
      myLog("******************** NETWORK ERROR " + url);
      myLog(e.toString());
      myLog("***************************************************************");
      if (isLoading) {
        stopLoading();
      }
    }
  }

  dispose() {
    try {
      stopLoading();
      //_dio.close();
      //_dio = null;
    } catch (e) {}
  }

  Future<NetEnum> hasNetwork() async {
    /*var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return await isServerLive();
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return await isServerLive();
    }
    return NetEnum.NotAvailable;*/
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return NetEnum.Available;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return NetEnum.Available;
    }

    return NetEnum.NotAvailable;
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
