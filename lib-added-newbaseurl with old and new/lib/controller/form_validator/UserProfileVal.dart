import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class UserProfileVal with Mixin {
  static const int PHONE_LIMIT = 8;
  static const EMAIL_REG =
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9-]+\.[a-zA-Z]+";
  static const PHONE_REG = r'(^(?:[+0])?[0-9]{10,12}$)';

  static bool isValid(String str) {
    if (str.isNotEmpty) {
      return true;
    }
    return false;
  }

  isEmpty(BuildContext context, TextEditingController tf, arg) {
    if (tf.text.isEmpty) {
      showToast(context: context, msg: arg);
      return true;
    }
    return false;
  }

  isFullName(BuildContext context, String name) {
    try {
      final isOk = (name.trim().contains(" ")) ? true : false;
      if (!isOk) {
        showToast(context: context, msg: "Please enter your valid full name");
      }
      return isOk;
    } catch (e) {
      return false;
    }
  }

  isFNameOK(BuildContext context, TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(context: context, msg: "Please enter your first name");
      return false;
    }
    return true;
  }

  isLNameOK(BuildContext context, TextEditingController tf) {
    if (tf.text.length == 0) {
      showToast(context: context, msg: "Please enter your last name");
      return false;
    }
    return true;
  }

  isEmailOK(BuildContext context, TextEditingController tf, String msg) {
    if (!RegExp(EMAIL_REG).hasMatch(tf.text.trim())) {
      showToast(context: context, msg: msg);
      return false;
    }
    return true;
  }

  isPhoneOK(BuildContext context, TextEditingController tf) {
    if (tf.text.length < PHONE_LIMIT) {
      showToast(context: context, msg: "Please enter your mobile number");
      return false;
    }
    return true;
  }

  isPwdOK(BuildContext context, TextEditingController tf) {
    if (tf.text.length < 3) {
      showToast(
          context: context,
          msg: "Please enter password at least 3 characters long or greater");
      return false;
    }
    return true;
  }

  isComNameOK(BuildContext context, TextEditingController tf) {
    if (tf.text.length < 6) {
      showToast(context: context, msg: "Please enter your company name");
      return false;
    }
    return true;
  }

  isDOBOK(BuildContext context, str) {
    if (str == '') {
      showToast(
          context: context, msg: "Please choose your valid date of birth");
      return false;
    }
    return true;
  }
}
