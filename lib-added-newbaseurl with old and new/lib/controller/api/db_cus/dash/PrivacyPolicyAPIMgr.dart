import 'dart:developer';

import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/tab_dash/PrivacyPolicyAPIModel.dart';
import 'package:flutter/material.dart';

import '../../../../config/Server.dart';
import '../../../../model/data/UserData.dart';

class PrivacyPolicyAPIMgr {
  static final PrivacyPolicyAPIMgr _shared = PrivacyPolicyAPIMgr._internal();

  factory PrivacyPolicyAPIMgr() {
    return _shared;
  }

  PrivacyPolicyAPIMgr._internal();

  wsOnLoad({
    BuildContext context,
    Function(PrivacyPolicyAPIModel) callback,
  }) async {
    try {
      var url = Server.TERMS_POLICY;
      url = url.replaceAll(
          "#UserCompanyId#", userData.userModel.userCompanyID.toString());
      log("PrivacyPolicyHelper= " + url);
      await NetworkMgr()
          .req<PrivacyPolicyAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      log(e.toString());
    }
  }
}
