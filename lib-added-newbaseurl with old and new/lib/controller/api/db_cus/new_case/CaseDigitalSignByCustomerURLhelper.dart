import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/UserData.dart';

class CaseDigitalSignByCustomerURLHelper {
  static getUrl({String caseID}) {
    var url = Server.CaseDigitalSignByCustomerURL;
    url = url.replaceAll("#CaseID#", caseID.toString());

    return url;
  }
}
