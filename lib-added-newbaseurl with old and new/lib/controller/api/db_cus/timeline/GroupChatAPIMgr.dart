import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_mycases/TaskInfoSearchAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/GetCaseGroupChatAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';

class GroupChatAPIMgr with Mixin {
  static final GroupChatAPIMgr _shared = GroupChatAPIMgr._internal();

  factory GroupChatAPIMgr() {
    return _shared;
  }

  GroupChatAPIMgr._internal();

  wsOnPageLoad({
    BuildContext context,
    int pageStart,
    int pageCount,
    int caseStatus,
    Function(GetCaseGroupChatAPIModel) callback,
  }) async {
    try {
      var url = Server.GETCASELISTGROUPCHAT_GET_URL;
      url = url.replaceAll("#customerId#", userData.userModel.id.toString());
      url = url.replaceAll(
          "#companyId#", userData.userModel.userCompanyID.toString());
      myLog("My Case URL " + url);
      await NetworkMgr()
          .req<GetCaseGroupChatAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: (pageStart == 0) ? true : false,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
