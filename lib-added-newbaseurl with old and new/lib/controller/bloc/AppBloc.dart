import 'dart:async';

import 'package:aitl/controller/bloc/LinkBloc.dart';

class AppBloc {
  AppBloc(data);

  final _userStreamController = StreamController<LinkState>();

  Stream<LinkState> get user => _userStreamController.stream;

  void loadUserData() {
    _userStreamController.sink.add(LinkState._userLoading());
  }

  void dispose() {
    _userStreamController.close();
  }
}

class LinkState {
  LinkState();
  factory LinkState._userLoading() = UserLoadingState;
}

class UserInitState extends LinkState {}

class UserLoadingState extends LinkState {}

class UserDataState extends LinkState {
  UserDataState();
}
