import 'package:aitl/config/Server.dart';

class CreditURLbHelper {
  getUrl({int userCompanyId, int userID}) {
    var url = Server.GET_USER_VALIDATION_FOR_OLD_USER_URL;
    url = url.replaceAll("#UserId#", userID.toString());
    url = url.replaceAll("#UserCompanyId#", userCompanyId.toString());
    return url;
  }
}
