import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/UserData.dart';

class MessageTypeURIHelper {
  getUrl({adviserOrIntroducerId}) {
    var url = Server.TIMELINE_MESSAGE_TYPE_URL;
    url = url.replaceAll(
        "#UserCompanyId#", userData.userModel.userCompanyID.toString());
    url = url.replaceAll("#CustomerId#", userData.userModel.id.toString());
    url = url.replaceAll(
        "#AdviserOrIntroducerId#", adviserOrIntroducerId.toString());

    return url;
  }
}
