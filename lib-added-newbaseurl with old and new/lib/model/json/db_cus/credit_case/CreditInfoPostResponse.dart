class CreditInfoPostResponse {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  CreditInfoPostResponseData responseData;

  CreditInfoPostResponse({this.success, this.errorMessages, this.messages, this.responseData});

  CreditInfoPostResponse.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null ? new ErrorMessages.fromJson(json['ErrorMessages']) : null;
    messages = json['Messages'] != null ? new ErrorMessages.fromJson(json['Messages']) : null;
    responseData = json['ResponseData'] != null ? new CreditInfoPostResponseData.fromJson(json['ResponseData']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {


ErrorMessages.fromJson(Map<String, dynamic> json) {
}

Map<String, dynamic> toJson() {
  final Map<String, dynamic> data = new Map<String, dynamic>();
  return data;
}
}

class CreditInfoPostResponseData {
  ValidateNewUser validateNewUser;

  CreditInfoPostResponseData({this.validateNewUser});

  CreditInfoPostResponseData.fromJson(Map<String, dynamic> json) {
    validateNewUser = json['ValidateNewUser'] != null ? new ValidateNewUser.fromJson(json['ValidateNewUser']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.validateNewUser != null) {
      data['ValidateNewUser'] = this.validateNewUser.toJson();
    }
    return data;
  }
}

class ValidateNewUser {
  ValidateNewUserResult validateNewUserResult;

  ValidateNewUser({this.validateNewUserResult});

  ValidateNewUser.fromJson(Map<String, dynamic> json) {
    validateNewUserResult = json['ValidateNewUserResult'] != null ? new ValidateNewUserResult.fromJson(json['ValidateNewUserResult']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.validateNewUserResult != null) {
      data['ValidateNewUserResult'] = this.validateNewUserResult.toJson();
    }
    return data;
  }
}

class ValidateNewUserResult {
  Null message;
  Null status;
  Null failureReason;
  int identityValidationOutcome;
  String validationIdentifier;

  ValidateNewUserResult({this.message, this.status, this.failureReason, this.identityValidationOutcome, this.validationIdentifier});

  ValidateNewUserResult.fromJson(Map<String, dynamic> json) {
    message = json['Message'];
    status = json['Status'];
    failureReason = json['FailureReason'];
    identityValidationOutcome = json['IdentityValidationOutcome'];
    validationIdentifier = json['ValidationIdentifier'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Message'] = this.message;
    data['Status'] = this.status;
    data['FailureReason'] = this.failureReason;
    data['IdentityValidationOutcome'] = this.identityValidationOutcome;
    data['ValidationIdentifier'] = this.validationIdentifier;
    return data;
  }
}
