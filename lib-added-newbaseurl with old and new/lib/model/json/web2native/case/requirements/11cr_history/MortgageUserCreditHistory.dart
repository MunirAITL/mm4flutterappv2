class MortgageUserCreditHistory {
  int userId;
  dynamic user;
  int companyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  String haveYouEverHadAMortgageOrLoanApplicationRefused;
  String haveYouEverHadAJudgementForDebtOrLoanDefaultRegisteredAgainstYou;
  String haveYouEverBeenDeclaredBankrupt;
  String haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgage;
  String
      haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreement;
  String haveYouEverHadAMortgageOrLoanApplicationRefusedDetails;
  String haveYouEverBeenDeclaredBankruptDetails;
  String
      haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgageDetails;
  String
      haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreementDetails;
  String remarks;
  String haveYouHadAMissedMortgagePayment;
  int id;

  MortgageUserCreditHistory(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.haveYouEverHadAMortgageOrLoanApplicationRefused,
      this.haveYouEverHadAJudgementForDebtOrLoanDefaultRegisteredAgainstYou,
      this.haveYouEverBeenDeclaredBankrupt,
      this.haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgage,
      this.haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreement,
      this.haveYouEverHadAMortgageOrLoanApplicationRefusedDetails,
      this.haveYouEverBeenDeclaredBankruptDetails,
      this.haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgageDetails,
      this.haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreementDetails,
      this.remarks,
      this.haveYouHadAMissedMortgagePayment,
      this.id});

  MortgageUserCreditHistory.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    companyId = json['CompanyId'] ?? 0;
    status = json['Status'] ?? 0;
    mortgageCaseInfoId = json['MortgageCaseInfoId'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    haveYouEverHadAMortgageOrLoanApplicationRefused =
        json['HaveYouEverHadAMortgageOrLoanApplicationRefused'] ?? '';
    haveYouEverHadAJudgementForDebtOrLoanDefaultRegisteredAgainstYou = json[
            'HaveYouEverHadAJudgementForDebtOrLoanDefaultRegisteredAgainstYou'] ??
        '';
    haveYouEverBeenDeclaredBankrupt =
        json['HaveYouEverBeenDeclaredBankrupt'] ?? '';
    haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgage = json[
            'HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgage'] ??
        '';
    haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreement =
        json['HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreement'] ??
            '';
    haveYouEverHadAMortgageOrLoanApplicationRefusedDetails =
        json['HaveYouEverHadAMortgageOrLoanApplicationRefusedDetails'] ?? '';
    haveYouEverBeenDeclaredBankruptDetails =
        json['HaveYouEverBeenDeclaredBankruptDetails'] ?? '';
    haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgageDetails =
        json['HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgageDetails'] ??
            '';
    haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreementDetails =
        json['HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreementDetails'] ??
            '';
    remarks = json['Remarks'] ?? '';
    haveYouHadAMissedMortgagePayment =
        json['HaveYouHadAMissedMortgagePayment'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['HaveYouEverHadAMortgageOrLoanApplicationRefused'] =
        this.haveYouEverHadAMortgageOrLoanApplicationRefused;
    data['HaveYouEverHadAJudgementForDebtOrLoanDefaultRegisteredAgainstYou'] =
        this.haveYouEverHadAJudgementForDebtOrLoanDefaultRegisteredAgainstYou;
    data['HaveYouEverBeenDeclaredBankrupt'] =
        this.haveYouEverBeenDeclaredBankrupt;
    data['HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgage'] =
        this.haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgage;
    data['HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreement'] =
        this.haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreement;
    data['HaveYouEverHadAMortgageOrLoanApplicationRefusedDetails'] =
        this.haveYouEverHadAMortgageOrLoanApplicationRefusedDetails;
    data['HaveYouEverBeenDeclaredBankruptDetails'] =
        this.haveYouEverBeenDeclaredBankruptDetails;
    data['HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgageDetails'] =
        this.haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentMortgageDetails;
    data['HaveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreementDetails'] =
        this.haveYouEverFailedToKeepUpRepaymentsUnderAnyPreviousOrCurrentRentalOrLoanAgreementDetails;
    data['Remarks'] = this.remarks;
    data['HaveYouHadAMissedMortgagePayment'] =
        this.haveYouHadAMissedMortgagePayment;
    data['Id'] = this.id;
    return data;
  }
}
