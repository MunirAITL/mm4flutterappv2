import 'package:aitl/model/json/web2native/case/requirements/11cr_history/MortgageUserCreditHistory.dart';

class PostCreditHisAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  PostCreditHisAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostCreditHisAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  MortgageUserCreditHistory mortgageUserCreditHistory;
  ResponseData({this.mortgageUserCreditHistory});
  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageUserCreditHistory = json['MortgageUserCreditHistory'] != null
        ? new MortgageUserCreditHistory.fromJson(
            json['MortgageUserCreditHistory'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserCreditHistory != null) {
      data['MortgageUserCreditHistory'] =
          this.mortgageUserCreditHistory.toJson();
    }
    return data;
  }
}
