import 'package:aitl/model/json/web2native/case/requirements/11cr_history/MortgageUserCreditHistoryItems.dart';

class PostCreditHisItemsAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  PostCreditHisItemsAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostCreditHisItemsAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  MortgageUserCreditHistoryItems mortgageUserCreditHistoryItem;
  ResponseData({this.mortgageUserCreditHistoryItem});
  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageUserCreditHistoryItem =
        json['MortgageUserCreditHistoryItem'] != null
            ? new MortgageUserCreditHistoryItems.fromJson(
                json['MortgageUserCreditHistoryItem'])
            : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserCreditHistoryItem != null) {
      data['MortgageUserCreditHistoryItem'] =
          this.mortgageUserCreditHistoryItem.toJson();
    }
    return data;
  }
}
