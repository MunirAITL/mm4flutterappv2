class MortgageEquityReleasePowerOfAttorneyModel {
  int status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  int userId;
  int companyId;
  int taskId;
  String haveArrangementsInPlaceDate;
  String haveArrangementsInPlaceDetails;
  String canOtherAssetsConsideredAsAlternative;
  String haveConsideredPensionAsAlternative;
  String willStateBenefitsAffected;
  String areAnyAdditionalGrantAvailable;
  String wouldFamilyPreparedForSupport;
  String wouldPreparedToPayMonthlyInterest;
  String doesClientConsiderLifetimeMortgage;
  String wouldYouWishFullOwnershipOfProperty;
  String haveConsideredRentingOutYourHome;
  String haveConsideredDownsizing;
  String areYouAwarethisWillReduceValueOfStateOnDeath;
  String haveMadeThisAwareToBeneficiaries;
  String howMuchCapitalYouLikeToReleaseFromProperty;
  String wouldYouLIkeToReleaseMaximumValueFromPropety;
  String doYouWishSmallerProportion;
  String remarks;
  String areLookingForAdditionalFunds;
  String thirdPartyAtDiscussion;
  String relationshipToOtherClient;
  String detailHereTheClientObjectivesAndPriorities;
  String wouldIncreaseInCapitalOrIncomeHaveEffect;
  String statementOfHealthIncludingMedication;
  String doesClientHaveSpecialConsideration;
  int id;

  MortgageEquityReleasePowerOfAttorneyModel(
      {this.status,
      this.creationDate,
      this.updatedDate,
      this.versionNumber,
      this.userId,
      this.companyId,
      this.taskId,
      this.haveArrangementsInPlaceDate,
      this.haveArrangementsInPlaceDetails,
      this.canOtherAssetsConsideredAsAlternative,
      this.haveConsideredPensionAsAlternative,
      this.willStateBenefitsAffected,
      this.areAnyAdditionalGrantAvailable,
      this.wouldFamilyPreparedForSupport,
      this.wouldPreparedToPayMonthlyInterest,
      this.doesClientConsiderLifetimeMortgage,
      this.wouldYouWishFullOwnershipOfProperty,
      this.haveConsideredRentingOutYourHome,
      this.haveConsideredDownsizing,
      this.areYouAwarethisWillReduceValueOfStateOnDeath,
      this.haveMadeThisAwareToBeneficiaries,
      this.howMuchCapitalYouLikeToReleaseFromProperty,
      this.wouldYouLIkeToReleaseMaximumValueFromPropety,
      this.doYouWishSmallerProportion,
      this.remarks,
      this.areLookingForAdditionalFunds,
      this.thirdPartyAtDiscussion,
      this.relationshipToOtherClient,
      this.detailHereTheClientObjectivesAndPriorities,
      this.wouldIncreaseInCapitalOrIncomeHaveEffect,
      this.statementOfHealthIncludingMedication,
      this.doesClientHaveSpecialConsideration,
      this.id});

  MortgageEquityReleasePowerOfAttorneyModel.fromJson(
      Map<String, dynamic> json) {
    status = json['Status'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    updatedDate = json['UpdatedDate'] ?? '';
    versionNumber = json['VersionNumber'] ?? 0;
    userId = json['UserId'] ?? 0;
    companyId = json['CompanyId'] ?? 0;
    taskId = json['TaskId'] ?? 0;
    haveArrangementsInPlaceDate = json['HaveArrangementsInPlaceDate'] ?? '';
    haveArrangementsInPlaceDetails =
        json['HaveArrangementsInPlaceDetails'] ?? '';
    canOtherAssetsConsideredAsAlternative =
        json['CanOtherAssetsConsideredAsAlternative'] ?? '';
    haveConsideredPensionAsAlternative =
        json['HaveConsideredPensionAsAlternative'] ?? '';
    willStateBenefitsAffected = json['WillStateBenefitsAffected'] ?? '';
    areAnyAdditionalGrantAvailable =
        json['AreAnyAdditionalGrantAvailable'] ?? '';
    wouldFamilyPreparedForSupport = json['WouldFamilyPreparedForSupport'] ?? '';
    wouldPreparedToPayMonthlyInterest =
        json['WouldPreparedToPayMonthlyInterest'] ?? '';
    doesClientConsiderLifetimeMortgage =
        json['DoesClientConsiderLifetimeMortgage'] ?? '';
    wouldYouWishFullOwnershipOfProperty =
        json['WouldYouWishFullOwnershipOfProperty'] ?? '';
    haveConsideredRentingOutYourHome =
        json['HaveConsideredRentingOutYourHome'] ?? '';
    haveConsideredDownsizing = json['HaveConsideredDownsizing'] ?? '';
    areYouAwarethisWillReduceValueOfStateOnDeath =
        json['AreYouAwarethisWillReduceValueOfStateOnDeath'] ?? '';
    haveMadeThisAwareToBeneficiaries =
        json['HaveMadeThisAwareToBeneficiaries'] ?? '';
    howMuchCapitalYouLikeToReleaseFromProperty =
        json['HowMuchCapitalYouLikeToReleaseFromProperty'] ?? '';
    wouldYouLIkeToReleaseMaximumValueFromPropety =
        json['WouldYouLIkeToReleaseMaximumValueFromPropety'] ?? '';
    doYouWishSmallerProportion = json['DoYouWishSmallerProportion'] ?? '';
    remarks = json['Remarks'] ?? '';
    areLookingForAdditionalFunds = json['AreLookingForAdditionalFunds'] ?? '';
    thirdPartyAtDiscussion = json['ThirdPartyAtDiscussion'] ?? '';
    relationshipToOtherClient = json['RelationshipToOtherClient'] ?? '';
    detailHereTheClientObjectivesAndPriorities =
        json['DetailHereTheClientObjectivesAndPriorities'] ?? '';
    wouldIncreaseInCapitalOrIncomeHaveEffect =
        json['WouldIncreaseInCapitalOrIncomeHaveEffect'] ?? '';
    statementOfHealthIncludingMedication =
        json['StatementOfHealthIncludingMedication'] ?? '';
    doesClientHaveSpecialConsideration =
        json['DoesClientHaveSpecialConsideration'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['UserId'] = this.userId;
    data['CompanyId'] = this.companyId;
    data['TaskId'] = this.taskId;
    data['HaveArrangementsInPlaceDate'] = this.haveArrangementsInPlaceDate;
    data['HaveArrangementsInPlaceDetails'] =
        this.haveArrangementsInPlaceDetails;
    data['CanOtherAssetsConsideredAsAlternative'] =
        this.canOtherAssetsConsideredAsAlternative;
    data['HaveConsideredPensionAsAlternative'] =
        this.haveConsideredPensionAsAlternative;
    data['WillStateBenefitsAffected'] = this.willStateBenefitsAffected;
    data['AreAnyAdditionalGrantAvailable'] =
        this.areAnyAdditionalGrantAvailable;
    data['WouldFamilyPreparedForSupport'] = this.wouldFamilyPreparedForSupport;
    data['WouldPreparedToPayMonthlyInterest'] =
        this.wouldPreparedToPayMonthlyInterest;
    data['DoesClientConsiderLifetimeMortgage'] =
        this.doesClientConsiderLifetimeMortgage;
    data['WouldYouWishFullOwnershipOfProperty'] =
        this.wouldYouWishFullOwnershipOfProperty;
    data['HaveConsideredRentingOutYourHome'] =
        this.haveConsideredRentingOutYourHome;
    data['HaveConsideredDownsizing'] = this.haveConsideredDownsizing;
    data['AreYouAwarethisWillReduceValueOfStateOnDeath'] =
        this.areYouAwarethisWillReduceValueOfStateOnDeath;
    data['HaveMadeThisAwareToBeneficiaries'] =
        this.haveMadeThisAwareToBeneficiaries;
    data['HowMuchCapitalYouLikeToReleaseFromProperty'] =
        this.howMuchCapitalYouLikeToReleaseFromProperty;
    data['WouldYouLIkeToReleaseMaximumValueFromPropety'] =
        this.wouldYouLIkeToReleaseMaximumValueFromPropety;
    data['DoYouWishSmallerProportion'] = this.doYouWishSmallerProportion;
    data['Remarks'] = this.remarks;
    data['AreLookingForAdditionalFunds'] = this.areLookingForAdditionalFunds;
    data['ThirdPartyAtDiscussion'] = this.thirdPartyAtDiscussion;
    data['RelationshipToOtherClient'] = this.relationshipToOtherClient;
    data['DetailHereTheClientObjectivesAndPriorities'] =
        this.detailHereTheClientObjectivesAndPriorities;
    data['WouldIncreaseInCapitalOrIncomeHaveEffect'] =
        this.wouldIncreaseInCapitalOrIncomeHaveEffect;
    data['StatementOfHealthIncludingMedication'] =
        this.statementOfHealthIncludingMedication;
    data['DoesClientHaveSpecialConsideration'] =
        this.doesClientHaveSpecialConsideration;
    data['Id'] = this.id;
    return data;
  }
}
