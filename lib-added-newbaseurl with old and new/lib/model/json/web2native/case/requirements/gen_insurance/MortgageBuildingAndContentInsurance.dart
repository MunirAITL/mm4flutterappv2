class MortgageBuildingAndContentInsurance {
  int status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  int userId;
  int companyId;
  int taskId;
  String type;
  String riskPropertyAddress;
  int numberOfBedrooms;
  String typeOfProperty;
  String yearOfConstruction;
  String doWantToAddAccidentalDamageCover;
  double buildingsCoverAmount;
  int noOfClaimsDiscount;
  double contentsCoverAmount;
  String contentsExcess;
  String accidentalDamage;
  String homeEmergencyCover;
  String familyLegalExpenses;
  String postCode;
  String remarks;
  int id;

  MortgageBuildingAndContentInsurance(
      {this.status,
      this.creationDate,
      this.updatedDate,
      this.versionNumber,
      this.userId,
      this.companyId,
      this.taskId,
      this.type,
      this.riskPropertyAddress,
      this.numberOfBedrooms,
      this.typeOfProperty,
      this.yearOfConstruction,
      this.doWantToAddAccidentalDamageCover,
      this.buildingsCoverAmount,
      this.noOfClaimsDiscount,
      this.contentsCoverAmount,
      this.contentsExcess,
      this.accidentalDamage,
      this.homeEmergencyCover,
      this.familyLegalExpenses,
      this.postCode,
      this.remarks,
      this.id});

  MortgageBuildingAndContentInsurance.fromJson(Map<String, dynamic> json) {
    status = json['Status'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    updatedDate = json['UpdatedDate'] ?? '';
    versionNumber = json['VersionNumber'] ?? 0;
    userId = json['UserId'];
    companyId = json['CompanyId'] ?? 0;
    taskId = json['TaskId'] ?? 0;
    type = json['Type'] ?? '';
    riskPropertyAddress = json['RiskPropertyAddress'] ?? '';
    numberOfBedrooms = json['NumberOfBedrooms'] ?? 0;
    typeOfProperty = json['TypeOfProperty'] ?? '';
    yearOfConstruction = json['YearOfConstruction'] ?? '';
    doWantToAddAccidentalDamageCover =
        json['DoWantToAddAccidentalDamageCover'] ?? '';
    buildingsCoverAmount = json['BuildingsCoverAmount'] ?? 0;
    noOfClaimsDiscount = json['NoOfClaimsDiscount'] ?? 0;
    contentsCoverAmount = json['ContentsCoverAmount'] ?? 0;
    contentsExcess = json['ContentsExcess'] ?? '';
    accidentalDamage = json['AccidentalDamage'] ?? '';
    homeEmergencyCover = json['HomeEmergencyCover'] ?? '';
    familyLegalExpenses = json['FamilyLegalExpenses'] ?? '';
    postCode = json['PostCode'] ?? '';
    remarks = json['Remarks'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['UserId'] = this.userId;
    data['CompanyId'] = this.companyId;
    data['TaskId'] = this.taskId;
    data['Type'] = this.type;
    data['RiskPropertyAddress'] = this.riskPropertyAddress;
    data['NumberOfBedrooms'] = this.numberOfBedrooms;
    data['TypeOfProperty'] = this.typeOfProperty;
    data['YearOfConstruction'] = this.yearOfConstruction;
    data['DoWantToAddAccidentalDamageCover'] =
        this.doWantToAddAccidentalDamageCover;
    data['BuildingsCoverAmount'] = this.buildingsCoverAmount;
    data['NoOfClaimsDiscount'] = this.noOfClaimsDiscount;
    data['ContentsCoverAmount'] = this.contentsCoverAmount;
    data['ContentsExcess'] = this.contentsExcess;
    data['AccidentalDamage'] = this.accidentalDamage;
    data['HomeEmergencyCover'] = this.homeEmergencyCover;
    data['FamilyLegalExpenses'] = this.familyLegalExpenses;
    data['PostCode'] = this.postCode;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}
