import 'MortgageFinancialDependantsModel.dart';

class GetDependentsAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetDependentsAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetDependentsAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<MortgageFinancialDependantsModel> mortgageFinancialDependants;
  ResponseData({this.mortgageFinancialDependants});
  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['MortgageFinancialDependants'] != null) {
      //  for get api
      mortgageFinancialDependants = <MortgageFinancialDependantsModel>[];
      final List<dynamic> list = json['MortgageFinancialDependants'];
      list.forEach((v) {
        if (v != null)
          mortgageFinancialDependants
              .add(new MortgageFinancialDependantsModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageFinancialDependants != null) {
      data['MortgageFinancialDependants'] =
          this.mortgageFinancialDependants.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
