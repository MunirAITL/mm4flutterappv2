import 'MobileUserOTPModel.dart';

class SendOtpNotiAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  SendOtpNotiAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  SendOtpNotiAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  List<String> postUserotp;
  ErrorMessages({this.postUserotp});
  ErrorMessages.fromJson(Map<String, dynamic> json) {
    try {
      postUserotp = json['post_userotp'].cast<String>() ?? [];
    } catch (e) {
      postUserotp = [];
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_userotp'] = this.postUserotp;
    return data;
  }
}

class Messages {
  List<String> postUserotp;
  Messages({this.postUserotp});
  Messages.fromJson(Map<String, dynamic> json) {
    try {
      postUserotp = json['post_userotp'].cast<String>() ?? [];
    } catch (e) {
      postUserotp = [];
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['post_userotp'] = this.postUserotp;
    return data;
  }
}

class ResponseData {
  MobileUserOTPModel userOTP;
  ResponseData({this.userOTP});
  ResponseData.fromJson(Map<String, dynamic> json) {
    userOTP = json['userOTP'] != null
        ? new MobileUserOTPModel.fromJson(json['userOTP'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userOTP != null) {
      data['userOTP'] = this.userOTP.toJson();
    }
    return data;
  }
}
