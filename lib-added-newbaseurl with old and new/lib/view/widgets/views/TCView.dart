import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view/widgets/webview/WebScreen.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class TCView extends StatelessWidget with Mixin {
  final screenName;

  TCView({@required this.screenName});

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                  text:
                      "By clicking you confirm that you accept the Finance Magic ",
                  style: TextStyle(
                    height: MyTheme.txtLineSpace,
                    color: MyTheme.inputColor,
                    fontSize: getTxtSize(
                        context: context, txtSize: MyTheme.txtSize - .1),
                  ),
                  children: <TextSpan>[
                    TextSpan(
                        text: 'Terms and Conditions',
                        style: TextStyle(
                            height: MyTheme.txtLineSpace,
                            decoration: TextDecoration.underline,
                            decorationThickness: 2,
                            color: MyTheme.brandColor,
                            fontSize: getTxtSize(
                                context: context,
                                txtSize: MyTheme.txtSize - .1),
                            fontWeight: FontWeight.w500),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            // navigate to desired screen
                            Get.to(
                                    () => PDFDocumentPage(
                                          title: "Privacy",
                                          url:
                                              "https://mortgage-magic.co.uk/assets/img/privacy_policy.pdf",
                                        ),
                                    transition: Transition.rightToLeft,
                                    duration: Duration(
                                        milliseconds:
                                            AppConfig.pageAnimationMilliSecond))
                                .then((value) {
                              //callback(route);
                            });
                          })
                  ]),
            ),
          ),
        ]);
  }
}
