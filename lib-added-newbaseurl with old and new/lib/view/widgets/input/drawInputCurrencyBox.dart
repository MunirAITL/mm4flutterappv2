import 'package:aitl/view/widgets/input/utils/DecimalTextInputFormatter.dart';
import 'package:flutter/material.dart';

import '../../../config/AppDefine.dart';
import '../../../config/MyTheme.dart';
import '../txt/Txt.dart';

drawInputCurrencyBox({
  BuildContext context,
  TextEditingController tf,
  double hintTxt = 0,
  String labelTxt,
  Color labelColor,
  bool isBold = false,
  int len,
  FocusNode focusNode,
  FocusNode focusNodeNext,
  String sign = AppDefine.CUR_SIGN,
  Function(String) onChange,
}) {
  return Container(
    //color: Colors.amber,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        labelTxt != null
            ? Padding(
                padding: const EdgeInsets.only(bottom: 5),
                child: Txt(
                    txt: labelTxt,
                    txtColor:
                        labelColor == null ? MyTheme.inputColor : Colors.black,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: isBold),
              )
            : SizedBox(),
        IntrinsicHeight(
          child: Container(
            decoration: BoxDecoration(
                //color: Colors.black,
                borderRadius: BorderRadius.circular(5),
                border: Border.all(color: Colors.grey)),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Colors.grey.shade400,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(8),
                    child: Txt(
                        txt: sign,
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
                ),
                Expanded(
                  child: TextField(
                    textInputAction: TextInputAction.next,
                    controller: tf,
                    focusNode: focusNode,
                    inputFormatters: [
                      DecimalTextInputFormatter(decimalRange: 2)
                    ],
                    keyboardType: TextInputType.numberWithOptions(
                        signed: true, decimal: true),
                    maxLength: len,
                    onTap: () {
                      if (tf.text == "0") {
                        tf.text = "";
                      }
                    },
                    onChanged: (v) {
                      if (onChange != null) onChange(v);
                    },
                    onEditingComplete: () {
                      // Move the focus to the next node explicitly.
                      if (focusNode != null) {
                        focusNode.unfocus();
                      } else {
                        FocusScope.of(context).requestFocus(new FocusNode());
                      }
                      if (focusNodeNext != null) {
                        FocusScope.of(context).requestFocus(focusNodeNext);
                      } else {
                        FocusScope.of(context).requestFocus(new FocusNode());
                      }
                    },
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                      height: MyTheme.txtLineSpace,
                    ),
                    decoration: new InputDecoration(
                      isDense: true,
                      counterText: "",
                      hintText:
                          hintTxt != null ? hintTxt.toStringAsFixed(0) : '',
                      hintStyle: new TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                        height: MyTheme.txtLineSpace,
                      ),
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 8, horizontal: 8),
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}
