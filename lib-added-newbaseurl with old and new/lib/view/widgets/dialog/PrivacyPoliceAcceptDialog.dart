import 'dart:ui';
import 'package:aitl/controller/api/db_cus/dash/PrivacyPolicyAcceptObject.dart';
import 'package:aitl/controller/api/db_cus/more/badge/BadgeAPIMgr.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgePhotoIDAPIModel.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';
import '../txt/Txt.dart';

class PrivacyPolicyDialog extends StatefulWidget {
  String webUrl;

  PrivacyPolicyDialog(this.webUrl);

  @override
  State createState() => _HelpTutDialogState();
}

class _HelpTutDialogState extends State<PrivacyPolicyDialog> with Mixin {
  final GlobalKey webViewKey = GlobalKey();

  double progress = 0;
  bool isLoading = true;
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: MyTheme.brandColor,
      insetPadding: EdgeInsets.zero,
      clipBehavior: Clip.antiAliasWithSaveLayer,
      child: Container(
        height: getH(context),
        width: getW(context),
        child: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Center(
                  child: Txt(
                    txt: "Privacy policy",
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    isBold: true,
                    //txtLineSpace: 1.5,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 9,
              child: Container(
                width: getW(context),
                height: getH(context),
                color: Colors.white,
                child: SfPdfViewer.network(
                  '${widget.webUrl}',
                  key: _pdfViewerKey,
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: const EdgeInsets.only(bottom: 1.0, top: 3.0),
                child: GestureDetector(
                  onTap: () {
                    BadgeAPIMgr().wsPostPhotoIDBadge(
                        context: context,
                        params: PrivacyPolicyAcceptObject()
                            .getParam(refrenceUrl: widget.webUrl),
                        callback: (BadgePhotoIDAPIModel model) async {
                          if (model.success) {
                            await PrefMgr.shared.setPrefStr("Accepted", "1");
                          }
                          Navigator.of(context, rootNavigator: true).pop();
                          //Navigator.of(context, rootNavigator: true)
                          //  .pop();
                        });
                  },
                  child: Container(
                    width: getW(context),
                    color: Colors.green,
                    child: Center(
                        child: Txt(
                            txt: "Accept",
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: true)),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Color getColor(Set<MaterialState> states) {
    const Set<MaterialState> interactiveStates = <MaterialState>{
      MaterialState.pressed,
      MaterialState.hovered,
      MaterialState.focused,
    };
    if (states.any(interactiveStates.contains)) {
      return Colors.blue;
    }
    return Colors.white;
  }
}
