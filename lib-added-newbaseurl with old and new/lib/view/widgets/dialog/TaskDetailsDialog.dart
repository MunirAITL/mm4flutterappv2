import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:awesome_dialog/awesome_dialog.dart';

// ignore: non_constant_identifier_names
taskDetailsDialog(
    {@required var caseAssignedBy,
    @required var caseDescription,
    @required var caseTitle,
    @required var caseID,
    @required var msg,
    @required var title,
    @required Function callbackYes,
    @required Function callbackNo,
    @required var context}) {
  AwesomeDialog(
      dialogBackgroundColor: Colors.white,
      context: context,
      animType: AnimType.SCALE,
      dialogType: DialogType.NO_HEADER,
      //showCloseIcon: true,
      //buttonsBorderRadius: BorderRadius.all(Radius.circular(2)),
      //customHeader: Icon(Icons.info, size: 50),
      body: Center(
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(height: 10),
                  Center(
                    child: Txt(
                      txt: title,
                      txtColor: MyTheme.dBlueAirColor,
                      txtSize: MyTheme.txtSize + 1,
                      isBold: true,
                      txtAlign: TextAlign.center,
                    ),
                  ),
                  Container(height: 1, color: Colors.black),
                  SizedBox(height: 10.0),
                  new RichText(
                    text: new TextSpan(
                      // Note: Styles for TextSpans must be explicitly defined.
                      // Child text spans will inherit styles from parent
                      style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        new TextSpan(
                            text: 'Case No: ',
                            style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: ResponsiveFlutter.of(context)
                                    .fontSize(MyTheme.txtSize))),
                        new TextSpan(
                            text: caseID,
                            style: new TextStyle(
                                fontSize: ResponsiveFlutter.of(context)
                                    .fontSize(MyTheme.txtSize))),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  new RichText(
                    text: new TextSpan(
                      // Note: Styles for TextSpans must be explicitly defined.
                      // Child text spans will inherit styles from parent
                      style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        new TextSpan(
                            text: 'Title: ',
                            style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: ResponsiveFlutter.of(context)
                                    .fontSize(MyTheme.txtSize))),
                        new TextSpan(
                            text: caseTitle,
                            style: new TextStyle(
                                fontSize: ResponsiveFlutter.of(context)
                                    .fontSize(MyTheme.txtSize))),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  new RichText(
                    text: new TextSpan(
                      // Note: Styles for TextSpans must be explicitly defined.
                      // Child text spans will inherit styles from parent
                      style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        new TextSpan(
                            text: 'Description: ',
                            style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: ResponsiveFlutter.of(context)
                                    .fontSize(MyTheme.txtSize))),
                        new TextSpan(
                            text: caseDescription,
                            style: new TextStyle(
                                fontSize: ResponsiveFlutter.of(context)
                                    .fontSize(MyTheme.txtSize))),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  new RichText(
                    text: new TextSpan(
                      // Note: Styles for TextSpans must be explicitly defined.
                      // Child text spans will inherit styles from parent
                      style: new TextStyle(
                        fontSize: 14.0,
                        color: Colors.black,
                      ),
                      children: <TextSpan>[
                        new TextSpan(
                            text: 'Assigned by: ',
                            style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: ResponsiveFlutter.of(context)
                                    .fontSize(MyTheme.txtSize))),
                        new TextSpan(
                            text: caseAssignedBy,
                            style: new TextStyle(
                                fontSize: ResponsiveFlutter.of(context)
                                    .fontSize(MyTheme.txtSize))),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  Center(
                    child: Txt(
                      txtAlign: TextAlign.center,
                      txt: msg,
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      isBold: false,
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
              Positioned(
                top: -50,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.info,
                        color: MyTheme.dBlueAirColor, size: 40)),
              )
            ]),
      ),
      //title: 'This is Ignored',
      //desc: 'This is also Ignored',
      btnOkText: "Yes",
      btnOkColor: MyTheme.titleColor,
      btnOkOnPress: () {
        callbackYes();
      },
      btnCancelText: "No",
      btnCancelColor: Colors.grey,
      btnCancelOnPress: () {
        if (callbackNo != null) callbackNo();
      })
    ..show();

  /*return showDialog(
    barrierDismissible: false,
    context: context,
    builder: (ctx) => Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      backgroundColor: MyTheme.bgColor2,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: Txt(
                    txt: title,
                    txtColor: MyTheme.brandColor,
                    txtSize: MyTheme.txtSize + 1,
                    isBold: true,
                    txtAlign: TextAlign.center,
                  ),
                ),
                Container(height: 1, color: Colors.black),
                SizedBox(height: 10.0),
                new RichText(
                  text: new TextSpan(
                    // Note: Styles for TextSpans must be explicitly defined.
                    // Child text spans will inherit styles from parent
                    style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text: 'Case No: ',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                      new TextSpan(
                          text: caseID,
                          style: new TextStyle(
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                new RichText(
                  text: new TextSpan(
                    // Note: Styles for TextSpans must be explicitly defined.
                    // Child text spans will inherit styles from parent
                    style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text: 'Title: ',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                      new TextSpan(
                          text: caseTitle,
                          style: new TextStyle(
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                new RichText(
                  text: new TextSpan(
                    // Note: Styles for TextSpans must be explicitly defined.
                    // Child text spans will inherit styles from parent
                    style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text: 'Description: ',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                      new TextSpan(
                          text: caseDescription,
                          style: new TextStyle(
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                    ],
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                new RichText(
                  text: new TextSpan(
                    // Note: Styles for TextSpans must be explicitly defined.
                    // Child text spans will inherit styles from parent
                    style: new TextStyle(
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      new TextSpan(
                          text: 'Assigned by: ',
                          style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                      new TextSpan(
                          text: caseAssignedBy,
                          style: new TextStyle(
                              fontSize: ResponsiveFlutter.of(context)
                                  .fontSize(MyTheme.txtSize))),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Center(
                  child: Txt(
                    txtAlign: TextAlign.center,
                    txt: msg,
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    isBold: false,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 15.0),
          
        ],
      ),
    ),
  );*/
}
