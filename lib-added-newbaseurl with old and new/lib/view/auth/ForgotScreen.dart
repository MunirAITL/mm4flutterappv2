import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/auth/ForgotAPIMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:get/get.dart';

import '../../view_model/helper/ui_helper.dart';

class ForgotScreen extends StatefulWidget {
  @override
  State createState() => _ForgotScreenState();
}

class _ForgotScreenState extends State<ForgotScreen> with Mixin {
  final TextEditingController _email = TextEditingController();
  final focusEmail = FocusNode();

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  FocusNode inputNode = FocusNode();
// to open keyboard call this function;
  void openKeyboard() {
    FocusScope.of(context).requestFocus(inputNode);
  }

  appInit() {
    try {
      if (!Server.isOtp) {
        _email.text = "anisur5001@yopmail.com";
        //"anisur5001@yopmail.com"; //"occbs.cust0081@yopmail.com"; //"tah12@yopmail.com";
      }
    } catch (e) {}
  }

  validate() {
    if (UserProfileVal()
        .isEmpty(context, _email, 'Please enter your email address')) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      width: getW(context),
      height: getH(context),
      child: ListView(
        shrinkWrap: true,
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                IconButton(
                    onPressed: () {
                      Get.back();
                    },
                    icon: Icon(
                      Icons.arrow_back,
                      color: Colors.black,
                    )),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 20),
                      Txt(
                          txt: "Forgot Password?",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize + .5,
                          txtAlign: TextAlign.left,
                          fontWeight: FontWeight.w500,
                          isBold: true),
                      SizedBox(height: 10),
                      Txt(
                          txt: "Please enter your email address",
                          txtColor: MyTheme.inputColor,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.left,
                          fontWeight: FontWeight.w500,
                          isBold: true),
                      Txt(
                        txt: "associated with your account.",
                        txtColor: MyTheme.inputColor,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.left,
                        fontWeight: FontWeight.w500,
                        isBold: true,
                      ),
                      SizedBox(height: 20),
                      drawInputBox(
                        context: context,
                        title: "Email",
                        input: _email,
                        ph: "mailaddress@email.com",
                        kbType: TextInputType.emailAddress,
                        inputAction: TextInputAction.done,
                        focusNode: focusEmail,
                        len: 50,
                        autofocus: true,
                      ),
                      /*Center(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: GestureDetector(
                        onTap: () {
                          navTo(
                              context: context,
                              isRep: true,
                              page: () => ChangePwdScreen());
                        },
                        child: Txt(
                            txt: "Try another way",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize - .1,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ),
                    ),
                  ),*/
                      SizedBox(height: 20),
                      MMBtn(
                          txt: "Send",
                          height: getHP(context, 7),
                          width: getW(context),
                          radius: 8,
                          callback: () {
                            if (validate()) {
                              ForgotAPIMgr().wsForgotAPI(
                                context: context,
                                email: _email.text.trim(),
                                callback: (model) {
                                  if (model != null && mounted) {
                                    try {
                                      if (model.success) {
                                        try {
                                          final msg = model
                                              .messages.forgotpassword[0]
                                              .toString();
                                          showToast(
                                              context: context,
                                              msg: msg,
                                              which: 1);
                                        } catch (e) {
                                          myLog(e.toString());
                                        }
                                      } else {
                                        try {
                                          if (mounted) {
                                            final err = model
                                                .errorMessages.forgotpassword[0]
                                                .toString();
                                            showToast(
                                                context: context, msg: err);
                                          }
                                        } catch (e) {
                                          myLog(e.toString());
                                        }
                                      }
                                    } catch (e) {
                                      myLog(e.toString());
                                    }
                                  }
                                },
                              );
                            }
                          }),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
