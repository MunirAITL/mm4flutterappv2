import 'dart:developer';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/LoginAPIModel.dart';
import 'package:aitl/model/json/auth/UserModel.dart';
import 'package:aitl/view/NoAccess.dart';
import 'package:aitl/view/auth/company_accounts/comp_acc_page.dart';
import 'package:aitl/view/auth/otp/OtpByEmailPage.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class AuthBase<T extends StatefulWidget> extends State<T> with Mixin {
  onLoginUserModelRoute(
      {@required UserModel user,
      @required String accessToken,
      @required String refreshToken,
      @required dynamic actionPage,
      dynamic actionPageNF2Reg,
      @required VoidCallback callbackSwitchUrl}) async {
    try {
      //@sardar vai -> "if after login you will get id UserCompanyId = 2 , then you will re login to enm.mortgage-magic.co.uk from back end
      //UserCompanyId = 2 will use   base url enm.mortgage-magic.co.uk for MM app"
      /*if (user.userCompanyID == 2) {
        Server(true);
        log(Server.BASE_URL);
        callbackSwitchUrl();
      } else {*/
      if (user.communityID.toString() == "1") {
        try {
          await PrefMgr.shared.setPrefStr("accessToken", accessToken);
          await PrefMgr.shared.setPrefStr("refreshToken", refreshToken);
          await DBMgr.shared.setUserProfile(user: user);
          await userData.setUserModel();
        } catch (e) {
          log(e.toString());
        }
        if (!Server.isOtp) {
          if (userData.userModel.companyCount > 1) {
            Get.off(() => CompAccScreen());
          } else {
            Get.off(() => MainCustomerScreen());
          }
        } else {
          if (user.isMobileNumberVerified) {
            if (userData.userModel.companyCount > 1) {
              Get.off(() => CompAccScreen());
            } else {
              Get.off(() => MainCustomerScreen());
            }
          } else {
            Get.to(actionPage);
          }
        }
      } else {
        if (actionPageNF2Reg != null) {
          Get.off(actionPageNF2Reg);
        } else {
          Get.to(() => NoAccess());
        }
      }
      //}
    } catch (e) {}
  }
}
