import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/controller/api/db_cus/credit_case/GetUserValidationForOldUserAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/my_cases/MyCasesAPIMgr.dart';
import 'package:aitl/controller/helper/db_cus/tab_mycases/MyCasesHelper.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/action_req_helper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/DashBoardListType.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/credit_case/GetUserValidationOldUserResponse.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseInfos.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseRecomendationInfos.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/Task.dart';
import 'package:aitl/view/db_cus/calculator/buy2let_cal.dart';
import 'package:aitl/view/db_cus/calculator/mortgage_cal.dart';
import 'package:aitl/view/db_cus/credit_case/CreditDashboardTabController.dart';
import 'package:aitl/view/db_cus/credit_case/CreditUserInformation.dart';
import 'package:aitl/view/db_cus/credit_case/MultipleChoiceQuestions.dart';
import 'package:aitl/view/db_cus/credit_case/credit_report_cfg.dart';
import 'package:aitl/view/db_cus/more/reviews/ReviewRatingListPage.dart';
import 'package:aitl/view/db_cus/open_banking/open_bank_page.dart';
import 'package:aitl/view/eid/doc_scan/scan/selfie/selfie_page.dart';
import 'package:aitl/view/widgets/dialog/AlrtDialog.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/ActionReqController.dart';
import 'package:aitl/view_model/rx/viewController.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:new_version/new_version.dart';
import '../../../controller/helper/db_cus/tab_newcase/NewCaseHelper.dart';
import 'NewCaseTab.dart';

class DashBoardScreen extends StatefulWidget {
  @override
  State createState() => NewCaseTabState();
}

class NewCaseTabState extends State<DashBoardScreen> with Mixin, StateListener {
  final actionReqControler = Get.put(ActionReqController());

  List<Task> listTaskInfoSearchModel = [];

  Widget widActionRequired;

  bool isLoading = false;

  StateProvider _stateProvider;

  ViewController viewController = Get.find();

  bool isMyCasesFound = false;

  checkNewerVersion() async {
    // Instantiate NewVersion manager object (Using GCP Console app as example)
    if (!Server.isTest) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (mounted) {
          final newVersion = NewVersion(
            iOSId: 'uk.co.mortgagemagic.client',
            androidId: 'uk.co.mortgagemagic.client',
          );
          newVersion.showAlertIfNecessary(context: context);
        }
      });
    }
  }

  startNewCaseClicked() {
    Get.to(() => NewCaseTab());
  }

  myCasesClicked() {
    StateProvider().notify(ObserverState.STATE_CHANGED_tabbar2);
  }

  latestMortgageClicked() {
    showAlrtDialog(
      context: context,
      title: "Unauthorised!",
      msg: "Your account is not authorised to use this feature.",
    );
  }

  creditReportClicked() {
    if (Server.isTest) {
      /*Get.to(() => CreditDashBoardTabController(
          responseData: TestCreditReport().getUserValidationOldUser()));*/
      GetUserValidationOldUserApiMgr().creditCaseOldUserInformation(
        context: context,
        userID: userData.userModel.id, //115767,
        userCompanyId: userData.userModel.userCompanyID, //1003,
        callback: (model) {
          if (model != null && model.success) {
            GetUserValidationOldUser getUerValidationOldUser =
                model.responseData;
            if (getUerValidationOldUser.validateNewUser.validateNewUserResult
                    .identityValidationOutcome ==
                CreditReportCfg.IdentityValidationOutcome10) {
              Get.to(
                  () => CreditDashBoardTabController(
                      responseData: model.responseData),
                  transition: Transition.rightToLeft,
                  duration: Duration(
                      milliseconds: AppConfig.pageAnimationMilliSecond));
            } else if (getUerValidationOldUser.validateNewUser
                    .validateNewUserResult.identityValidationOutcome ==
                CreditReportCfg.IdentityValidationOutcome2) {
              Get.to(
                  () =>
                      MultipleChoiceQuestions(responseData: model.responseData),
                  transition: Transition.rightToLeft,
                  duration: Duration(
                      milliseconds: AppConfig.pageAnimationMilliSecond));
            } else {
              Get.to(
                  () => CreditUserInformation(responseData: model.responseData),
                  transition: Transition.rightToLeft,
                  duration: Duration(
                      milliseconds: AppConfig.pageAnimationMilliSecond));
            }
          }
        },
      );
    } else {
      showAlrtDialog(
        context: context,
        title: "Unauthorised!",
        msg: "Your account is not authorised to use this feature.",
      );
    }
  }

  connectOpenBankingClicked() {
    if (Server.isTest) {
      Get.to(() => OpenBankPage());
    } else {
      showAlrtDialog(
        context: context,
        title: "Unauthorised!",
        msg: "Your account is not authorised to use this feature.",
      );
    }
  }

  mortgageCalClicked() {
    //if (Server.isTest) {
    Get.to(() => MortgageCalPage());
    /*} else {
      showAlrtDialog(
        context: context,
        title: "Unauthorised!",
        msg: "Your account is not authorised to use this feature.",
      );
    }*/
  }

  buy2LetCalClicked() {
    //if (Server.isTest) {
    Get.to(() => Buy2LetCalPage());
    /*} else {
      showAlrtDialog(
        context: context,
        title: "Unauthorised!",
        msg: "Your account is not authorised to use this feature.",
      );
    }*/
  }

  var listBtn = [];
  int btnIndex = -1;

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_tabbar1_reload_case_api) {
      btnIndex = -1;
      await refreshData();
      checkNewerVersion();
    }
  }

  //  Action Required Start...

  Future<void> refreshData() async {
    try {
      if (mounted) {
        setState(() {
          isLoading = true;
        });
        await MyCasesAPIMgr().wsOnPageLoad(
          context: context,
          pageStart: 0,
          pageCount: 1,
          caseStatus: NewCaseCfg.ALL,
          callback: (model) {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    listTaskInfoSearchModel.clear();
                    final List<dynamic> locations =
                        model.responseData.locations;
                    for (Task location in locations) {
                      listTaskInfoSearchModel.add(location);
                      final icon = NewCaseHelper()
                          .getCreateCaseIconByTitle(location.title);
                      if (icon != null) isMyCasesFound = true;
                    }
                  } catch (e) {}
                }
              } catch (e) {}
            }
          },
        );
        if (mounted) {
          await ActionReqHelper().wsActionReqAPIcall(
            context: context,
            callback: (
              List<DashBoardListType> listUserList,
              List<dynamic> listUserNotesModel,
              List<MortgageCaseRecomendationInfos> caseReviewModelList,
              List<MortgageCaseInfos> caseMortgageAgreementReviewInfosList,
            ) {
              if (mounted && listUserList != null) {
                if (listUserList.length > 0) {
                  drawActionRequiredItems(
                      listUserList,
                      listUserNotesModel,
                      caseReviewModelList,
                      caseMortgageAgreementReviewInfosList);
                }
              }
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            },
          );
        }
      }
    } catch (e) {}
  }

  drawActionRequiredItems(listUserList, listUserNotesModel, caseReviewModelList,
      caseMortgageAgreementReviewInfosList) {
    widActionRequired = ActionReqHelper().drawActionRequiredItems(
      context: context,
      listUserList: listUserList,
      listUserNotesModel: listUserNotesModel,
      caseReviewModelList: caseReviewModelList,
      caseMortgageAgreementReviewInfosList:
          caseMortgageAgreementReviewInfosList,
      callbackReload: () {
        refreshData();
      },
      callbackRefresh: () {
        setState(() {
          drawActionRequiredItems(listUserList, listUserNotesModel,
              caseReviewModelList, caseMortgageAgreementReviewInfosList);
        });
      },
      callbackUserBadges: (Map<String, dynamic> map) {
        if (map["email"] == true) {
          final isOk = map["email"];
          if (isOk) {
            showToast(
                context: context,
                msg:
                    "We have sent an email for verification, please check your email",
                which: 1);
          } else {
            showToast(
                context: context, msg: "Sorry, something went wrong", which: 0);
          }
        } else if (map["eid"] == true) {
          Get.to(() => SelfiePage()).then((value) {
            refreshData();
          });
        } else {
          if (mounted) {
            setState(() {
              isLoading = false;
            });
          }
        }
      },
      callbackUserNotes: (userNotesModel) {
        if (mounted) {
          refreshData();
        }
      },
    );
  }
  //  Action Required End...

  @override
  void initState() {
    super.initState();
    try {
      /*Future.delayed(Duration(seconds: 1), () {
        confirmDialog(
          context: context,
          title: "Uploading Alert",
          msg: "asdfasdfl asdfjlasdfjasd f",
          callbackYes: () {},
        );
      });*/
      appInit();
    } catch (e) {}
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      listBtn = [
        {
          "icon": Image.asset(
            'assets/images/icons/new_case_icon.png',
            width: 30,
            height: 30,
            color: MyTheme.titleColor,
          ),
          "title": "Start a new case",
          "callback": () => startNewCaseClicked(),
        },
        {
          "icon": Image.asset(
            'assets/images/icons/my_case_icon.png',
            width: 25,
            height: 25,
            color: MyTheme.titleColor,
          ),
          "title": "My cases",
          "callback": () => myCasesClicked(),
        },
        /*{
          "icon": Image.asset(
            'assets/images/icons/check_lmr_icon.png',
            width: 25,
            height: 25,
            color: MyTheme.titleColor,
          ),
          "title": "Check the latest mortgage",
          "callback": () => latestMortgageClicked(),
        },
        {
          "icon": Image.asset(
            'assets/images/icons/my_credit_icon.png',
            width: 25,
            height: 25,
            color: MyTheme.titleColor,
          ),
          "title": "My credit report",
          "callback": () => creditReportClicked(),
        },
        {
          "icon": Image.asset(
            'assets/images/icons/open_banking.png',
            width: 25,
            height: 25,
            color: MyTheme.titleColor,
          ),
          "title": "Connect open banking",
          "callback": () => connectOpenBankingClicked(),
        },
        {
          "icon": Image.asset(
            'assets/images/icons/open_banking.png',
            width: 25,
            height: 25,
            color: MyTheme.titleColor,
          ),
          "title": "Mortgage calculator",
          "callback": () => mortgageCalClicked(),
        },
        {
          "icon": Image.asset(
            'assets/images/icons/open_banking.png',
            width: 25,
            height: 25,
            color: MyTheme.titleColor,
          ),
          "title": "Buy to let calculator",
          "callback": () => buy2LetCalClicked(),
        },*/
      ];
    } catch (e) {}
    try {
      await refreshData();
      checkNewerVersion();
    } catch (e) {}
  }

  DateTime currentBackPressTime = DateTime.now();

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            content: Text(
              "Press back again to leave",
              style: TextStyle(color: Colors.white),
            ),
            backgroundColor: Colors.redAccent),
      );
      return Future.value(false);
    }
    SystemChannels.platform.invokeMethod('SystemNavigator.pop');

    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: MyTheme.bgColor2,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: MyTheme.statusBarColor,
        elevation: 0,
        centerTitle: false,
        titleSpacing: 0,
        title: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            SizedBox(width: 20),
            UIHelper().drawAppbarTitle(title: "Dashboard"),
            Spacer(),
            GestureDetector(
              onTap: () {
                Get.to(() => NewCaseTab());
              },
              child: Container(
                margin: const EdgeInsets.all(1),
                padding: const EdgeInsets.all(1),
                decoration:
                    BoxDecoration(border: Border.all(color: Colors.white)),
                child: Icon(Icons.add_outlined, color: Colors.white, size: 20),
              ),
            ),
            SizedBox(width: 10),
            /*Container(
              width: 45,
              height: 40,
              decoration: BoxDecoration(
                  color: Color(0xFFACC7DE),
                  borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(15),
                    bottomLeft: const Radius.circular(15),
                  )),
              child: GestureDetector(
                  onTap: () {
                    /*Get.to(() => HelpScreen(),
                        transition: Transition.rightToLeft,
                        duration: Duration(
                            milliseconds: AppConfig.pageAnimationMilliSecond));*/

                    Get.to(() => NewCaseTab());
                  },
                  child: Icon(Icons.add,
                      color: MyTheme
                          .brandColor) /*Image.asset(
                  "assets/images/icons/help_circle_icon.png",
                  color: MyTheme.brandColor,
                ),*/
                  ),
            )*/
          ],
        ),
        bottom: PreferredSize(
          preferredSize: Size.fromHeight((isLoading) ? .5 : 0),
          child: (isLoading)
              ? AppbarBotProgBar(
                  backgroundColor: MyTheme.appbarProgColor,
                )
              : SizedBox(),
        ),
      ),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onPanDown: (detail) {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: drawLayout(),
      ),
    );
  }

  drawLayout() {
    return isLoading
        ? SizedBox()
        : Container(
            child: SingleChildScrollView(
              primary: true,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  (widActionRequired != null)
                      ? Container(child: widActionRequired)
                      : SizedBox(),
                  drawCaseBox(),
                  /*Center(
                    child: Obx(
                      () => viewController.isOpened.value
                          ? Image.asset(
                              "assets/images/icons/help_hand3.png",
                              width: 60,
                              height: 70,
                            )
                          : SizedBox(),
                    ),
                  ),*/
                  drawCaseBtnList(),
                ],
              ),
            ),
          );
  }

  drawCaseBox() {
    if (listTaskInfoSearchModel.length == 0 || !isMyCasesFound)
      return SizedBox();
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
              txt: "Recent Cases",
              txtColor: MyTheme.titleColor,
              txtSize: MyTheme.txtSize + .2,
              txtAlign: TextAlign.left,
              //fontWeight: FontWeight.w600,
              isBold: true,
            ),
            SizedBox(height: 10),
            Container(
              padding: EdgeInsets.zero,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(width: .5),
                  borderRadius: BorderRadius.all(Radius.circular(
                          10) //                 <--- border radius here
                      )),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ListView.builder(
                    primary: false,
                    addAutomaticKeepAlives: true,
                    cacheExtent: AppConfig.page_limit.toDouble(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    //primary: false,
                    itemCount: listTaskInfoSearchModel.length,
                    itemBuilder: (BuildContext context, int index) {
                      return MyCasesHelper().drawRecentCaseItem(
                          context: context,
                          caseModel: listTaskInfoSearchModel[index],
                          isDashboard: true,
                          callbackModel: (model) {
                            if (mounted) {
                              if (model != null) {
                                if (model.success) {
                                  Get.to(() => ReviewRatingListPage(
                                        reviewRatingModel: model.responseData
                                            .reviewRatingFormSetups,
                                        locationModel:
                                            listTaskInfoSearchModel[index],
                                      ));
                                }
                              }
                            }
                          });
                    },
                  ),
                  isMyCasesFound
                      ? OutlinedButton(
                          onPressed: () {
                            StateProvider()
                                .notify(ObserverState.STATE_CHANGED_tabbar2);
                          },
                          child: Text(
                            'Show More',
                            style: TextStyle(
                                color: Color(0xFF28415F), fontSize: 12),
                          ),
                          style: OutlinedButton.styleFrom(
                            minimumSize: Size.zero, // <-- Add this
                            padding: EdgeInsets.all(5),
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                            side: BorderSide(width: .5, color: Colors.grey),
                          ),
                        )
                      : SizedBox(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  drawCaseBtnList() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
      child: ListView.builder(
        primary: false,
        shrinkWrap: true,
        itemCount: listBtn.length,
        itemBuilder: (context, i) {
          final map = listBtn[i];
          return ButtonTheme(
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            child: Padding(
              padding: const EdgeInsets.only(top: 5, bottom: 5),
              child: TextButton(
                style: (btnIndex == i)
                    ? ButtonStyle(
                        padding: MaterialStateProperty.all(EdgeInsets.only(
                            top: 15, bottom: 15, right: 5, left: 10)),
                        backgroundColor: MaterialStateProperty.all(
                            viewController.isOpened.value
                                ? HexColor.fromHex("#CD113B")
                                : Colors.white),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    side:
                                        BorderSide(color: Color(0xFF828687)))))
                    : ButtonStyle(
                        padding: MaterialStateProperty.all(EdgeInsets.only(
                            top: 15, bottom: 15, right: 5, left: 10)),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                    side:
                                        BorderSide(color: Color(0xFF828687)))),
                      ),
                onPressed: () {
                  btnIndex = i;
                  Function.apply(map['callback'], []);
                  setState(() {});
                },
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(width: 20),
                    map['icon'],
                    SizedBox(width: 20),
                    Flexible(
                      child: Txt(
                        txt: map['title'],
                        txtColor: MyTheme.titleColor,
                        txtSize: MyTheme.txtSize - .1,
                        txtAlign: TextAlign.start,
                        isBold: false,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
