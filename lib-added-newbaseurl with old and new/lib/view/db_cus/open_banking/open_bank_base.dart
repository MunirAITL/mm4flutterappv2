// ignore_for_file: equal_elements_in_set

import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/db_cus/open_banking/GetForeCasts4PersonalAccountsAPIModel.dart';
import 'package:aitl/model/json/db_cus/open_banking/GetInsight4PersonnelAccountsAPIModel.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/paint/CirclePainter.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import '../../../../../Mixin.dart';
import 'package:aitl/view/widgets/dropdown/custom_expansion_tile.dart'
    as custom;

abstract class OpenBankBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  GetInsight4AccResponseData getInsight4AccResponseDataModel;
  GetForeCasts4AccResponseData getForeCasts4AccResponseData;
  List<_ChartData> chartData;
  List<_BarData> barData;
  List<_ChartData2> chartData2;
  final txtStyle = TextStyle(
    color: Colors.black,
    fontFamily: 'Roboto',
    fontSize: 12,
    fontStyle: FontStyle.italic,
  );
  final _tooltipBehavior = TooltipBehavior(
    enable: true,
    color: Colors.white,
    canShowMarker: true,
    shouldAlwaysShow: true,
  );
  final _tooltipBehavior2 = TooltipBehavior(
    enable: true,
    color: Colors.white,
    canShowMarker: true,
    shouldAlwaysShow: true,
  );
  final _tooltipBehavior3 = TooltipBehavior(
    enable: true,
    color: Colors.white,
    canShowMarker: true,
    shouldAlwaysShow: true,
  );
  var listCatIncome = [];

  final listCatExp = [
    {
      'title': 'Bank Transactions',
      'amounts': 0.0,
      'pa': '0.00%',
      "level": [
        {'title': 'Transfer Out', 'amounts': 0.0, 'pa': '0.00%', "level": []},
        {'title': 'Cash Widrawal', 'amounts': 0.0, 'pa': '0.00%', "level": []},
      ]
    },
    {
      'title': 'Bills',
      'amounts': 0.0,
      'pa': '0.00%',
      "level": [
        {'title': 'Council Tax', 'amounts': 0.0, 'pa': '0.00%', "level": []},
        {'title': 'Mortgage Rent', 'amounts': 0.0, 'pa': '0.00%', "level": []},
        {
          'title': 'TV, Internet & Communication',
          'amounts': 0.0,
          'pa': '0.00%',
          "level": []
        },
        {'title': 'Utilities', 'amounts': 0.0, 'pa': '0.00%', "level": []},
        {'title': 'Bank Charges', 'amounts': 0.0, 'pa': '0.00%', "level": []},
      ]
    },
    {'title': 'Credit Repayment', 'amounts': 0.0, 'pa': '0.00%', "level": []},
    {'title': 'Education', 'amounts': 0.0, 'pa': '0.00%', "level": []},
    {
      'title': 'Enjoyment',
      'amounts': 0.0,
      'pa': '0.00%',
      "level": [
        {'title': 'Entertainment', 'amounts': 0.0, 'pa': '0.00%', "level": []},
        {'title': 'Food & Drink', 'amounts': 0.0, 'pa': '0.00%', "level": []},
        {'title': 'Gambling', 'amounts': 0.0, 'pa': '0.00%', "level": []},
        {
          'title': 'Hobbies & Interests',
          'amounts': 0.0,
          'pa': '0.00%',
          "level": []
        },
        {
          'title': 'Social Activities',
          'amounts': 0.0,
          'pa': '0.00%',
          "level": []
        },
      ]
    },
    {
      'title': 'Groceries & Housekeeping',
      'amounts': 0.0,
      'pa': '0.00%',
      "level": []
    },
    {
      'title': 'Health',
      'amounts': 0.0,
      'pa': '0.00%',
      "level": [
        {'title': 'Healthcare', 'amounts': 0.0, 'pa': '0.00%', "level": []},
        {
          'title': 'Health & Beauty',
          'amounts': 0.0,
          'pa': '0.00%',
          "level": []
        },
        {
          'title': 'Sports & Fitness',
          'amounts': 0.0,
          'pa': '0.00%',
          "level": []
        },
      ]
    },
    {
      'title': 'Home',
      'amounts': 0.0,
      'pa': '0.00%',
      "level": [
        {
          'title': 'Furniture & Appliance Rentals',
          'amounts': 0.0,
          'pa': '0.00%',
          "level": []
        },
        {'title': 'Family', 'amounts': 0.0, 'pa': '0.00%', "level": []},
        {'title': 'Home', 'amounts': 0.0, 'pa': '0.00%', "level": []},
      ]
    },
    {'title': 'Insurance', 'amounts': 0.0, 'pa': '0.00%', "level": []},
    {'title': 'Other Expenditure', 'amounts': 0.0, 'pa': '0.00%', "level": []},
    {
      'title': 'Professional Services',
      'amounts': 0.0,
      'pa': '0.00%',
      "level": [
        {'title': 'Legal', 'amounts': 0.0, 'pa': '0.00%', "level": []},
        {
          'title': 'Professional Services',
          'amounts': 0.0,
          'pa': '0.00%',
          "level": []
        },
      ]
    },
    {
      'title': 'Retail',
      'amounts': 0.0,
      'pa': '0.00%',
      "level": [
        {
          'title': 'Clothing & Fashion',
          'amounts': 0.0,
          'pa': '0.00%',
          "level": []
        },
        {
          'title': 'General Retails',
          'amounts': 0.0,
          'pa': '0.00%',
          "level": []
        },
        {
          'title': 'Gifts, Postage & Stationery',
          'amounts': 0.0,
          'pa': '0.00%',
          "level": []
        },
        {
          'title': 'Personal Technology',
          'amounts': 0.0,
          'pa': '0.00%',
          "level": []
        },
      ]
    },
    {
      'title': 'Savings & Investments',
      'amounts': 0.0,
      'pa': '0.00%',
      "level": []
    },
  ];

  final ScrollController scrollController = ScrollController();

  OptionItem optTitle = OptionItem(id: null, title: "Reason Codes");
  bool isChartBalanceStrike = false;
  bool isChartPredicationStrike = false;

  bool isChart2IncomingsStrike = false;
  bool isChart2OutgoingsStrike = false;
  bool isChart2BalanceStrike = false;

  drawLayout();

  setIncomeList() {
    final listIncomeCat = getInsight4AccResponseDataModel.income.categories;
    //  adding into list
    listCatIncome.add(
      {'title': 'Salary', 'amounts': 0.0, 'pa': '0.00%', "level": []},
    );

    var listSubCatIncome = [];
    var grandTotal = 0.0;
    for (Categories cat in listIncomeCat) {
      for (var amt in cat.monthlyAmounts) {
        grandTotal += amt.amount ?? 0;
      }
    }
    for (Categories cat in listIncomeCat) {
      var total = 0.0;
      for (var amt in cat.monthlyAmounts) {
        total += amt.amount ?? 0;
      }
      final map = {
        'title': cat.name ?? '',
        'amounts': total,
        'pa': ((total / grandTotal) * 100).toStringAsFixed(2) + '%',
        "level": []
      };
      listSubCatIncome.add(map);
    }
    listCatIncome.add({
      'title': 'Other Income',
      'amounts': grandTotal,
      'pa': '100.00%',
      "level": listSubCatIncome
    });
  }

  setExpList() {
    final listExpCat = getInsight4AccResponseDataModel.expenditure.categories;
    //  adding into list
    listCatExp.add(
      {'title': 'Salary', 'amounts': 0.0, 'pa': '0.00%', "level": []},
    );

    var listSubCatIncome = [];
    var grandTotal = 0.0;
    for (Categories cat in listExpCat) {
      for (var amt in cat.monthlyAmounts) {
        grandTotal += amt.amount ?? 0;
      }
    }
    for (Categories cat in listExpCat) {
      var total = 0.0;
      for (var amt in cat.monthlyAmounts) {
        total += amt.amount ?? 0;
      }
      final map = {
        'title': cat.name ?? '',
        'amounts': total,
        'pa': ((total / grandTotal) * 100).toStringAsFixed(2) + '%',
        "level": []
      };
      listSubCatIncome.add(map);
    }
    listCatIncome.add({
      'title': 'Other Income',
      'amounts': grandTotal,
      'pa': '100.00%',
      "level": listSubCatIncome
    });
  }

  setChartData() {
    barData = <_BarData>[
      _BarData("Jan 19th 20", 0),
      _BarData("Jan 20th 20", 0.1),
      _BarData("Jan 21th 20", 0.2),
      _BarData("Jan 22th 20", 0.3),
      _BarData("Jan 23th 20", 0.4),
      _BarData("Jan 24th 20", 0.6),
    ];

    //  if both ON
    if (isChartBalanceStrike && isChartPredicationStrike) {
      chartData = <_ChartData>[
        _ChartData("Jan 19th 20", 0),
        _ChartData("Jan 20th 20", 0.1),
        _ChartData("Jan 21th 20", 0.2),
        _ChartData("Jan 22th 20", 0.3),
        _ChartData("Jan 23th 20", 0.4),
        _ChartData("Jan 24th 20", 0.6),
      ];
      return;
    }
    //  if balance ON
    if (isChartBalanceStrike) {
      chartData = <_ChartData>[
        _ChartData("Jan 19th 20", 0),
        _ChartData("Jan 20th 20", 2000),
        _ChartData("Jan 21th 20", 4000),
        _ChartData("Jan 22th 20", 6000),
        _ChartData("Jan 23th 20", 8000),
        _ChartData("Jan 24th 20", 10000),
        // _ChartData("Jan 25th 20", 12000),
      ];
    } else {
      chartData = <_ChartData>[
        _ChartData("Jan 19th 20", 0),
        _ChartData("Jan 20th 20", 2000),
        _ChartData("Jan 21th 20", 4000),
        _ChartData("Jan 22th 20", 6000),
        _ChartData("Jan 23th 20", 8000),
        _ChartData("Jan 24th 20", 10000),
        _ChartData("Jan 25th 20", 12000),
        // _ChartData("Jan 25th 20", 14000),
      ];
    }
    //  if predication ON
    if (isChartPredicationStrike) {
      chartData = <_ChartData>[
        _ChartData("Jan 19th 20", 0),
        _ChartData("Jan 20th 20", 2000),
        _ChartData("Jan 21th 20", 4000),
        _ChartData("Jan 22th 20", 6000),
        _ChartData("Jan 23th 20", 8000),
        _ChartData("Jan 24th 20", 10000),
        // _ChartData("Jan 25th 20", 12000),
      ];
    } else {
      chartData = <_ChartData>[
        _ChartData("Jan 19th 20", 0),
        _ChartData("Jan 20th 20", 2000),
        _ChartData("Jan 21th 20", 4000),
        _ChartData("Jan 22th 20", 6000),
        _ChartData("Jan 23th 20", 8000),
        _ChartData("Jan 24th 20", 10000),
        _ChartData("Jan 25th 20", 12000),
        // _ChartData("Jan 25th 20", 14000),
      ];
    }
  }

  setChart2Data() {
    chartData2 = <_ChartData2>[
      _ChartData2("Jan 19th 20", 0),
      _ChartData2("Jan 20th 20", 0.1),
      _ChartData2("Jan 21th 20", 0.2),
      _ChartData2("Jan 22th 20", 0.3),
      _ChartData2("Jan 23th 20", 0.4),
      _ChartData2("Jan 24th 20", 0.5),
      _ChartData2("Jan 25th 20", 0.6),
    ];
  }

  /*drawReportView() {
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
      child: Container(
        child: Row(
          children: [
            Txt(
                txt: "Report",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize + .2,
                txtAlign: TextAlign.center,
                isBold: true),
            SizedBox(width: 10),
            Container(
              width: 7,
              height: 7,
              decoration:
                  BoxDecoration(shape: BoxShape.circle, color: Colors.grey),
            ),
            SizedBox(width: 10),
            Txt(
                txt: "2021 September 30, Tuesday",
                txtColor: Colors.grey,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: true),
          ],
        ),
      ),
    );
  }*/

  drawTabView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        /*Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Txt(
                  txt: "Tanjir Sugar",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize + .1,
                  txtAlign: TextAlign.center,
                  isBold: true),
              IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.delete_outline,
                    color: MyTheme.brandColor,
                  )),
            ],
          ),
        ),*/
        Container(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              color: Colors.white,
              width: getWP(context, 50),
              child: Padding(
                padding: const EdgeInsets.only(top: 8, bottom: 8, left: 20),
                child: Txt(
                    txt: "Friendly Score",
                    txtColor: MyTheme.brandColor,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
            ),
            Container(
                color: Colors.white,
                width: getW(context),
                //height: getHP(context, 50),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(height: 20),
                    drawTitle("Financial Credibility"),
                    drawCircleBar1(),
                    SizedBox(height: 20),
                    drawTitle("Liquidity"),
                    drawCircleBar2(),
                    SizedBox(height: 20),
                  ],
                ))
          ],
        )),
      ],
    );
  }

  drawCircleBar1() {
    final financialCredibilityScore =
        getForeCasts4AccResponseData.financialCredibilityScore;
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 20, bottom: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Stack(
            children: [
              Container(
                height: getWP(context, 40),
                width: getWP(context, 40),
                child: CustomPaint(
                  foregroundPainter: CirclePainter(
                    total: financialCredibilityScore.generic.score ?? 0,
                    red: 60,
                    lYellow: 10,
                    yellow: 10,
                    lGreen: 10,
                    width: getWP(context, 100),
                    height: getWP(context, 100),
                    fontSize:
                        getTxtSize(context: context, txtSize: MyTheme.txtSize),
                  ),
                ),
              ),
              Positioned(
                top: 20,
                bottom: 20,
                left: 20,
                right: 20,
                child: Container(
                  height: getWP(context, 40),
                  width: getWP(context, 40),
                  child: CustomPaint(
                    foregroundPainter: CircleDotPainter(
                        total:
                            ((financialCredibilityScore.generic.score / 710) *
                                    100)
                                .round()),
                  ),
                ),
              ),
              Positioned(
                left: (getWP(context, 40) / 2) - getWP(context, 3.4) / 2,
                top: getWP(context, 3.4),
                child: CircleAvatar(
                  radius: 5,
                  backgroundColor: Colors.purple,
                ),
              )
            ],
          ),
          Container(
            width: getWP(context, 40),
            height: getWP(context, 60),
            child: Scrollbar(
              thickness: 10,
              isAlwaysShown: false,
              controller: scrollController,
              child: ListView(
                shrinkWrap: true,
                controller: scrollController,
                children: [
                  Txt(
                      txt: "Reason Codes",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  ListView.builder(
                    controller: scrollController,
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    itemCount:
                        financialCredibilityScore.generic.reasonCodes.length,
                    itemBuilder: (BuildContext context, int index) {
                      final model =
                          financialCredibilityScore.generic.reasonCodes[index];
                      //var bgColor = Colors.pink.shade50;
                      var bgColorLine = Colors.pink;
                      if (index % 2 != 0) {
                        //bgColor = Colors.blue.shade50;
                        bgColorLine = Colors.blue;
                      }
                      return Card(
                        //color: bgColor,
                        margin: EdgeInsets.only(top: 2, bottom: 2),
                        child: IntrinsicHeight(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(child: Container(color: bgColorLine)),
                              Container(
                                width: getWP(context, 39.5),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                      top: 10, left: 10, right: 10, bottom: 10),
                                  child: Txt(
                                      txt: model.description,
                                      txtColor: Colors.black,
                                      txtSize: MyTheme.txtSize - .5,
                                      txtAlign: TextAlign.start,
                                      isBold: false),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawCircleBar2() {
    final liquidity = getForeCasts4AccResponseData.liquidity;
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 20, bottom: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Stack(
            children: [
              Container(
                height: getWP(context, 40),
                width: getWP(context, 40),
                child: CustomPaint(
                  foregroundPainter: CirclePainter(
                    total: liquidity[0].score ?? 0,
                    red: 40,
                    lYellow: 10,
                    yellow: 20,
                    lGreen: 10,
                    width: getWP(context, 100),
                    height: getWP(context, 100),
                    fontSize:
                        getTxtSize(context: context, txtSize: MyTheme.txtSize),
                  ),
                ),
              ),
              Positioned(
                top: 20,
                bottom: 20,
                left: 20,
                right: 20,
                child: Container(
                  height: getWP(context, 40),
                  width: getWP(context, 40),
                  child: CustomPaint(
                    foregroundPainter: CircleDotPainter(
                        total: ((liquidity[0].score / 710) * 100).round()),
                  ),
                ),
              ),
              Positioned(
                left: (getWP(context, 40) / 2) - getWP(context, 3.4) / 2,
                top: getWP(context, 3.4),
                child: CircleAvatar(
                  radius: 5,
                  backgroundColor: Colors.purple,
                ),
              )
            ],
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Card(
                  elevation: 5,
                  child: Container(
                    width: getWP(context, 40),
                    //color: HexColor.fromHex("#D3D3D3"),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Txt(
                              txt: "Average income:",
                              txtColor: Colors.black54,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          Txt(
                              txt: AppDefine.CUR_SIGN + "0.00",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: true),
                        ],
                      ),
                    ),
                  ),
                ),
                Card(
                  elevation: 5,
                  child: Container(
                    width: getWP(context, 40),
                    //color: HexColor.fromHex("#D3D3D3"),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Txt(
                              txt: "Average Fixed\nExpenditures:",
                              txtColor: Colors.black54,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          Txt(
                              txt: AppDefine.CUR_SIGN + "233.53",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: true),
                        ],
                      ),
                    ),
                  ),
                ),
                Card(
                  elevation: 5,
                  child: Container(
                    width: getWP(context, 40),
                    //color: HexColor.fromHex("#D3D3D3"),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Txt(
                              txt: "Average Flexible\nExpenditures:",
                              txtColor: Colors.black54,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          Txt(
                              txt: AppDefine.CUR_SIGN + "304.17",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: true),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  drawStats() {
    final employment = getInsight4AccResponseDataModel.employment;
    var totalExpectedSalary = 0.0;
    for (var expectedSalary in employment.expectedSalary) {
      try {
        totalExpectedSalary += expectedSalary;
      } catch (e) {}
    }
    return Container(
      //color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Txt(
                  txt: "Employment:",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: true),
              SizedBox(width: 10),
              Expanded(
                child: Txt(
                    txt: "Recent salary, last 3 months",
                    txtColor: Colors.grey,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ],
          ),
          SizedBox(height: 10),
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Txt(
                      txt: "Average expected salary:",
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(width: 10),
                  Expanded(
                    child: Txt(
                        txt: AppDefine.CUR_SIGN +
                            totalExpectedSalary.toStringAsFixed(2),
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: true),
                  ),
                ],
              ),
            ),
          ),
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(top: 10, left: 10, right: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Txt(
                      txt: "Number of salaries:",
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(width: 10),
                  Expanded(
                    child: Txt(
                        txt: employment.numberOfSalaries.toString() ?? '0',
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: true),
                  ),
                ],
              ),
            ),
          ),
          Container(
            color: Colors.white,
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 10, left: 10, right: 10, bottom: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Txt(
                      txt: "Next expected salary:",
                      txtColor: Colors.grey,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  SizedBox(width: 10),
                  Expanded(
                    child: Txt(
                        txt: "-",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: true),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawTitle(String str) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Txt(
                txt: str,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: true),
            Container(color: Colors.pinkAccent, width: 20, height: 3),
          ],
        ),
      ),
    );
  }

  //  *********************************** FINANCIAL MARKERS

  drawFinancialMarkers() {
    final financialMarkers = getInsight4AccResponseDataModel.financialMarkers;
    final list = [
      {'title': "Bank Charges", 'l3m': "-", 'l12m': "-"},
      {'title': "Returned Direct Debit", 'l3m': "-", 'l12m': "-"},
    ];
    return Container(
        width: getW(context),
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            drawTitle("Financial Markers"),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 20),
              child: Txt(
                  txt: "PROPORTION OF INCOME (£0.00)",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            Padding(
              padding: const EdgeInsets.all(5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  _drawPropotionIncomeRow(
                      pa: financialMarkers.creditRepayments.rateOfChange
                              .toStringAsFixed(0) +
                          "%",
                      amounts: financialMarkers.creditRepayments.predNextMonth,
                      desc: "Credit Repayments"),
                  _drawPropotionIncomeRow(
                      pa: financialMarkers.gambling.rateOfChange
                              .toStringAsFixed(0) +
                          "%",
                      amounts: financialMarkers.gambling.predNextMonth,
                      desc: "Gambling"),
                  _drawPropotionIncomeRow(
                      pa: financialMarkers.cashWithdrawals.rateOfChange
                              .toStringAsFixed(0) +
                          "%",
                      amounts: financialMarkers.cashWithdrawals.predNextMonth,
                      desc: "Cash Withdrawals"),
                  _drawPropotionIncomeRow(
                      pa: financialMarkers.debitBankTransfer.rateOfChange
                              .toStringAsFixed(0) +
                          "%",
                      amounts: financialMarkers.debitBankTransfer.predNextMonth,
                      desc: "Debit Bank Transfer"),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20, left: 15),
              child: Txt(
                  txt: "INDICATORS",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
            _drawIndicatorRow1("New Debt", null),
            _drawIndicatorRow1("Debt Management", null),
            _drawIndicatorRow1("Possible Loss of Income", null),
            _drawIndicatorRow1("Out of Work Benefits", null),
            _drawIndicatorRow1("New Employer", null),
            _drawIndicatorRow1("Negative Balance Ratio", "0%"),
            SizedBox(height: 10),
            _drawIndicatorRow2(list)
          ],
        ));
  }

  _drawPropotionIncomeRow({String pa, double amounts, String desc}) {
    return Card(
      elevation: 10,
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 2,
                child: Txt(
                    txt: desc,
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Txt(
                    txt: pa,
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Txt(
                    txt: AppDefine.CUR_SIGN + amounts.toStringAsFixed(2),
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _drawIndicatorRow1(String title, String pa) {
    return Padding(
      padding: const EdgeInsets.only(left: 8, right: 8),
      child: Card(
        //color: Colors.yellow,
        elevation: 10,
        child: Padding(
          padding: const EdgeInsets.only(left: 5, right: 5),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Txt(
                    txt: title,
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              (pa == null)
                  ? Flexible(
                      child: IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.close,
                            color: MyTheme.brandColor,
                          )))
                  : Flexible(
                      child: Padding(
                        padding: const EdgeInsets.only(top: 8.0, bottom: 8),
                        child: Txt(
                            txt: pa,
                            txtColor: Colors.black87,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }

  _drawIndicatorRow2(List<dynamic> list) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(10.0),
      child: Table(
          border: TableBorder.all(
              width: .4), // Allows to add a border decoration around your table
          children: [
            TableRow(children: [
              Text(''),
              Text(
                'L3M',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black87,
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              ),
              Text(
                'L12M',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.black87,
                    fontSize: 15,
                    fontWeight: FontWeight.bold),
              ),
            ]),
            for (var map in list)
              TableRow(children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Txt(
                      txt: map['title'],
                      txtColor: Colors.black87,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Txt(
                      txt: map['l3m'],
                      txtColor: Colors.black87,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Txt(
                      txt: map['l12m'],
                      txtColor: Colors.black87,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false),
                ),
              ]),
          ]),
    );
  }

  drawCatIncome() {
    return Container(
      width: getW(context),
      color: Colors.white,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            drawTitle("Categories of Income"),
            _drawListCat(listCatIncome)
          ]),
    );
  }

  drawCatExpense() {
    return Container(
      width: getW(context),
      color: Colors.white,
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(height: 20),
            drawTitle("Categories of Expenses"),
            _drawListCat(listCatExp)
          ]),
    );
  }

  //  Generic Function by munir@AITL
  _drawListCat(List<dynamic> mainList) {
    final List<Widget> list = [];
    for (var map in mainList) {
      final title = map['title'];
      double amounts = map['amounts'];
      final pa = map['pa'];
      List<dynamic> level = map['level'];

      //  2nd level
      List<Widget> list2 = [];
      for (var map2 in level) {
        final title2 = map2['title'];
        double amounts2 = map2['amounts'];
        final pa2 = map2['pa'];
        List<dynamic> level2 = map2['level'];

        final row2 = _getCatRow(title2, amounts2, pa2);

        /*List<Widget> list3 = [];
        for (var map3 in level2) {
          final title3 = map3['title'];
          double amounts3 = map3['amounts'];
          final pa3 = map3['pa'];
          List<dynamic> level3 = map3['level'];

          final row3 = _getCatRow(title3, amounts3, pa3);

          List<Widget> list4 = [];
          for (var map4 in level3) {
            final title4 = map4['title'];
            double amounts4 = map4['amounts'];
            final pa4 = map4['pa'];
            //List<dynamic> level4 = map4['level'];

            final row4 = _getCatRow(title4, amounts4, pa4);

            list3.add(row4);
          }
        }*/

        list2.add(custom.ExpansionTile(
            headerBackgroundColor: Colors.white,
            iconColor: (level2.length > 0) ? Colors.black : Colors.transparent,
            title: row2,
            children: level2.length > 0 ? [_drawListCat(level2)] : []));
      }

      final row = _getCatRow(title, amounts, pa);
      list.add(custom.ExpansionTile(
          headerBackgroundColor: Colors.grey.shade100,
          iconColor: (level.length > 0) ? Colors.black : Colors.transparent,
          title: row,
          children: list2));
    }
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Column(
        children: list,
      ),
    );
  }

  _getCatRow(String title, double amounts, String pa) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //Icon(Icons.picture_in_picture, color: Colors.grey),
          Container(
            //width: getWP(context, 20),
            //color: Colors.yellow,
            child: Expanded(
                child: Txt(
                    txt: title,
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false)),
          ),
          Flexible(
              child: Txt(
                  txt: AppDefine.CUR_SIGN + amounts.toStringAsFixed(0),
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: false)),
          Flexible(
              child: Txt(
                  txt: pa,
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: true)),
        ],
      ),
    );
  }

  //  ********************* CHART
  //Initialize the data source

  drawLineChart() {
    return Container(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          drawTitle("Finances"),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Txt(
                txt: "20.12.2020 - 14.12.2021",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 20),
            child: Txt(
                txt: "Net Worth",
                txtColor: Colors.grey,
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () {
                      isChartBalanceStrike = !isChartBalanceStrike;
                      setChartData();
                      setState(() {});
                    },
                    child: Row(
                      children: [
                        UIHelper().drawCircle(
                            context: context,
                            color: Colors.blueAccent.withOpacity(.5),
                            size: 2),
                        SizedBox(width: 5),
                        Text('Balance',
                            style: TextStyle(
                                decoration: (isChartBalanceStrike)
                                    ? TextDecoration.lineThrough
                                    : null,
                                color: Colors.black,
                                fontSize: 14)),
                      ],
                    ),
                  ),
                  SizedBox(width: 10),
                  GestureDetector(
                    onTap: () {
                      isChartPredicationStrike = !isChartPredicationStrike;
                      setChartData();
                      setState(() {});
                    },
                    child: Row(
                      children: [
                        UIHelper().drawCircle(
                            context: context,
                            color: Colors.grey.withOpacity(.5),
                            size: 2),
                        SizedBox(width: 5),
                        Text('Prediction',
                            style: TextStyle(
                                decoration: (isChartPredicationStrike)
                                    ? TextDecoration.lineThrough
                                    : null,
                                color: Colors.black,
                                fontSize: 14)),
                      ],
                    ),
                  ),
                ],
              ),
              Stack(
                children: [
                  SfCartesianChart(
                    backgroundColor: MyTheme.bgColor2, //He
                    primaryXAxis: CategoryAxis(
                      labelStyle: txtStyle,
                    ),
                    primaryYAxis: NumericAxis(
                      labelStyle: txtStyle,
                    ),
                    // Enable tooltip
                    tooltipBehavior: _tooltipBehavior,
                    series: <LineSeries<_ChartData, String>>[
                      LineSeries<_ChartData, String>(
                        // Binding the chartData to the dataSource of the line series.
                        color: Colors.red,
                        markerSettings:
                            MarkerSettings(isVisible: true, color: Colors.red),
                        dashArray: <double>[15, 10],
                        dataSource: chartData,
                        xValueMapper: (_ChartData sales, _) => sales.year,
                        yValueMapper: (_ChartData sales, _) => sales.sales,
                      ),
                    ],
                  ),
                  SfCartesianChart(
                    plotAreaBorderWidth: 0,
                    margin: EdgeInsets.only(bottom: 30),
                    primaryXAxis: CategoryAxis(
                      labelStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 0,
                      ),
                      majorGridLines: MajorGridLines(width: 0),
                      axisLine: AxisLine(width: 0),
                      isInversed: false,
                    ),
                    primaryYAxis: NumericAxis(
                      labelStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 0,
                      ),
                      majorGridLines: MajorGridLines(width: 0),
                      axisLine: AxisLine(width: 0),
                      isInversed: false,
                    ),
                    // Enable tooltip
                    tooltipBehavior: _tooltipBehavior2,
                    series: <ColumnSeries<_BarData, String>>[
                      ColumnSeries<_BarData, String>(
                        color: Colors.blueAccent.withOpacity(.5),
                        dataSource: barData,
                        xValueMapper: (_BarData sales, _) => sales.year,
                        yValueMapper: (_BarData sales, _) => sales.sales,
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 20, left: 20, bottom: 10),
            child: Txt(
                txt: "Incomings vs Outgoings",
                txtColor: Colors.grey,
                txtSize: MyTheme.txtSize - .4,
                txtAlign: TextAlign.start,
                isBold: false),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GestureDetector(
                onTap: () {
                  isChart2IncomingsStrike = !isChart2IncomingsStrike;
                  setChart2Data();
                  setState(() {});
                },
                child: Row(
                  children: [
                    UIHelper().drawCircle(
                        context: context,
                        color: Colors.greenAccent.withOpacity(.5),
                        size: 2),
                    SizedBox(width: 5),
                    Text('Incomings',
                        style: TextStyle(
                            decoration: (isChart2IncomingsStrike)
                                ? TextDecoration.lineThrough
                                : null,
                            color: Colors.black,
                            fontSize: 14)),
                  ],
                ),
              ),
              SizedBox(width: 10),
              GestureDetector(
                onTap: () {
                  isChart2OutgoingsStrike = !isChart2OutgoingsStrike;
                  setChart2Data();
                  setState(() {});
                },
                child: Row(
                  children: [
                    UIHelper().drawCircle(
                        context: context,
                        color: Colors.red.withOpacity(.5),
                        size: 2),
                    SizedBox(width: 5),
                    Text('Outgoings',
                        style: TextStyle(
                            decoration: (isChart2OutgoingsStrike)
                                ? TextDecoration.lineThrough
                                : null,
                            color: Colors.black,
                            fontSize: 14)),
                  ],
                ),
              ),
              SizedBox(width: 20),
              GestureDetector(
                onTap: () {
                  isChart2BalanceStrike = !isChart2BalanceStrike;
                  setChart2Data();
                  setState(() {});
                },
                child: Row(
                  children: [
                    UIHelper().drawCircle(
                        context: context,
                        color: Colors.orange.withOpacity(.5),
                        size: 2),
                    SizedBox(width: 5),
                    Text('Balance',
                        style: TextStyle(
                            decoration: (isChart2BalanceStrike)
                                ? TextDecoration.lineThrough
                                : null,
                            color: Colors.black,
                            fontSize: 14)),
                  ],
                ),
              ),
            ],
          ),
          SfCartesianChart(
            primaryXAxis: CategoryAxis(
              labelStyle: txtStyle,
            ),
            primaryYAxis: NumericAxis(
              labelStyle: txtStyle,
            ),
            // Enable tooltip
            tooltipBehavior: _tooltipBehavior3,
            series: <ColumnSeries<_ChartData2, String>>[
              ColumnSeries<_ChartData2, String>(
                color: Colors.blueAccent.withOpacity(.5),
                // Binding the chartData to the dataSource of the column series.
                dataSource: chartData2,
                xValueMapper: (_ChartData2 sales, _) => sales.year,
                yValueMapper: (_ChartData2 sales, _) => sales.sales,
              ),
            ],
          ),
        ],
      ),
    );
  }
}

/// Sample ordinal data type.
class _ChartData {
  final String year;
  final double sales;
  _ChartData(this.year, this.sales);
}

class _BarData {
  final String year;
  final double sales;
  _BarData(this.year, this.sales);
}

class _ChartData2 {
  final String year;
  final double sales;
  _ChartData2(this.year, this.sales);
}
