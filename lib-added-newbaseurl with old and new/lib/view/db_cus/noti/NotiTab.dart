import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/api/db_cus/noti/DeleteNotification.dart';
import 'package:aitl/controller/api/db_cus/noti/NotiAPIMgr.dart';
import 'package:aitl/controller/helper/db_cus/tab_newcase/NewCaseHelper.dart';
import 'package:aitl/controller/helper/db_cus/tab_noti/NotiHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/AppData.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/json/db_cus/tab_noti/NotiModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/GetTaskAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/case_details_dart.dart';
import 'package:aitl/view/widgets/btn/BtnOutlineAndBackground.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';

class NotiTab extends StatefulWidget {
  @override
  State createState() => _NotiTabState();
}

class _NotiTabState extends State<NotiTab> with Mixin, StateListener {
  StateProvider _stateProvider;

  List<NotiModel> listNotiModel = [];

  //List<NotiModel> listNotiModel_recent = [];
  //List<NotiModel> listNotiModel_older = [];

  //  page stuff start here
  bool isPageDone = false;
  bool isLoading = false;
  int pageStart = 0;
  int pageCount = AppConfig.page_limit;

  @override
  void onStateChangedWithData(state, data) async {
    try {
      if (state == ObserverState.STATE_BADGE_NOTIFICATION_COUNT && data == 3) {
        if (mounted) {
          final numberOfUnReadNotification =
              appData.taskNotificationCountAndChatUnreadCountData != null
                  ? appData.taskNotificationCountAndChatUnreadCountData
                      .numberOfUnReadNotification
                  : 0;
          if (numberOfUnReadNotification > 0) {
            appData.taskNotificationCountAndChatUnreadCountData
                .numberOfUnReadNotification = 0;
            await PrefMgr.shared.setPrefInt(
                "NumberOfUnReadNotification", numberOfUnReadNotification);
          }
          StateProvider().notifyWithData(ObserverState.STATE_BOTNAV, null);
          //setState(() {});
        }
      }
    } catch (e) {}
  }

  @override
  onStateChanged(ObserverState state) async {
    isLoading = false;
    if (state == ObserverState.STATE_CHANGED_tabbar4_reload_case_api) {
      _getRefreshData();
    }
  }

  onPageLoad() async {
    setState(() {
      isLoading = true;
    });
    try {
      NotiAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final List<dynamic> notifications =
                      model.responseData.notifications;

                  if (notifications != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                    if (notifications.length != pageCount) {
                      isPageDone = true;
                    }
                    try {
                      /*for (NotiModel noti in notifications) {
                        var date1 = Jiffy(noti.publishDateTime).dateTime;
                        final numDays =
                            DateFun.daysBetween(date1, DateTime.now());
                        if (numDays > 1) {
                          listNotiModel_older.add(noti);
                        } else {
                          listNotiModel_recent.add(noti);
                        }
                      }*/
                      for (NotiModel noti in notifications) {
                        listNotiModel.add(noti);
                      }
                    } catch (e) {
                      myLog(e.toString());
                    }

                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      if (mounted) {
                        setState(() {
                          isLoading = false;
                        });
                      }
                    }
                  }
                } catch (e) {
                  myLog(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showToast(context: context, msg: "Notifications not found");
                  }
                } catch (e) {
                  myLog(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              myLog(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          }
        },
      );
    } catch (e) {
      myLog(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> _getRefreshData() async {
    pageStart = 0;
    isPageDone = false;
    listNotiModel.clear();
    //listNotiModel_recent.clear();
    //listNotiModel_older.clear();
    onPageLoad();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    //listNotiModel_recent = null;
    listNotiModel = null;
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    try {
      _getRefreshData();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          centerTitle: false,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Notifications"),
          bottom: PreferredSize(
            preferredSize: Size.fromHeight((isLoading) ? .5 : 0),
            child: (isLoading)
                ? AppbarBotProgBar(
                    backgroundColor: MyTheme.appbarProgColor,
                  )
                : SizedBox(),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    try {
      return Container(
        child: (listNotiModel.length > 0)
            ? NotificationListener(
                onNotification: (scrollNotification) {
                  if (scrollNotification is ScrollStartNotification) {
                    //print('Widget has started scrolling');
                  } else if (scrollNotification is ScrollEndNotification) {
                    if (!isPageDone) {
                      pageStart++;
                      onPageLoad();
                    }
                  }
                  return true;
                },
                child: RefreshIndicator(
                  color: Colors.white,
                  backgroundColor: MyTheme.statusBarColor,
                  onRefresh: _getRefreshData,
                  child: SingleChildScrollView(
                    primary: true,
                    physics: AlwaysScrollableScrollPhysics(),
                    child: Padding(
                      padding:
                          const EdgeInsets.only(left: 20, top: 10, right: 20),
                      child: (listNotiModel.length > 0)
                          ? ListView.builder(
                              addAutomaticKeepAlives: true,
                              cacheExtent: AppConfig.page_limit.toDouble(),
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              primary: false,
                              itemCount: listNotiModel.length,
                              itemBuilder: (BuildContext context, int index) {
                                return drawNotiItem(listNotiModel[index]);
                              },
                            )
                          : SizedBox(),
                    ),
                  ),
                ),
              )
            : (!isLoading)
                ? Center(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 50, right: 50),
                      child: ListView(
                        //mainAxisAlignment: MainAxisAlignment.start,
                        shrinkWrap: true,
                        children: [
                          Container(
                            width: getWP(context, 100),
                            height: getHP(context, 48),
                            child: Image.asset(
                              'assets/images/screens/db_cus/my_cases/case_nf.png',
                              fit: BoxFit.fill,
                            ),
                          ),
                          //SizedBox(height: 40),
                          Padding(
                            padding: const EdgeInsets.only(left: 10, right: 10),
                            child: Container(
                              width: getWP(context, 70),
                              child: Txt(
                                txt:
                                    "Looks like you haven't got any notification yet",
                                txtColor: MyTheme.mycasesNFBtnColor,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                isBold: false,
                                //txtLineSpace: 1.5,
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Padding(
                            padding: const EdgeInsets.only(left: 20, right: 20),
                            child: MMBtn(
                              txt: "Refresh",
                              bgColor: MyTheme.titleColor,
                              width: null, //getWP(context, 50),
                              height: getHP(context, 6),
                              callback: () {
                                onPageLoad();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : SizedBox(),
      );
    } catch (e) {}
  }

  drawNotiItem(NotiModel notiModel) {
    try {
      if (notiModel == null) return SizedBox();
      Map<String, dynamic> notiMap = NotiHelper().getNotiMap(model: notiModel);
      if (notiMap.length == 0) return SizedBox();

      String icon =
          NewCaseHelper().getCreateCaseIconByTitle(notiModel.eventName);
      if (icon == null) return SizedBox();
      String txt = notiMap['txt'].toString();
      /*  String eventName = '';
      if ((txt.endsWith(' ' + notiModel.eventName))) {
        // txt = txt.replaceAll(notiModel.eventName.trim(), '');
        eventName = notiModel.eventName;
      }

      String initiatorDisplayName = '';
      if ((txt.startsWith(notiModel.initiatorDisplayName))) {
        // txt = txt.replaceAll(notiModel.initiatorDisplayName.trim(), '');
        initiatorDisplayName = notiModel.initiatorDisplayName;
      }*/
      return GestureDetector(
        onTap: () async {
          /*  if (mounted) {
            await NotiHelper().setRoute(
                context: context,
                notiModel: notiModel,
                notiMap: notiMap,
                callback: () {
                  _stateProvider.notify(ObserverState.STATE_CHANGED_tabbar3);
                });
          }*/
        },
        child: Padding(
          padding: const EdgeInsets.only(top: 10),
          child: Container(
            //height: getHP(context, 25),
            //color: Colors.blue,
            child: Card(
              color: MyTheme.bgColor2,
              elevation: 0,
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 50,
                        height: 50,
                        decoration: new BoxDecoration(
                          shape: BoxShape.rectangle,
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15),
                          border:
                              Border.all(color: Color(0xFFD9E1E7), width: 2),
                        ),
                        child: Image.asset(icon, color: MyTheme.titleColor),
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20, right: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Txt(
                                txt: notiModel.initiatorDisplayName
                                    .toString()
                                    .uFirst(),
                                txtColor: MyTheme.titleColor,
                                txtSize: MyTheme.txtSize - .2,
                                isBold: true,
                                txtAlign: TextAlign.start,
                                maxLines: 1,
                              ),
                              SizedBox(height: 5),
                              Txt(
                                txt: notiModel.initiatorDisplayName
                                        .toString()
                                        .uFirst() +
                                    " sent message on case",
                                txtColor: Color(0xFF1F3548),
                                txtSize: MyTheme.txtSize - .8,
                                isBold: false,
                                txtAlign: TextAlign.start,
                                maxLines: 1,
                              ),
                              SizedBox(height: 5),
                              Txt(
                                txt: txt,
                                txtColor: Color(0xFF2D4665),
                                txtSize: MyTheme.txtSize - .5,
                                isBold: false,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                maxLines: 2,
                              ),
                              SizedBox(height: 5),
                              Txt(
                                txt: notiMap['publishDateTime'],
                                txtColor: MyTheme.mycasesNFBtnColor,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                isBold: false,
                              ),
                              //SizedBox(height: 5),
                              Container(
                                width: getW(context),
                                //height: 45,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Expanded(
                                      child: Container(
                                        child: BtnOutlineAndBackground(
                                          txt: "View Case",
                                          backgroundColor: Colors.white,
                                          txtColor: MyTheme.titleColor,
                                          borderColor: Color(0xFF2D4665),
                                          callback: () async {
                                            await APIViewModel()
                                                .req<GetTaskAPIModel>(
                                                    context: context,
                                                    url: SrvW2NCase
                                                            .GET_TASK_URL +
                                                        notiModel.entityId
                                                            .toString(),
                                                    reqType: ReqType.Get,
                                                    callback: (model) async {
                                                      if (mounted &&
                                                          model != null) {
                                                        if (model.success) {
                                                          Get.to(() => CaseDetailsPage(
                                                              caseModel: model
                                                                  .responseData
                                                                  .task)); /*.then((value) =>
                                                              _stateProvider.notify(
                                                                  ObserverState
                                                                      .STATE_CHANGED_tabbar3));*/
                                                        }
                                                      }
                                                    });
                                          },
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 5,
                                    ),
                                    Expanded(
                                      child: Container(
                                        child: BtnOutlineAndBackground(
                                          txt: "Delete",
                                          backgroundColor: Colors.white,
                                          txtColor: MyTheme.brandColor,
                                          borderColor: MyTheme.brandColor,
                                          callback: () {
                                            confirmDialog(
                                                callbackYes: () {
                                                  NotiAPIMgr()
                                                      .wsDeleteNotification(
                                                          context: context,
                                                          id: notiModel.id
                                                              .toString(),
                                                          callback:
                                                              (DeleteNotificationModel
                                                                  model) {
                                                            if (model != null &&
                                                                mounted) {
                                                              try {
                                                                if (model
                                                                    .success) {
                                                                  myLog("Noti delete model = " +
                                                                      model
                                                                          .toJson()
                                                                          .toString());

                                                                  _getRefreshData();
                                                                }
                                                              } catch (e) {
                                                                myLog("Noti delete err = " +
                                                                    e.toString());
                                                              }
                                                            } else {
                                                              myLog(
                                                                  "Notification delete model getting null ");
                                                            }
                                                          });
                                                },
                                                callbackNo: () {},
                                                context: context,
                                                msg:
                                                    "Press \'Delete\' to Remove this notification ?",
                                                title: "Delete Alert !");
                                          },
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 10),
                            ],
                          ),
                        ),
                      ),
                      notiModel.isRead
                          ? Image.asset("assets/images/icons/check2_icon.png",
                              width: 20, height: 20, color: Colors.green)
                          : Image.asset("assets/images/icons/check1_icon.png",
                              width: 20, height: 20)
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    width: getW(context),
                    color: Colors.grey[300],
                    height: 1,
                  )
                ],
              ),
            ),
          ),
        ),
      );
    } catch (e) {
      myLog(e.toString());
    }
    return SizedBox();
  }
}
