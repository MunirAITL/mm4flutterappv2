import 'dart:io';

import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/media/MediaUploadAPIMgr.dart';
import 'package:aitl/model/json/web2native/case/requirements/13btl_portfolio/MortgageUserPortfolios.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/13btl_portfolio/req_btl_portfolio_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/13btl_portfolio/btl_portf_details.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/helper/util/ftp_mgr.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step13_btl_portfolio_ctrl.dart';
import 'package:flutter/material.dart';
import 'package:aitl/view/db_cus/web2native/case_base.dart';
import 'package:get/get.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:file_picker/file_picker.dart';

abstract class BtlPortFolioBase<T extends StatefulWidget> extends CaseBase<T> {
  onDelBtlPortFolioItem(MortgageUserPortfolios model);

  final listOtherMortRB = {0: "Yes", 1: "No"};
  int otherMortRBIndex = 0;

  browseFiles() async {
    try {
      FilePickerResult result = await FilePicker.platform.pickFiles(
        allowMultiple: false,
        type: FileType.custom,
        allowedExtensions: [
          'xlsx',
          'xls',
        ],
        // type: FileType.any,
        /*allowedExtensions: [
                      'jpg',
                      'jpeg',
                      'png',
                      'pdf',
                      'doc',
                      'docx'
                    ],*/
      );

      if (result != null) {
        MediaUploadAPIMgr().wsUploadXLSFileAPI(
          context: context,
          file: File(result.files.single.path),
          callback: (model) {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  showToast(
                      context: context, msg: 'File uploaded successfully');
                } else {
                  showToast(context: context, msg: "Failed to upload file");
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          },
        );
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  drawForm() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DottedBorder(
            borderType: BorderType.RRect,
            radius: Radius.circular(10),
            padding: EdgeInsets.all(5),
            color: Colors.grey,
            strokeWidth: .5,
            child: GestureDetector(
              onTap: () async {
                //await browseFiles();
                final file = await FTPMgr().uploadXLS("btl_portfolio");
                if (file != null) {
                  MediaUploadAPIMgr().wsUploadXLSFileAPI(
                    context: context,
                    file: file,
                    callback: (model) {
                      if (model != null && mounted) {
                        try {
                          if (model.success) {
                            showToast(
                                context: context,
                                msg:
                                    "File uploaded successfully and saved into your device path.\n\n" +
                                        file.path.replaceAll("emulated/0/", ""),
                                which: 1);
                          } else {
                            showToast(
                                context: context, msg: "Failed to upload file");
                          }
                        } catch (e) {
                          myLog(e.toString());
                        }
                      }
                    },
                  );
                }
              },
              child: Container(
                height: getHP(context, 5),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 3,
                      child: Txt(
                          txt: "Upload BTL Portfolio Spreadsheet File",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: false),
                    ),
                    Flexible(
                      child: Icon(
                        Icons.attach_file,
                        color: Colors.grey,
                        size: 25,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 20),
          /*Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 3,
                child: GestureDetector(
                  child: Text(
                      "CLICK HERE to Download Sample Spreadsheet to Upload",
                      style: TextStyle(
                          decoration: TextDecoration.underline,
                          color: Colors.blue,
                          fontSize: 13)),
                  onTap: () async {
                    Map<Permission, PermissionStatus> statuses = await [
                      Permission.storage,
                    ].request();

                    final file =
                        await DownloadFile().saveCSV();
                    showToast(
                        context: context,
                        msg: "File downloaded successfully.\n\n" +
                            file.path.replaceAll("emulated/0/", ""),
                        which: 1);
                  },
                ),
              ),
              Flexible(
                  child: GestureDetector(
                      onTap: () {
                        showToolTips(
                            context: context,
                            txt:
                                "You can download a sample template in excel format which you can populate with your data and upload using the upload box on the left.");
                      },
                      child: Icon(
                        Icons.info_outline_rounded,
                        color: MyTheme.statusBarColor,
                        size: 20,
                      )))
            ],
          ),
          SizedBox(height: 20),*/
          Txt(
              txt:
                  "Lenders need to know about all the mortgages you have right now. That includes a mortgage on the place you're living in, and mortgages on buy-to-let properties like rentals or holiday homes.",
              txtColor: Colors.black54,
              txtSize: MyTheme.txtSize - .4,
              txtAlign: TextAlign.start,
              isBold: false),
          SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              title:
                  "Do you have any other mortgages (not including the mortgage this application is for)?",
              list: listOtherMortRB,
              index: otherMortRBIndex,
              callback: (index) {
                setState(() => otherMortRBIndex = index);
              }),
          otherMortRBIndex == 0
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10),
                    Txt(
                        txt: "Tell us about your existing mortgages",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .4,
                        txtAlign: TextAlign.start,
                        isBold: true),
                    SizedBox(height: 10),
                    drawDottedBox("Add BTL Portfolio", () {
                      Get.to(() => BtlPortfolioDetails()).then((value) {
                        setState(() {});
                      });
                    }),
                    SizedBox(height: 10),
                    drawBtlPortfolioItems(
                        context, true, btlPortfCtrl.listBtlPortModel,
                        (model, isEdit) {
                      if (isEdit) {
                        Get.to(() => BtlPortfolioDetails(model: model))
                            .then((value) {
                          setState(() {});
                        });
                      } else {
                        onDelBtlPortFolioItem(model);
                      }
                    })
                  ],
                )
              : SizedBox(),
        ],
      ),
    );
  }
}
