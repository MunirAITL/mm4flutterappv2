import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';

import '../../../../../../../config/MyTheme.dart';
import '../../../../../../../model/json/web2native/case/requirements/remortgage/reasons/MortgageLoanReasonsModel.dart';
import '../../../../../../../view_model/rx/web2native/requirements/req_remortgage_ctrl.dart';
import '../../../../../../widgets/dialog/DatePickerView.dart';
import '../../../../../../widgets/dropdown/DropDownListDialog.dart';
import '../../../../../../widgets/dropdown/DropListModel.dart';
import '../../../../../../widgets/gplaces/GPlacesView.dart';
import '../../../../../../widgets/input/InputTitleBox.dart';
import '../../../../../../widgets/radio/draw_radio_group.dart';
import '../../../../../../widgets/txt/Txt.dart';
import '../../../../case_base.dart';

enum eERSubList { Equity_Release, Home_Reversion }

abstract class ReqERBase<T extends StatefulWidget> extends CaseBase<T> {
  onAddReasonDialog();
  onEditReasonDialog(MortgageLoanReasonsModel model);
  onDelReasonDialog(MortgageLoanReasonsModel model);
  onEquityReleasePowerAttorneyAPI();
  getSubCase();

  final requirementController = Get.put(ReqReMortgageCtrl());

  var ddRelationShip = DropListModel([
    OptionItem(id: "1", title: "Son"),
    OptionItem(id: "2", title: "Daughter"),
    OptionItem(id: "3", title: "Spouse"),
    OptionItem(id: "4", title: "Brother"),
    OptionItem(id: "5", title: "Sister"),
    OptionItem(id: "6", title: "Nephew"),
    OptionItem(id: "7", title: "Niece"),
    OptionItem(id: "8", title: "Uncle"),
    OptionItem(id: "9", title: "Aunt"),
    OptionItem(id: "10", title: "Solicitor"),
    OptionItem(id: "11", title: "Friend"),
    OptionItem(id: "12", title: "Associate"),
    OptionItem(id: "13", title: "Carer"),
    OptionItem(id: "14", title: "Social Worker"),
    OptionItem(id: "15", title: "Others"),
  ]).obs;
  var optRelationShip = OptionItem(id: null, title: "Select Relationship");

  int existingLifttimeMortRBIndex = 1;

  int fundsAvailableRBIndex = 1;

  int incentivesRBIndex = 1;

  int capitalRaisingRBIndex = 1;

  int curResidingRBIndex = 1;

  int retainingPropertyRBIndex = 1;

  int expLandlord12mRBIndex = 1;

  int everLivedPropertyRBIndex = 1;

  int firstTimeLandlordRBIndex = 1;

  final listPrefRepayTypeRB = {
    0: 'Capital interest',
    1: 'Interest only',
    2: 'Part & part',
    3: 'No preference'
  };
  int prefRepayTypeRBIndex = 0;

  int propertyTenantedRBIndex = 1;

  final listPropertyTenantedTypeRB = {
    0: 'Standard Tenancy',
    1: 'HMO (House in Multiple Occupation)',
    2: 'Multi Unit Block',
    3: 'Periodic Tenancy',
    4: 'Corporate Let'
  };
  int propertyTenantedTypeRBIndex = 0;

  final listPropertyTypeRB = {0: 'House', 1: 'Flat'};
  int propertyTypeIndex = 0;

  final listPropertyTenureRB = {0: 'Freehold', 1: 'Leasehold', 2: 'Feudal'};
  int propertyTenureIndex = 0;

  int extLoftConvRBIndex = 1;

  int nonStandardConstrRBIndex = 1;

  int exCouncilRBIndex = 1;

  int xAccessRBIndex = 1;

  int newBuiltPropRBIndex = 1;

  int flatHasShopRBIndex = 1;

  int additionalFundsRBIndex = 1;

  int ageAllowanceRBIndex = 1;

  int thirdPartyRBIndex = 1;

  int doesWarantyRBIndex = 1;

  int haveLiftRBIndex = 1;

  var optDoesWarranty = OptionItem(id: null, title: "Select warranty").obs;

  final currentValProperty = TextEditingController();
  final currentMortOutstanding = TextEditingController();
  final monthlyMortgagePayment = TextEditingController();
  final currentLender = TextEditingController();
  final currentLenderAccNo = TextEditingController();
  final purchasePrice = TextEditingController();
  final loanAmount = TextEditingController();
  final depositAmount = TextEditingController();
  final incentiveAmount = TextEditingController();
  final prefMortTermsYY = TextEditingController();
  final prefMortTermsMM = TextEditingController();
  final noBedrooms = TextEditingController();
  final noKitchens = TextEditingController();
  final noBathrooms = TextEditingController();
  final groundRent = TextEditingController();
  final srvCharges = TextEditingController();
  final noWC = TextEditingController();
  final floorBuilding = TextEditingController();
  final whichFloorProperty = TextEditingController();
  final yearBuilt = TextEditingController();
  final notes = TextEditingController();
  final nonStandardConstrDetails = TextEditingController();
  final yearLeftLease = TextEditingController();
  final rentalIncome = TextEditingController();
  final capitalRaising = TextEditingController();
  final howmuchFund = TextEditingController();
  //
  final statementHealth = TextEditingController();
  final clientObj = TextEditingController();
  final specialConsideration = TextEditingController();
  final arrangementInPlace = TextEditingController();
  final alternativeAssets = TextEditingController();
  final pensionBenefits = TextEditingController();
  final stateBenefits = TextEditingController();
  final finSupports = TextEditingController();
  final monthlyInterest = TextEditingController();
  final lifetimeMortgage = TextEditingController();
  final retainOwnership = TextEditingController();
  final rentingPart = TextEditingController();
  final downsizing = TextEditingController();
  final beneficiariesState = TextEditingController();
  final howmuchCapital = TextEditingController();
  final maxPropertyValue = TextEditingController();
  final adhocInterval = TextEditingController();

  final focusCurrentValProperty = FocusNode();
  final focusCurrentMortOutstanding = FocusNode();
  final focusMonthlyMortgagePayment = FocusNode();
  final focusCurrentLender = FocusNode();
  final focusCurrentLenderAccNo = FocusNode();
  final focusPurchasePrice = FocusNode();
  final focusLoanAmount = FocusNode();
  final focusDepositAmount = FocusNode();
  final focusIncentivesAmount = FocusNode();
  final focusPrefMortTermsYY = FocusNode();
  final focusPrefMortTermsMM = FocusNode();
  final focusNoBedrooms = FocusNode();
  final focusNoKitchens = FocusNode();
  final focusNoBathrooms = FocusNode();
  final focusGroundRent = FocusNode();
  final focusSrvCharges = FocusNode();
  final focusNoWC = FocusNode();
  final focusFloorBuilding = FocusNode();
  final focusYearBuilt = FocusNode();
  final focusNonStandardConstrDetails = FocusNode();
  final focusYearLeftLease = FocusNode();
  final focusWhichFloorProperty = FocusNode();
  final focusRentalIncome = FocusNode();
  final focusCapitalRaising = FocusNode();
  final focusHowmuchFund = FocusNode();
  //
  final focusStatementHealth = FocusNode();
  final focusClientObj = FocusNode();
  final focusSpecialConsideration = FocusNode();
  final focusArrangementInPlace = FocusNode();
  final focusAlternativeAssets = FocusNode();
  final focusPensionBenefits = FocusNode();
  final focusStateBenefits = FocusNode();
  final focusFinSupports = FocusNode();
  final focusMonthlyInterest = FocusNode();
  final focusLifetimeMortgage = FocusNode();
  final focusRetainOwnership = FocusNode();
  final focusRentingPart = FocusNode();
  final focusDownsizing = FocusNode();
  final focusBeneficiariesState = FocusNode();
  final focusHowmuchCapital = FocusNode();
  final focusMaxPropertyValue = FocusNode();
  final focusAdhocInterval = FocusNode();

  String datePropertyPurchase = "";
  String addrOfProperty = "";

  onValidate() {
    return true;
  }

  drawForm() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          GPlacesView(
              title: "Current Property Address & Security Property?",
              subTitle:
                  "Your current residential address MUST be the same as the security property. Otherwise this application can not continue.",
              titleColor: Colors.black,
              isBold: true,
              txtSize: MyTheme.txtSize - .2,
              address: addrOfProperty,
              callback: (String address, String postCode, Location loc) {
                addrOfProperty = address;
                setState(() {});
              }),
          SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              title: getSubCase() == eERSubList.Equity_Release
                  ? "Do you have any existing lifetime mortgage (Equity Release product) on this property?"
                  : "Do you have any mortgage on this property?",
              list: W2NLocalData.listYesNoRB,
              index: existingLifttimeMortRBIndex,
              callback: (index) {
                setState(() => existingLifttimeMortRBIndex = index);
              }),
          existingLifttimeMortRBIndex == 0
              ? Column(
                  children: [
                    SizedBox(height: 10),
                    drawCurrencyBox(
                        getSubCase() == eERSubList.Equity_Release
                            ? "Current Balance Amount"
                            : "Current Balance Outstanding",
                        "If you're not sure, your best guess is fine at this point.",
                        currentMortOutstanding,
                        focusCurrentMortOutstanding,
                        focusPurchasePrice,
                        (v) {}),
                    SizedBox(height: 10),
                    drawInputBox(
                      context: context,
                      title: "Current Lender Name",
                      ph: "Current Lender",
                      input: currentLender,
                      len: 50,
                      txtColor: Colors.black,
                      isBold: true,
                      kbType: TextInputType.text,
                      inputAction: TextInputAction.next,
                      focusNode: focusCurrentLender,
                      focusNodeNext: focusCurrentLenderAccNo,
                    ),
                    SizedBox(height: 10),
                    drawInputBox(
                      context: context,
                      title: "Current Lender Account Number",
                      ph: "Account Number",
                      input: currentLenderAccNo,
                      len: 20,
                      txtColor: Colors.black,
                      isBold: true,
                      kbType: TextInputType.number,
                      inputAction: TextInputAction.next,
                      focusNode: focusCurrentLenderAccNo,
                      focusNodeNext: focusPurchasePrice,
                    ),
                  ],
                )
              : SizedBox(),
          SizedBox(height: 10),
          DatePickerView(
            cap: "Date property purchased",
            dt: (datePropertyPurchase == '')
                ? 'Select Date'
                : datePropertyPurchase,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            initialDate: DateTime.now(),
            firstDate: DateTime(1900, 1, 1),
            lastDate: DateTime.now(),
            padding: 5,
            radius: 5,
            callback: (value) {
              if (mounted) {
                setState(() {
                  try {
                    datePropertyPurchase =
                        DateFormat('dd-MMM-yyyy').format(value).toString();
                  } catch (e) {
                    myLog(e.toString());
                  }
                });
              }
            },
          ),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Purchase price",
              "If you're not sure, your best guess is fine at this point.",
              purchasePrice,
              focusPurchasePrice,
              focusLoanAmount, (v) {
            try {
              final v1 = v.trim().isEmpty ? '0' : v.trim();
              final v2 =
                  loanAmount.text.trim().isEmpty ? '0' : loanAmount.text.trim();
              final purPrice = int.parse(v1);
              final lonAmt = int.parse(v2);
              depositAmount.text = lonAmt > purPrice
                  ? (lonAmt - purPrice).toString()
                  : (purPrice - lonAmt).toString();
              setState(() {});
            } catch (e) {}
          }),
          SizedBox(height: 10),
          drawCurrencyBox(
              "How much do you want to raise (Loan Amount)?",
              "If you're not sure, your best guess is fine at this point.",
              loanAmount,
              focusLoanAmount,
              focusDepositAmount, (v) {
            try {
              final v1 = v.trim().isEmpty ? '0' : v.trim();
              final v2 = purchasePrice.text.trim().isEmpty
                  ? '0'
                  : purchasePrice.text.trim();
              final lonAmt = int.parse(v1);
              final purPrice = int.parse(v2);
              depositAmount.text = lonAmt > purPrice
                  ? (lonAmt - purPrice).toString()
                  : (purPrice - lonAmt).toString();
              setState(() {});
            } catch (e) {}
          }),
          _drawPrefMortgageTermsView(),
          _drawPrefRepayTypeView(),
          SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              title:
                  "Are funds available to pay fees in connection with mortgage?",
              list: W2NLocalData.listYesNoRB,
              index: fundsAvailableRBIndex,
              callback: (index) {
                setState(() => fundsAvailableRBIndex = index);
              }),
          fundsAvailableRBIndex == 0
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: drawCurrencyBox(
                      "How much funds available to pay fees in connection with mortgage?",
                      "If you're not sure, your best guess is fine at this point.",
                      howmuchFund,
                      focusHowmuchFund,
                      null,
                      (v) {}))
              : SizedBox(),
          _drawPropertyTypeView(),
          _drawPropertyTenureView(),
          SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: drawInputBox(
                  context: context,
                  title: "Number of Bedrooms",
                  ph: "0",
                  input: noBedrooms,
                  len: 2,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusNoBedrooms,
                  focusNodeNext: focusNoKitchens,
                ),
              ),
              SizedBox(width: 10),
              Flexible(
                child: drawInputBox(
                  context: context,
                  title: "Number of Kitchens",
                  ph: "0",
                  input: noKitchens,
                  len: 2,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusNoKitchens,
                  focusNodeNext: focusGroundRent,
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: drawInputBox(
                  context: context,
                  title: "Number of Bathrooms",
                  ph: "0",
                  input: noBathrooms,
                  len: 2,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusNoBathrooms,
                  focusNodeNext: focusNoWC,
                ),
              ),
              SizedBox(width: 10),
              Flexible(
                child: drawInputBox(
                  context: context,
                  title: "Number of W/C",
                  ph: "0",
                  input: noWC,
                  len: 2,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusNoWC,
                  focusNodeNext: focusFloorBuilding,
                ),
              ),
            ],
          ),
          propertyTypeIndex == 1
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: drawInputBox(
                          context: context,
                          title: "How much is the ground rent?",
                          ph: "0",
                          input: groundRent,
                          len: 10,
                          txtColor: Colors.black,
                          isBold: true,
                          kbType: TextInputType.number,
                          inputAction: TextInputAction.next,
                          focusNode: focusGroundRent,
                          focusNodeNext: focusSrvCharges,
                        ),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        child: drawInputBox(
                          context: context,
                          title: "How much is the service charge?",
                          ph: "0",
                          input: srvCharges,
                          len: 10,
                          txtColor: Colors.black,
                          isBold: true,
                          kbType: TextInputType.number,
                          inputAction: TextInputAction.next,
                          focusNode: focusSrvCharges,
                          focusNodeNext: focusNoBathrooms,
                        ),
                      ),
                    ],
                  ))
              : SizedBox(),
          propertyTypeIndex == 1
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Column(
                    children: [
                      drawInputBox(
                        context: context,
                        title: "Floors in the building",
                        ph: "Floors in the building",
                        input: floorBuilding,
                        len: 20,
                        txtColor: Colors.black,
                        isBold: true,
                        kbType: TextInputType.text,
                        inputAction: TextInputAction.next,
                        focusNode: focusFloorBuilding,
                        focusNodeNext: focusWhichFloorProperty,
                      ),
                      SizedBox(height: 10),
                      UIHelper().drawRadioTitle(
                          context: context,
                          title: "Is the flat above shop/ restaurant?",
                          list: W2NLocalData.listYesNoRB,
                          index: flatHasShopRBIndex,
                          callback: (index) {
                            setState(() => flatHasShopRBIndex = index);
                          }),
                      SizedBox(height: 10),
                      UIHelper().drawRadioTitle(
                          context: context,
                          title: "Does the property have a lift?",
                          list: W2NLocalData.listYesNoRB,
                          index: haveLiftRBIndex,
                          callback: (index) {
                            setState(() => haveLiftRBIndex = index);
                          }),
                      SizedBox(height: 10),
                      drawInputBox(
                        context: context,
                        title: "Which floor is the property",
                        ph: "Which floor is the property",
                        input: whichFloorProperty,
                        len: 50,
                        txtColor: Colors.black,
                        isBold: true,
                        kbType: TextInputType.text,
                        inputAction: TextInputAction.next,
                        focusNode: focusWhichFloorProperty,
                        focusNodeNext: focusYearBuilt,
                      )
                    ],
                  ),
                )
              : SizedBox(),
          SizedBox(height: 10),
          drawInputBox(
            context: context,
            title: "Year property was built",
            ph: "YYYY",
            input: yearBuilt,
            len: 4,
            txtColor: Colors.black,
            isBold: true,
            kbType: TextInputType.number,
            inputAction: TextInputAction.next,
            focusNode: focusYearBuilt,
            focusNodeNext: null,
          ),
          SizedBox(height: 10),
          _drawRadioQ(),
          _drawExtraForm(),
        ],
      ),
    );
  }

  _drawExtraForm() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        getSubCase() == eERSubList.Equity_Release
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  existingLifttimeMortRBIndex == 0
                      ? Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              UIHelper().drawRadioTitle(
                                  context: context,
                                  title:
                                      "Are you looking for additional funds above current outstanding from same provider?",
                                  list: W2NLocalData.listYesNoRB,
                                  index: fundsAvailableRBIndex,
                                  callback: (index) {
                                    setState(
                                        () => fundsAvailableRBIndex = index);
                                  }),
                            ],
                          ),
                        )
                      : SizedBox(),
                  SizedBox(height: 10),
                  UIHelper().drawRadioTitle(
                      context: context,
                      title:
                          "Would an increase in capital or income have an effect on any means tested benefits or age allowances?",
                      list: W2NLocalData.listYesNoRB,
                      index: ageAllowanceRBIndex,
                      callback: (index) {
                        setState(() => ageAllowanceRBIndex = index);
                      }),
                ],
              )
            : SizedBox(),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Third party present at the discussion?",
            list: W2NLocalData.listYesNoRB,
            index: thirdPartyRBIndex,
            callback: (index) {
              setState(() => thirdPartyRBIndex = index);
            }),
        thirdPartyRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: DropDownListDialog(
                  context: context,
                  h1: "Select relationship",
                  heading: "Select Relationship To Other Client",
                  title: optRelationShip.title,
                  ddTitleList: ddRelationShip.value,
                  callback: (optionItem) {
                    optRelationShip = optionItem;
                  },
                ))
            : SizedBox(),
        SizedBox(height: 10),
        /*
    statementHealth.dispose();
    clientObj.dispose();
    specialConsideration.dispose();
    arrangementInPlace.dispose();
    alternativeAssets.dispose();
    pensionBenefits.dispose();
    stateBenefits.dispose();
    finSupports.dispose();
    monthlyInterest.dispose();
    lifetimeMortgage.dispose();
    retainOwnership.dispose();
    rentingPart.dispose();
    downsizing.dispose();
    beneficiariesState.dispose();
    howmuchCapital.dispose();
    maxPropertyValue.dispose();
    adhocInterval.dispose();
      */
        drawTextArea(
            title: "Statement of Health including medications, etc",
            ph: 'Please provide as much details as possible',
            tf: statementHealth),
        SizedBox(height: 10),
        drawTextArea(
            title:
                "Please provide details of client(s) objectives and priorities:",
            tf: clientObj),
        SizedBox(height: 10),
        drawTextArea(
            title:
                "Does the client(s) have any special considerations or placed any limitations of the service they wish to receive?",
            tf: specialConsideration),
        SizedBox(height: 30),
        Txt(
            txt: "(Lasting) Power of Attorney",
            txtColor: MyTheme.statusBarColor,
            txtSize: MyTheme.txtSize + .3,
            txtAlign: TextAlign.start,
            isBold: true),
        SizedBox(height: 20),
        drawTextArea(
            title: "Do you have any arrangements in place?",
            tf: arrangementInPlace),
        SizedBox(height: 10),
        drawTextArea(
            title: "Can other assets be considered as an alternative?",
            tf: alternativeAssets),
        SizedBox(height: 10),
        drawTextArea(
            title:
                "Have you considered accessing pension benefits as an alternative?",
            tf: pensionBenefits),
        SizedBox(height: 10),
        drawTextArea(
            title: "Will any state benefits be affected?", tf: stateBenefits),
        SizedBox(height: 10),
        drawTextArea(
            title:
                "Would family members or friends be prepared to provide financial support?",
            tf: finSupports),
        SizedBox(height: 10),
        drawTextArea(
            title:
                "Would you be prepared to pay monthly interest on any monies released?",
            tf: monthlyInterest),
        SizedBox(height: 10),
        drawTextArea(
            title:
                "Do clients want to consider lifetime mortgage or unsecured lending?",
            tf: lifetimeMortgage),
        SizedBox(height: 10),
        drawTextArea(
            title: "Would you wish to retain full ownership of the property?",
            tf: retainOwnership),
        SizedBox(height: 10),
        drawTextArea(
            title: "Have you considered renting out part of your home?",
            tf: rentingPart),
        SizedBox(height: 10),
        drawTextArea(title: "Have you considered downsizing?", tf: downsizing),
        SizedBox(height: 10),
        drawTextArea(
            title:
                "Are you aware this will reduce the value of your estate on death and the amount paid to your beneficiaries? Have you made this aware to the beneficiaries of your estate?",
            tf: beneficiariesState),
        SizedBox(height: 10),
        drawTextArea(
            title:
                "How much capital / what percentage(%) of the value would you like to release from your property?",
            tf: howmuchCapital),
        SizedBox(height: 10),
        drawTextArea(
            title:
                "Would you wish to release the maximum value from your property immediately?",
            tf: maxPropertyValue),
        SizedBox(height: 10),
        drawTextArea(
            title:
                "Do you wish to release smaller proportions are ad hoc intervals?",
            tf: adhocInterval),
      ],
    );
  }

  _drawRadioQ() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        UIHelper().drawRadioTitle(
            context: context,
            title: "Any extension or loft conversion done?",
            list: W2NLocalData.listYesNoRB,
            index: extLoftConvRBIndex,
            callback: (index) {
              setState(() => extLoftConvRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title:
                "Is the property of a non-standard construction (ie. thatched roof, barn conversion etc)",
            list: W2NLocalData.listYesNoRB,
            index: nonStandardConstrRBIndex,
            callback: (index) {
              setState(() => nonStandardConstrRBIndex = index);
            }),
        SizedBox(height: 10),
        nonStandardConstrRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: drawInputBox(
                  context: context,
                  title: "Non-standard Construction Details",
                  ph: "Construction Details",
                  input: nonStandardConstrDetails,
                  len: 255,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  focusNode: focusNonStandardConstrDetails,
                  focusNodeNext: null,
                ))
            : SizedBox(),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is it Ex Council? Or purchased from council directly",
            list: W2NLocalData.listYesNoRB,
            index: exCouncilRBIndex,
            callback: (index) {
              setState(() => exCouncilRBIndex = index);
            }),
        propertyTypeIndex == 1
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: UIHelper().drawRadioTitle(
                    context: context,
                    title: "Does the flat have deck or balcony access?",
                    list: W2NLocalData.listYesNoRB,
                    index: xAccessRBIndex,
                    callback: (index) {
                      setState(() => xAccessRBIndex = index);
                    }))
            : SizedBox(),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is this a new built property?",
            list: W2NLocalData.listYesNoRB,
            index: newBuiltPropRBIndex,
            callback: (index) {
              setState(() => newBuiltPropRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Dose this property have warranty?",
            list: W2NLocalData.listYesNoRB,
            index: doesWarantyRBIndex,
            callback: (index) {
              setState(() => doesWarantyRBIndex = index);
            }),
        doesWarantyRBIndex == 0
            ? Obx(() => Padding(
                padding: const EdgeInsets.only(top: 10),
                child: DropDownListDialog(
                  context: context,
                  title: optDoesWarranty.value.title,
                  h1: "Select warranty",
                  heading: "If so what warranty",
                  ddTitleList: W2NLocalData.ddDoesWarranty,
                  vPadding: 3,
                  callback: (optionItem) {
                    optDoesWarranty.value = optionItem;
                  },
                )))
            : SizedBox(),
        SizedBox(height: 10),
        drawTextArea(title: "Notes", tf: notes),
      ],
    );
  }

  _drawPropertyTenureView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Property Tenure",
            list: listPropertyTenureRB,
            index: propertyTenureIndex,
            radioType: eRadioType.HORIZONTAL,
            callback: (index) {
              setState(() => propertyTenureIndex = index);
            }),
        SizedBox(height: 5),
        propertyTenureIndex == 1
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: drawInputBox(
                  context: context,
                  title: "Year left on lease",
                  ph: "Year left on lease",
                  input: yearLeftLease,
                  len: 4,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  focusNode: focusYearLeftLease,
                  focusNodeNext: null,
                ))
            : SizedBox()
      ],
    );
  }

  _drawPropertyTypeView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Security Property Details",
            subTitle: "What type of property is this?",
            list: listPropertyTypeRB,
            index: propertyTypeIndex,
            callback: (index) {
              setState(() => propertyTypeIndex = index);
            }),
        SizedBox(height: 5),
      ],
    );
  }

  _drawPrefRepayTypeView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Preferred repayment type",
            list: listPrefRepayTypeRB,
            index: prefRepayTypeRBIndex,
            callback: (index) {
              setState(() => prefRepayTypeRBIndex = index);
            }),
      ],
    );
  }

  _drawPrefMortgageTermsView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        Txt(
            txt: "Preferred mortgage term",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: true),
        SizedBox(height: 5),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
                child: drawInputCurrencyBox(
                    sign: "Year",
                    context: context,
                    tf: prefMortTermsYY,
                    hintTxt: null,
                    len: 4,
                    focusNode: focusPrefMortTermsYY,
                    focusNodeNext: focusPrefMortTermsMM)),
            SizedBox(width: 10),
            Flexible(
                child: drawInputCurrencyBox(
                    sign: "Month",
                    context: context,
                    tf: prefMortTermsMM,
                    hintTxt: null,
                    len: 2,
                    focusNode: focusPrefMortTermsMM,
                    focusNodeNext: null)),
          ],
        )
      ],
    );
  }

  _drawIncentivesView() {
    return Container(
      child: Column(
        children: [
          SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              title:
                  "Are you receiving any incentives for buying this property such as discounts, cash back, free services or goods?",
              list: W2NLocalData.listYesNoRB,
              index: incentivesRBIndex,
              callback: (index) {
                setState(() => incentivesRBIndex = index);
              }),
          SizedBox(height: 10),
        ],
      ),
    );
  }
}
