import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/model/json/web2native/case/requirements/8existing_policy/PostExistingPolicyAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/8existing_policy/existing_policy_base.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/9saving_investment/saving_inv_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'policy_details_page.dart';

class ExistingPolicyPage extends StatefulWidget {
  const ExistingPolicyPage({Key key}) : super(key: key);
  @override
  State<ExistingPolicyPage> createState() => _ExistingPolicyPageState();
}

class _ExistingPolicyPageState extends ExistingPolicyBase<ExistingPolicyPage> {
  int anyExistingPolicyRBIndex = 0;

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(title: "Existing Policies"),
              ),
              bottomNavigationBar: BottomAppBar(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: MMBtn(
                            txt: "Back",
                            height: getHP(context, 7),
                            width: getW(context),
                            bgColor: Colors.grey,
                            radius: 10,
                            callback: () async {
                              Get.back();
                            }),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        child: MMBtn(
                            txt: "Continue",
                            height: getHP(context, 7),
                            width: getW(context),
                            radius: 10,
                            callback: () async {
                              Get.off(() => SavingInvPage());
                            }),
                      ),
                    ],
                  ),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  drawProgressView(74),
                  SizedBox(height: 20),
                  UIHelper().drawRadioTitle(
                      context: context,
                      title: "Any existing policy",
                      list: W2NLocalData.listYesNoRB,
                      index: anyExistingPolicyRBIndex,
                      callback: (index) {
                        setState(() => anyExistingPolicyRBIndex = index);
                      }),
                ],
              ),
            ),
            anyExistingPolicyRBIndex == 0
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                          padding: const EdgeInsets.only(left: 20, right: 20),
                          child: drawDottedBox("Add Existing Policies", () {
                            Get.to(() => PolicyDetailsPage());
                          })),
                      existingPolicyCtrl.existingPolicyCtrl.length > 0
                          ? drawExistingPolicyList(context, true,
                              existingPolicyCtrl.existingPolicyCtrl,
                              (model, isEdit) async {
                              if (isEdit) {
                                Get.to(() =>
                                    PolicyDetailsPage(policyModel: model));
                              } else {
                                try {
                                  await APIViewModel()
                                      .req<PostExistingPolicyAPIModel>(
                                          context: context,
                                          url: SrvW2NCase
                                                  .DEL_EXISTING_POLICY_URL +
                                              model.id.toString(),
                                          reqType: ReqType.Delete,
                                          callback: (model2) async {
                                            if (mounted && model2 != null) {
                                              if (model2.success) {
                                                existingPolicyCtrl
                                                    .existingPolicyCtrl
                                                    .remove(model);
                                                setState(() {});
                                              }
                                            }
                                          });
                                } catch (e) {}
                              }
                            })
                          : SizedBox(),
                    ],
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }
}
