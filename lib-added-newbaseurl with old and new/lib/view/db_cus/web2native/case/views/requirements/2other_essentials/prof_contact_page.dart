import 'dart:async';

import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/GetContactAutoSugAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserOtherContactInfos.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/PostContactAutoSugAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/validate/professional_contact_validation.dart';
import 'package:aitl/view/db_cus/web2native/case_base.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:stream_transform/stream_transform.dart';

import '../../../../../../../config/Server.dart';
import '../../../../../../../config/SrvW2NCase.dart';
import '../../../../../../../controller/network/NetworkMgr.dart';
import '../../../../../../../view_model/api/api_view_model.dart';
import '../../../../../../../view_model/helper/ui_helper.dart';
import '../../../../../../widgets/btn/MMBtn.dart';
import '../../../../../../widgets/dropdown/DropDownListDialog.dart';
import '../../../../../../widgets/dropdown/DropListModel.dart';
import '../../../../../../widgets/gplaces/GPlacesView.dart';
import '../../../../../../widgets/images/MyNetworkImage.dart';
import '../../../../../../widgets/input/InputW2N.dart';
import '../../../../../../widgets/progress/AppbarBotProgbar.dart';

class ProfContactPage extends StatefulWidget {
  final MortgageUserOtherContactInfos contactModel;
  const ProfContactPage({Key key, this.contactModel}) : super(key: key);
  @override
  State<ProfContactPage> createState() => _ProfContactPageState();
}

class _ProfContactPageState extends CaseBase<ProfContactPage> {
  MortgageUserOtherContactInfos modelContact;

  DropListModel dd = DropListModel([
    OptionItem(id: "Solicitors", title: "Solicitors"),
    OptionItem(id: "Estate Agents", title: "Estate Agents"),
    OptionItem(id: "Landlord's", title: "Landlords"),
    OptionItem(id: "Managing Agent's", title: "Managing Agents"),
    OptionItem(id: "Accountant Details", title: "Accountant Details"),
    OptionItem(id: "Surveyors", title: "Surveyors"),
    OptionItem(id: "Others", title: "Others"),
  ]);
  OptionItem opt = OptionItem(id: "Solicitors", title: "Solicitors");

  final listAutoSug = List<MortgageUserOtherContactInfos>().obs;

  final searchController = TextEditingController();
  final companyName = TextEditingController();
  final email = TextEditingController();
  final mobile = TextEditingController();
  final postcode = TextEditingController();
  final ScrollController scrollController = ScrollController();
  final ExpandableController expandableController = ExpandableController();
  final StreamController<String> streamController = StreamController();

  var addr = "";

  bool isLoading = false;

//function I am using to perform some logic
  onAutoSug(bool isSkipSearch) async {
    /*for (final m in listAutoSug) {
      final txt = m.contactName;
      if (searchController.text.startsWith(txt)) return;
    }*/
    if ((searchController.text.length > 2 || isSkipSearch) && !isLoading) {
      if (mounted) {
        //resetInput();
        setState(() {
          isLoading = true;
        });
        try {
          final url = SrvW2NCase.GET_CONTACT_AUTO_SUG_URL +
              "UserCompanyInfoId=" +
              userData.userModel.userCompanyInfo.id.toString() +
              "&AgentType=" +
              opt.id +
              "&ContactName=" +
              searchController.text.trim();
          await APIViewModel().req<GetContactAutoSugAPIModel>(
              context: context,
              url: url,
              reqType: ReqType.Get,
              isLoading: false,
              callback: (model) async {
                if (mounted && model != null) {
                  if (model.success) {
                    listAutoSug.clear();
                    listAutoSug.value =
                        model.responseData.mortgageUserOtherContactInfos;
                    if (listAutoSug.length > 0) {
                      expandableController.expanded = true;
                    }
                    FocusScope.of(context).requestFocus(new FocusNode());
                    setState(() {
                      isLoading = false;
                    });
                  } else {
                    expandableController.expanded = false;
                    listAutoSug.clear();
                    FocusScope.of(context).requestFocus(new FocusNode());
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              });
          if (mounted) {
            setState(() {
              isLoading = false;
            });
          }
        } catch (e) {
          if (mounted) {
            setState(() {
              isLoading = false;
            });
          }
        }
      }
    }
  }

  resetInput() {
    modelContact = null;
    companyName.clear();
    email.clear();
    mobile.clear();
    addr = "";
    postcode.clear();
  }

  /*onValidate() {

    if (searchController.text.trim().length == 0 || modelContact == null) {
      showToast(
          context: context,
          msg:
              "Please enter valid Contact Name Agent type already exist.\n\nPlease select from contact drop-down.");
      return false;
    } else if (companyName.text.trim().length == 0) {
      showToast(context: context, msg: "Please enter valid Company Name");
      return false;
    }
    return true;
  }*/

  initPage() async {
    try {
      if (widget.contactModel != null) {
        modelContact = widget.contactModel;
        searchController.text = modelContact.contactName ?? '';
        companyName.text = modelContact.companyName ?? '';
        email.text = modelContact.email ?? '';
        mobile.text = modelContact.phone ?? '';
        postcode.text = modelContact.postcode ?? '';
        addr = modelContact.address ?? '';
      }
    } catch (e) {}
    try {
      streamController.stream
          .debounce(Duration(seconds: 1))
          .listen((s) => onAutoSug(false));
      onAutoSug(true);
    } catch (e) {}
    //setState(() {});
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    scrollController.dispose();
    expandableController.dispose();
    streamController.close();
    searchController.dispose();
    companyName.dispose();
    email.dispose();
    mobile.dispose();
    postcode.dispose();
    dd = null;
    opt = null;
    NetworkMgr().dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //print(widget.mortgageUserRequirementModel);
    //updateFields();
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper()
                    .drawAppbarTitle(title: "Professional Contact Information"),
                bottom: PreferredSize(
                  preferredSize: Size.fromHeight((isLoading) ? .5 : 0),
                  child: (isLoading)
                      ? AppbarBotProgBar(
                          backgroundColor: MyTheme.appbarProgColor,
                        )
                      : SizedBox(),
                ),
              ),
              bottomNavigationBar: BottomAppBar(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: MMBtn(
                            txt: "Back",
                            height: getHP(context, 7),
                            width: getW(context),
                            bgColor: Colors.grey,
                            radius: 10,
                            callback: () async {
                              Get.back();
                            }),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        child: MMBtn(
                            txt: "Save",
                            height: getHP(context, 7),
                            width: getW(context),
                            radius: 10,
                            callback: () async {
                              if (ProfessionalContactValidation.onValidate(
                                      modelContact: modelContact,
                                      search: searchController.text,
                                      companyName: companyName.text) ==
                                  "") {
                                await APIViewModel().req<
                                        PostContactAutoSugAPIModel>(
                                    context: context,
                                    url: widget.contactModel == null
                                        ? SrvW2NCase.POST_CONTACT_AUTO_SUG_URL
                                        : SrvW2NCase.PUT_CONTACT_AUTO_SUG_URL,
                                    param: {
                                      "Id": widget.contactModel == null
                                          ? 0
                                          : widget.contactModel.id,
                                      "UserId": userData.userModel.id,
                                      "CompanyId":
                                          userData.userModel.userCompanyInfo.id,
                                      "CreationDate": DateTime.now().toString(),
                                      "TaskId": caseModelCtrl.locModel.value.id,
                                      "ContactId": modelContact != null
                                          ? modelContact.id
                                          : 0,
                                      "ContactName":
                                          searchController.text.trim(),
                                      "CompanyName": companyName.text.trim(),
                                      "Address": addr,
                                      "Postcode": postcode.text.trim(),
                                      "Phone": mobile.text.trim(),
                                      "Email": email.text.trim(),
                                      "AgentType": opt.id,
                                      "Remarks": ""
                                    },
                                    reqType: widget.contactModel == null
                                        ? ReqType.Post
                                        : ReqType.Put,
                                    callback: (model) async {
                                      if (model.success) {
                                        Get.back(
                                            result: model.responseData
                                                .mortgageUserOtherContactInfo);
                                      }
                                    });
                              }
                            }),
                      ),
                    ],
                  ),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              SizedBox(height: 10),
              DropDownListDialog(
                context: context,
                title: opt.title,
                h1: "Select category",
                heading: "Select category",
                ddTitleList: dd,
                vPadding: 4,
                txtSize: (MyTheme.txtSize - .4),
                callback: (optionItem) {
                  opt = optionItem;
                  searchController.clear();
                  onAutoSug(true);
                },
              ),
              SizedBox(height: 10),
              _drawContactNameAutoSugView(),
              SizedBox(height: 10),
              InputW2N(
                title: "Company Name",
                input: companyName,
                ph: "",
                kbType: TextInputType.text,
                len: 100,
              ),
              InputW2N(
                title: "Email",
                input: email,
                ph: "",
                kbType: TextInputType.emailAddress,
                len: 50,
              ),
              InputW2N(
                title: "Phone",
                input: mobile,
                ph: "",
                kbType: TextInputType.phone,
                len: 20,
              ),
              GPlacesView(
                  title: "Address",
                  titleColor: Colors.black,
                  isBold: false,
                  txtSize: MyTheme.txtSize - .2,
                  address: addr,
                  callback: (String address, String postCode, Location loc) {
                    addr = address;
                    postcode.text = postCode;
                    setState(() {});
                  }),
              SizedBox(height: 10),
              InputW2N(
                title: "Postcode",
                input: postcode,
                ph: "",
                kbType: TextInputType.streetAddress,
                len: 10,
              ),
              SizedBox(height: 10),
            ],
          ),
        ),
      ),
    );
  }

  _drawContactNameAutoSugView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Txt(
            txt: "Contact Name",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: false),
        SizedBox(height: 5),
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border(
                left: BorderSide(color: Colors.grey, width: 1),
                right: BorderSide(color: Colors.grey, width: 1),
                top: BorderSide(color: Colors.grey, width: 1),
                bottom: BorderSide(color: Colors.grey, width: 1)),
            color: Colors.transparent,
          ),
          child: ExpandablePanel(
            theme: ExpandableThemeData(
                expandIcon: Icons.arrow_drop_down_outlined,
                collapseIcon: Icons.arrow_drop_up_outlined,
                iconSize: 35),
            controller: expandableController,
            collapsed: null,
            header: Container(
              color: Colors.transparent,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                      flex: 1,
                      child: Icon(
                        Icons.search,
                        color: Colors.grey,
                        size: 20,
                      )),
                  Expanded(
                    flex: 6,
                    child: TextField(
                      controller: searchController,
                      //focusNode: focusSearch,
                      textInputAction: TextInputAction.next,
                      onChanged: (t) async {
                        streamController.add(t);
                      },
                      decoration: new InputDecoration(
                        isDense: false,
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        hintText: "Search for existing contact name",
                        hintStyle: TextStyle(
                            color: Colors.grey,
                            fontWeight: FontWeight.normal,
                            fontSize: 15),
                        //fillColor: Colors.black,
                      ),
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                      ),
                      keyboardType: TextInputType.text,
                    ),
                  ),
                  Expanded(
                      flex: 1,
                      child: GestureDetector(
                        onTap: () {
                          searchController.clear();
                          FocusScope.of(context).requestFocus(new FocusNode());
                          onAutoSug(true);
                          //setState(() {});
                        },
                        child: searchController.text.length > 0
                            ? Icon(
                                Icons.close,
                                color: Colors.grey,
                                size: 20,
                              )
                            : SizedBox(),
                      )),
                ],
              ),
            ),
            expanded: Container(
              height: getHP(context, listAutoSug.length > 0 ? 25 : 0),
              //width: getW(context),
              child: Scrollbar(
                isAlwaysShown: listAutoSug.length > 0 ? true : false,
                controller: scrollController,
                child: ListView(
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    controller: scrollController,
                    children: [
                      ...listAutoSug.map((model) {
                        return GestureDetector(
                          onTap: () {
                            modelContact = model;
                            expandableController.expanded = false;
                            searchController.text = model.contactName;
                            companyName.text = model.companyName;
                            email.text = model.email;
                            mobile.text = model.phone;
                            addr = model.address;
                            postcode.text = model.postcode;
                            setState(() {});
                          },
                          child: Card(
                            color: MyTheme.bgColor2.withOpacity(.9),
                            elevation: 1,
                            child: ListTile(
                              minLeadingWidth: 0,
                              leading: CircleAvatar(
                                //radius: 17,
                                backgroundColor: Colors.transparent,
                                backgroundImage: new CachedNetworkImageProvider(
                                    MyNetworkImage.checkUrl(
                                        Server.MISSING_IMG)),
                              ),
                              title: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    model.contactName,
                                    style: TextStyle(
                                        color: Colors.black, fontSize: 14),
                                  ),
                                  SizedBox(height: 5),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Flexible(
                                          child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Icon(
                                            Icons.email,
                                            color: Colors.grey,
                                            size: 15,
                                          ),
                                          Expanded(
                                            child: Txt(
                                                txt: model.email,
                                                txtColor: MyTheme.inputColor,
                                                txtSize: MyTheme.txtSize - .6,
                                                txtAlign: TextAlign.start,
                                                isBold: false),
                                          ),
                                        ],
                                      )),
                                      Flexible(
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            Icon(
                                              Icons.phone,
                                              color: Colors.grey,
                                              size: 15,
                                            ),
                                            Expanded(
                                              child: Txt(
                                                  txt: model.phone,
                                                  txtColor: MyTheme.inputColor,
                                                  txtSize: MyTheme.txtSize - .6,
                                                  txtAlign: TextAlign.start,
                                                  isBold: false),
                                            )
                                          ],
                                        ),
                                      )
                                    ],
                                  ),

                                  /*SizedBox(height: 5),
                                              Container(
                                                color: Colors.white,
                                                height: 2,
                                              )*/
                                ],
                              ),
                            ),
                          ),
                        );
                      }).toList(),
                    ]),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
