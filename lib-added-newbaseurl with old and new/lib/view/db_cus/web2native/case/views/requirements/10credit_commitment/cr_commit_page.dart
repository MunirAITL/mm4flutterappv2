import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/web2native/case/requirements/10cr_commit/PostCreditCommitAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/dialogs/requirements/10credit_commitment/cr_com_dialog.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/10credit_commitment/cr_commit_base.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/11credit_history/cr_his_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CrCommitPage extends StatefulWidget {
  const CrCommitPage({Key key}) : super(key: key);
  @override
  State<CrCommitPage> createState() => _CrCommitPageState();
}

class _CrCommitPageState extends CrCommitBase<CrCommitPage> {
  bool isLoaded = false;

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        // executes after build
        isLoaded = true;
        setState(() {});
      });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(title: "Credit Commitment"),
              ),
              bottomNavigationBar: BottomAppBar(
                color: MyTheme.bgColor2,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: MMBtn(
                            txt: "Back",
                            height: getHP(context, 7),
                            width: getW(context),
                            bgColor: Colors.grey,
                            radius: 10,
                            callback: () async {
                              Get.back();
                            }),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        child: MMBtn(
                            txt: "Continue",
                            height: getHP(context, 7),
                            width: getW(context),
                            radius: 10,
                            callback: () async {
                              Get.off(() => CreditHistoryPage());
                            }),
                      ),
                    ],
                  ),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    return isLoaded
        ? Obx(() => Container(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                              padding:
                                  const EdgeInsets.only(left: 10, right: 10),
                              child: drawProgressView(80)),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 20, left: 10, right: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Txt(
                                    txt: "Tell us about your Credit Commitment",
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    fontWeight: FontWeight.w500,
                                    isBold: false),
                                SizedBox(height: 10),
                                drawDottedBox("Add Credit Commitments", () {
                                  Get.dialog(reqStep10CrComDialog(
                                      context: context,
                                      taskId: caseModelCtrl.locModel.value.id,
                                      model2: null,
                                      callbackFailed: (err) {
                                        showToast(context: context, msg: err);
                                      })).then((value) {
                                    if (value != null) {
                                      showToast(
                                          context: context,
                                          msg: value,
                                          which: 1);
                                    }
                                  });
                                }),
                              ],
                            ),
                          ),
                          drawCrCommitList(context, true, crComCtrl,
                              (model, isEdit) async {
                            if (isEdit) {
                              Get.dialog(reqStep10CrComDialog(
                                  context: context,
                                  taskId: caseModelCtrl.locModel.value.id,
                                  model2: model,
                                  callbackFailed: (err) {
                                    showToast(context: context, msg: err);
                                  })).then((value) {
                                if (value != null) {
                                  showToast(
                                      context: context, msg: value, which: 1);
                                }
                              });
                            } else {
                              //  delete
                              try {
                                await APIViewModel().req<
                                        PostCreditCommitAPIModel>(
                                    context: context,
                                    url: SrvW2NCase.DEL_CREDIT_COMMITMENT_URL +
                                        model.id.toString(),
                                    reqType: ReqType.Delete,
                                    callback: (model2) async {
                                      if (mounted && model2 != null) {
                                        if (model2.success) {
                                          crComCtrl.crCommitCtrl.remove(model);
                                          setState(() {});
                                        }
                                      }
                                    });
                              } catch (e) {}
                            }
                          }),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ))
        : SizedBox();
  }
}
