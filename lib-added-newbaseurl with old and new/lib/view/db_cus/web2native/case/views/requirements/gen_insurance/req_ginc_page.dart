import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/3profile/profile_details_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/gen_insurance/req_ginc_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../../config/SrvW2NCase.dart';
import '../../../../../../../controller/network/NetworkMgr.dart';
import '../../../../../../../model/json/web2native/case/requirements/gen_insurance/GenInsAPIModel.dart';
import '../../../../../../../model/json/web2native/case/requirements/gen_insurance/GetPersonalProcessionAPIModel.dart';
import '../../../../../../../model/json/web2native/case/requirements/gen_insurance/MortgageBuildingAndContentInsuranceItem.dart';
import '../../../../../../../view_model/api/api_view_model.dart';
import '../../../../../../../view_model/generic/enum_gen.dart';
import '../../../../../../../view_model/helper/ui_helper.dart';
import '../../../dialogs/requirements/req_ginc_dialog.dart';

class ReqGIncPage extends StatefulWidget {
  @override
  State<ReqGIncPage> createState() => _ReqGIncState();
}

class _ReqGIncState extends ReqGIncBase<ReqGIncPage> {
  //  phone bike dialog
  onBikePhoneDialog(MortgageBuildingAndContentInsuranceItem model2) {
    Get.dialog(reqBikePhoneDialog(
        context: context,
        taskId: caseModelCtrl.locModel.value.id,
        model2: model2,
        callbackSuccess: (model) async {
          try {
            if (mounted) {
              if (model2 != null)
                requirementsCtrl.listInsuranceItems.remove(model2);
              requirementsCtrl.listInsuranceItems.add(model);
              requirementsCtrl.update();
              StateProvider()
                  .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
              setState(() {});
            }
          } catch (e) {}
        },
        callbackFailed: (err) {
          showToast(context: context, msg: err);
        }));
  }

  //  Additional Items dialog
  onAdditionalItemDialog(MortgageBuildingAndContentInsuranceItem model2) {
    Get.dialog(reqAdditionalItemDialog(
        context: context,
        taskId: caseModelCtrl.locModel.value.id,
        model2: model2,
        callbackSuccess: (model) async {
          try {
            if (mounted) {
              if (model2 != null)
                requirementsCtrl.listInsuranceItems.remove(model2);
              requirementsCtrl.listInsuranceItems.add(model);
              requirementsCtrl.update();
              StateProvider()
                  .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
              setState(() {});
            }
          } catch (e) {}
        },
        callbackFailed: (err) {
          showToast(context: context, msg: err);
        }));
  }

  onDelItemDialog(MortgageBuildingAndContentInsuranceItem modelOld) async {
    try {
      await APIViewModel().req<GetPersonalProcessionAPIModel>(
          context: context,
          url: SrvW2NCase.DEL_PERSONAL_PROCESSION_URL + modelOld.id.toString(),
          reqType: ReqType.Delete,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                requirementsCtrl.listInsuranceItems.remove(modelOld);
                requirementsCtrl.update();
                StateProvider()
                    .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
                setState(() {});
                showToast(
                    context: context,
                    msg: "Item deleted successfully.",
                    which: 1);
              }
            }
          });
    } catch (e) {}
  }

  //
  onRequirementAPI() async {
    var id;
    try {
      if (requirementsCtrl.mortgageBuildingAndContentInsurance != null)
        id = requirementsCtrl.mortgageBuildingAndContentInsurance.value.id;
    } catch (e) {}

    var _buildingCoverAmount = 0;
    var _coverAmount = 0;
    var _contentAccess = 0;

    try {
      _buildingCoverAmount = int.parse(buildingCoverAmount.text.trim());
    } catch (e) {}
    try {
      _coverAmount = int.parse(coverAmount.text.trim());
    } catch (e) {}
    try {
      _contentAccess = int.parse(contentAccess.text.trim());
    } catch (e) {}

    final noOfBedRooms = optNoBedrooms.id != null ? optNoBedrooms.title : '0';
    final noOfClaimDiscount =
        optNoClaimDis.id != null ? optNoClaimDis.title : '0';
    try {
      var param = {
        "Id": id != null ? id : 0,
        "UserId": userData.userModel.id,
        "CompanyId": userData.userModel.userCompanyID,
        "CreationDate":
            DateFun.getDate(DateTime.now().toString(), "dd-MMM-yyyy"),
        "UpdatedDate":
            DateFun.getDate(DateTime.now().toString(), "dd-MMM-yyyy"),
        "Status": 0,
        "Remarks": "",
        "TaskId": caseModelCtrl.locModel.value.id,
        "VersionNumber": 0,
        "MortgageType": "",
        "Notes": "",
        "Type": caseModelCtrl.locModel.value.description,
        "RiskPropertyAddress": addrOfProperty,
        "TypeOfProperty": optPropType.id != null ? optPropType.title : '',
        "YearOfConstruction":
            optYearConstr.id != null ? optYearConstr.title : '',
        "DoWantToAddAccidentalDamageCover":
            W2NLocalData.listYesNoRB[accDamageCoverRBIndex],
        "ContentsCoverAmount": _coverAmount,
        "ContentsExcess": getSubCase() == eGIncSubList.Buildings_xANDx_Contents
            ? _contentAccess
            : contentAccess.text.trim(),
        "AccidentalDamage": W2NLocalData.listYesNoRB[accDamageRBIndex],
        "FamilyLegalExpenses": W2NLocalData.listYesNoRB[familyExpRBIndex],
        "NumberOfBedrooms":
            getSubCase() == eGIncSubList.Buildings_xANDx_Contents
                ? noOfBedRooms
                : int.parse(noOfBedRooms),
        "NoOfClaimsDiscount":
            getSubCase() == eGIncSubList.Buildings_xANDx_Contents
                ? noOfClaimDiscount
                : int.parse(noOfClaimDiscount),
        "HomeEmergencyCover": W2NLocalData.listYesNoRB[homeInsRBIndex],
        "BuildingsCoverAmount": _buildingCoverAmount,
        "MortgageBuildingAndContentInsuranceItemList": [],
        "UserCompanyId": userData.userModel.userCompanyID
      };

      await APIViewModel().req<GenInsAPIModel>(
          context: context,
          url: id == null
              ? SrvW2NCase.POST_REQUIREMENTS_GEN_INC_URL
              : SrvW2NCase.PUT_REQUIREMENTS_GEN_INC_URL,
          reqType: id == null ? ReqType.Post : ReqType.Put,
          param: param,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                requirementsCtrl.mortgageBuildingAndContentInsurance.value =
                    model.responseData.mortgageBuildingAndContentInsurance;
                requirementsCtrl.update();
                StateProvider()
                    .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
                Get.off(() => ProfileDetailsPage());
              }
            }
          });
    } catch (e) {}
  }

  //  API end...
  updateFields() {
    final model = requirementsCtrl.mortgageBuildingAndContentInsurance.value;
    try {
      final List<OptionItem> list = [];
      for (int i = 1; i < 21; i++)
        list.add(OptionItem(id: i.toString(), title: i.toString()));
      ddNoBedrooms = DropListModel(list);
      final List<OptionItem> list2 = [];
      for (int i = 1; i < 26; i++)
        list2.add(OptionItem(id: i.toString(), title: i.toString()));
      ddNoClaimDis = DropListModel(list2);
    } catch (e) {}
    try {
      addrOfProperty = model.riskPropertyAddress ?? '';
    } catch (e) {}
    try {
      if (model.typeOfProperty != null)
        optPropType = OptionItem(id: "0", title: model.typeOfProperty ?? '');
    } catch (e) {}
    try {
      if (model.yearOfConstruction != null)
        optYearConstr =
            OptionItem(id: "0", title: model.yearOfConstruction ?? '');
    } catch (e) {}
    try {
      if (model.numberOfBedrooms != null)
        optNoBedrooms =
            OptionItem(id: "0", title: model.numberOfBedrooms.toString() ?? '');
    } catch (e) {}
    try {
      accDamageCoverRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.doWantToAddAccidentalDamageCover);
    } catch (e) {}
    try {
      if (model.buildingsCoverAmount != null)
        buildingCoverAmount.text =
            model.buildingsCoverAmount.toStringAsFixed(0);
    } catch (e) {}
    try {
      if (model.noOfClaimsDiscount != null)
        optNoClaimDis =
            OptionItem(id: "0", title: model.noOfClaimsDiscount.toString());
    } catch (e) {}
    try {
      if (model.contentsCoverAmount != null)
        coverAmount.text = model.contentsCoverAmount.toStringAsFixed(0);
    } catch (e) {}
    try {
      if (model.contentsExcess != null)
        contentAccess.text = model.contentsExcess.toString();
    } catch (e) {}
    try {
      accDamageRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.accidentalDamage);
    } catch (e) {}
    try {
      homeInsRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.homeEmergencyCover);
    } catch (e) {}
    try {
      familyExpRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.familyLegalExpenses);
    } catch (e) {}
  }

  getSubCase() {
    for (var value in eGIncSubList.values) {
      try {
        final str = caseModelCtrl.locModel.value.description
            .toLowerCase()
            .replaceAll("xANDx", "&");
        if (EnumGen.getEnum2Str(value).toLowerCase() == str) {
          return value;
        }
      } catch (e) {}
    }
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {
      updateFields();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    //print(widget.mortgageUserRequirementModel);
    //updateFields();
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(
                    title: caseModelCtrl.locModel.value.title +
                        " | " +
                        caseModelCtrl.locModel.value.id.toString()),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    drawProgressView(5),
                    drawForm(),
                    SizedBox(height: 30),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: MMBtn(
                              txt: "Back",
                              height: getHP(context, 7),
                              width: getW(context),
                              bgColor: Colors.grey,
                              radius: 10,
                              callback: () async {
                                Get.back();
                              }),
                        ),
                        SizedBox(width: 10),
                        Flexible(
                          child: MMBtn(
                              txt: "Save & continue",
                              height: getHP(context, 7),
                              width: getW(context),
                              radius: 10,
                              callback: () async {
                                if (onValidate()) {
                                  onRequirementAPI();
                                }
                              }),
                        ),
                      ],
                    ),
                    SizedBox(height: 40),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
