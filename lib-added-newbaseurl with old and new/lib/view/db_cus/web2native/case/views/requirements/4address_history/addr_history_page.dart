import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/web2native/case/requirements/4address_history/PostAddHistoryAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/4address_history/UserAddresss.dart';
import 'package:aitl/view/db_cus/web2native/case/dialogs/requirements/4address_history/addr_his_dialog.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/6cur_residential_mort/cur_res_mort_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'addr_history_base.dart';

class AddrHistoryPage extends StatefulWidget {
  @override
  State createState() => _AddrHistoryState();
}

class _AddrHistoryState extends AddrHistoryBase<AddrHistoryPage> {
  onAddressDialog(UserAddresss model2) {
    Get.dialog(reqStep4AddrHistoryDialog(
        context: context,
        taskId: caseModelCtrl.locModel.value.id,
        model2: model2,
        callbackFailed: (err) {
          showToast(context: context, msg: err);
        },
        callbackPost: (modelPost) async {
          try {
            addrHistoryCtrl.listUserAddress.add(modelPost);
            addrHistoryCtrl.update();
            setState(() {});
          } catch (e) {}
        },
        callbackPut: (modelEdit, modelOld) async {
          try {
            addrHistoryCtrl.listUserAddress.remove(modelOld);
            addrHistoryCtrl.listUserAddress.add(modelEdit);
            addrHistoryCtrl.update();
            setState(() {});
          } catch (e) {}
        }));
  }

  onDelAddressHistoryAPI(UserAddresss addrModel) async {
    try {
      await APIViewModel().req<PostAddrHistoryAPIModel>(
          context: context,
          url: SrvW2NCase.DEL_ADDR_HISTORY_URL + addrModel.id.toString(),
          reqType: ReqType.Delete,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                addrHistoryCtrl.listUserAddress.remove(addrModel);
                addrHistoryCtrl.update();
                setState(() {});
              }
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title:
                    UIHelper().drawAppbarTitle(title: "Your address history"),
              ),
              bottomNavigationBar: BottomAppBar(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: MMBtn(
                            txt: "Back",
                            height: getHP(context, 7),
                            width: getW(context),
                            bgColor: Colors.grey,
                            radius: 10,
                            callback: () async {
                              Get.back();
                            }),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        child: MMBtn(
                            txt: "Save & continue",
                            height: getHP(context, 7),
                            width: getW(context),
                            radius: 10,
                            callback: () async {
                              Get.off(() => CurResMortPage());
                            }),
                      ),
                    ],
                  ),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 5, right: 5),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    drawProgressView(50),
                    SizedBox(height: 20),
                    Txt(
                        txt:
                            "Please enter your most recent address first. If you have not lived in the current property for more than 5 years, please add all the addresses covering the last 5 years. You can add a new address by clicking on the 'Add' button.",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(height: 10),
                    drawDottedBox("Add address", () {
                      onAddressDialog(null);
                    }),
                  ],
                ),
              ),
              addrHistoryCtrl.listUserAddress != null
                  ? drawAddrHistoryView(
                      context, addrHistoryCtrl.listUserAddress, true,
                      (modelEdit) {
                      onAddressDialog(modelEdit);
                    }, (modelDel) {
                      onDelAddressHistoryAPI(modelDel);
                    })
                  : SizedBox(),
              SizedBox(height: 40),
            ],
          ),
        ),
      ),
    );
  }
}
