import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/2other_essentials/es_other_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/buy2letmortgage/req_b2letm_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../../config/SrvW2NCase.dart';
import '../../../../../../../controller/network/NetworkMgr.dart';
import '../../../../../../../model/json/web2native/case/requirements/mortgage/MortgageRequirementsAPIModel.dart';
import '../../../../../../../model/json/web2native/case/requirements/mortgage/mortgage_model.dart';
import '../../../../../../../view_model/api/api_view_model.dart';
import '../../../../../../../view_model/generic/enum_gen.dart';
import '../../../../../../../view_model/helper/ui_helper.dart';
import '../../../../../../widgets/dropdown/DropListModel.dart';
import '../../../dialogs/requirements/req_mortgage_dialog.dart';

class ReqB2LetMPage extends StatefulWidget {
  @override
  State<ReqB2LetMPage> createState() => _ReqB2LetMState();
}

class _ReqB2LetMState extends ReqB2LetMBase<ReqB2LetMPage> {
  final amount = TextEditingController();

  //  API start...
  onRequirementAPI() async {
    var id;
    try {
      if (requirementsCtrl.requirementsModelAPIModel != null)
        id = requirementsCtrl.requirementsModelAPIModel.value.id;
    } catch (e) {}

    var _loanAmount = 0;
    var _depositAmount = 0;
    var _noBedrooms = 0;
    var _incentiveAmount = 0;
    var _prefMortTermsYY = 0;
    var _prefMortTermsMM = 0;
    var _noKitchens = 0;
    var _noBathrooms = 0;
    var _noWC = 0;
    var _purchasePrice = 0;
    var _currentValProperty = 0;
    var _groundRent = 0;
    var _srvCharges = 0;
    var _rentalIncome = 0;

    try {
      _loanAmount = int.parse(loanAmount.text.trim());
    } catch (e) {}
    try {
      _depositAmount = int.parse(depositAmount.text.trim());
    } catch (e) {}
    try {
      _noBedrooms = int.parse(noBedrooms.text.trim());
    } catch (e) {}
    try {
      _incentiveAmount = int.parse(incentiveAmount.text.trim());
    } catch (e) {}
    try {
      _prefMortTermsYY = int.parse(prefMortTermsYY.text.trim());
    } catch (e) {}
    try {
      _prefMortTermsMM = int.parse(prefMortTermsMM.text.trim());
    } catch (e) {}
    try {
      _noKitchens = int.parse(noKitchens.text.trim());
    } catch (e) {}
    try {
      _noBathrooms = int.parse(noBathrooms.text.trim());
    } catch (e) {}
    try {
      _noWC = int.parse(noWC.text.trim());
    } catch (e) {}
    try {
      _purchasePrice = int.parse(purchasePrice.text.trim());
    } catch (e) {}
    try {
      _currentValProperty = int.parse(currentValProperty.text.trim());
    } catch (e) {}
    try {
      _groundRent = int.parse(groundRent.text.trim());
    } catch (e) {}
    try {
      _srvCharges = int.parse(srvCharges.text.trim());
    } catch (e) {}
    try {
      _rentalIncome = int.parse(rentalIncome.text.trim());
    } catch (e) {}

    try {
      var param = {
        "Id": id != null ? id : 0,
        "UserId": userData.userModel.id,
        "User": null,
        "CompanyId": userData.userModel.userCompanyID,
        "Status": 101,
        "MortgageCaseInfoId": 0,
        "CreationDate": DateTime.now().toString(),
        "MortgageType": "", //listMortgageTypeRB[mortgageTypeRBIndex],
        "AddressOfPropertyToBeMortgaged": addrOfProperty,
        "PriceOfPropertyBeingPurchasedCurrentValuationOfProperty":
            _currentValProperty,
        "HowMuchDoYouWishToBorrow": _loanAmount,
        "AmountOfDepositEquity": _depositAmount,
        "SourceOfDeposit": "",
        "Savings": 0,
        "OtherStateWhat": 0,
        "AreFundsAvailableToPayFeesInConnectionWithMortgage": "No",
        "DoesExistingLenderFacilitateFurtherAdvances": "No",
        "CurrentLenderAccountNumber": "",
        "PropertyType": listPropertyTypeRB[propertyTypeIndex],
        "PropertyTenure": listPropertyTenureRB[propertyTenureIndex],
        "IfLeaseholdHowLongIsLeftOnTheLease": yearLeftLease.text.trim(),
        "NumberOfBedrooms": _noBedrooms,
        "FloorsInTheBuilding": floorBuilding.text.trim(),
        "WhichFloorIsTheProperty": whichFloorProperty.text.trim(),
        "YearPropertyWasBuilt": yearBuilt.text.trim(),
        "AnyExtensionOrLoftConversionDone":
            W2NLocalData.listYesNoRB[extLoftConvRBIndex],
        "IsThePropertyOfANonStandardConstruction":
            W2NLocalData.listYesNoRB[nonStandardConstrRBIndex],
        "IsItExCouncil": W2NLocalData.listYesNoRB[exCouncilRBIndex],
        "IsThisANewBuiltProperty":
            W2NLocalData.listYesNoRB[newBuiltPropRBIndex],
        "HasTheVendorOwnedThePropertyOverSixMonths": "",
        "AreYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesOrGoods":
            W2NLocalData.listYesNoRB[incentivesRBIndex],
        "IsThisAnAuctionOrRepossessionPurchase": "No",
        "HasThisPropertyBeenFoundWithTheAssistanceOfAPropertyClub": "No",
        "Notes": notes.text.trim(),
        "Remarks": "",
        "FinanceType": "",
        "AuctionHouseLotDetails": "",
        "RepaymentType": "",
        "ExitStrategy": "",
        "Details": "",
        "GVD": "",
        "AreYouLooking": "",
        "BalanceOutstanding": 0,
        "LTVAmount": 0,
        "AmountofLoanRequired": 0,
        "TypeofLoan": "",
        "DoYouHaveAccountsForTheBusiness": "",
        "DoYouHaveManagementAccounts": "",
        "NonstandardConstructionDetails": nonStandardConstrDetails.text.trim(),
        "AreYouRaisingAnyMoneyfromAboveProperty": "No",
        "AreYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesValue":
            _incentiveAmount,
        "AreFundsAvailableToPayFeesInConnectionWithMortgageValue": 0,
        "PreferredMortgageTermYear": _prefMortTermsYY,
        "PreferredMortgageTermMonth": _prefMortTermsMM,
        "NumberOfKoichens": _noKitchens,
        "NumberOfBaahroom": _noBathrooms,
        "NumberOfWC": _noWC,
        "IsTheFlatAboveShopRestaurant":
            W2NLocalData.listYesNoRB[flatHasShopRBIndex],
        "DoesTheFlatHaveDeckORBalconyAccess":
            W2NLocalData.listYesNoRB[xAccessRBIndex],
        "AreYouLookingToRemortgageYourCurrentResidentialProperty": "No",
        "DidYouPurchaseThePropertyDirectFromTheCouncilHousingAssociation": "No",
        "AreThereAnyRightToBuyOrSimilarRestrictionImposed": "No",
        "RestrictionExpireDate": AppConfig.date1970,
        "AreYouAllowedToRemortgageThePropertyByTheCouncilHousingAssociation":
            "No",
        "DoYouHaveExistingMortgageOnTheProperty": "No",
        "CurrentLender": "",
        "DatePropertyPurchased": AppConfig.date1970,
        "CurrentMortgageToBeRedeemed": 0,
        "PurchasePrice": _purchasePrice,
        "AmountRaisingOverCurrentMortgage": 0,
        "DoYouWantToApplyForInterestOnlyLoan": "No",
        "MonthlyMortgagePayment": 0,
        "RentalIncomeAchievableAmount": _rentalIncome,
        "IsThePropertyAlreadyTenanted":
            W2NLocalData.listYesNoRB[propertyTenantedRBIndex],
        "TenancyType": listPropertyTenantedTypeRB[propertyTenantedTypeRBIndex],
        "AreYouCurrentlyResidingInThisProperty": "No",
        "AreYouRetainingThisPropertyAsABuyToLetPropertyToPurchaseANewResidentialProperty":
            "No",
        "AreYouAFirstTimeLandLord": "No",
        "HaveYouBeenAnExperiencedLandlordForMoreThan12Months": "No",
        "HaveYouEverLivedInThisProperty": "No",
        "AreYouLookingToTakeASecondChargeLoanOnYourInvestmentProperty": "Yes",
        "PropertyDescription": "",
        "IncomeFromTheProperty": "",
        "WhatIsYourLoanRepaymentStrategy": "",
        "EquityAmount": 0,
        "PreferredRepaymentType": listPrefRepayTypeRB[prefRepayTypeRBIndex],
        "ServiceChargeAmount": _srvCharges,
        "GroundRentAmount": _groundRent,
        "ClientExperience": "",
        "Plan": "",
        "CostofRefurb": "0",
        "PostCode": "",
        "PropertyEPCRating": null,
        "SharedHolderPercentage": 0,
        "DoesThePropertyHaveALift": W2NLocalData.listYesNoRB[haveLiftRBIndex],
        "DoesThisPropertyHaveWarranty":
            W2NLocalData.listYesNoRB[doesWarantyRBIndex],
        "WarrantyType": optDoesWarranty.value.title,
        "DatePropertyPurchasedDD": "",
        "DatePropertyPurchasedMM": "",
        "DatePropertyPurchasedYY": "",
        "RestrictionExpireDateDD": "",
        "RestrictionExpireDateMM": "",
        "RestrictionExpireDateYY": "",
        "TaskId": caseModelCtrl.locModel.value.id
      };

      for (final mod in listDepositedBy) {
        param[mod.key] = mod.amount.round();
      }

      await APIViewModel().req<MortgageRequirementsAPIModel>(
          context: context,
          url: id == null
              ? SrvW2NCase.POST_REQUIREMENTS_URL
              : SrvW2NCase.PUT_REQUIREMENTS_URL,
          reqType: id == null ? ReqType.Post : ReqType.Put,
          param: param,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                requirementsCtrl.requirementsModelAPIModel.value =
                    model.responseData.mortgageUserRequirement;
                requirementsCtrl.update();
                StateProvider()
                    .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
                Get.off(() => ESOtherPage());
              }
            }
          });
    } catch (e) {}
  }

  //  API end...

  onAddAmountDialog() {
    resetData();
    Get.dialog(reqMortgageDialog(
        context: context,
        amount: amount,
        title: "Add Amount",
        callbackSuccess: () {
          /*for (final m in listDepositedBy) {
            if (m.key.toLowerCase() ==
                requirementController.optReq.value.id.toLowerCase()) {
              listDepositedBy.remove(m);
              break;
            }
          }*/
          var amt = 0.0;
          try {
            amt = double.parse(amount.text.trim());
          } catch (e) {}
          listDepositedBy.add(MortgageDepositByModel(
              key: requirementController.optReq.value.id.toLowerCase(),
              from: requirementController.optReq.value.title,
              amount: amt));
          setState(() {});
        },
        callbackFailed: (err) {
          showToast(context: context, msg: err);
        }));
  }

  onEditAmountDialog(MortgageDepositByModel model) {
    amount.text = model.amount.toStringAsFixed(0);
    requirementController.optReq.value =
        OptionItem(id: model.key, title: model.from);
    Get.dialog(reqMortgageDialog(
        context: context,
        amount: amount,
        title: "Edit Amount",
        callbackSuccess: () {
          listDepositedBy.remove(model);
          var amt = 0.0;
          try {
            amt = double.parse(amount.text.trim());
          } catch (e) {}
          listDepositedBy.add(MortgageDepositByModel(
              key: requirementController.optReq.value.id.toLowerCase(),
              from: requirementController.optReq.value.title,
              amount: amt));
          setState(() {});
        },
        callbackFailed: (err) {
          showToast(context: context, msg: err);
        }));
  }

  resetData() {
    try {
      amount.clear();
      requirementController.optReq = OptionItem(id: null, title: "Select").obs;
    } catch (e) {}
  }

  updateFields() {
    final model = requirementsCtrl.requirementsModelAPIModel.value;
    try {
      //mortgageTypeRBIndex =
      // Common.getMapKeyByVal(listMortgageTypeRB, model.mortgageType);
    } catch (e) {}
    try {
      addrOfProperty = model.addressOfPropertyToBeMortgaged ?? '';
    } catch (e) {}
    try {
      currentValProperty.text = model
          .priceOfPropertyBeingPurchasedCurrentValuationOfProperty
          .toStringAsFixed(0);
    } catch (e) {}
    try {
      purchasePrice.text = model.purchasePrice.toStringAsFixed(0);
    } catch (e) {}
    try {
      loanAmount.text = model.howMuchDoYouWishToBorrow.toStringAsFixed(0);
    } catch (e) {}
    try {
      depositAmount.text = model.amountOfDepositEquity.toStringAsFixed(0);
    } catch (e) {}
    try {
      rentalIncome.text = model.rentalIncomeAchievableAmount.toStringAsFixed(0);
    } catch (e) {}
    try {
      propertyTenantedRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.isThePropertyAlreadyTenanted);
    } catch (e) {}
    try {
      propertyTenantedTypeRBIndex =
          Common.getMapKeyByVal(listPropertyTenantedTypeRB, model.tenancyType);
    } catch (e) {}
    try {
      for (final j in model.toJson().entries) {
        if (j.value != null) {
          requirementController.ddReq.value.listOptionItems.forEach((opt) {
            if (j.key.toLowerCase() == opt.id.toLowerCase() && j.value > 0) {
              listDepositedBy.add(MortgageDepositByModel(
                  key: j.key, from: opt.title, amount: j.value));
            }
          });
        }
      }
      //listDepositedBy.clear();
    } catch (e) {}
    try {
      incentivesRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB,
          model
              .areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesOrGoods);
    } catch (e) {}
    try {
      incentiveAmount.text = model
          .areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesValue
          .toStringAsFixed(0);
    } catch (e) {}
    try {
      prefMortTermsYY.text = model.preferredMortgageTermYear.toString();
    } catch (e) {}
    try {
      prefMortTermsMM.text = model.preferredMortgageTermMonth.toString();
    } catch (e) {}
    try {
      prefRepayTypeRBIndex = Common.getMapKeyByVal(
          listPrefRepayTypeRB, model.preferredRepaymentType);
    } catch (e) {}
    try {
      propertyTypeIndex =
          Common.getMapKeyByVal(listPropertyTypeRB, model.propertyType);
    } catch (e) {}
    try {
      propertyTenureIndex =
          Common.getMapKeyByVal(listPropertyTenureRB, model.propertyTenure);
    } catch (e) {}
    try {
      flatHasShopRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.isTheFlatAboveShopRestaurant);
    } catch (e) {}
    try {
      yearLeftLease.text = model.ifLeaseholdHowLongIsLeftOnTheLease ?? '';
    } catch (e) {}
    try {
      noBedrooms.text = model.numberOfBedrooms.toString();
    } catch (e) {}
    try {
      noKitchens.text = model.numberOfKoichens.toString();
    } catch (e) {}
    try {
      noBathrooms.text = model.numberOfBaahroom.toString();
    } catch (e) {}
    try {
      noWC.text = model.numberOfWC.toString();
    } catch (e) {}
    try {
      floorBuilding.text = model.floorsInTheBuilding ?? '';
    } catch (e) {}
    try {
      groundRent.text = model.groundRentAmount.toStringAsFixed(0);
    } catch (e) {}
    try {
      srvCharges.text = model.serviceChargeAmount.toStringAsFixed(0);
    } catch (e) {}
    try {
      yearBuilt.text = model.yearPropertyWasBuilt ?? '';
    } catch (e) {}
    try {
      whichFloorProperty.text = model.whichFloorIsTheProperty ?? '';
    } catch (e) {}
    try {
      extLoftConvRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.anyExtensionOrLoftConversionDone);
    } catch (e) {}
    try {
      nonStandardConstrRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
          model.isThePropertyOfANonStandardConstruction);
    } catch (e) {}
    try {
      nonStandardConstrDetails.text = model.nonstandardConstructionDetails;
    } catch (e) {}
    try {
      exCouncilRBIndex =
          Common.getMapKeyByVal(W2NLocalData.listYesNoRB, model.isItExCouncil);
    } catch (e) {}
    try {
      xAccessRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.doesTheFlatHaveDeckORBalconyAccess);
    } catch (e) {}
    try {
      newBuiltPropRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.isThisANewBuiltProperty);
    } catch (e) {}
    try {
      haveLiftRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.doesThePropertyHaveALift);
    } catch (e) {}
    try {
      doesWarantyRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.doesThisPropertyHaveWarranty);
    } catch (e) {}
    try {
      optDoesWarranty = OptionItem(id: "0", title: model.warrantyType).obs;
    } catch (e) {}
    try {
      notes.text = model.notes ?? '';
    } catch (e) {}
    //  calculation for deposit amount
    try {
      depositAmount.text = calDepositAmount(
              loanAmt: loanAmount.text, purchasePrice: purchasePrice.text)
          .toString();
    } catch (e) {}
  }

  getSubCase() {
    for (var value in eB2LetMSubList.values) {
      try {
        if (EnumGen.getEnum2Str(value).toLowerCase() ==
            caseModelCtrl.locModel.value.description.toLowerCase()) {
          return value;
        }
      } catch (e) {}
    }
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    amount.dispose();
    currentValProperty.dispose();
    purchasePrice.dispose();
    loanAmount.dispose();
    depositAmount.dispose();
    incentiveAmount.dispose();
    prefMortTermsYY.dispose();
    prefMortTermsMM.dispose();
    noBedrooms.dispose();
    noKitchens.dispose();
    noBathrooms.dispose();
    groundRent.dispose();
    srvCharges.dispose();
    noWC.dispose();
    floorBuilding.dispose();
    whichFloorProperty.dispose();
    yearBuilt.dispose();
    notes.dispose();
    nonStandardConstrDetails.dispose();
    yearLeftLease.dispose();
    rentalIncome.dispose();

    focusCurrentValProperty.dispose();
    focusPurchasePrice.dispose();
    focusLoanAmount.dispose();
    focusDepositAmount.dispose();
    focusIncentivesAmount.dispose();
    focusPrefMortTermsYY.dispose();
    focusPrefMortTermsMM.dispose();
    focusNoBedrooms.dispose();
    focusNoKitchens.dispose();
    focusNoBathrooms.dispose();
    focusGroundRent.dispose();
    focusSrvCharges.dispose();
    focusNoWC.dispose();
    focusFloorBuilding.dispose();
    focusYearBuilt.dispose();
    focusNonStandardConstrDetails.dispose();
    focusYearLeftLease.dispose();
    focusWhichFloorProperty.dispose();
    focusRentalIncome.dispose();

    super.dispose();
  }

  initPage() async {
    try {
      updateFields();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    //print(widget.mortgageUserRequirementModel);
    //updateFields();
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(
                    title: caseModelCtrl.locModel.value.title +
                        " | " +
                        caseModelCtrl.locModel.value.id.toString()),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    drawProgressView(5),
                    drawForm(),
                    SizedBox(height: 30),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: MMBtn(
                              txt: "Back",
                              height: getHP(context, 7),
                              width: getW(context),
                              bgColor: Colors.grey,
                              radius: 10,
                              callback: () async {
                                Get.back();
                              }),
                        ),
                        SizedBox(width: 10),
                        Flexible(
                          child: MMBtn(
                              txt: "Save & continue",
                              height: getHP(context, 7),
                              width: getW(context),
                              radius: 10,
                              callback: () {
                                if (onValidate()) {
                                  onRequirementAPI();
                                }
                              }),
                        ),
                      ],
                    ),
                    SizedBox(height: 40),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
