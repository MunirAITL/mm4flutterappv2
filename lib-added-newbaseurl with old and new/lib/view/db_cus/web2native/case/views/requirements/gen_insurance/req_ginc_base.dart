import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_webservice/geolocation.dart';

import '../../../../../../../config/MyTheme.dart';
import '../../../../../../../model/json/web2native/case/requirements/gen_insurance/MortgageBuildingAndContentInsuranceItem.dart';
import '../../../../../../widgets/dropdown/DropDownListDialog.dart';
import '../../../../../../widgets/dropdown/DropListModel.dart';
import '../../../../../../widgets/gplaces/GPlacesView.dart';
import '../../../../../../widgets/txt/Txt.dart';
import '../../../../case_base.dart';
import '../../../dialogs/requirements/req_ginc_dialog.dart';

enum eGIncSubList {
  Buildings_xANDx_Contents,
  Buildings_Only,
  Contents_Only,
}

abstract class ReqGIncBase<T extends StatefulWidget> extends CaseBase<T> {
  onBikePhoneDialog(MortgageBuildingAndContentInsuranceItem model);
  onAdditionalItemDialog(MortgageBuildingAndContentInsuranceItem model);
  onDelItemDialog(MortgageBuildingAndContentInsuranceItem modelOld);
  getSubCase();

  var ddPropType = DropListModel([
    OptionItem(id: "1", title: "Terraced House"),
    OptionItem(id: "2", title: "Detached House"),
    OptionItem(id: "3", title: "Semi Detached House"),
    OptionItem(id: "4", title: "Terraced Bungalow"),
    OptionItem(id: "5", title: "Detached Bungalow"),
    OptionItem(id: "6", title: "Semi Detached Bungalow"),
    OptionItem(id: "7", title: "Flat or Maisonette"),
    OptionItem(id: "8", title: "Coach House"),
  ]);
  var optPropType = OptionItem(id: null, title: "Select Type of Property");

  var ddYearConstr = DropListModel([
    OptionItem(id: "1", title: "Pre 1750"),
    OptionItem(id: "2", title: "Between 1750 and 1869"),
    OptionItem(id: "3", title: "Between 1870 and 1899"),
    OptionItem(id: "4", title: "Between 1900 and 1919"),
    OptionItem(id: "5", title: "Between 1920 and 1945"),
    OptionItem(id: "6", title: "Between 1946 and 1959"),
    OptionItem(id: "7", title: "Between 1960 and 1979"),
    OptionItem(id: "8", title: "Between 1980 and 1989"),
    OptionItem(id: "9", title: "Between 1990 and 1999"),
    OptionItem(id: "10", title: "Between 2000 and 2009"),
    OptionItem(id: "11", title: "Between 2010 and 2019"),
    OptionItem(id: "12", title: "2020 onwards"),
  ]);
  var optYearConstr =
      OptionItem(id: null, title: "Select Year of Construction");

  var ddNoBedrooms = DropListModel([]);
  var optNoBedrooms = OptionItem(id: null, title: "Select Number of Bedroom");

  var ddNoClaimDis = DropListModel([]);
  var optNoClaimDis =
      OptionItem(id: null, title: "Select No. Of Claims Discount");

  int accDamageCoverRBIndex = 1;

  int accDamageRBIndex = 1;

  int homeInsRBIndex = 1;

  int familyExpRBIndex = 1;

  final buildingCoverAmount = TextEditingController();
  final coverAmount = TextEditingController();
  final contentAccess = TextEditingController();

  final focusBuildingCoverAmount = FocusNode();
  final focusCoverAmount = FocusNode();
  final focusContentAccess = FocusNode();

  String addrOfProperty = "";

  onValidate() {
    return true;
  }

  drawForm() {
    var titleAddr = "";
    var subTitleArr = "";
    if (getSubCase() == eGIncSubList.Buildings_xANDx_Contents) {
      titleAddr = "Current Property Address & Security Property?";
      subTitleArr =
          "Your current residential address MUST be the same as the security property. Otherwise this application can not continue.";
    } else {
      titleAddr = "Risk Property Address";
      subTitleArr = null;
    }

    return Container(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      SizedBox(height: 20),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GPlacesView(
              title: titleAddr,
              subTitle: subTitleArr,
              titleColor: Colors.black,
              isBold: true,
              txtSize: MyTheme.txtSize - .2,
              address: addrOfProperty,
              callback: (String address, String postCode, Location loc) {
                addrOfProperty = address;
                setState(() {});
              }),
          SizedBox(height: 10),
          DropDownListDialog(
            context: context,
            heading: "Select Type Of Property",
            h1: "Select type of property",
            title: optPropType.title,
            ddTitleList: ddPropType,
            callback: (optionItem) {
              optPropType = optionItem;
            },
          ),
          SizedBox(height: 10),
          DropDownListDialog(
            context: context,
            heading: "Year of Construction",
            h1: "Select year of construction",
            title: optYearConstr.title,
            ddTitleList: ddYearConstr,
            callback: (optionItem) {
              optYearConstr = optionItem;
            },
          ),
        ],
      ),
      caseModelCtrl.locModel.value.description != ''
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                getSubCase() != eGIncSubList.Contents_Only
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 10),
                          DropDownListDialog(
                            context: context,
                            heading: "Number Of Bedrooms",
                            h1: "Select number of bedrooms",
                            title: optNoBedrooms.title,
                            ddTitleList: ddNoBedrooms,
                            callback: (optionItem) {
                              optNoBedrooms = optionItem;
                            },
                          ),
                          SizedBox(height: 10),
                          UIHelper().drawRadioTitle(
                              context: context,
                              title:
                                  "Do you want to add Accidental Damage Cover?",
                              subTitle:
                                  "(Notes from standard insurer: Included as standard within buildings insurance is accidental breakage to fixed glass, ceramic hobs built into cookers that are permanent fixtures in your home, sanitary fixtures and fittings in your home; and solar panels. Available as an optional extra is Accidental Damage cover. This covers unexpected and unintended damage caused by something sudden and external. Accidental Damage would be required in order to claim for events that are not covered under the accidental breakage benefits. An example of this would be putting your foot through a loft ceiling or drilling through a pipe, where you would only be able to make a claim if you had purchased Accidental Damage cover)",
                              list: W2NLocalData.listYesNoRB,
                              index: accDamageCoverRBIndex,
                              callback: (index) {
                                setState(() => accDamageCoverRBIndex = index);
                              }),
                          SizedBox(height: 10),
                          drawCurrencyBox(
                              "Buildings Cover Amount",
                              null,
                              buildingCoverAmount,
                              focusBuildingCoverAmount,
                              focusCoverAmount,
                              (v) {}),
                          SizedBox(height: 10),
                          DropDownListDialog(
                            context: context,
                            heading: "No. Of Claims Discount",
                            h1: "Select no. of claims discount",
                            title: optNoClaimDis.title,
                            ddTitleList: ddNoClaimDis,
                            callback: (optionItem) {
                              optNoClaimDis = optionItem;
                            },
                          ),
                        ],
                      )
                    : SizedBox(),
                getSubCase() != eGIncSubList.Buildings_Only
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 10),
                          Txt(
                              txt: "Contents Cover",
                              txtColor: MyTheme.statusBarColor,
                              txtSize: MyTheme.txtSize + .5,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 10),
                          drawCurrencyBox("Cover Amount", null, coverAmount,
                              focusCoverAmount, focusContentAccess, (v) {}),
                          SizedBox(height: 10),
                          drawCurrencyBox(
                              "Contents Excess",
                              "(Notes: This is the amount you would be required to pay in the event of a claim on the contents element of your policy. In the event of a claim involving both buildings and contents, only the higher of the buildings or contents excess will be payable (not both).)",
                              contentAccess,
                              focusContentAccess,
                              null,
                              (v) {}),
                          SizedBox(height: 10),
                          UIHelper().drawRadioTitle(
                              context: context,
                              title: "Accidental Damage",
                              list: W2NLocalData.listYesNoRB,
                              index: accDamageRBIndex,
                              callback: (index) {
                                setState(() => accDamageRBIndex = index);
                              }),
                          SizedBox(height: 10),
                          Txt(
                              txt: "Personal Possessions:",
                              txtColor: MyTheme.statusBarColor,
                              txtSize: MyTheme.txtSize + .5,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 10),
                          Txt(
                              txt:
                                  "(NOTES: Items you take outside your home are not covered automatically for loss, theft, or damage under Contents Insurance. You must choose to take additional Possessions cover for them to be insured. There may be a claim limit of £2,500 per individual item except for phones where the claim limit maybe £500 and pedal cycles may have a claim limit of £1000. Items worth more than these limits can be insured to a higher value but will need to be individually listed on the policy as specified items.Items worth less than £2,500 individually, that you take outside the home, can be covered collectively by also choosing an amount of cover here.)",
                              txtColor: Colors.black54,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(height: 10),
                          Txt(
                              txt: "Phones and Bikes",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 5),
                          Txt(
                              txt:
                                  "(Notes: Mobile phones with a value over £500 and pedal cycles worth more than £1000 should be listed as specified items here to be covered to their full value when outside the home)",
                              txtColor: Colors.black54,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(height: 10),
                          drawDottedBox("Add", () {
                            onBikePhoneDialog(null);
                          }),
                          SizedBox(height: 10),
                          drawGIncItems(context, 'Phone & Bike',
                              requirementsCtrl.listInsuranceItems, (modelEdit) {
                            onBikePhoneDialog(modelEdit);
                          }, (modelDel) {
                            onDelItemDialog(modelDel);
                          }),
                          SizedBox(height: 20),
                          Txt(
                              txt: "Additional Covers:",
                              txtColor: MyTheme.statusBarColor,
                              txtSize: MyTheme.txtSize + .5,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 10),
                          Txt(
                              txt: "Home Emergency Cover?",
                              txtColor: Colors.black,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: true),
                          SizedBox(height: 5),
                          Txt(
                              txt:
                                  "(Notes: From standard insurer: This cover costs £48.00 per year Home Emergency Cover provides up to £1,500 per incident, with no limit on the number of incidents per year, for a number of household emergencies. Cover is provided for the cost of a contractor’s emergency call-out, labour charges and parts and materials. Cover is provided in the following areas:",
                              txtColor: Colors.black54,
                              txtSize: MyTheme.txtSize - .2,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(height: 5),
                          drawBullet(
                              "Failure of the boiler or primary heating system"),
                          drawBullet(
                              "£500 towards the replacement cost of a new boiler or primary heating system if the existing one cannot be repaired • A gas leak"),
                          drawBullet("Electricity supply failure"),
                          drawBullet("Water supply system failure;"),
                          drawBullet(
                              "Damage to or failure of the plumbing and drainage system where internal flooding or water damage is likely"),
                          drawBullet(
                              " External door or window lock failure or lost keys"),
                          drawBullet("Pest infestation"),
                          drawBullet(
                              "Sudden damage to the roofing causing internal damage"),
                          drawBullet("A blocked toilet"),
                          drawBullet(
                              "Blocked external drains within the boundaries of your home where this can be resolved by jetting.\nThis optional cover is designed to provide assistance in emergencies and is not designed to be a maintenance plan. This means we will not provide cover in circumstances such as:"),
                          drawBullet("A light bulb or fuse blowing"),
                          drawBullet(
                              "If you lose the keys for your shed or garage"),
                          drawBullet(
                              "If the boiler breaks down and it hasn't been serviced in the last 12 months"),
                          drawBullet(
                              "If you have a small leak which is not causing any damage)"),
                          SizedBox(height: 5),
                          UIHelper().drawRadioTitle(
                              context: context,
                              title: null,
                              list: W2NLocalData.listYesNoRB,
                              index: homeInsRBIndex,
                              callback: (index) {
                                setState(() => homeInsRBIndex = index);
                              }),
                          SizedBox(height: 10),
                          UIHelper().drawRadioTitle(
                              context: context,
                              title: "Family Legal Expenses?",
                              list: W2NLocalData.listYesNoRB,
                              index: familyExpRBIndex,
                              callback: (index) {
                                setState(() => familyExpRBIndex = index);
                              }),
                          SizedBox(height: 10),
                          drawDottedBox("Additional items to cover", () {
                            onAdditionalItemDialog(null);
                          }),
                          drawGIncItems(context, 'High value',
                              requirementsCtrl.listInsuranceItems, (modelEdit) {
                            onAdditionalItemDialog(modelEdit);
                          }, (modelDel) {
                            onDelItemDialog(modelDel);
                          }),
                        ],
                      )
                    : SizedBox(),
              ],
            )
          : SizedBox()
    ]));
  }
}
