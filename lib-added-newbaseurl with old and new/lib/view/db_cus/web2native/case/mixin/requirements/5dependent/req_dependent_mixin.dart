import 'package:aitl/model/json/web2native/case/requirements/5dependents/MortgageFinancialDependantsModel.dart';
import 'package:flutter/material.dart';

import '../../../../../../../config/MyTheme.dart';
import '../../../../../../../view_model/helper/ui_helper.dart';
import '../../../../../../widgets/dialog/ConfirmationDialog.dart';
import '../../../../../../widgets/tile/ExpansionTileCard.dart';
import '../../../../../../widgets/txt/Txt.dart';

mixin ReqStep5DependentMixin {
  drawDependentItems(
      BuildContext context,
      bool isShowAction,
      List<MortgageFinancialDependantsModel> listDependentsModelAPIModel,
      Function(MortgageFinancialDependantsModel) callbackOnEdit,
      Function(MortgageFinancialDependantsModel) callbackOnDel) {
    if (listDependentsModelAPIModel == null) return SizedBox();
    return (listDependentsModelAPIModel.length > 0)
        ? Container(
            child: Padding(
              padding: const EdgeInsets.all(5),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Material(
                    elevation: 2,
                    color: MyTheme.bgColor,
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Txt(
                                txt: "Dependant Name",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                          Expanded(
                            child: Txt(
                                txt: "Date of Birth",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                          Expanded(
                            child: Txt(
                                txt: "Relation to applicant",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                          isShowAction
                              ? Expanded(
                                  child: Txt(
                                      txt: "Action",
                                      txtColor: Colors.white,
                                      txtSize: MyTheme.txtSize - .6,
                                      txtAlign: TextAlign.start,
                                      fontWeight: FontWeight.w500,
                                      isBold: true),
                                )
                              : SizedBox(),
                        ],
                      ),
                    ),
                  ),
                  /*Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        _getTableHeading('Dependant Name'),
                        _getTableHeading('Date of Birth'),
                        _getTableHeading('Relation to applicant'),
                        !isHideAction ? _getTableHeading('Action') : SizedBox(),
                        !isHideAction ? _getTableHeading('') : SizedBox(),
                      ]),*/
                  ...listDependentsModelAPIModel.map((model) {
                    return _drawTableItems(context, isShowAction, model,
                        callbackOnEdit, callbackOnDel);
                  }),
                ],
              ),
            ),
          )
        : SizedBox();
  }

  _drawTableItems(
      BuildContext context,
      bool isShowAction,
      MortgageFinancialDependantsModel model,
      Function(MortgageFinancialDependantsModel) callbackOnEdit,
      Function(MortgageFinancialDependantsModel) callbackOnDel) {
    return ListTileTheme(
      contentPadding: EdgeInsets.all(0),
      iconColor: Colors.black,
      child: Theme(
        data: ThemeData.light()
            .copyWith(accentColor: Colors.black, primaryColor: Colors.red),
        child: ExpansionTile(
          //trailing: SizedBox.shrink(),
          title: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: isShowAction ? 2 : 3,
                child: Txt(
                    txt: model.name ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              Expanded(
                flex: isShowAction ? 2 : 3,
                child: Txt(
                    txt: model.dateOfBirth ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              Expanded(
                flex: 2,
                child: Txt(
                    txt: model.relationship ?? '',
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize - .6,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              isShowAction ? _getTableHeading('') : SizedBox(),
              isShowAction
                  ? Flexible(
                      child: Row(
                        children: [
                          Flexible(
                            child: GestureDetector(
                                onTap: () {
                                  callbackOnEdit(model);
                                },
                                child: Icon(Icons.edit_outlined,
                                    color: Colors.grey, size: 20)),
                          ),
                          SizedBox(width: 10),
                          Flexible(
                            child: GestureDetector(
                                onTap: () {
                                  confirmDialog(
                                      context: context,
                                      title: "Confirmation!!",
                                      msg: "Are you sure to delete this item?",
                                      callbackYes: () {
                                        callbackOnDel(model);
                                      });
                                },
                                child: Icon(Icons.delete_outline,
                                    color: Colors.grey, size: 20)),
                          ),
                        ],
                      ),
                    )
                  : SizedBox()
            ],
          ),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  UIHelper().draw2Row("Living with", model.livingWith),
                  SizedBox(height: 5),
                  UIHelper().draw2Row("Is the dependent with joint Applicant",
                      model.isTheDependantSameAsFirstApplicant),
                  //SizedBox(height: 5),
                  //UIHelper().draw2Row("Share dependent with another applicant?",
                  //model.customerName),
                  //SizedBox(height: 5),
                  //UIHelper().draw2Row("Relationship to another applicant",
                  //model.custmerRelationShipWithAnotherCustomer),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _getTableHeading(String txt) {
    return Flexible(
      child: Text(txt,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.black, fontSize: 12, fontWeight: FontWeight.bold)),
    );
  }
}
