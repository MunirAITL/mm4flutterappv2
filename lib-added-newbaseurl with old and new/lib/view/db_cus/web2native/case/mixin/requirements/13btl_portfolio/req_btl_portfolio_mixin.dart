import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/web2native/case/requirements/13btl_portfolio/MortgageUserPortfolios.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

mixin ReqStep13BtlPortfolioMixin {
  drawBtlPortfolioItems(
      BuildContext context,
      bool isShowAction,
      List<MortgageUserPortfolios> list,
      Function(MortgageUserPortfolios, bool) callback) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        list.length > 0
            ? Material(
                elevation: 2,
                color: MyTheme.bgColor,
                child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        flex: 3,
                        child: Txt(
                            txt: "Property Type",
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize - .6,
                            txtAlign: TextAlign.start,
                            fontWeight: FontWeight.w500,
                            isBold: true),
                      ),
                      Expanded(
                        flex: 3,
                        child: Txt(
                            txt: "Owners",
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize - .6,
                            txtAlign: TextAlign.start,
                            fontWeight: FontWeight.w500,
                            isBold: true),
                      ),
                      isShowAction
                          ? Expanded(
                              flex: 2,
                              child: Txt(
                                  txt: "Action",
                                  txtColor: Colors.white,
                                  txtSize: MyTheme.txtSize - .6,
                                  txtAlign: TextAlign.start,
                                  fontWeight: FontWeight.w500,
                                  isBold: true),
                            )
                          : SizedBox(),
                      //SizedBox(),
                    ],
                  ),
                ),
              )
            : SizedBox(),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ...list.map((model) => Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ListTileTheme(
                      contentPadding: EdgeInsets.all(0),
                      iconColor: Colors.black,
                      child: Theme(
                        data: ThemeData.light().copyWith(
                            accentColor: Colors.black,
                            primaryColor: Colors.red),
                        child: ExpansionTile(
                          //trailing: SizedBox.shrink(),
                          title: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                flex: isShowAction ? 2 : 3,
                                child: Txt(
                                    txt: model.propertyType,
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .4,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                              ),
                              Expanded(
                                flex: 2,
                                child: Txt(
                                    txt: model.owner ?? '',
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.start,
                                    isBold: false),
                              ),
                              isShowAction
                                  ? Flexible(
                                      child: Row(
                                        children: [
                                          Flexible(
                                            child: GestureDetector(
                                                onTap: () {
                                                  callback(model, true);
                                                },
                                                child: Icon(Icons.edit_outlined,
                                                    color: Colors.grey,
                                                    size: 20)),
                                          ),
                                          SizedBox(width: 10),
                                          Flexible(
                                            child: GestureDetector(
                                                onTap: () {
                                                  confirmDialog(
                                                      context: context,
                                                      title: "Confirmation!!",
                                                      msg:
                                                          "Are you sure to delete this item?",
                                                      callbackYes: () {
                                                        callback(model, false);
                                                      });
                                                },
                                                child: Icon(
                                                    Icons.delete_outline,
                                                    color: Colors.grey,
                                                    size: 20)),
                                          ),
                                        ],
                                      ),
                                    )
                                  : SizedBox()
                            ],
                          ),
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.all(isShowAction ? 10 : 5),
                              child: Column(
                                children: [
                                  model.owner == "SPV Company"
                                      ? Padding(
                                          padding: EdgeInsets.only(bottom: 5),
                                          child: UIHelper().draw2Row(
                                              "SPV Company Name",
                                              model.sPVCompanyName))
                                      : SizedBox(),
                                  UIHelper().draw2Row("Security Property Type",
                                      model.propertyStyle),
                                  SizedBox(height: 5),
                                  UIHelper().draw2Row(
                                      "Property Address",
                                      (model.address1 +
                                              " " +
                                              model.address2 +
                                              " " +
                                              model.address3)
                                          .trim()),
                                  SizedBox(height: 5),
                                  UIHelper().draw2Row(
                                      "Purchase Price (approximate)",
                                      AppDefine.CUR_SIGN +
                                          model.purchasePrice
                                              .toStringAsFixed(0)),
                                  SizedBox(height: 5),
                                  UIHelper().draw2Row(
                                      "Date Purchased",
                                      !model.datePurchased.contains("1970")
                                          ? DateFun.getDate(model.datePurchased,
                                              "dd-MMM-yyyy")
                                          : "N/A"),
                                  SizedBox(height: 5),
                                  UIHelper().draw2Row(
                                      "Current Value",
                                      AppDefine.CUR_SIGN +
                                          model.currentValue
                                              .toStringAsFixed(0)),
                                  SizedBox(height: 5),
                                  UIHelper().draw2Row(
                                      "Monthly Rental Income",
                                      AppDefine.CUR_SIGN +
                                          model.rentalIncome
                                              .toStringAsFixed(0)),
                                  SizedBox(height: 5),
                                  UIHelper().draw2Row(
                                      "Monthly Mortgage Payment",
                                      AppDefine.CUR_SIGN +
                                          model.monthlyMortgage
                                              .toStringAsFixed(0)),
                                  SizedBox(height: 5),
                                  UIHelper()
                                      .draw2Row("Lender Name", model.lender),
                                  SizedBox(height: 5),
                                  UIHelper().draw2Row(
                                      "Current Mortgage Balance",
                                      AppDefine.CUR_SIGN +
                                          model.mortgageBalance
                                              .toStringAsFixed(0)),
                                  SizedBox(height: 5),
                                  UIHelper().draw2Row("LTV(%)",
                                      model.lTV.toStringAsFixed(0) + "%"),
                                  SizedBox(height: 5),
                                  UIHelper().draw2Row(
                                      "Current Mortgage Interest Rate",
                                      AppDefine.CUR_SIGN +
                                          model.currentMortgageInterestRate
                                              .toStringAsFixed(0)),
                                  SizedBox(height: 5),
                                  UIHelper().draw2Row(
                                      "Mortgage Term Remaining with Current Lender",
                                      model
                                          .mortgageTermRemainingWithCurrentLender),
                                  SizedBox(height: 5),
                                  UIHelper().draw2Row(
                                      "Current Fixed Period End Date",
                                      !model.currentFixedPeriodEndDate
                                              .contains("1970")
                                          ? DateFun.getDate(
                                              model.currentFixedPeriodEndDate,
                                              "dd-MMM-yyyy")
                                          : "N/A"),
                                  SizedBox(height: 5),
                                  UIHelper().draw2Row(
                                      "Interest Type", model.interestType),
                                  SizedBox(height: 5),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ],
                ))
          ],
        ),
      ],
    );
  }
}
