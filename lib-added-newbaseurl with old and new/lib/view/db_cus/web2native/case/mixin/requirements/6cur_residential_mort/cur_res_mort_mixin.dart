import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/Task.dart';
import 'package:aitl/model/json/web2native/case/requirements/cur_res_mort/MortgageUserCurrentResidentialMortgage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

mixin ReqStep6CurResMortMixin {
  drawCurResMortDetailsView(
    BuildContext context,
    MortgageUserCurrentResidentialMortgage model,
    Task caseModel,
  ) {
    if (model == null) return SizedBox();
    return model != null
        ? model.occupantType == 'Homeowner with mortgage'
            ? Container(
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      UIHelper().draw2Row("What type of occupant are you?",
                          model.occupantType ?? ''),
                      SizedBox(height: 10),
                      UIHelper().draw2Row("When did you purchase the property?",
                          DateFun.getDate(model.purchaseDate, 'dd-MMM-yyyy')),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Current Value of the Residential Property",
                          UIHelper().addCurSign(
                              model.currentValueOfTheResidentialProperty)),
                      SizedBox(height: 10),
                      UIHelper().draw2Row("Original Purchase Price",
                          UIHelper().addCurSign(model.purchasePrice)),
                      SizedBox(height: 10),
                      UIHelper().draw2Row("Amount outstanding",
                          UIHelper().addCurSign(model.amountOutstanding)),
                      SizedBox(height: 10),
                      UIHelper().draw2Row("Lender Name", model.lender ?? ''),
                      SizedBox(height: 10),
                      UIHelper()
                          .draw2Row("Term outstanding", model.termOutstanding),
                      SizedBox(height: 10),
                      UIHelper()
                          .draw2Row("Repayment Type", model.repaymentType),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Current Monthly Payment",
                          UIHelper().addCurSign(
                              model.currentMonthlyPaymentAndInterestRate)),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "Is the current mortgage out of fixed rate period?",
                          model.interestRateType ?? 'No'),
                      SizedBox(height: 10),
                      UIHelper().draw2Row(
                          "End of fixed period",
                          DateFun.getDate(
                              model.endDateForInterestRateType, 'dd-MMM-yyyy')),
                      SizedBox(height: 10),
                      UIHelper().draw2Row("Notes", model.remarks),
                    ],
                  ),
                ),
              )
            : model.occupantType == 'Home owner without mortgage (unencumbered)'
                ? Container(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UIHelper().draw2Row("What type of occupant are you?",
                              model.occupantType ?? ''),
                          SizedBox(height: 10),
                          UIHelper().draw2Row(
                              "Current Value of the Residential Property",
                              UIHelper().addCurSign(
                                  model.currentValueOfTheResidentialProperty)),
                          SizedBox(height: 10),
                          UIHelper().draw2Row(
                              "When did you purchase the property?",
                              DateFun.getDate(
                                  model.purchaseDate, 'dd-MMM-yyyy')),
                          SizedBox(height: 10),
                          UIHelper().draw2Row("Original Purchase Price",
                              UIHelper().addCurSign(model.purchasePrice)),
                          SizedBox(height: 10),
                          UIHelper().draw2Row("Notes", model.remarks),
                        ],
                      ),
                    ),
                  )
                : model.occupantType == 'Tenant'
                    ? Container(
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              UIHelper().draw2Row(
                                  "What type of occupant are you?",
                                  model.occupantType ?? ''),
                              SizedBox(height: 10),
                              UIHelper().draw2Row(
                                  "How much is your monthly rental?",
                                  UIHelper().addCurSign(model
                                      .currentMonthlyPaymentAndInterestRate)),
                              SizedBox(height: 10),
                              Txt(
                                  txt: "Landlord Details:",
                                  txtColor: MyTheme.statusBarColor,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.start,
                                  fontWeight: FontWeight.w500,
                                  isBold: false),
                              SizedBox(height: 10),
                              UIHelper()
                                  .draw2Row("Name", model.landlordName ?? ''),
                              SizedBox(height: 10),
                              UIHelper().draw2Row(
                                  "Contact Number", model.contactNumber ?? ''),
                              SizedBox(height: 10),
                              UIHelper().draw2Row(
                                  "Email Address", model.emailAddress ?? ''),
                              SizedBox(height: 10),
                              UIHelper()
                                  .draw2Row("Address", model.address ?? ''),
                              SizedBox(height: 10),
                              UIHelper().draw2Row("Notes", model.remarks),
                            ],
                          ),
                        ),
                      )
                    : model.occupantType == 'Living with Family' ||
                            model.occupantType == 'Living with Parents' ||
                            model.occupantType == 'Other'
                        ? Container(
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  UIHelper().draw2Row(
                                      "What type of occupant are you?",
                                      model.occupantType ?? ''),
                                  SizedBox(height: 10),
                                  UIHelper().draw2Row(
                                      model.occupantType != 'Other'
                                          ? "Notes"
                                          : "Please Specify",
                                      model.remarks),
                                ],
                              ),
                            ),
                          )
                        : Container(
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  UIHelper().draw2Row(
                                      "What type of occupant are you?",
                                      model.occupantType ?? ''),
                                  SizedBox(height: 10),
                                  UIHelper().draw2Row(
                                      "Current Value of the Residential Property",
                                      UIHelper().addCurSign(model
                                          .currentValueOfTheResidentialProperty)),
                                  SizedBox(height: 10),
                                  UIHelper().draw2Row(
                                      "Original Purchase Price",
                                      UIHelper()
                                          .addCurSign(model.purchasePrice)),
                                  SizedBox(height: 10),
                                  UIHelper().draw2Row(
                                      "Amount outstanding",
                                      UIHelper()
                                          .addCurSign(model.amountOutstanding)),
                                  SizedBox(height: 10),
                                  UIHelper().draw2Row(
                                      "Lender Name", model.lender ?? ''),
                                  SizedBox(height: 10),
                                  UIHelper().draw2Row("Term outstanding",
                                      model.termOutstanding),
                                  SizedBox(height: 10),
                                  UIHelper().draw2Row(
                                      "Repayment Type", model.repaymentType),
                                  SizedBox(height: 10),
                                  UIHelper().draw2Row(
                                      "Current Monthly Payment",
                                      UIHelper().addCurSign(model
                                          .currentMonthlyPaymentAndInterestRate)),
                                  SizedBox(height: 10),
                                  UIHelper().draw2Row(
                                      "Is the current mortgage out of fixed rate period?",
                                      model.isTheCurrentMortgagePaymentCoveredInTheEventOfAccidentSicknessOrRedundancy ??
                                          'No'),
                                  SizedBox(height: 10),
                                  UIHelper().draw2Row("Notes", model.remarks),
                                ],
                              ),
                            ),
                          )
        : SizedBox();
  }
}
