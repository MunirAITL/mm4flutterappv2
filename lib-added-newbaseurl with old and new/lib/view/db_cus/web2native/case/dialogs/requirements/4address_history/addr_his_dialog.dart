import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/model/json/web2native/case/requirements/4address_history/PostAddHistoryAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/4address_history/UserAddresss.dart';
import 'package:aitl/view/widgets/dialog/DatePickerView.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';

import '../../../../../../widgets/btn/MMBtn.dart';
import '../../../../../../widgets/gplaces/GPlacesView.dart';
import '../../../../../../widgets/input/InputW2N.dart';

reqStep4AddrHistoryDialog({
  @required BuildContext context,
  int taskId,
  UserAddresss model2,
  @required Function(UserAddresss) callbackPost,
  @required Function(UserAddresss, UserAddresss) callbackPut,
  @required Function(String) callbackFailed,
}) {
  final flatNo = TextEditingController();
  final houeNo = TextEditingController();
  var postcode = TextEditingController().obs;
  var addr = "".obs;
  var dtLiving = "".obs;
  var dtLeft = "".obs;
  var stillLivingRBIndex = 1.obs;

  var id = 0;
  try {
    try {
      dtLiving.value =
          DateFun.getDate(DateTime.now().toString(), "dd-MMM-yyyy");
    } catch (e) {}
    try {
      dtLeft.value = DateFun.getDate(DateTime.now().toString(), "dd-MMM-yyyy");
    } catch (e) {}

    if (model2 != null) {
      id = model2.id;
      try {
        flatNo.text = model2.addressLine1 ?? '';
      } catch (e) {}
      try {
        houeNo.text = model2.addressLine2;
      } catch (e) {}
      try {
        addr.value = model2.addressLine3;
      } catch (e) {}
      try {
        postcode.value.text = model2.postcode;
      } catch (e) {}
      try {
        dtLiving.value = DateFun.getDate(model2.livingDate, "dd-MMM-yyyy");
      } catch (e) {}
      try {
        dtLeft.value = DateFun.getDate(model2.livingEndDate, "dd-MMM-yyyy");
      } catch (e) {}
      try {
        stillLivingRBIndex.value = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.areYouLivingAtThisAddress);
      } catch (e) {}
    }
  } catch (e) {}

  final DateTime now = DateTime.now();
  final old = DateTime(now.year - 100, now.month, now.day);
  final next = DateTime(now.year + 10, now.month, now.day);

  return Dialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    insetPadding: EdgeInsets.only(
      left: 50,
      right: 50,
    ),
    child: Container(
      height: MediaQuery.of(context).size.height * .7,
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              Scrollbar(
                isAlwaysShown: true,
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: Obx(
                      () => Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Txt(
                                txt: "Add address",
                                txtColor: Colors.black,
                                txtSize: 2,
                                txtAlign: TextAlign.center,
                                isBold: true),
                          ),
                          Txt(
                              txt:
                                  "Please enter your postcode and select the address from the drop down list. If the postcode does not return any address, enter manually",
                              txtColor: Colors.black54,
                              txtSize: 1.6,
                              txtAlign: TextAlign.start,
                              isBold: false),
                          SizedBox(height: 5),
                          GPlacesView(
                              title: "Address",
                              titleColor: Colors.black,
                              isBold: false,
                              txtSize: MyTheme.txtSize - .2,
                              address: addr.value,
                              callback: (String address, String postCode,
                                  Location loc) {
                                addr.value = address;
                                postcode.value.text = postCode;
                              }),
                          InputW2N(
                            title: "Postcode",
                            input: postcode.value,
                            ph: "",
                            kbType: TextInputType.streetAddress,
                            len: 10,
                          ),
                          InputW2N(
                            title: "Flat or apartment number",
                            input: flatNo,
                            ph: "",
                            kbType: TextInputType.text,
                            len: 50,
                          ),
                          InputW2N(
                            title: "House or building name & number",
                            input: houeNo,
                            ph: "",
                            kbType: TextInputType.text,
                            len: 50,
                          ),
                          DatePickerView(
                            cap: "End Date",
                            dt: (dtLiving.value == '')
                                ? 'Select Date'
                                : dtLiving.value,
                            padding: 5,
                            radius: 5,
                            txtColor: Colors.black,
                            initialDate: now,
                            firstDate: old,
                            lastDate: next,
                            callback: (value) {
                              try {
                                dtLiving.value = DateFormat('dd-MMM-yyyy')
                                    .format(value)
                                    .toString();
                              } catch (e) {}
                            },
                          ),
                          SizedBox(height: 5),
                          UIHelper().drawRadioTitle(
                              context: context,
                              title: "Are you still living at this address?",
                              list: W2NLocalData.listYesNoRB,
                              index: stillLivingRBIndex.value,
                              isTitleBold: false,
                              callback: (index) =>
                                  stillLivingRBIndex.value = index),
                          stillLivingRBIndex.value == 1
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 5),
                                  child: DatePickerView(
                                    cap: "When did you leave the address?",
                                    dt: (dtLeft.value == '')
                                        ? 'Select Date'
                                        : dtLeft.value,
                                    padding: 5,
                                    radius: 5,
                                    txtColor: Colors.black,
                                    initialDate: now,
                                    firstDate: old,
                                    lastDate: next,
                                    callback: (value) {
                                      try {
                                        dtLeft.value = DateFormat('dd-MMM-yyyy')
                                            .format(value)
                                            .toString();
                                      } catch (e) {}
                                    },
                                  ),
                                )
                              : SizedBox(),
                          SizedBox(height: 10),
                          MMBtn(
                              txt: id == 0 ? "Save" : "Update",
                              height: 35,
                              width: null,
                              callback: () async {
                                if (addr.isEmpty) {
                                  callbackFailed("Missing address");
                                } else if (postcode.value.text.isEmpty) {
                                  callbackFailed("Missing post code");
                                } else {
                                  await APIViewModel().req<
                                          PostAddrHistoryAPIModel>(
                                      context: context,
                                      url: id == 0
                                          ? SrvW2NCase.POST_ADDR_HISTORY_URL
                                          : SrvW2NCase.PUT_ADDR_HISTORY_URL,
                                      param: {
                                        "Id": id,
                                        "UserId": userData.userModel.id,
                                        "CompanyId": userData
                                            .userModel.userCompanyInfo.id,
                                        "Status": 0,
                                        "CreationDate": DateFun.getDate(
                                            DateTime.now().toString(),
                                            "dd-MMM-yyyy"),
                                        "AddressLine1": flatNo.text.trim(),
                                        "AddressLine2": houeNo.text.trim(),
                                        "AddressLine3": addr.value,
                                        "Town": "",
                                        "County": "",
                                        "Postcode": postcode.value.text.trim(),
                                        "TelNumber": "",
                                        "NationalInsuranceNumber": "",
                                        "Nationality": "",
                                        "CountryofBirth": "",
                                        "CountryofResidency": "",
                                        "PassportNumber": "",
                                        "MaritalStatus": "",
                                        "LivingDate": dtLiving.value ?? '',
                                        "LivingEndDate": dtLeft.value ?? '',
                                        "AreYouLivingAtThisAddress":
                                            W2NLocalData.listYesNoRB[
                                                stillLivingRBIndex.value],
                                        "OccupantType": "",
                                        "LivingDate1": "12",
                                        "LivingDate2": "12",
                                        "LivingDate3": "1980",
                                        "LivingEndDate1": "12",
                                        "LivingEndDate2": "11",
                                        "LivingEndDate3": "2000",
                                        "TaskId": taskId,
                                        "Remarks": "CASEADDRESS"
                                      },
                                      reqType:
                                          id == 0 ? ReqType.Post : ReqType.Put,
                                      callback: (model) async {
                                        if (id == 0)
                                          callbackPost(
                                              model.responseData.userAddress);
                                        else
                                          callbackPut(
                                              model.responseData.userAddress,
                                              model2);
                                      });

                                  Get.back();
                                }
                              })
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: -40,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.location_history_outlined,
                        color: MyTheme.dBlueAirColor, size: 40)),
              ),
              Positioned(
                top: -15,
                right: -10,
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                      child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            2), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: MyTheme.statusBarColor,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () async {
                        Get.back();
                      }),
                ),
              ),
            ]),
      ),
    ),
  );
}
