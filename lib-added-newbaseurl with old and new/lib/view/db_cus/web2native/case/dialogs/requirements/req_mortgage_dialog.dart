import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:intl/intl.dart';
import '../../../../../../config/AppDefine.dart';
import '../../../../../../model/json/web2native/case/requirements/mortgage/mortgage_model.dart';
import '../../../../../../view_model/rx/web2native/requirements/req_mortgage_ctrl.dart';
import '../../../../../widgets/btn/MMBtn.dart';
import '../../../../../widgets/dropdown/DropDownListDialog.dart';
import 'package:get/get.dart';
import '../../../../../widgets/input/drawInputCurrencyBox.dart';
import 'package:dotted_border/dotted_border.dart';

reqMortgageDialog({
  @required BuildContext context,
  @required String title,
  @required TextEditingController amount,
  @required Function callbackSuccess,
  @required Function callbackFailed,
}) {
  final requirementController = Get.put(ReqMortgageCtrl());
  final focus = FocusNode();
  return Dialog(
    /*shape: RoundedRectangleBorder(
                side: BorderSide(color: MyTheme.dBlueAirColor),
                borderRadius: BorderRadius.all(Radius.circular(5))),*/
    backgroundColor: Colors.transparent,
    elevation: 0,
    insetPadding: EdgeInsets.only(
      left: 50,
      right: 50,
    ), //index == 0 ? getHP(context, 35) : getHP(context, 20)),
    child: Container(
      height: MediaQuery.of(context).size.height * .45,
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Obx(
                    () => Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.center,
                          child: Txt(
                              txt: title,
                              txtColor: Colors.black,
                              txtSize: 2,
                              txtAlign: TextAlign.center,
                              isBold: true),
                        ),
                        SizedBox(height: 20),
                        DropDownListDialog(
                          context: context,
                          h1: "Select where from?",
                          heading: "Where from?",
                          title: requirementController.optReq.value.title,
                          ddTitleList: requirementController.ddReq.value,
                          vPadding: 2,
                          callback: (optionItem) {
                            requirementController.optReq.value = optionItem;
                          },
                        ),
                        SizedBox(height: 5),
                        drawInputCurrencyBox(
                            context: context,
                            tf: amount,
                            hintTxt: 0,
                            labelColor: Colors.black,
                            labelTxt: "Amount",
                            isBold: true,
                            len: 10,
                            focusNode: focus,
                            focusNodeNext: null),
                        SizedBox(height: 20),
                        MMBtn(
                            txt: title,
                            height: 35,
                            width: null,
                            callback: () {
                              if (requirementController.optReq.value.id ==
                                  null) {
                                callbackFailed("Please select Deposit Source");
                              } else if (amount.text.trim().isEmpty) {
                                callbackFailed(
                                    "Please enter valid Deposit value");
                              } else {
                                callbackSuccess();
                                Get.back();
                              }
                            })
                      ],
                    ),
                  ),
                ),
              ),
              Positioned(
                top: -40,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.currency_pound_outlined,
                        color: MyTheme.dBlueAirColor, size: 40)),
              ),
              Positioned(
                top: -15,
                right: -10,
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                      child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            2), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: MyTheme.statusBarColor,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () {
                        Get.back();
                      }),
                ),
              ),
            ]),
      ),
    ),
  );
}

drawDepositAmountItems(
    BuildContext context,
    List<MortgageDepositByModel> listDepositedBy,
    Function(MortgageDepositByModel) callbackOnEdit,
    Function(MortgageDepositByModel) callbackOnDel) {
  return listDepositedBy.length > 0
      ? Container(
          child: Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Material(
                  elevation: 2,
                  color: MyTheme.bgColor,
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Txt(
                              txt: "Reason for addition loan",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - .6,
                              txtAlign: TextAlign.start,
                              fontWeight: FontWeight.w500,
                              isBold: true),
                        ),
                        Expanded(
                          child: Txt(
                              txt: "Amount",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize - .6,
                              txtAlign: TextAlign.start,
                              fontWeight: FontWeight.w500,
                              isBold: true),
                        ),
                        callbackOnEdit != null
                            ? Expanded(
                                child: Txt(
                                    txt: "Action",
                                    txtColor: Colors.white,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.start,
                                    fontWeight: FontWeight.w500,
                                    isBold: true),
                              )
                            : SizedBox(),
                      ],
                    ),
                  ),
                ), //munir
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ...listDepositedBy.map((model) {
                      return Padding(
                        padding: const EdgeInsets.only(top: 3),
                        child: DottedBorder(
                          borderType: BorderType.Rect,
                          //radius: Radius.circular(5),
                          //padding: EdgeInsets.only(top: 10, bottom: 10),
                          color: callbackOnEdit != null
                              ? Colors.grey
                              : Colors.transparent,
                          strokeWidth: callbackOnEdit != null ? 1 : 0,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 5, right: 5),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Expanded(
                                  flex: 2,
                                  child: Txt(
                                      txt: model.from,
                                      txtColor: Colors.black,
                                      txtSize: MyTheme.txtSize - .5,
                                      txtAlign: TextAlign.start,
                                      fontWeight: FontWeight.w500,
                                      isBold: true),
                                ),
                                Flexible(
                                  child: Txt(
                                      txt: AppDefine.CUR_SIGN +
                                          model.amount.toStringAsFixed(0),
                                      txtColor: Colors.black,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.start,
                                      isBold: false),
                                ),
                                callbackOnEdit != null
                                    ? Flexible(
                                        child: Row(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Flexible(
                                            child: IconButton(
                                                onPressed: () {
                                                  callbackOnEdit(model);
                                                },
                                                icon: Icon(Icons.edit_outlined,
                                                    color: Colors.grey,
                                                    size: 20)),
                                          ),
                                          Flexible(
                                            child: IconButton(
                                                onPressed: () {
                                                  confirmDialog(
                                                      context: context,
                                                      title: "Confirmation!!",
                                                      msg:
                                                          "Are you sure to delete this item?",
                                                      callbackYes: () {
                                                        callbackOnDel(model);
                                                      });
                                                },
                                                icon: Icon(Icons.close_outlined,
                                                    color: Colors.grey,
                                                    size: 20)),
                                          ),
                                        ],
                                      ))
                                    : SizedBox()
                              ],
                            ),
                          ),
                        ),
                      );
                    })
                  ],
                ),
              ],
            ),
          ),
        )
      : SizedBox();
}
