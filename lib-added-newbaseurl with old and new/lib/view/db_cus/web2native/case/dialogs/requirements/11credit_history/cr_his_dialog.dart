import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/MortgageUserCreditHistoryItems.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/PostCreditHisItemsAPIModel.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/DatePickerView.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/input/InputW2N.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step11_cr_his_ctrl.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

reqStep11CrHisDialog({
  @required BuildContext context,
  int taskId,
  MortgageUserCreditHistoryItems model2,
  @required Function(String) callbackFailed,
}) {
  final crHisItemsCtrl = Get.put(ReqStep11CrHisCtrl());
  DropListModel dd = DropListModel([
    OptionItem(id: "1", title: "Missed Mortgage Payment"),
    OptionItem(id: "1", title: "Arrears"),
    OptionItem(id: "1", title: "Individual Voluntary Arrangements (IVA)"),
    OptionItem(id: "1", title: "County Court Judgement (CCJ)"),
    OptionItem(id: "1", title: "Defaults"),
    OptionItem(id: "1", title: "Payday Loans"),
    OptionItem(id: "1", title: "Debt Management Plans"),
    OptionItem(id: "1", title: "Repossession"),
    OptionItem(id: "1", title: "Missed Loan Payment"),
    OptionItem(id: "1", title: "Trust Deed + (Scotland)"),
    OptionItem(id: "1", title: "Sequestration + (Scotland)"),
  ]);
  var opt = OptionItem(id: null, title: "Select type").obs;

  final amount = TextEditingController();
  final reason = TextEditingController();

  var settleRBIndex = 1.obs;

  var dtReg = "".obs;
  var dtSettle = "".obs;

  var id = 0;
  try {
    try {
      dtReg.value = DateFun.getDate(DateTime.now().toString(), "dd-MMM-yyyy");
    } catch (e) {}
    try {
      dtSettle.value =
          DateFun.getDate(DateTime.now().toString(), "dd-MMM-yyyy");
    } catch (e) {}
    if (model2 != null) {
      id = model2.id;
      try {
        opt = OptionItem(id: "1", title: model2.type).obs;
      } catch (e) {}
      try {
        amount.text = model2.amount.toStringAsFixed(0);
      } catch (e) {}
      try {
        reason.text = model2.reasonWhyThisHasHappended;
      } catch (e) {}

      try {
        dtReg.value = DateFun.getDate(model2.dateofRegistered, "dd-MMM-yyyy");
      } catch (e) {}
      try {
        dtSettle.value = DateFun.getDate(model2.dateofSettled, "dd-MMM-yyyy");
      } catch (e) {}
      try {
        settleRBIndex.value = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.isThisSettled);
      } catch (e) {}
    }
  } catch (e) {}

  final DateTime now = DateTime.now();
  final old = DateTime(now.year - 100, now.month, now.day);
  final next = DateTime(now.year + 10, now.month, now.day);

  return Dialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    insetPadding: EdgeInsets.only(
      left: 50,
      right: 50,
    ),
    child: Container(
      height: MediaQuery.of(context).size.height * .7,
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              Scrollbar(
                isAlwaysShown: true,
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(20),
                    child: Obx(
                      () => Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.center,
                            child: Txt(
                                txt: id == 0
                                    ? "Add Judgement Details"
                                    : "Edit Judgement Details",
                                txtColor: Colors.black,
                                txtSize: 2,
                                txtAlign: TextAlign.center,
                                isBold: true),
                          ),
                          SizedBox(height: 15),
                          DropDownListDialog(
                            context: context,
                            title: opt.value.title,
                            h1: "Select type",
                            heading: "Type",
                            ddTitleList: dd,
                            vPadding: 3,
                            callback: (optionItem) {
                              opt.value = optionItem;
                            },
                          ),
                          SizedBox(height: 10),
                          DatePickerView(
                            cap: "Date of Registered",
                            dt: (dtReg.value == '')
                                ? 'Select Date'
                                : dtReg.value,
                            padding: 5,
                            radius: 5,
                            txtColor: Colors.black,
                            fontWeight: FontWeight.bold,
                            initialDate: now,
                            firstDate: old,
                            lastDate: now,
                            callback: (value) {
                              try {
                                dtReg.value = DateFormat('dd-MMM-yyyy')
                                    .format(value)
                                    .toString();
                              } catch (e) {}
                            },
                          ),
                          SizedBox(height: 10),
                          drawInputCurrencyBox(
                            context: context,
                            tf: amount,
                            hintTxt: null,
                            labelColor: Colors.black,
                            labelTxt: "Amount",
                            isBold: true,
                            len: 10,
                            focusNode: null,
                            focusNodeNext: null,
                          ),
                          SizedBox(height: 10),
                          UIHelper().drawRadioTitle(
                              context: context,
                              title: "Is This Settled?",
                              list: W2NLocalData.listYesNoRB,
                              index: settleRBIndex.value,
                              callback: (index) {
                                settleRBIndex.value = index;
                              }),
                          settleRBIndex.value == 0
                              ? Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: DatePickerView(
                                    cap: "Date of Settled",
                                    dt: (dtSettle.value == '')
                                        ? 'Select Date'
                                        : dtSettle.value,
                                    padding: 5,
                                    radius: 5,
                                    txtColor: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    initialDate: now,
                                    firstDate: old,
                                    lastDate: next,
                                    callback: (value) {
                                      try {
                                        dtSettle.value =
                                            DateFormat('dd-MMM-yyyy')
                                                .format(value)
                                                .toString();
                                      } catch (e) {}
                                    },
                                  ),
                                )
                              : SizedBox(),
                          SizedBox(height: 10),
                          InputW2N(
                            title: "Reason why this has happended",
                            input: reason,
                            isBold: true,
                            ph: "Reason",
                            kbType: TextInputType.text,
                            len: 100,
                          ),
                          SizedBox(height: 10),
                          MMBtn(
                              txt: id == 0 ? "Save" : "Update",
                              height: 35,
                              width: null,
                              callback: () async {
                                if (opt.value.id == null) {
                                  callbackFailed("Please select type");
                                } else if (amount.text.isEmpty) {
                                  callbackFailed("Please enter valid amount");
                                } else {
                                  var _amount = 0;
                                  try {
                                    _amount = int.parse(amount.text);
                                  } catch (e) {}

                                  await APIViewModel().req<
                                          PostCreditHisItemsAPIModel>(
                                      context: context,
                                      url: id == 0
                                          ? SrvW2NCase
                                              .POST_CREDIT_HISTORYITEMS_URL
                                          : SrvW2NCase
                                              .PUT_CREDIT_HISTORYITEMS_URL,
                                      param: {
                                        "UserId": userData.userModel.id,
                                        "User": null,
                                        "CompanyId": userData
                                            .userModel.userCompanyInfo.id,
                                        "Status": 101,
                                        "MortgageCaseInfoId": 0,
                                        "CreationDate":
                                            DateTime.now().toString(),
                                        "TaskId": taskId,
                                        "MortgageUserCreditHistoryId": 0,
                                        "Type": opt.value.title,
                                        "DateofRegistered": dtReg.value,
                                        "Amount": _amount,
                                        "IsThisSettled": W2NLocalData
                                            .listYesNoRB[settleRBIndex.value],
                                        "DateofSettled": dtSettle.value,
                                        "ReasonWhyThisHasHappended":
                                            reason.text.trim(),
                                        "Remarks": "",
                                        "MissedMortgagePaymentInLast12Months":
                                            "",
                                        "MissedMortgagePaymentInLast24Months":
                                            "",
                                        "MissedMortgagePaymentInLast36Months":
                                            "",
                                        "MissedUnsecuredCreditPaymentInLast12Months":
                                            "",
                                        "PaydayLoansInLast12Month": "",
                                        "EntityId": 0,
                                        "EntityName": null,
                                        "Id": id ?? 0
                                      },
                                      reqType:
                                          id == 0 ? ReqType.Post : ReqType.Put,
                                      callback: (model) async {
                                        if (id == 0)
                                          crHisItemsCtrl.listCrHisItemsCtrl.add(
                                              model.responseData
                                                  .mortgageUserCreditHistoryItem);
                                        else {
                                          crHisItemsCtrl.listCrHisItemsCtrl
                                              .remove(model2);
                                          crHisItemsCtrl.listCrHisItemsCtrl.add(
                                              model.responseData
                                                  .mortgageUserCreditHistoryItem);
                                        }
                                        crHisItemsCtrl.update();
                                        Get.back();
                                      });
                                }
                              })
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Positioned(
                top: -40,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.history_outlined,
                        color: MyTheme.dBlueAirColor, size: 40)),
              ),
              Positioned(
                top: -15,
                right: -10,
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                      child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            2), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: MyTheme.statusBarColor,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () async {
                        Get.back();
                      }),
                ),
              ),
            ]),
      ),
    ),
  );
}
