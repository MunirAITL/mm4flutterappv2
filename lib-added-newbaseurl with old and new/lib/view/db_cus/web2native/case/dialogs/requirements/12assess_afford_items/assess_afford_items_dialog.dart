import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/MortgageUserAssesmentOfAffordAbilityItems.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/PostAssessAffordItemAPIModel.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/input/InputW2N.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

reqAccessAffordDialog({
  @required BuildContext context,
  int taskId,
  MortgageUserAssesmentOfAffordAbilityItems model2,
  @required Function(MortgageUserAssesmentOfAffordAbilityItems) callbackSuccess,
  @required Function(String) callbackFailed,
}) {
  final itemType = TextEditingController();
  final remark = TextEditingController();
  final itemAmount = TextEditingController();

  var id = 0;
  var _itemAmount = 0.0;
  try {
    _itemAmount = model2.itemAmount;
  } catch (e) {}
  try {
    id = model2.id ?? 0;
  } catch (e) {}
  try {
    itemType.text = model2.itemType;
  } catch (e) {}
  try {
    remark.text = model2.remarks;
  } catch (e) {}
  try {
    itemAmount.text = _itemAmount == 0 ? '' : _itemAmount.toStringAsFixed(0);
  } catch (e) {}

  return Dialog(
    backgroundColor: Colors.transparent,
    elevation: 0,
    insetPadding: EdgeInsets.only(
      left: 50,
      right: 50,
    ), //index == 0 ? getHP(context, 35) : getHP(context, 20)),
    child: Container(
      height: MediaQuery.of(context).size.height * .6,
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: Txt(
                            txt: "Affordibility Item",
                            txtColor: Colors.black,
                            txtSize: 2,
                            txtAlign: TextAlign.center,
                            isBold: true),
                      ),
                      SizedBox(height: 10),
                      InputW2N(
                        title: "Item Type",
                        ph: "",
                        input: itemType,
                        kbType: TextInputType.name,
                        len: 50,
                        isBold: true,
                      ),
                      SizedBox(height: 5),
                      drawInputCurrencyBox(
                          context: context,
                          tf: itemAmount,
                          hintTxt: 0,
                          labelColor: Colors.black,
                          labelTxt: "Item Amount",
                          isBold: true,
                          len: 10,
                          focusNode: FocusNode(),
                          focusNodeNext: null),
                      SizedBox(height: 10),
                      Txt(
                          txt: "Remarks",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: true),
                      SizedBox(height: 5),
                      Container(
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey, width: 1),
                            borderRadius: BorderRadius.all(Radius.circular(5))),
                        child: TextField(
                          controller: remark,
                          minLines: 2,
                          maxLines: 3,
                          //expands: true,
                          autocorrect: false,
                          maxLength: 100,
                          keyboardType: TextInputType.multiline,
                          style: TextStyle(color: Colors.black, fontSize: 15),
                          decoration: InputDecoration(
                            hintText: "",
                            hintStyle:
                                TextStyle(color: Colors.grey, fontSize: 15),
                            //labelText: 'Your message',
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            contentPadding: EdgeInsets.all(10),
                          ),
                        ),
                      ),
                      SizedBox(height: 20),
                      MMBtn(
                          txt: id == 0 ? "Add" : "Update",
                          height: 35,
                          width: null,
                          callback: () async {
                            if (itemType.text.trim().isEmpty) {
                              callbackFailed("Please enter item type");
                            } else if (itemAmount.text.isEmpty) {
                              callbackFailed("Please enter item amount");
                            } else {
                              var _itemAmount = 0.0;
                              try {
                                _itemAmount =
                                    double.parse(itemAmount.text.trim());
                              } catch (e) {}
                              var url = id == 0
                                  ? SrvW2NCase.POST_ASSESS_AFFORD_ITEM_URL
                                  : SrvW2NCase.PUT_ASSESS_AFFORD_ITEM_URL;
                              await APIViewModel().req<
                                      PostAssessAffordItemAPIModel>(
                                  context: context,
                                  url: url,
                                  param: {
                                    "id": id ?? 0,
                                    "UserId": userData.userModel.id,
                                    "UserCompanyId":
                                        userData.userModel.userCompanyInfo.id,
                                    "TaskId": taskId,
                                    "Status": 0,
                                    "CreationDate": DateTime.now().toString(),
                                    "MortgageUserAssesmentOfAffordAbilityId": 0,
                                    "ItemType": itemType.text.trim(),
                                    "ItemAmount": _itemAmount,
                                    "Category": "",
                                    "Remarks": remark.text.trim()
                                  },
                                  reqType: id == 0 ? ReqType.Post : ReqType.Put,
                                  callback: (model) async {
                                    if (model.success) {
                                      callbackSuccess(
                                        model.responseData
                                            .mortgageUserAssesmentOfAffordAbilityItem,
                                      );
                                    }
                                    Get.back();
                                  });
                            }
                          })
                    ],
                  ),
                ),
              ),
              Positioned(
                top: -40,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                    ),
                    child: Icon(Icons.info_outline,
                        color: MyTheme.dBlueAirColor, size: 40)),
              ),
              Positioned(
                top: -15,
                right: -10,
                child: Align(
                  alignment: Alignment.topRight,
                  child: InkWell(
                      child: Container(
                        //width: 50.0,
                        //height: 50.0,
                        padding: const EdgeInsets.all(
                            2), //I used some padding without fixed width and height
                        decoration: new BoxDecoration(
                          shape: BoxShape
                              .circle, // You can use like this way or like the below line
                          //borderRadius: new BorderRadius.circular(30.0),
                          color: Colors.white,
                          border: Border.all(
                            width: 1,
                            color: MyTheme.statusBarColor,
                            style: BorderStyle.solid,
                          ),
                        ),
                        child: Icon(
                          Icons.close,
                          color: Colors.black,
                        ),
                      ),
                      onTap: () async {
                        Get.back();
                      }),
                ),
              ),
            ]),
      ),
    ),
  );
}
