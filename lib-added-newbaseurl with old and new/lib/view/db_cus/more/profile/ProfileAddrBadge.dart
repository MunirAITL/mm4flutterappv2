import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/db_cus/more/badge/BadgeAPIMgr.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/view/db_cus/more/settings/SettingsScreen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';

import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/UserBadgeModel.dart';
import 'package:aitl/view/db_cus/more/badges/BadgeScreen.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:get/get.dart';

import '../../MainCustomerScreen.dart';
import 'EditProfileScreen.dart';

class ProfileAddrBadge extends StatefulWidget {
  const ProfileAddrBadge({Key key}) : super(key: key);

  @override
  State createState() => _ProfileAddrBadgeState();
}

class _ProfileAddrBadgeState extends State<ProfileAddrBadge> with Mixin {
  List<dynamic> listItems = [];
  List<UserBadgeModel> listUserBadgeModel = [];
  List<UserBadgeModel> listUserBadgeModelShowList = [];
  UserBadgeModel userBadgeModel;
  bool isLoading = true;

  final menuItems = [
    {'index': 0, 'icon': 'assets/images/ico/star_ico.png', 'title': 'Badges'},
    {
      'index': 1,
      'icon': 'assets/images/ico/setting_ico.png',
      'title': 'Settings'
    },
    {'index': 2, 'icon': 'assets/images/ico/logout_ico.png', 'title': 'Logout'},
  ];

  updateUserBadges() async {
    if (mounted) {
      BadgeAPIMgr().wsGetUserBadge(
        context: context,
        userId: userData.userModel.id,
        callback: (model) {
          if (model != null) {
            try {
              if (model.success) {
                listUserBadgeModel = model.responseData.userBadges;
                for (UserBadgeModel userBadge in listUserBadgeModel) {
                  if (userBadge.isVerified) {
                    listUserBadgeModelShowList.add(userBadge);
                  }
                }
                setState(() {
                  isLoading = false;
                });
              } else {
                //showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.brandColor,msg: model.errorMessages.toString());
                showToast(context: context, msg: "Sorry, something went wrong");
                setState(() {
                  isLoading = false;
                });
              }
            } catch (e) {
              setState(() {
                isLoading = false;
              });
              myLog(e.toString());
            }
          } else {
            setState(() {
              isLoading = false;
            });
            myLog("profile screen new not in");
          }
        },
      );
    }
  }

  @override
  initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    listItems = null;
    listUserBadgeModel = null;
    listUserBadgeModelShowList = null;
    userBadgeModel = null;
    super.dispose();
  }

  appInit() async {
    try {
      listItems = [
        {
          "icon": "assets/images/screens/db_cus/more/badge/phone_verified.png",
          "title": "Mobile",
          "desc":
              "You have received this badge because you're mobile number is verified.",
          // "You will receive this badge when you mobile number is verified. This will also allow you to receive Case related notifications and make calls* from the portal.",
          "btnTitle": null,
        },
        {
          "icon": "assets/images/screens/db_cus/more/badge/gmail_verified.png",
          "title": "Email",
          "desc": "You have received this badge, your email is verified.",

          // "You receive this badge upon verification of your email address. This will also allow you to receive case related notifications and send or receive emails from the portal.",
        },
        {
          "icon":
              "assets/images/screens/db_cus/more/badge/passport_verified.png",
          "title": "Passport / Photo ID",
          "desc": "You have received this badge, your ID Card is verified",
          // "You receive this badge when your ID is verified with your Passport, Driving License or any other acceptable form of ID.",
        },
        {
          "icon":
              "assets/images/screens/db_cus/more/badge/facebook_verified.png",
          "title": "Facebook",
          "desc":
              "You have received this badge, connected with Facebook account.",
          // "You receive this badge when you connect your Facebook account.",
        },
        {
          "icon":
              "assets/images/screens/db_cus/more/badge/customer_privacy_verified.png",
          "title": "Customer Privacy",
          "desc":
              "You have received this badge, Accepted Customer Privacy Statement",
          // "You receive this badge when you agree to the Customer Privacy Statement. Customer Privacy accepted at 16-Mar-2020 18:43",
        },
        {
          "icon": "assets/images/screens/db_cus/more/badge/eid_verified.png",
          "title": "Electronic ID Verification",
          "desc":
              "You have received this badge, Your ID is verified through an EID system.",
          // "You receive this badge when your ID is verified through an EID system.",
        },
        //"Application tutorial"
      ];

      updateUserBadges();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          /*appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: 0,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          //title: UIHelper().drawAppbarTitle(title: "Basic information")),
          actions: [
            IconButton(
                onPressed: () {
                  Get.to(() => EditProfileScreen()).then((value) async {
                    await userData.setUserModel();
                    setState(() {});
                  });
                },
                icon: Icon(Icons.edit)),
          ],
        ),
        body: drawLayout(),*/
          body: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onPanDown: (detail) {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child: NestedScrollView(
                  headerSliverBuilder:
                      (BuildContext context, bool innerBoxIsScrolled) {
                    return <Widget>[
                      SliverAppBar(
                        automaticallyImplyLeading: true,
                        backgroundColor: MyTheme.statusBarColor,
                        iconTheme: IconThemeData(color: Colors.white),
                        leading: IconButton(
                            onPressed: () {
                              Get.back();
                            },
                            icon: Icon(Icons.arrow_back)),
                        title: UIHelper().drawAppbarTitle(title: "Profile"),
                        centerTitle: true,
                        expandedHeight: getHP(context, 38),
                        pinned: true,
                        elevation: 0,
                        titleSpacing: 0,
                        floating: true,
                        flexibleSpace: FlexibleSpaceBar(
                          collapseMode: CollapseMode.parallax,
                          background: drawHeader(),
                        ),
                        actions: [
                          IconButton(
                              onPressed: () {
                                Get.to(() => EditProfileScreen())
                                    .then((value) async {
                                  await userData.setUserModel();
                                  setState(() {});
                                });
                              },
                              icon: Icon(Icons.edit)),
                        ],
                      ),
                      //pinned: true,
                      //snap: false,
                      //forceElevated: true,
                    ];
                  },
                  body: GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      onPanDown: (detail) {
                        FocusScope.of(context).requestFocus(new FocusNode());
                      },
                      onTap: () {
                        FocusScope.of(context).requestFocus(new FocusNode());
                      },
                      child: drawLayout())))),
    );
  }

  drawHeader() {
    return Container(
        //height: getHP(context, !isPublicUser ? h1 : h2),
        width: getW(context),
        decoration: BoxDecoration(
          //color: MyTheme.statusBarColor,
          border: Border(
            bottom: BorderSide(
              color: MyTheme.bgColor2,
              width: 10,
            ),
          ),
        ),
        child: Stack(alignment: Alignment.center, children: [
          Container(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(height: 50),
                  Container(
                    width: getWP(context, 20),
                    height: getWP(context, 20),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: Colors.white,
                        width: 5,
                      ),
                    ),
                    child: CircleAvatar(
                      radius: 20,
                      backgroundColor: Colors.transparent,
                      backgroundImage: CachedNetworkImageProvider(
                        MyNetworkImage.checkUrl(
                            userData.userModel.profileImageURL != null &&
                                    userData
                                        .userModel.profileImageURL.isNotEmpty
                                ? userData.userModel.profileImageURL
                                : Server.MISSING_IMG),
                      ),
                    ),
                    //borderRadius: BorderRadius.all(Radius.circular(20))),
                  ),
                  SizedBox(height: 10),
                  Txt(
                    maxLines: 1,
                    txt: userData.userModel.name.uFirst(),
                    txtColor: Colors.white,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: true,
                  ),
                  userData.userModel.dateCreatedLocal != ''
                      ? Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: Txt(
                            maxLines: 1,
                            txt: "Joined in " +
                                DateFun.getDate(
                                    userData.userModel.dateCreatedLocal,
                                    "MMM yyyy"),
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.start,
                            isBold: false,
                          ),
                        )
                      : SizedBox(),
                  userData.userModel.isElectronicIDVerified
                      ? Padding(
                          padding: const EdgeInsets.only(top: 5),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              CircleAvatar(
                                  radius: 8,
                                  backgroundColor: Colors.green,
                                  child: Icon(
                                    Icons.check,
                                    color: Colors.white,
                                    size: 12,
                                  )),
                              SizedBox(width: 5),
                              Txt(
                                maxLines: 1,
                                txt: "E-ID VERIFIED",
                                txtColor: Colors.green,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.start,
                                isBold: false,
                              ),
                            ],
                          ),
                        )
                      : SizedBox(),
                ],
              ),
            ),
          )
        ]));
  }

  drawLayout() {
    return Container(
      width: getW(context),
      //height: getH(context),
      child: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: ListView(
          physics: const AlwaysScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          primary: true,
          shrinkWrap: true,
          children: [
            drawBasicInfo(),
            //drawGeneralInfo(),
            drawBadges(),
            SizedBox(height: 100),
          ],
        ),
      ),
    );
  }

  drawBasicInfo() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
              txt: "Basic Information :",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              fontWeight: FontWeight.w500,
              isBold: true,
            ),
            userData.userModel.email != null
                ? userData.userModel.email.isNotEmpty
                    ? Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset(
                              "assets/images/icons/email_icon.png",
                              width: 16,
                              color: Colors.black54,
                              fit: BoxFit.cover,
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              flex: 9,
                              child: Txt(
                                txt: "${userData.userModel.email}",
                                txtColor: Colors.black54,
                                txtSize: MyTheme.txtSize - .3,
                                txtAlign: TextAlign.start,
                                isBold: false,
                                maxLines: 2,
                              ),
                            ),
                          ],
                        ),
                      )
                    : SizedBox()
                : SizedBox(),
            userData.userModel.mobileNumber != null
                ? userData.userModel.mobileNumber.isNotEmpty
                    ? Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset(
                              "assets/images/icons/mobile_icon.png",
                              width: 20,
                              color: Colors.black54,
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              flex: 9,
                              child: Txt(
                                txt: "${userData.userModel.mobileNumber}",
                                txtColor: Colors.black54,
                                txtSize: MyTheme.txtSize - .3,
                                txtAlign: TextAlign.start,
                                isBold: false,
                                maxLines: 2,
                              ),
                            ),
                          ],
                        ),
                      )
                    : SizedBox()
                : SizedBox(),
            userData.userModel.dateofBirth != null
                ? userData.userModel.dateofBirth.isNotEmpty
                    ? Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset(
                              "assets/images/icons/date_icon.png",
                              width: 20,
                              color: Colors.black54,
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              flex: 9,
                              child: Txt(
                                txt: "${userData.userModel.dateofBirth}",
                                txtColor: Colors.black54,
                                txtSize: MyTheme.txtSize - .3,
                                txtAlign: TextAlign.start,
                                isBold: false,
                              ),
                            ),
                          ],
                        ),
                      )
                    : SizedBox()
                : SizedBox(),
            userData.userModel.cohort != null
                ? userData.userModel.cohort.isNotEmpty
                    ? Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Image.asset(
                              "assets/images/icons/gender_icon.png",
                              width: 20,
                              color: Colors.black54,
                            ),
                            SizedBox(width: 10),
                            Expanded(
                              flex: 9,
                              child: Txt(
                                txt: "${userData.userModel.cohort}",
                                txtColor: Colors.black54,
                                txtSize: MyTheme.txtSize - .3,
                                txtAlign: TextAlign.start,
                                isBold: false,
                              ),
                            ),
                          ],
                        ),
                      )
                    : SizedBox()
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  /*drawGeneralInfo() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 30, right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 10, left: 10),
              child: Txt(
                txt: "General",
                txtColor: MyTheme.brandColor,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true,
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: menuItems.length,
              itemBuilder: (context, index) {
                final map = menuItems[index];
                return GestureDetector(
                  onTap: () {
                    switch (index) {
                      case 0:
                        Get.to(() => BadgeScreen());
                        break;
                      case 1:
                        Get.to(() => SettingsScreen());
                        break;
                      case 2:
                        ConfirmationAlertDialog(
                            alertBody: "Are you sure, you want to log out ?",
                            alertTitle: "Alert !",
                            deleteTxt: "Yes",
                            cancelTxt: "No",
                            deleteClick: () {
                              MainCustomerScreenState.currentTab = 0;
                              StateProvider()
                                  .notify(ObserverState.STATE_CHANGED_logout);
                              Get.back();
                            },
                            cancelClick: () {
                              Navigator.of(context, rootNavigator: true).pop();
                            },
                            context: context);
                        break;
                      default:
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Card(
                      elevation: 0,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Image.asset(
                                  map['icon'],
                                  width: 20,
                                  height: 20,
                                  color:
                                      (index != 2) ? Colors.grey : Colors.red,
                                ),
                                SizedBox(width: 20),
                                Expanded(
                                  child: Txt(
                                    txt: map['title'],
                                    txtColor: (index != 2)
                                        ? Colors.black
                                        : Colors.red,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    fontWeight: FontWeight.w500,
                                    isBold: (index != 2) ? false : true,
                                  ),
                                ),
                                (index != 2)
                                    ? Icon(
                                        Icons.arrow_forward_ios,
                                        color: Colors.grey,
                                        size: 20,
                                      )
                                    : SizedBox(),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }*/

  drawBadges() {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.all(10.0),
          // margin: EdgeInsets.all(8.0),
          color: Colors.grey[200],
          child: Row(
            // crossAxisAlignment: CrossAxisAlignment.s,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Txt(
                txt: "Badges",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true,
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                decoration: BoxDecoration(
                    color: MyTheme.bgColor2,
                    shape: BoxShape.rectangle,
                    border: Border.all(
                      width: 1,
                    )),
                child: GestureDetector(
                    onTap: () {
                      Get.to(() => BadgeScreen());
                    },
                    child: Icon(
                      Icons.add,
                      color: MyTheme.dBlueAirColor,
                      size: 20,
                    )),
              ),
              /*MMBtn(
                  txt: "Add More",
                  txtColor: Colors.white,
                  width: getWP(context, 22),
                  height: getHP(context, 5),
                  callback: () {
                    Get.to(() => BadgeScreen());
                  }),*/
            ],
          ),
        ),
        ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: listUserBadgeModelShowList.length,
          itemBuilder: (context, index) {
            var item;
            debugPrint("badge type  ${listUserBadgeModelShowList[index].type}");

            if (listUserBadgeModelShowList[index].type == "Mobile") {
              item = listItems[index];
            }
            if (listUserBadgeModelShowList[index].type == "Email") {
              item = listItems[index];
            }
            if (listUserBadgeModelShowList[index].type == "Passport") {
              item = listItems[index];
            }
            if (listUserBadgeModelShowList[index].type == "Facebook") {
              item = listItems[index];
            }
            if (listUserBadgeModelShowList[index].type == "CustomerPrivacy") {
              item = listItems[index];
            }
            if (listUserBadgeModelShowList[index].type == "NationalIDCard") {
              item = listItems[index];
            }
            if (listUserBadgeModelShowList[index].type == "ElectricId") {
              item = listItems[index];
            }
            return (item == null)
                ? SizedBox()
                : Card(
                    elevation: 0,
                    child: Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 10),
                      child: ListTile(
                        // leading: Image.asset(item['icon']),
                        title: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                                width: getWP(context, 8),
                                height: getWP(context, 8),
                                child: Image.asset(item['icon'])),
                            SizedBox(width: 10),
                            Expanded(
                              child: Txt(
                                  txt: item["title"],
                                  txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize + .4,
                                  txtAlign: TextAlign.start,
                                  isOverflow: true,
                                  isBold: false),
                            ),
                          ],
                        ),
                        subtitle: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(top: 10, bottom: 10),
                              child: Txt(
                                  txt: item["title"] == "Customer Privacy"
                                      ? item["desc"] +
                                          " at ${DateFormat('dd-MMM-yyyy').format(DateTime.parse(listUserBadgeModelShowList[index].creationDate.toString()))}"
                                      : item["desc"],
                                  txtColor: Colors.grey,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  //txtLineSpace: 1.4,
                                  isBold: false),
                            ),
                            Container(
                              color: Colors.grey[200],
                              height: 1,
                              width: getW(context),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
          },
        ),
      ],
    );
  }
}
