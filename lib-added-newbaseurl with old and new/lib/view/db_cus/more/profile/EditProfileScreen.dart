import 'dart:io';
import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/db_cus/more/settings/EditProfileAPIMgr.dart';
import 'package:aitl/controller/api/media/MediaUploadAPIMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/misc/CommonAPIModel.dart';
import 'package:aitl/view/db_cus/more/profile/EditProfileBase.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/picker/CamPicker.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../widgets/dialog/DeactivateProfileDialog.dart';

/*
/// <summary>
        /// UpdateProfilePictureAndCover
        /// </summary>
        /// <remarks>       
        ///Id is required field , please send user id which profile image to be update
        ///CoverImageId is required field when you will update cover image send media id otherwise send zero 
        ///If you want to delete cover image , then send to zero
        ///ProfileImageId is required field when you will update profile image send media id otherwise send zero
        //////If you want to delete profile image , then send to zero
        ///ImageType set value 1 for Cover and set value 2 for Profile update, this is required field, otherwise reponse will be send to failure         
        /// </remarks>
*/

class EditProfileScreen extends StatefulWidget {
  const EditProfileScreen({Key key}) : super(key: key);
  @override
  State createState() => _EditProfileState();
}

class _EditProfileState extends EditProfileBase<EditProfileScreen> {
  @override
  void initState() {
    super.initState();
    isExtAccess = false;
    updateData();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final isCoverBG =
        (MyNetworkImage.isValidUrl(userData.userModel.coverImageURL))
            ? true
            : false;
    return SafeArea(
        child: Scaffold(
            backgroundColor: MyTheme.bgColor2,
            //resizeToAvoidBottomPadding: true,
            body: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: NestedScrollView(
                    headerSliverBuilder:
                        (BuildContext context, bool innerBoxIsScrolled) {
                      return <Widget>[
                        SliverAppBar(
                          automaticallyImplyLeading: true,
                          backgroundColor: MyTheme.statusBarColor,
                          iconTheme: IconThemeData(color: Colors.white),
                          leading: IconButton(
                              onPressed: () {
                                Get.back();
                              },
                              icon: Icon(Icons.arrow_back)),
                          title: UIHelper()
                              .drawAppbarTitle(title: "Edit Your Profile"),
                          centerTitle: false,
                          expandedHeight: getHP(context, 30),
                          pinned: true,
                          elevation: 0,
                          titleSpacing: 0,
                          floating: true,
                          flexibleSpace: FlexibleSpaceBar(
                            collapseMode: CollapseMode.parallax,
                            background: drawHeader(isCoverBG),
                          ),
                        ),
                        //pinned: true,
                        //snap: false,
                        //forceElevated: true,
                      ];
                    },
                    body: GestureDetector(
                        behavior: HitTestBehavior.opaque,
                        onPanDown: (detail) {
                          FocusScope.of(context).requestFocus(new FocusNode());
                        },
                        onTap: () {
                          FocusScope.of(context).requestFocus(new FocusNode());
                        },
                        child: drawLayout())))));
  }

  drawHeader(isCoverBG) {
    return Container(
      //height: getHP(context, !isPublicUser ? h1 : h2),
      width: getW(context),
      decoration: isCoverBG
          ? BoxDecoration(
              //color: MyTheme.statusBarColor,
              image: DecorationImage(
                image: NetworkImage(
                  userData.userModel.coverImageURL,
                ),
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.7), BlendMode.dstATop),
                fit: BoxFit.cover,
              ),
            )
          : BoxDecoration(
              color: MyTheme.statusBarColor,
              border: Border(
                bottom: BorderSide(
                  color: MyTheme.bgColor2,
                  width: 10,
                ),
              ),
            ),

      child: Stack(
        alignment: Alignment.topRight,
        children: [
          Positioned(
            bottom: 90,
            child: Padding(
              padding: const EdgeInsets.only(right: 10),
              child: MMBtn(
                  txt: "Edit Cover",
                  bgColor: HexColor.fromHex("#FFFFFF").withAlpha(24),
                  width: getWP(context, 22),
                  height: getHP(context, 5),
                  radius: 0,
                  callback: () {
                    CamPicker().showCamDialog(
                      context: context,
                      isRear: false,
                      callback: (File path) {
                        if (path != null) {
                          MediaUploadAPIMgr().wsMediaUploadFileAPI(
                            context: context,
                            file: path,
                            callback: (model) async {
                              if (model != null && mounted) {
                                try {
                                  if (model.success) {
                                    coverImageId = model
                                        .responseData.images[0].id
                                        .toString();
                                    await APIViewModel().req<CommonAPIModel>(
                                        context: context,
                                        url: Server.PUT_MEDIA_PROFILE_IMAGE_URL,
                                        reqType: ReqType.Put,
                                        param: {
                                          "coverImageId": coverImageId,
                                          "profileImageId": profileImageId,
                                          "imageType": 1,
                                          "id": userData.userModel.id,
                                        },
                                        callback: (model2) async {
                                          coverImageId = "0";
                                          profileImageId = "0";
                                          if (mounted && model2 != null) {
                                            if (model2.success) {
                                              setState(() {
                                                userData.userModel
                                                        .coverImageURL =
                                                    model.responseData.images[0]
                                                        .url;
                                              });
                                              updateData();
                                            }
                                          }
                                        });

                                    //updateProfileAPI(
                                    //coverImageId: coverImageId.toString());
                                  } else {
                                    final err = model
                                        .errorMessages.upload_pictures[0]
                                        .toString();
                                    showToast(context: context, msg: err);
                                  }
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              }
                            },
                          );
                        }
                      },
                    );
                  }),
            ),
          ),
          Positioned(
              bottom: -5,
              child: Container(
                width: getW(context),
                height: getHP(context, 8),
                color: MyTheme.bgColor2,
              )),
          Stack(
            alignment: Alignment.bottomLeft,
            children: [
              Align(
                alignment: Alignment.bottomLeft,
                child: Padding(
                  padding: const EdgeInsets.only(left: 30),
                  child: Container(
                    width: getWP(context, 20),
                    height: getWP(context, 20),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: Colors.white,
                        width: 5,
                      ),
                    ),
                    child: CircleAvatar(
                      radius: 25,
                      backgroundColor: Colors.transparent,
                      backgroundImage: CachedNetworkImageProvider(
                        MyNetworkImage.checkUrl(
                            userData.userModel.profileImageURL != null &&
                                    userData
                                        .userModel.profileImageURL.isNotEmpty
                                ? userData.userModel.profileImageURL
                                : Server.MISSING_IMG),
                      ),
                    ),
                    //borderRadius: BorderRadius.all(Radius.circular(20))),
                  ),
                ),
              ),
              Positioned(
                left: getWP(context, 30) - 40,
                child: Container(
                  width: 25,
                  height: 25,
                  child: MaterialButton(
                    onPressed: () {
                      CamPicker().showCamDialog(
                        context: context,
                        isRear: false,
                        callback: (File path) {
                          if (path != null) {
                            MediaUploadAPIMgr().wsMediaUploadFileAPI(
                              context: context,
                              file: path,
                              callback: (model) async {
                                if (model != null && mounted) {
                                  try {
                                    if (model.success) {
                                      profileImageId = model
                                          .responseData.images[0].id
                                          .toString();
                                      await APIViewModel().req<CommonAPIModel>(
                                          context: context,
                                          url: Server
                                              .PUT_MEDIA_PROFILE_IMAGE_URL,
                                          reqType: ReqType.Put,
                                          param: {
                                            "coverImageId": coverImageId,
                                            "profileImageId": profileImageId,
                                            "imageType": 2,
                                            "id": userData.userModel.id,
                                          },
                                          callback: (model2) async {
                                            coverImageId = "0";
                                            profileImageId = "0";
                                            if (mounted && model2 != null) {
                                              if (model2.success) {
                                                setState(() {
                                                  userData.userModel
                                                          .profileImageURL =
                                                      model.responseData
                                                          .images[0].url;
                                                });
                                                updateData();
                                              }
                                            }
                                          });
                                    } else {
                                      final err = model
                                          .errorMessages.upload_pictures[0]
                                          .toString();
                                      showToast(context: context, msg: err);
                                    }
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                }
                              },
                            );
                          }
                        },
                      );
                    },
                    color: HexColor.fromHex("#4F9DAA"),
                    child: Icon(
                      Icons.camera_alt_outlined,
                      size: 15,
                    ),
                    padding: EdgeInsets.all(0),
                    shape: CircleBorder(),
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: getWP(context, 35)),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Txt(
                          txt: "Personal\nInformation",
                          txtColor: MyTheme.titleColor,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: true),
                    ),
                    SizedBox(width: 10),
                    Flexible(
                        child: Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child:
                          Image.asset("assets/images/ico/checked_blue_ico.png"),
                    )),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  drawLayout() {
    return Container(
      child: (isLoading)
          ? SizedBox()
          : Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: ListView(
                shrinkWrap: true,
                children: [
                  SizedBox(height: 10),
                  drawPersonalInfoView(),
                  SizedBox(height: 40),
                  drawVisaInfoView(),
                  SizedBox(height: 40),
                  GestureDetector(
                    onTap: () {
                      updateProfileAPI(
                          profileImageId: profileImageId.toString(),
                          coverImageId: coverImageId.toString());
                    },
                    child: Container(
                      width: getW(context),
                      height: getHP(context, 7),
                      alignment: Alignment.center,
                      //color: MyTheme.brownColor,
                      decoration: new BoxDecoration(
                        color: MyTheme.titleColor,
                        borderRadius: new BorderRadius.circular(10),
                      ),
                      child: Txt(
                        txt: "Save Profile",
                        txtColor: Colors.white,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.center,
                        isBold: false,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 60),
                    child: Container(
                      decoration: new BoxDecoration(
                        color: MyTheme.dRedColor,
                        borderRadius: new BorderRadius.circular(10),
                      ),
                      width: getW(context),
                      height: getHP(context, 7),
                      child: new ElevatedButton(
                          child: Txt(
                              txt: "Deactivate My Account",
                              txtColor: Colors.white,
                              txtSize: MyTheme.txtSize,
                              txtAlign: TextAlign.center,
                              isBold: false),
                          onPressed: () {
                            showDeactivateProfileDialog(
                                context: context,
                                email: deactivateReason,
                                callback: (String reason) {
                                  //
                                  if (reason != null) {
                                    EditProfileAPIMgr().wsDeactivateProfileAPI(
                                      context: context,
                                      reason: reason,
                                      callback: (model) {
                                        if (model != null && mounted) {
                                          try {
                                            if (model.success) {
                                              try {
                                                final msg =
                                                    "Your account has been deleted.\nYou can always sign up again."; //model
                                                //.messages.delete[0]
                                                //.toString();
                                                showToast(
                                                    context: context,
                                                    msg: msg,
                                                    which: 1);
                                                Future.delayed(
                                                    Duration(
                                                        seconds: AppConfig
                                                            .AlertDismisSec),
                                                    () {
                                                  //  signout
                                                  StateProvider().notify(
                                                      ObserverState
                                                          .STATE_CHANGED_logout);
                                                });
                                              } catch (e) {
                                                myLog(e.toString());
                                              }
                                            } else {
                                              try {
                                                if (mounted) {
                                                  final err = model
                                                      .messages.delete[0]
                                                      .toString();
                                                  showToast(
                                                      context: context,
                                                      msg: err);
                                                }
                                              } catch (e) {
                                                myLog(e.toString());
                                              }
                                            }
                                          } catch (e) {
                                            myLog(e.toString());
                                          }
                                        }
                                      },
                                    );
                                  }
                                });
                          },
                          style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                Colors.transparent,
                              ),
                              elevation: MaterialStateProperty.all<double>(0),
                              shape: MaterialStateProperty.all(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(10),
                                      ),
                                      side: BorderSide(
                                          color: Colors.white, width: 1))))),
                    ),
                  ),
                  SizedBox(height: 50),
                ],
              ),
            ),
    );
  }
}
