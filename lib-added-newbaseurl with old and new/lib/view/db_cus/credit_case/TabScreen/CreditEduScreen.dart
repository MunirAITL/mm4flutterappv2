import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:pluto_menu_bar/pluto_menu_bar.dart' as item;
import 'base/CreditEduBase.dart';

class CreditEduScreen extends StatefulWidget {
  const CreditEduScreen({Key key}) : super(key: key);
  @override
  State createState() => _CreditEduScreenState();
}

class _CreditEduScreenState extends CreditEduBase<CreditEduScreen> {
  //dynamic callback;

  String title = "Credit Report Information";
  final url = "https://mortgage-magic.co.uk/apps/credit-education-report";

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    cookieStr = null;

    //webView = null;
    super.dispose();
  }

  appInit() async {
    try {
      /*CookieJar cj = await CookieMgr().getCookiee();
      final listCookies = cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();*/

      CookieJar cj = await CookieMgr().getCookiee();
      final listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();
      if (cookieStr.length > 0) {
        cookieManager.setCookie(
          url: Uri.parse(Server.BASE_URL),
          name: listCookies[0].name,
          value: listCookies[0].value,
          domain: listCookies[0].domain,
          path: listCookies[0].path,
          maxAge: listCookies[0].maxAge,
          //expiresDate: listCookies[0].expires.,
          isSecure: true,
        );
        setState(() {});
      }
    } catch (e) {
      cookieStr = "";
      if (mounted) setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        bottomNavigationBar: BottomAppBar(
          child: item.PlutoMenuBar(
            textStyle: TextStyle(color: MyTheme.brandColor, fontSize: 12),
            menuIconColor: MyTheme.brandColor,
            menuIconSize: 20,
            menus: [
              item.MenuItem(
                title: 'Credit Basic',
                icon: Icons.arrow_drop_up,
                children: [
                  item.MenuItem(
                    title: 'Credit Report',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://mortgage-magic.co.uk/apps/credit-education-report")));
                    },
                  ),
                  item.MenuItem(
                    title: 'Credit Score Recipe',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://mortgage-magic.co.uk/apps/credit-score-recipe")));
                    },
                  ),
                  item.MenuItem(
                    title: 'Credit Myths and Misconceptions',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-misconceptions")));
                    },
                  ),
                  item.MenuItem(
                    title: 'Getting Credit',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-how-to-get-credit")));
                    },
                  ),
                ],
              ),
              item.MenuItem(
                title: 'Managing Your Money',
                icon: Icons.arrow_drop_up,
                children: [
                  item.MenuItem(
                    title: 'Fixing your financial past',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-fixing-mistakes")));
                    },
                  ),
                  item.MenuItem(
                    title: 'Budgeting Tips',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-budgeting-tips")));
                    },
                  ),
                  item.MenuItem(
                    title: 'Savings',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-savings")));
                    },
                  ),
                ],
              ),
              item.MenuItem(
                title: 'Life Events',
                icon: Icons.arrow_drop_up,
                children: [
                  item.MenuItem(
                    title: 'Do Financial Management Reviews',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-planning")));
                    },
                  ),
                ],
              ),
              item.MenuItem(
                title: 'Identity and Safety',
                icon: Icons.arrow_drop_up,
                children: [
                  item.MenuItem(
                    title: 'Cifas: The UKs leading fraud prevention service',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-cifas")));
                    },
                  ),
                  item.MenuItem(
                    title: 'Powerful Ways to Protect Against Identity Theft',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-identity-theft-protection")));
                    },
                  ),
                  item.MenuItem(
                    title: 'Spotting Identity Theft',
                    onTap: () {
                      webViewController.loadUrl(
                          urlRequest: URLRequest(
                              url: Uri.parse(
                                  "https://app.mortgage-magic.co.uk/apps/credit-spotting-identity-theft")));
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
        body: Stack(
          children: <Widget>[
            InAppWebView(
              gestureRecognizers: Set()
                ..add(Factory<VerticalDragGestureRecognizer>(
                    () => VerticalDragGestureRecognizer())),
              initialUrlRequest: URLRequest(url: Uri.parse(url)),
              initialOptions: options,
              onWebViewCreated: (controller) {
                webViewController = controller;
              },
              onLoadStart: (controller, url) {
                //setState(() {
                //this.url = url.toString();
                isLoading = true;
                //urlController.text = this.url;
                //});
              },
              androidOnPermissionRequest:
                  (controller, origin, resources) async {
                return PermissionRequestResponse(
                    resources: resources,
                    action: PermissionRequestResponseAction.GRANT);
              },
              shouldOverrideUrlLoading: (controller, navigationAction) async {
                return NavigationActionPolicy.ALLOW;
              },
              onLoadStop: (controller, url) async {
                //setState(() {
                //this.url = url.toString();
                isLoading = false;

                //urlController.text = this.url;
                //});
              },
              onLoadError: (controller, url, code, message) {},
              onProgressChanged: (controller, progress) {
                if (progress == 100) {}
                setState(() {
                  this.progress = progress / 100;
                  //urlController.text = this.url;
                });
              },
              onConsoleMessage: (controller, consoleMessage) {
                print(consoleMessage);
              },
            ),
            isLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Stack(),
          ],
        ));
  }
}
