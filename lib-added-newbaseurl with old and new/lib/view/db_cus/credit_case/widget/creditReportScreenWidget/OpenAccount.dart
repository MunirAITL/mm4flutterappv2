import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/view/db_cus/credit_case/AlertDialog/AlertDialogCreditCardDetails.dart';
import 'package:aitl/view/db_cus/credit_case/CreditDashboardTabController.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OpenAccounts extends StatefulWidget {
  @override
  State<OpenAccounts> createState() => _OpenAccountsState();
}

class _OpenAccountsState extends State<OpenAccounts> with Mixin {
  CreditDashBoardReport creditDashBoardReport;

  @override
  Widget build(BuildContext context) {
    creditDashBoardReport =
        CreditDashBoardTabControllerState.creditDashBoardReport;

    return Container(
      child: Column(
        children: [
          SizedBox(height: 15),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: getW(context),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Txt(
                        txt: "LENDER",
                        txtColor: Colors.black87,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: true),
                  ),
                  Flexible(
                    child: Txt(
                        txt: "BALANCE",
                        txtColor: Colors.black87,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: true),
                  ),
                  Flexible(
                    child: Txt(
                        txt: "UPDATE",
                        txtColor: Colors.black87,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: true),
                  ),
                  Flexible(
                    child: Txt(
                        txt: "STATUS",
                        txtColor: Colors.black87,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: true),
                  ),
                  Flexible(child: SizedBox(width: getWP(context, 5))),
                ],
              ),
            ),
          ),
          drawAccountTypeItems(["CreditCards"], "Credit Cards"),
          drawAccountTypeItems(
              ["PersonalLoans", "Mortgages"], "Personal loans and mortgages"),
          drawAccountTypeItems(["OtherAccounts"], "Other accounts"),
        ],
      ),
    );
  }

  drawAccountTypeItems(List<String> listAccountType, String title) {
    if (creditDashBoardReport == null) return SizedBox();
    return Container(
      child: Column(
        children: [
          Container(
            width: getW(context),
            decoration: BoxDecoration(color: Colors.grey),
            child: Padding(
              padding: const EdgeInsets.all(5),
              child: Txt(
                  txt: title,
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
          ),
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemCount: creditDashBoardReport.cRBankAccountList.length,
            itemBuilder: (context, index) {
              final model = creditDashBoardReport.cRBankAccountList[index];

              bool isReturn = false;
              if (!listAccountType.contains(model.accountType)) {
                isReturn = true;
              }

              return (isReturn)
                  ? SizedBox()
                  : Container(
                      margin: EdgeInsets.only(top: 5, bottom: 5),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black87),
                          borderRadius: BorderRadius.all(
                            Radius.circular(7),
                          )),
                      child: ExpansionTile(
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Txt(
                                  txt: model.lenderName ?? '',
                                  txtColor: Colors.black87,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                            Flexible(
                              child: Txt(
                                  txt: DateFun.getDate(
                                      model.updatedDate, "dd MMM yyyy"),
                                  txtColor: Colors.black87,
                                  txtSize: MyTheme.txtSize - .3,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                            Flexible(
                              child: Txt(
                                  txt: AppDefine.CUR_SIGN +
                                      (double.parse(model.balance.toString())
                                              .round()
                                              .toString() ??
                                          '0'),
                                  txtColor: Colors.black87,
                                  txtSize: MyTheme.txtSize - .3,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                            Flexible(
                              child: Txt(
                                  txt: model.cRBankAccountStatus,
                                  txtColor: Colors.black87,
                                  txtSize: MyTheme.txtSize - .3,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                          ],
                        ),
                        iconColor: Colors.black,
                        collapsedIconColor: Colors.black,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              child: Column(
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.end,
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              left: 10, right: 10),
                                          child: Column(
                                            children: [
                                              userInfoItem(
                                                  title: "Name",
                                                  value: model.name ?? ''),
                                              userInfoItem(
                                                  title: "Address",
                                                  value: model.accountAddress ??
                                                      ''),
                                              userInfoItem(
                                                  title: "Date of birth",
                                                  value: DateFun.getDate(
                                                      model.dateOfBirth,
                                                      "dd/MM/yyyy")),
                                              userInfoItem(
                                                  title: "Account type",
                                                  value:
                                                      model.accountType ?? ''),
                                              userInfoItem(
                                                  title: "Account number",
                                                  value: model.accountNumber ??
                                                      ''),
                                              userInfoItem(
                                                  title: "Account start date",
                                                  value: DateFun.getDate(
                                                      model.accountStartDate,
                                                      "dd/MM/yyyy")),
                                              userInfoItem(
                                                  title: "Account end date",
                                                  value: DateFun.getDate(
                                                      model.accountEndDate,
                                                      "dd/MM/yyyy")),
                                              userInfoItem(
                                                  title: "Payment start date",
                                                  value: DateFun.getDate(
                                                      model.paymentStartDate,
                                                      "dd/MM/yyyy")),
                                              userInfoItem(
                                                  title: "Repayment frequency",
                                                  value: model
                                                          .repaymentFrequency ??
                                                      ''),
                                              SizedBox(height: 20),
                                            ],
                                          ),
                                        ),
                                        /*Container(
                width: MediaQuery.of(context).size.width,
                child: Btn(
                    txt: "Ok",
                    txtColor: Colors.white,
                    bgColor: MyTheme.brandColor,
                    width: 100,
                    height: 50,
                    callback: () {
                      Get.back();
                    }),
              )*/
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    );
            },
          ),
        ],
      ),
    );
  }

  userInfoItem({String title, String value}) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Txt(
              txt: "$title: ",
              txtColor: Colors.black87,
              txtSize: MyTheme.txtSize - .4,
              txtAlign: TextAlign.start,
              isBold: true),
          Expanded(
              child: Txt(
                  txt: "$value",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .4,
                  txtAlign: TextAlign.start,
                  isBold: false))
        ],
      ),
    );
  }
}
