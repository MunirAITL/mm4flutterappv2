import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class MODA extends StatelessWidget with Mixin {
  const MODA({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 10,
        child: Container(
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Container(
                width: getW(context),
                child: Txt(
                    txt: "MODA",
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize + .2,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                width: getW(context),
                child: Txt(
                    txt:
                        "MODA is a database of credit account information held by TransUnion and is updated more timely. MODA displays recent important events affecting credit files. Therefore, lenders will be able to assess credit risk, affordability and fraud risk more timely and accurately. If available, MODA data will be sent to TransUnion and updated daily."
                        " \n\nNote that accounts shown in this section will also appear in the Financial Account Information section. This allows lenders to view daily updates for a short period of time (accounts are retained in the MODA section for 50 days), while viewing information on a longer-term basis with financial account information (accounts are retained for six years)."
                        " \n\nThere is no MODA data on your report.",
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize - .2,
                    txtAlign: TextAlign.start,
                    isBold: false),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ));
  }
}
