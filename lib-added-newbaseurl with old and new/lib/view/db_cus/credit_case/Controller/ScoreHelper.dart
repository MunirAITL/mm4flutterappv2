import 'package:flutter/material.dart';

class ScoreHelper {
  Color getCircleBarColor(int score) {
    return score > 627
        ? Color(0XFF128B49)
        : score < 628 && score > 603
            ? Color(0XFF55A84E)
            : score < 604 && score > 565
                ? Color(0XFFFAD937)
                : score < 566 && score > 510
                    ? Color(0XFFF17731)
                    : Color(0XFFC41C2B);
  }

  String getCircleStatus(int score) {
    return score > 627
        ? "Excellent"
        : score < 628 && score > 603
            ? "Good"
            : score < 604 && score > 565
                ? "Fair"
                : score < 566 && score > 550
                    ? "Poor"
                    : "Very Poor";
  }
}
