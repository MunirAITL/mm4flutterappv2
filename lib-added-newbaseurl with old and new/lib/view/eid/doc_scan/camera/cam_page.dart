import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
//import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'cam_base.dart';

import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';

class CamPage extends StatefulWidget {
  final bool isFront;
  final String title;
  const CamPage({Key key, @required this.isFront, @required this.title})
      : super(key: key);
  @override
  State createState() {
    return _CamPageState();
  }
}

class _CamPageState extends CamBase<CamPage> {
  initPage() async {
    try {
      final cameras = await availableCameras();
      WidgetsBinding.instance.addPostFrameCallback((_) {
        if (cameras.isEmpty) {
          return const Text('No camera found');
        } else {
          CameraDescription newDescription;
          if (widget.isFront) {
            newDescription = cameras.firstWhere((description) =>
                description.lensDirection == CameraLensDirection.front);
          } else {
            newDescription = cameras.firstWhere((description) =>
                description.lensDirection == CameraLensDirection.back);
          }
          onNewCameraSelected(newDescription);
        }
        setState(() {});
      });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    initPage();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    controller.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // App state changed before we got the chance to initialize.
    if (controller == null || !controller.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      controller?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      if (controller != null) {
        onNewCameraSelected(controller.description);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        //automaticallyImplyLeading: false,
        iconTheme: IconThemeData(color: Colors.black //change your color here
            ),
        elevation: MyTheme.appbarElevation,
        backgroundColor: Colors.white,
        title: Txt(
            txt: widget.title,
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.start,
            isBold: true),
        centerTitle: false,
      ),
      body: controller != null
          ? controller.value.isInitialized
              ? Column(
                  children: <Widget>[
                    Container(
                      child: Expanded(
                        child: Stack(
                          children: [
                            Positioned.fill(
                                child: AspectRatio(
                                    aspectRatio: controller.value.aspectRatio,
                                    child: CameraPreview(controller))),
                            Positioned.fill(
                              child: new Image.asset(
                                widget.isFront
                                    ? 'assets/images/doc_scan/cam_face_bg.png'
                                    : 'assets/images/doc_scan/cam_card_bg.png',
                                fit: BoxFit.fill,
                              ),
                            ),
                            Positioned(
                              top: 0,
                              right: 0,
                              child: flashButton(),
                            ),
                            Positioned(
                              left: getWP(context, 50) - 25,
                              bottom: 100,
                              child: IconButton(
                                  icon: const Icon(Icons.camera_alt),
                                  color: MyTheme.statusBarColor,
                                  iconSize: 60,
                                  onPressed: () {
                                    if (controller != null &&
                                        controller.value.isInitialized)
                                      onTakePictureButtonPressed();
                                  }),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              : SizedBox()
          : SizedBox(),
    );
  }
}
