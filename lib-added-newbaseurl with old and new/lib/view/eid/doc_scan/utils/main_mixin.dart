import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:path_provider/path_provider.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
//import 'package:image/image.dart' as IMG;

//import 'package:mypkg/image_processing/src/image.dart' as Image;

mixin MainMixin {
  double w(context, double p) => (p != null)
      ? MediaQuery.of(context).size.width * p / 100
      : MediaQuery.of(context).size.width;
  double h(context, double p) => (p != null)
      ? MediaQuery.of(context).size.height * p / 100
      : MediaQuery.of(context).size.height;

  Future<File> getImageFileFromAssets(File assetImage) async {
    final byteData = await rootBundle.load('assets/$assetImage.path');
    final file = File(
        '${(await getApplicationDocumentsDirectory()).path}/$assetImage.path');
    await file.writeAsBytes(byteData.buffer
        .asUint8List(byteData.offsetInBytes, byteData.lengthInBytes));
    return file;
  }

  Future<Image> convertFileToImage(File picture) async {
    List<int> imageBase64 = picture.readAsBytesSync();
    String imageAsString = base64Encode(imageBase64);
    Uint8List uint8list = base64.decode(imageAsString);
    Image image = Image.memory(uint8list);
    return image;
  }

  /*Uint8List resizeImage({Uint8List data, int width, int height}) {
    Uint8List resizedData = data;
    IMG.Image img = IMG.decodeImage(data);
    IMG.Image resized = IMG.copyResize(img, width: width, height: height);
    resizedData = IMG.encodeJpg(resized);
    return resizedData;
  }

  /// Returns a cropped copy of [src].
  IMG.Image copyCrop2(IMG.Image src, int x, int y, int w, int h) {
    IMG.Image dst = IMG.Image(w, h,
        channels: src.channels, exif: src.exif, iccp: src.iccProfile);
    for (int yi = 0, sy = y; yi < h; ++yi, ++sy) {
      for (int xi = 0, sx = x; xi < w; ++xi, ++sx) {
        dst.setPixel(xi, yi, src.getPixel(sx, sy));
      }
    }
    return dst;
  }*/

  Future<String> doGoogleVisionOCR(File file) async {
    try {
      String base64Image = base64Encode(file.readAsBytesSync());
      String body =
          """{
  'requests': [
    {
      'image': {
        'content':  '$base64Image'        
      },
      'features': [
        {
          'type': 'TEXT_DETECTION'
        }
      ]
    }
  ]
}""";

      http.Response res = await http.post(
          Uri.parse("https://vision.googleapis.com/v1/images:annotate?key=" +
              "AIzaSyBDm53kJqyuTaGgcs0fhEW3MQ7ntSXMrto"),
          body: body);
      //print(res.body);
      //myLog(json.encode(json.decode("${res.body}")));

      String gbody = res.body;
      const start = '"description": "';
      const end = '",';
      final startIndex = gbody.indexOf(start);
      final endIndex = gbody.indexOf(end, startIndex + start.length);
      gbody = gbody.substring(startIndex + start.length, endIndex);
      return gbody;
    } catch (e) {
      print(e.toString());
    }
    return null;
  }
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}
