import 'package:aitl/view_model/generic/enum_gen.dart';

import '../Server.dart';

//  0HYPN0 = -
//  0AND0 = &
//  0SPLASH0 = /
enum eCaseTitle {
  Residential_Mortgage,
  Residential_Remortgage,
  Second_Charge_0HYPN0_Residential,
  Buy_to_Let_Mortgage,
  Buy_to_Let_Remortgage,
  Business_Lending,
  Second_Charge_0HYPN0_Buy_to_Let_0AND0_Commercial,
  Commercial_Mortgages0SPLASH0_Loans,
  Development_Finance,
  Let_to_Buy,
  Equity_Release,
  Bridging_Loan,
  General_Insurance,
}

class NewCaseCfg {
  static const int ALL = 901;
  static const int IN_PROGRESS = 902;
  static const int SUBMITTED = 903;
  static const int FMA_SUBMITTED = 904;
  static const int COMPLETED = 905;

  static const caseStatusCfg = {
    0: "",
    101: "FactFind Submitted",
    117: "Recommendation",
    102: "FMA Submitted",
    103: "Case Completed",
    903: "Factfind Ongoing",
    114: "AIP Obtained",
    115: "Valuation Instructed",
    112: "Valuation Satisfied",
    113: "Case Offered",
    104: "Case Cancelled",
    116: "Case Declined",
  };

  static List<String> listSliderImages = [
    Server.DOMAIN + "/assets/img/slider/1.jpg",
    Server.DOMAIN + "/assets/img/slider/2.jpg",
    Server.DOMAIN + "/assets/img/slider/3.jpg",
  ];

  static getEnumCaseTitle(eCaseTitle e) {
    //  0HYPN0 = -
    //  0AND0 = &
    //  0SPLASH0 = /
    return EnumGen.getEnum2Str(e)
        .replaceAll("0HYPN0", "-")
        .replaceAll("0AND0", "&")
        .replaceAll("0SPLASH0", "/");
  }

  static final List<Map<String, dynamic>> listCreateNewCase = [
    {
      "index": 0,
      "title": getEnumCaseTitle(eCaseTitle.Residential_Mortgage),
      "url": "assets/images/screens/db_cus/new_case/ic_case_1.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": residentialMortgageSubList,
    },
    {
      "index": 1,
      "title": getEnumCaseTitle(eCaseTitle.Residential_Remortgage),
      "url": "assets/images/screens/db_cus/new_case/ic_case_2.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": residentialRemortgageSubList,
    },
    {
      "index": 2,
      "title": getEnumCaseTitle(eCaseTitle.Second_Charge_0HYPN0_Residential),
      "url": "assets/images/screens/db_cus/new_case/ic_case_3.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": secondChargeResidentialSubList,
    },
    {
      "index": 3,
      "title": getEnumCaseTitle(eCaseTitle.Buy_to_Let_Mortgage),
      "url": "assets/images/screens/db_cus/new_case/ic_case_4.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": buyToLetMortgageSubList,
    },
    {
      "index": 4,
      "title": getEnumCaseTitle(eCaseTitle.Buy_to_Let_Remortgage),
      "url": "assets/images/screens/db_cus/new_case/ic_case_5.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": buyToLetRemortgageSubList,
    },
    {
      "index": 5,
      "title": getEnumCaseTitle(eCaseTitle.Business_Lending),
      "url": "assets/images/screens/db_cus/new_case/ic_case_6.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": businessLendingSubList,
    },
    {
      "index": 6,
      "title": getEnumCaseTitle(
          eCaseTitle.Second_Charge_0HYPN0_Buy_to_Let_0AND0_Commercial),
      "url": "assets/images/screens/db_cus/new_case/ic_case_7.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": secondChargeBuyToLetCommercialSubList,
    },
    {
      "index": 7,
      "title": getEnumCaseTitle(eCaseTitle.Commercial_Mortgages0SPLASH0_Loans),
      "url": "assets/images/screens/db_cus/new_case/ic_case_8.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": commercialMortgagesOrLoansSubList,
    },
    {
      "index": 8,
      "title": getEnumCaseTitle(eCaseTitle.Development_Finance),
      "url": "assets/images/screens/db_cus/new_case/ic_case_9.png",
      "isOtherApplicant": true,
      "isSPV": true,
      "subItem": developmentFinanceSubList,
    },
    {
      "index": 9,
      "title": getEnumCaseTitle(eCaseTitle.Let_to_Buy),
      "url": "assets/images/screens/db_cus/new_case/ic_case_10.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": letToBuySubList,
    },
    {
      "index": 10,
      "title": getEnumCaseTitle(eCaseTitle.Equity_Release),
      "url": "assets/images/screens/db_cus/new_case/ic_case_11.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": equeityReleaseSubList,
    },
    {
      "index": 11,
      "title": getEnumCaseTitle(eCaseTitle.Bridging_Loan),
      "url": "assets/images/screens/db_cus/new_case/ic_case_12.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": bridgingLoanSubList,
    },
    {
      "index": 12,
      "title": getEnumCaseTitle(eCaseTitle.General_Insurance),
      "url": "assets/images/screens/db_cus/new_case/ic_case_13.png",
      "isOtherApplicant": true,
      "isSPV": false,
      "subItem": generalInsuranceSubList,
    },
  ];
  //
  static final List<Map<String, dynamic>> generalInsuranceSubList = [
    {
      "index": 0,
      "title": "Buildings & Contents",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "Building Only",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 2,
      "title": "Contents Only",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> bridgingLoanSubList = [
    {
      "index": 0,
      "title": "Auction Purchase",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "Standard Bridging Loan",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 2,
      "title": "Semi Commercial Bridging Loan",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 3,
      "title": "Commercial Bridging Loan",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 4,
      "title": "Regulated Bridging Loan",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 5,
      "title": "Structured Short Term Finance",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> equeityReleaseSubList = [
    {
      "index": 0,
      "title": "Equity Release",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "Home Reversion",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> residentialMortgageSubList = [
    {
      "index": 0,
      "title": "Home Mover",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "First time buyer",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 2,
      "title": "Help to Buy Mortgage",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 3,
      "title": "Right to Buy",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": true,
    },
    {
      "index": 4,
      "title": "Shared Ownership",
      "url": "assets/images/screens/db_cus/new_case/ic_case_5.png",
      "isOtherApplicant": true,
      "isSPV": true,
    },
  ];
  static final List<Map<String, dynamic>> residentialRemortgageSubList = [
    {
      "index": 0,
      "title": "Right to Buy",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "Shared Ownership",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 2,
      "title": "Standard Remortgage",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> secondChargeResidentialSubList = [];

  static final List<Map<String, dynamic>> buyToLetMortgageSubList = [
    {
      "index": 0,
      "title": "Buy to Let Mortgage",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "First time Landlord",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 2,
      "title": "Consumer buy to Let",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> buyToLetRemortgageSubList = [
    {
      "index": 0,
      "title": "Experienced Landlord",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "Consumer buy to Let",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> businessLendingSubList = [];
  static final List<Map<String, dynamic>>
      secondChargeBuyToLetCommercialSubList = [];
  static final List<Map<String, dynamic>> commercialMortgagesOrLoansSubList =
      [];

  static final List<Map<String, dynamic>> developmentFinanceSubList = [
    {
      "index": 0,
      "title": "Full Development Project",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 1,
      "title": "Conversion Project",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 2,
      "title": "Heavy Refurbishment",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
    {
      "index": 3,
      "title": "Light Refurbishment",
      "url": "",
      "isOtherApplicant": true,
      "isSPV": false,
    },
  ];
  static final List<Map<String, dynamic>> letToBuySubList = [];

  addOtherTitleInCase() {
    if (listCreateNewCase.length == 9) {
      listCreateNewCase.add({
        "index": 9,
        "title": "Others",
        "url": "assets/images/screens/db_cus/new_case/ic_case_10.png",
        "isOtherApplicant": false,
        "isSPV": false,
        "subItem": "",
      });
    } else {
      listCreateNewCase[9] = {
        "index": 9,
        "title": "Others",
        "url": "assets/images/screens/db_cus/new_case/ic_case_10.png",
        "isOtherApplicant": false,
        "isSPV": false,
        "subItem": "",
      };
    }
  }
}
