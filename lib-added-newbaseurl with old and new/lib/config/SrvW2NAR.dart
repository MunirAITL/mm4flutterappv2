import 'package:aitl/config/Server.dart';

class SrvW2NAR {
  static String GET_MORTCASEINFO_BYTASKID_URL = Server.BASE_URL +
      "/api/mortgagecaseinfo/getmortgagecaseinfowithlocationbytaskid?TaskId=";

  static String POST_ACCEPT_CLIENT_AGREEMENT_URL =
      Server.BASE_URL + "/api/mortgagecaseinfo/acceptclientagreement";
  static String GET_ACCEPTED_CLIENT_AGREEMENT_EMAIL_URL =
      Server.BASE_URL + "/api/users/sendclientagrementacceptedemail?";
}
