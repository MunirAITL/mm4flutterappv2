import 'Server.dart';

class SrvW2NMisc {
  static final listUserType = [
    {
      'title': 'Advisor',
      'communityId': [9, 18],
    },
    {
      'title': 'Introducer',
      'communityId': [2, 19]
    },
    {
      'title': 'Admin',
      'communityId': [5]
    },
  ];

  //  Case User Type -> Advisor, Introducer and Admin
  static String GET_CASE_USERTYPE_DETAILS_URL = Server.BASE_URL +
      "/api/taskbidding/get?IsAll=true&count=15&page=0&taskId=";

  static String PUT_INTRODUCER_PERMISSION_URL =
      Server.BASE_URL + "/api/taskbidding/post/setintroducerpermission";
}
