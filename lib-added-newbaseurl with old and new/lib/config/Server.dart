class Server {
  static const isTest = false;
  static const bool isOtp = true; // if you want otp skip than set to false

  static String BASE_URL;
  Server(bool isFMServer) {
    BASE_URL = (isTest)
        ? "https://app.mortgage-magic.co.uk" //"https://supportteam.mortgage-magic.co.uk" //"https://app.mortgage-magic.co.uk"
        : !isFMServer
            ? "https://mortgage-magic.co.uk"
            : "https://enm.financemagic.co.uk";

    //LOGIN_URL = BASE_URL + "/api/authentication/login";
  }

  static String MISSING_IMG =
      BASE_URL + "/api/content/media/default_avatar.png";

  //  Device Info & APNS Stuff
  static String FCM_DEVICE_INFO_URL = BASE_URL + "/api/fcmdeviceinfo/post";

  //  sendemailandnotification
  static String CASE_EMAI_NOTI_GET_URL =
      BASE_URL + "/api/task/sendemailandnotification/#caseId#";

  static String MORTGAGE_CASEINFO_LOCATIONTASKUSERID_GET = BASE_URL +
      "/api/mortgagecaseinfo/getmortgagecaseinfowithlocationbytaskidanduserid?TaskId=#caseId#&UserId=#userId#";

  //  Company Account
  static String COMP_ACC_GET_URL =
      BASE_URL + "/api/users/get/getcompanyaccountsbyuserid?UserId=#userId#";

  //  Login
  static String LOGIN_URL = BASE_URL + "/api/authentication/login";
  static String GOOGLE_LOGIN_URL = BASE_URL + "/api/authentication/logingoogle";

  //  Login by Mobile OTP
  static String LOGIN_MOBILE_OTP_POST_URL = BASE_URL + "/api/userotp/post";
  static String SEND_OTP_NOTI_URL =
      BASE_URL + "/api/userotp/sendotpnotification?otpId=#otpId#";
  static String LOGIN_MOBILE_OTP_PUT_URL = BASE_URL + "/api/userotp/put";
  static String LOGIN_REG_OTP_FB_URL =
      BASE_URL + "/api/authentication/loginregistrationfacebookmobile";

  //  Login by FB Native

  //  Login by Gmail Native

  //  Login by Apple Native

  // case review
  static String CASE_REVIEW = BASE_URL +
      "/api/mortgagecaseinfo/getbyuseridandcompanyidandclientagreementstatus?UserId=#UserId#&UserCompanyInfoId=#UserCompanyInfoId#&ClientAgreementStatus=#ClientAgreementStatus#";
  static String TERMS_POLICY = BASE_URL +
      "/api/termsprivacynoticesetup/getbycompanyid?UserCompanyId=#UserCompanyId#";

  //  review rating
  static String REVIEW_RATING_FORMSETUP_GET_URL = BASE_URL +
      "/api/reviewratingformsetup/getbycompanyid?UserCompanyId=#userCompanyId#";

  static String REVIEW_RATING_FORMSETUP_POST_URL =
      BASE_URL + "/api/userrating/post";

  //  Forgot
  static String FORGOT_URL = BASE_URL + "/api/authentication/forgotpassword";

  // Change password
  static String CHANGE_PWD_URL = BASE_URL + "/api/users/put/change-password";

  //  EmailVerify
  static String EMAILVERIFY_URL = BASE_URL + "/api/authentication/verifyemail";

  //  Register
  static String REG_URL = BASE_URL + "/api/authentication/register";

  //  Deactivate Profile
  static String DEACTIVATE_PROFILE_URL =
      BASE_URL + "/api/users/deactivebyuserownerbyreason";

  //  Dashboard Action Alert and Cases
  static String USERNOTEBYENTITY_URL = BASE_URL +
      "/api/usernote/getusernotebyentityidandentitynameanduseridandstatus?EntityId=#entityId#&EntityName=#entityName#&Status=#status#&UserId=#userId#";

  //  User Note Popup Put on Done
  static String USERNOTE_PUT_URL = BASE_URL + "/api/usernote/put";

  //  New Case
  // static String NEWCASE_URL = BASE_URL + "/api/task/taskinformationbysearch" + "/get?SearchText=&Distance=50&Location=Dhaka,%20Bangladesh&InPersonOrOnline=0&FromPrice=50&ToPrice=100000&IsHideAssignTask=0&Latitude=23.810469&Longitude=90.412918&UserId=#userId#&Page=#page#&Count=#count#&Status=#status#&UserCompanyId=#userCompanyId#";
  static String NEWCASE_URL = BASE_URL +
      "/api/task/taskinformationbysearchformobileapplication" +
      "/get?SearchText=&Distance=50&Location=London, UK&InPersonOrOnline=0&FromPrice=50&ToPrice=100000&IsHideAssignTask=0&Latitude=23.810469&Longitude=90.412918&UserId=#userId#&Page=#page#&Count=#count#&Status=#status#&UserCompanyId=#userCompanyId#";

  //  Create Case
  static String POSTCASE_URL = BASE_URL + "/api/task/post";

  //  create Case
  static String SUBMITCASE_URL = BASE_URL + "/api/mortgagecasepaymentinfo/post";

  //  Edit Case
  static String EDITCASE_URL = BASE_URL + "/api/task/put";

  //Case Infos
  static String CASE_INFO_URL =
      BASE_URL + "/api/mortgagecaseinfo/getbytaskid?TaskId=#TaskId#";

  //  Notification
  static String NOTI_URL = BASE_URL + "/api/notifications/get?userId=#userId#";

  static String NOTI_DELETE_URL =
      BASE_URL + "/api/notifications/delete/#notiId#";

  static String GET_BADGE_COUNTER_URL = BASE_URL +
      "/api/notifications/get/gettasknotificationcountandchatunreadcountdata?CustomerId=#userId#";

  //  Timeline
  //static String TIMELINE_URL = BASE_URL +
  // "/api/timeline/get?IsPrivate=#isPrivate#&ReceiverId=#receiverId#&SenderId=#senderId#&TaskId=#taskId#&Count=#count#&CustomerId=#customerId#&Page=#page#&timeLineId=#timeLineId#";
  static String TIMELINE_ADVISOR_URL = BASE_URL +
      "/api/users/get/userbycomunityidandcompanyuseridforprivatemessage?UserId=#userId#&CommunityId=#communityId#&UserCompanyId=#companyId#";
  static String TIMELINE_MESSAGE_TYPE_URL = BASE_URL +
      "/api/casereport/get/caselistforprivatemessagedata?UserCompanyId=#UserCompanyId#&CustomerId=#CustomerId#&AdviserOrIntroducerId=#AdviserOrIntroducerId#";
  static String TASKBIDDING_URL =
      BASE_URL + "/api/taskbidding/get?taskId=#taskId#";
  static String TIMELINE_URL = BASE_URL +
      "/api/timeline/gettimelinebyapp?Count=#count#&count=#count#&IsPrivate=#isPrivate#&ReceiverId=#receiverId#&SenderId=#senderId#&TaskId=#taskId#&customerId=#customerId#&page=#page#&timeLineId=#timeLineId#";
  static String TIMELINE_POST_URL = BASE_URL + "/api/timeline/post";
  static String GETCASELISTGROUPCHAT_GET_URL = BASE_URL +
      "/api/casereport/get/getcaselistforgroupchatdata?UserCompanyId=#companyId#&CustomerId=#customerId#";

  //  More::Help->Support->Send Email  + Attachments
  static String MEDIA_UPLOADFILES_URL = BASE_URL + "/api/media/uploadpictures";

  static String PUT_MEDIA_PROFILE_IMAGE_URL =
      BASE_URL + "/api/users/put/updateprofilepictureandcover";

  static String RESOLUTION_URL = BASE_URL + "/api/resolution/post";

  //  More::Settings->Edit Profile
  static String EDIT_PROFILE_URL = BASE_URL + "/api/users/put";
  static String Get_PROFILE_INFO_URL =
      BASE_URL + "/api/users/get/#UserID#/basic";

  //  More::Settings->User Notification Settings
  //static String NOTI_SETTINGS_URL =
  //BASE_URL + "/api/usernotificationsetting/get?userId=#userId#";
  static String NOTI_SETTINGS_URL = BASE_URL +
      "/api/usernotificationsetting/getnotificationsettingbyusercompanyidanduserid?UserCompanyId=#userCompanyId#&UserId=#userId#";
  static String NOTI_SETTINGS_POST_URL =
      BASE_URL + "/api/usernotificationsetting/post";
  static String FCM_TEST_NOTI_URL =
      BASE_URL + "/api/notifications/sendtestpushnotificationtouser/#userId#";

  //  More::Badge
  static String BADGE_USER_GET_URL =
      BASE_URL + "/api/userBadge/get?UserId=#userId#";
  static String BADGE_EMAIL_URL = BASE_URL + "/api/userBadge/postemailbadge";
  static String BADGE_PHOTOID_URL = BASE_URL + "/api/userBadge/post";
  static String BADGE_PHOTOID_DEL_URL =
      BASE_URL + "/api/userBadge/delete/#badgeId#";

  //  WEBVIEW::
  //  Case Details WebView
  static String CASEDETAILS_WEBVIEW_URL =
      BASE_URL + "/apps/about-me/#title#-#taskId#";
  static String BASE_URL_NOTI_WEB = BASE_URL + "/apps/about-me";

  //  Misc
  static String DOMAIN = "https://mortgage-magic.co.uk";
  static String ABOUTUS_URL = "https://mortgage-magic.co.uk/apps/about-me/";
  static String HELP_INFO_URL = "https://mortgage-magic.co.uk/privacy-policy/";

  // static String TC_URL = "https://www.mortgagemagic.com/privacy-policy/";
  static String PRIVACY_URL = BASE_URL + "/assets/img/privacy_policy.pdf";
  static String FAQ_URL = BASE_URL + "/apps/faq-customer";
  static String EID_URL = BASE_URL + "/mrg/mark-eid-mobile-verification";
  static String TC_CASE_URL =
      BASE_URL + "/company-term-of-business/MORTGAGE%20MAGIC%20PLATFORM";

  static String CaseDigitalSignByCustomerURL =
      BASE_URL + "/apps/case-digital-sign-by-customer/-#CaseID#";
  static String CLIENT_AGREEMENT_URL =
      BASE_URL + "/apps/client-agreement/-#CaseID#";

  //  credit report
  static String CREDIT_DATA_DASHBOARD_URL =
      BASE_URL + "/api/crcreditdata/post/getreport";
  static String CREDIT_USER_INFO_POST_URL =
      BASE_URL + "/api/crusermanage/post/validatenewuser";
  static String GET_KBA_QUESTIONS_URL =
      BASE_URL + "/api/crusermanage/post/getkbaquestions";
  static String POST_KBA_QUESTIONS_URL =
      BASE_URL + "/api/crusermanage/post/answerkbaquestions";
  static String GET_SUMMARY_REPORT_URL =
      BASE_URL + "/api/crcreditdata/post/getsummaryreport";
  static String GET_SIMSCOREWITHUSERID =
      BASE_URL + "/api/crcreditdata/post/getsimulatedscorewithuserid";
  static String GET_USER_VALIDATION_FOR_OLD_USER_URL = BASE_URL +
      "/api/crusermanage/get/getuservalidationforolduser?UserId=#UserId#&UserCompanyId=#UserCompanyId#";

  //  Open Bank
  static String GET_INSIGHT4PERSONNELACCOUNTS_URL = BASE_URL +
      "/api/friendlyscore/getinsightsforpersonalaccounts?customerid=#customerId#";
  static String GET_FORECASTS4PERSONNELACCOUNT_URL = BASE_URL +
      "/api/friendlyscore/getforecastsforpersonalaccounts?customerid=#customerId#";

  //  href login
  static String LOGINACCEXISTSBYEMAIL_POST_URL =
      BASE_URL + "/api/authentication/post/loginaccountexistscheckbyEmail";
  static String POSTEMAILOTP_POST_URL = BASE_URL + "/api/userotp/postemailotp";
  static String SENDUSEREMAILOTP_GET_URL =
      BASE_URL + "/api/userotp/senduseremailotp?otpId=#otpId#";
  static String LOGINEMAILOTPBYMOBAPP_POST_URL =
      BASE_URL + "/api/authentication/loginemailotpbymobileapp";
}
