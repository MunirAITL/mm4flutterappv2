import 'package:aitl/config/Server.dart';

class SrvW2NCase {
  //  **************************  Dependents
  static String GET_DEPENDENTS_URL = Server.BASE_URL +
      "/api/mortgagefinancialdependants/getbyuseridandusercompanyid?UserId=#userId#&UserCompanyId=#userCompanyId#";
  static String POST_DEPENDENT_URL =
      Server.BASE_URL + "/api/mortgagefinancialdependants/post";
  static String PUT_DEPENDENT_URL =
      Server.BASE_URL + "/api/mortgagefinancialdependants/put";
  static String DEL_DEPENDENT_URL =
      Server.BASE_URL + "/api/mortgagefinancialdependants/delete/";

  //  **************************  Requirements ->  Step 1: Cases
  //
  static String GET_REQUIREMENTS_URL =
      Server.BASE_URL + "/api/mortgageuserrequirement/getbytaskid?TaskId=";
  static String POST_REQUIREMENTS_URL =
      Server.BASE_URL + "/api/mortgageuserrequirement/post";
  static String PUT_REQUIREMENTS_URL =
      Server.BASE_URL + "/api/mortgageuserrequirement/put";

  //  remortgage loan reasons
  static String GET_MORTGAGELOAN_REASONS_URL =
      Server.BASE_URL + "/api/mortgageloanreason/getbytaskid?TaskId=";
  static String POST_MORTGAGELOAN_REASONS_URL =
      Server.BASE_URL + "/api/mortgageloanreason/post";
  static String PUT_MORTGAGELOAN_REASONS_URL =
      Server.BASE_URL + "/api/mortgageloanreason/put";
  static String DEL_MORTGAGELOAN_REASONS_URL =
      Server.BASE_URL + "/api/mortgageloanreason/delete/";

  //  extra form e.g Equity Release case
  static String GET_EQUITYRELEASE_POWERATTORNEY_URL = Server.BASE_URL +
      "/api/mortgageequityreleasepowerofattorney/getbytaskid?TaskId=";
  static String POST_EQUITYRELEASE_POWERATTORNEY_URL =
      Server.BASE_URL + "/api/mortgageequityreleasepowerofattorney/post";
  static String PUT_EQUITYRELEASE_POWERATTORNEY_URL =
      Server.BASE_URL + "/api/mortgageequityreleasepowerofattorney/put";

  //
  static String GET_REQUIREMENTS_GEN_INC_URL = Server.BASE_URL +
      "/api/mortgagebuildingandcontentinsurance/getbytaskid?TaskId="; //
  static String POST_REQUIREMENTS_GEN_INC_URL =
      Server.BASE_URL + "/api/mortgagebuildingandcontentinsurance/post";
  static String PUT_REQUIREMENTS_GEN_INC_URL =
      Server.BASE_URL + "/api/mortgagebuildingandcontentinsurance/put";
  //
  static String GET_PERSONAL_PROCESSION_URL = Server.BASE_URL +
      "/api/mortgagebuildingandcontentinsuranceitem/getbytaskid?TaskId="; //
  static String POST_PERSONAL_PROCESSION_URL =
      Server.BASE_URL + "/api/mortgagebuildingandcontentinsuranceitem/post";
  static String PUT_PERSONAL_PROCESSION_URL =
      Server.BASE_URL + "/api/mortgagebuildingandcontentinsuranceitem/put";
  static String DEL_PERSONAL_PROCESSION_URL =
      Server.BASE_URL + "/api/mortgagebuildingandcontentinsuranceitem/delete/";

  //  **************************  Requirements ->  Step 2: Essential Informations
  //  main page ES api
  //  other
  static String GET_ES1_URL =
      Server.BASE_URL + "/api/mortgageuserotherinfo/getbytaskid?TaskId=";
  static String POST_ES1_URL =
      Server.BASE_URL + "/api/mortgageuserotherinfo/post";
  static String PUT_ES1_URL =
      Server.BASE_URL + "/api/mortgageuserotherinfo/put";
  //  key info
  static String GET_ES2_URL =
      Server.BASE_URL + "/api/mortgageuserkeyinformation/getbytaskid?TaskId=";
  static String POST_ES2_URL =
      Server.BASE_URL + "/api/mortgageuserkeyinformation/post";
  static String PUT_ES2_URL =
      Server.BASE_URL + "/api/mortgageuserkeyinformation/put";

  //  auto sug -> contact
  static String GET_CONTACT_AUTO_SUG_URL = Server.BASE_URL +
      "/api/mortgageuserothercontactinfo/getbyusercompanyinfoidandagenttypeandcontactname?";
  static String POST_CONTACT_AUTO_SUG_URL =
      Server.BASE_URL + "/api/mortgageuserothercontactinfo/post";
  static String PUT_CONTACT_AUTO_SUG_URL =
      Server.BASE_URL + "/api/mortgageuserothercontactinfo/put";
  static String GET_LIST_CONTACT_AUTO_SUG_URL =
      Server.BASE_URL + "/api/mortgageuserothercontactinfo/getbytaskid?TaskId=";
  static String DEL_CONTACT_AUTO_SUG_URL =
      Server.BASE_URL + "/api/mortgageuserothercontactinfo/delete/";
  //  bnk
  static String GET_ESBANK_URL = Server.BASE_URL +
      "/api/mortgageuserbankdetails/getmortgageuserbankdetailsbytaskid?TaskId=";
  static String POST_ESBANK_URL =
      Server.BASE_URL + "/api/mortgageuserbankdetails/post";
  static String PUT_ESBANK_URL =
      Server.BASE_URL + "/api/mortgageuserbankdetails/put";
  static String DEL_ESBANK_URL =
      Server.BASE_URL + "/api/mortgageuserbankdetails/delete/";

  //  **************************  Requirements ->  Step 3: Your Dependents
  static String PUT_MORT_CASE_URL =
      Server.BASE_URL + "/api/mortgagecaseinfo/put";

  //  **************************  Requirements ->  Step 4: Your Address History
  static String GET_ADDR_HISTORY_URL =
      Server.BASE_URL + "/api/useraddress/getbyuseridandusercompanyid?";
  static String POST_ADDR_HISTORY_URL =
      Server.BASE_URL + "/api/useraddress/post";
  static String PUT_ADDR_HISTORY_URL = Server.BASE_URL + "/api/useraddress/put";
  static String DEL_ADDR_HISTORY_URL =
      Server.BASE_URL + "/api/useraddress/delete/";

  //  **************************  Requirements ->  Step 6: Current Residential Mortgage
  static String GET_CUR_RES_MORT_URL = Server.BASE_URL +
      "/api/mortgageusercurrentresidentialmortgage/getbyuseridandusercompanyid?";
  static String POST_CUR_RES_MORT_URL =
      Server.BASE_URL + "/api/mortgageusercurrentresidentialmortgage/post";
  static String PUT_CUR_RES_MORT_URL =
      Server.BASE_URL + "/api/mortgageusercurrentresidentialmortgage/put";

  //  **************************  Requirements ->  Step 7: Emp & Income
  static String GET_EMP_URL = Server.BASE_URL +
      "/api/mortgageuseroccupation/getbyuseridandusercompanyid?";
  static String POST_EMP_URL =
      Server.BASE_URL + "/api/mortgageuseroccupation/post";
  static String PUT_EMP_URL =
      Server.BASE_URL + "/api/mortgageuseroccupation/put";
  static String DEL_EMP_URL =
      Server.BASE_URL + "/api/mortgageuseroccupation/delete/";

  static String GET_INCOME_URL =
      Server.BASE_URL + "/api/mortgageuserincome/getbyuseridandusercompanyid?";
  static String POST_INCOME_URL =
      Server.BASE_URL + "/api/mortgageuserincome/post";
  static String PUT_INCOME_URL =
      Server.BASE_URL + "/api/mortgageuserincome/put";
  static String DEL_INCOME_URL =
      Server.BASE_URL + "/api/mortgageuserincome/delete/";

  //  **************************  Requirements ->  Step 8: Existing Policy Item
  static String GET_EXISTING_POLICY_URL = Server.BASE_URL +
      "/api/mortgageuserexistingpolicyitem/getbyuseridandusercompanyid?";
  static String POST_EXISTING_POLICY_URL =
      Server.BASE_URL + "/api/mortgageuserexistingpolicyitem/post";
  static String PUT_EXISTING_POLICY_URL =
      Server.BASE_URL + "/api/mortgageuserexistingpolicyitem/put";
  static String DEL_EXISTING_POLICY_URL =
      Server.BASE_URL + "/api/mortgageuserexistingpolicyitem/delete/";

  //  **************************  Requirements ->  Step 9: Saving Investment
  static String GET_SAVING_INV_URL = Server.BASE_URL +
      "/api/mortgageusersavingorinvestmentitem/getbyuseridandusercompanyid?";
  static String POST_SAVING_INV_URL =
      Server.BASE_URL + "/api/mortgageusersavingorinvestmentitem/post";
  static String PUT_SAVING_INV_URL =
      Server.BASE_URL + "/api/mortgageusersavingorinvestmentitem/put";
  static String DEL_SAVING_INV_URL =
      Server.BASE_URL + "/api/mortgageusersavingorinvestmentitem/delete/";

  //  **************************  Requirements ->  Step 10: Credit Commitment
  static String GET_CREDIT_COMMITMENT_URL = Server.BASE_URL +
      "/api/mortgageuseraffordability/getbyuseridandusercompanyid?";
  static String POST_CREDIT_COMMITMENT_URL =
      Server.BASE_URL + "/api/mortgageuseraffordability/post";
  static String PUT_CREDIT_COMMITMENT_URL =
      Server.BASE_URL + "/api/mortgageuseraffordability/put";
  static String DEL_CREDIT_COMMITMENT_URL =
      Server.BASE_URL + "/api/mortgageuseraffordability/delete/";

  //  **************************  Requirements ->  Step 11: Credit History
  //  main
  static String GET_USERHISTORY_URL =
      Server.BASE_URL + "/api/mortgageusercredithistory/getbytaskid?TaskId=";
  static String GET_CREDIT_HISTORY_URL = Server.BASE_URL +
      "/api/mortgageusercredithistory/getbyuseridandusercompanyid?";
  static String POST_CREDIT_HISTORY_URL =
      Server.BASE_URL + "/api/mortgageusercredithistory/post";
  static String PUT_CREDIT_HISTORY_URL =
      Server.BASE_URL + "/api/mortgageusercredithistory/put";
  //  items
  static String GET_CREDIT_HISTORYITEMS_URL =
      Server.BASE_URL + "/api/mortgageusercredithistoryitem/getbyuserid?";
  static String POST_CREDIT_HISTORYITEMS_URL =
      Server.BASE_URL + "/api/mortgageusercredithistoryitem/post";
  static String PUT_CREDIT_HISTORYITEMS_URL =
      Server.BASE_URL + "/api/mortgageusercredithistoryitem/put";
  static String DEL_CREDIT_HISTORYITEMS_URL =
      Server.BASE_URL + "/api/mortgageusercredithistoryitem/delete/";

  // Assessment of Affordability
  static String GET_TASK_URL = Server.BASE_URL + "/api/task/get/";
  static String GET_ASSESS_AFFORD_URL = Server.BASE_URL +
      "/api/mortgageuserassesmentofaffordability/getbyuseridandusercompanyid?";
  static String GET_ASSESS_AFFORD_ITEMS_URL = Server.BASE_URL +
      "/api/mortgageuserassesmentofaffordabilityitem/getbyuseridandusercompanyid?";
  static String POST_ASSESS_AFFORD_URL =
      Server.BASE_URL + "/api/mortgageuserassesmentofaffordability/post";

  static String PUT_ASSESS_AFFORD_URL =
      Server.BASE_URL + "/api/mortgageuserassesmentofaffordability/put";
  //  ***items
  static String GET_ASSESS_AFFORD_ITEM_URL =
      Server.BASE_URL + "/api/mortgageuserassesmentofaffordabilityitem/get/";
  static String POST_ASSESS_AFFORD_ITEM_URL =
      Server.BASE_URL + "/api/mortgageuserassesmentofaffordabilityitem/post";

  static String PUT_ASSESS_AFFORD_ITEM_URL =
      Server.BASE_URL + "/api/mortgageuserassesmentofaffordabilityitem/put";

  static String DEL_ASSESS_AFFORD_ITEM_URL =
      Server.BASE_URL + "/api/mortgageuserassesmentofaffordabilityitem/delete/";

  // Btl Portfolio
  static String UPLOAD_XLS_FILE_URL =
      Server.BASE_URL + "/api/fileupload/uploadbtlportfoliofileexceel";
  static String GET_BTL_PORTFOLIO_URL = Server.BASE_URL +
      "/api/mortgageuserportfolio/getbyuseridandusercompanyid?";
  static String POST_BTL_PORTFOLIO_URL =
      Server.BASE_URL + "/api/mortgageuserportfolio/post";
  static String PUT_BTL_PORTFOLIO_URL =
      Server.BASE_URL + "/api/mortgageuserportfolio/put";
  static String DEL_BTL_PORTFOLIO_URL =
      Server.BASE_URL + "/api/mortgageuserportfolio/delete/";

  // Case Documents
  static String GET_DOC_CASE_URL =
      Server.BASE_URL + "/api/mortgagecasedocumentinfo/getbytaskid?TaskId=";
  static String POST_UPLOAD_DOC_URL =
      Server.BASE_URL + "/api/mortgagecasedocumentinfo/post";
  static String PUT_UPLOAD_DOC_URL =
      Server.BASE_URL + "/api/mortgagecasedocumentinfo/put";

  //  Case Application Submission
  static String GET_MORT_CASE_PAYMENT_URL =
      Server.BASE_URL + "/api/mortgagecasepaymentinfo/getbytaskid?TaskId=";
}
