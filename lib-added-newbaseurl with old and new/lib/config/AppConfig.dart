class AppConfig {
  //static const
  static const int page_limit_dashboard = 3;
  static const int page_limit = 30;

  static const double picSize = 400;
  static const AlertDismisSec = 5;

  static const int pageAnimationMilliSecond = 100;

  static const date1970 = "01-Jan-1970";
}
