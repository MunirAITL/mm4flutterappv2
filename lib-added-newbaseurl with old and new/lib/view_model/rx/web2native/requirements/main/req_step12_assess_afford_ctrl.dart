import 'package:aitl/model/json/web2native/case/requirements/12access_afford/MortgageUserAssesmentOfAffordAbility.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/MortgageUserAssesmentOfAffordAbilityItems.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/Task.dart';
import 'package:get/get.dart';

class ReqStep12AssessAffordCtrl extends GetxController {
  var totalExpenses = 0.0.obs;
  var totalOtherCustom = 0.0.obs;
  //
  var taskCtrl = Task().obs;
  var assessAffordModel = MortgageUserAssesmentOfAffordAbility().obs;
  var listAssessAffordItemModel =
      List<MortgageUserAssesmentOfAffordAbilityItems>().obs;
}
