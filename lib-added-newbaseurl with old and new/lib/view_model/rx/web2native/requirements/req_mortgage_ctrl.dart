import 'package:get/get.dart';
import '../../../../view/widgets/dropdown/DropListModel.dart';

class ReqMortgageCtrl extends GetxController {
  var ddReq = DropListModel([
    OptionItem(
        id: "remortgageOfAnotherProperty",
        title: "Remortgage of another property"),
    OptionItem(id: "savings", title: "Savings"),
    OptionItem(id: "giftFromFamilyMember", title: "Gift from family member"),
    OptionItem(id: "equityAmount", title: "Equity"),
    OptionItem(id: "otherStateWhat", title: "Other"),
  ]).obs;
  var optReq = OptionItem(id: null, title: "Select").obs;
}
