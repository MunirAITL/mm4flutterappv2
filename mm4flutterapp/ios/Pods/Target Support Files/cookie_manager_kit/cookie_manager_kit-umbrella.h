#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "CookieManagerKitPlugin.h"

FOUNDATION_EXPORT double cookie_manager_kitVersionNumber;
FOUNDATION_EXPORT const unsigned char cookie_manager_kitVersionString[];

