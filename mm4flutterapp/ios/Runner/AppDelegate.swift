import UIKit
import Flutter
import GoogleMaps
import Firebase
import FirebaseMessaging
import FirebaseAnalytics
import OpenTok

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
 
  //var ChannelIncoming: FlutterMethodChannel? = nil
    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    
       FirebaseApp.configure()
       GMSServices.provideAPIKey("AIzaSyAr8ofzAsQgU85voEnr5NiUDWFSzMKF8iU")
      GeneratedPluginRegistrant.register(with: self)
      if #available(iOS 10.0, *) {
        UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
      }
      return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}


  //  FCM extension start
//@available(iOS 10.0, *)
//extension AppDelegate: UNUserNotificationCenterDelegate {
    
  /*override func userNotificationCenter(
    _ center: UNUserNotificationCenter,
    willPresent notification: UNNotification,
    withCompletionHandler completionHandler:
    @escaping (UNNotificationPresentationOptions) -> Void
  ) {
      process(notification)
      completionHandler([[.alert, .sound, .badge]])
  }

  override func userNotificationCenter(
    _ center: UNUserNotificationCenter,
    didReceive response: UNNotificationResponse,
    withCompletionHandler completionHandler: @escaping () -> Void
  ) {
    process(response.notification)
    completionHandler()
  }

    override func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult)
                       -> Void) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
      //if let messageID = userInfo[gcmMessageIDKey] {
        //print("Message ID: \(messageID)")
      //}

      // Print full message.
      print(userInfo)
      Messaging.messaging().appDidReceiveMessage(userInfo)
      completionHandler(UIBackgroundFetchResult.newData)
    }

    
  override func application(
    _ application: UIApplication,
    didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data
  ) {
    print(deviceToken)
    Messaging.messaging().apnsToken = deviceToken
  }

    
  private func process(_ notification: UNNotification) {
    let userInfo = notification.request.content.userInfo
    UIApplication.shared.applicationIconBadgeNumber = 0
      
      do {
          var json:String?
          if userInfo.count > 0 {
              
              let aps = userInfo["aps"] as? [String: Any]
              let alert = aps?["alert"] as? [String: String]
              let title = alert?["title"]
              let body = alert?["body"]
              print(title ?? "nil")
              print(body ?? "nil")
              let parameters = [
                "title":title,
                "body":body
              ]
              
                
              Messaging.messaging().appDidReceiveMessage(userInfo)
              Analytics.logEvent("NOTIFICATION_PROCESSED", parameters: parameters)
              
              let jsonData = try JSONSerialization.data(withJSONObject: parameters, options: [])
              //let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
              let json2 = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)
              json = json2!.replacingOccurrences(of: "\\", with: "")
              //parameters = [:]
              ChannelIncoming?.invokeMethod("invokeFCMRemoteMessageBody", arguments: json)
          }
      } catch {
          print(error.localizedDescription)
      }
  }
}

extension AppDelegate: MessagingDelegate {
  func messaging(
    _ messaging: Messaging,
    didReceiveRegistrationToken fcmToken: String?
  ) {
      print(fcmToken)
      ChannelIncoming?.invokeMethod("invokeFCMToken", arguments: fcmToken)
  }
}*/

//  FCM extension end


/*import UIKit
import Flutter
import GoogleMaps
import Firebase
import OpenTok

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
 
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    
        FirebaseApp.configure()
        GMSServices.provideAPIKey("AIzaSyAr8ofzAsQgU85voEnr5NiUDWFSzMKF8iU")
        GeneratedPluginRegistrant.register(with: self)
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate
        }
    
        //  https://flutter.dev/docs/development/platform-integration/platform-channels
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        let CHANNEL = FlutterMethodChannel(name: "flutter.native/tokbox",
                                           binaryMessenger: controller.binaryMessenger)
      
        let navigationController = UINavigationController(rootViewController: controller)
        self.window!.rootViewController = navigationController
        self.window!.makeKeyAndVisible()
        navigationController.setNavigationBarHidden(true, animated: true)
      
        CHANNEL.setMethodCallHandler({
          (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
          // Note: this method is invoked on the UI thread.
          switch call.method {
            case "start":
              if let args = call.arguments as? Dictionary<String, Any> {
                  
                  self.window!.rootViewController = navigationController
                  self.window!.makeKeyAndVisible()
                  let vc = TokboxController()
                  vc.args = args
                  
                  navigationController.pushViewController(vc, animated:true)
                 
              }
              default:
                break
          }
            
        })
   
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
*/
