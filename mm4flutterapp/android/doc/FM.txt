password = 

F8ficg**&&%%sar@dar2#$@**$!@u-o

key alias =
FynanceMagic


fm_app_server_v1.0_010522.apk

Release ---

keytool jre path
"H:\Program Files\Android\Android Studio\jre\bin\keytool"  

create keystore file
================================================================================================================
keytool -genkey -v -keystore fm.keystore -alias FynanceMagic -keyalg RSA -keysize 2048 -validity 10000

create jks file
=================================================================================================================

keytool -genkey -v -keystore fm.jks -storetype JKS -keyalg RSA -keysize 2048 -validity 10000 -alias FynanceMagic

The JKS keystore uses a proprietary format. It is recommended to migrate to PKCS12 which is an industry standard format using:
=================================================================================================================
keytool -importkeystore -srckeystore fm.jks -destkeystore fm.jks -deststoretype pkcs12

generate SSH1 Key
=================================================================================================================
keytool -keystore fm.jks -list -v


SHA-1 certificate fingerprint
46:ED:26:96:CC:EC:3C:AD:97:C6:94:1B:55:F7:1F:2B:A6:8D:9C:F3

SHA-256 certificate fingerprint
51:0F:F7:4A:76:CC:16:17:4C:F3:03:9F:83:7C:16:EB:66:E9:70:D6:9F:B0:C9:0D:E0:28:D6:77:B4:BB:BE:1B

