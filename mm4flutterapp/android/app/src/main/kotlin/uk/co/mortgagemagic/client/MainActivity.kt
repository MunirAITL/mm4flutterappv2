package uk.co.mortgagemagic.client

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
/*import com.opentok.android.BaseVideoRenderer
import com.opentok.android.OpentokError
import com.opentok.android.Publisher
import com.opentok.android.PublisherKit
import com.opentok.android.PublisherKit.PublisherListener
import com.opentok.android.Session
import com.opentok.android.Session.SessionListener
import com.opentok.android.Subscriber
import com.opentok.android.SubscriberKit
import com.opentok.android.SubscriberKit.SubscriberListener*/
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
//import com.opentok.android.Stream
import io.flutter.plugins.firebase.messaging.FlutterFirebaseMessagingBackgroundService
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugins.GeneratedPluginRegistrant
import io.flutter.plugins.firebase.messaging.FlutterFirebaseMessagingPlugin

class MainActivity : FlutterActivity(), PluginRegistry.PluginRegistrantCallback {

    /*private var session:Session? = null
    private var publisher:Publisher? = null
    private var subscriber:Subscriber? = null*/

    //private lateinit var opentokVideoPlatformView: OpentokVideoPlatformView

    //var updateUIReciver: BroadcastReceiver? = null

    var methodResult: MethodChannel.Result? = null

    override fun onStart() {
        super.onStart()
        //LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, IntentFilter("MyData"))
    }

    override fun onStop() {
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(messageReceiver)
        super.onStop()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //FlutterFirebaseMessagingBackgroundService.setPluginRegistrant(this)

        /*FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("MainActivity", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            val token = task.result
            print(token)
            NativeMethodChannelIncoming.invokeFlutter(token, "invokeFCMToken")
        })

        val filter = IntentFilter()
        filter.addAction("service.to.activity.incoming")
        updateUIReciver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                //UI update here
                if (intent != null) {
                    val message = intent.getStringExtra("message")
                    print(message)
                    NativeMethodChannelIncoming.invokeFlutter(Gson().toJson(message), "invokeFCMRemoteMessageBody")
                }
            }
        }
        registerReceiver(updateUIReciver, filter)*/
    }

    /*private val messageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            val msg = intent.extras?.getString("message")
            print(msg)
            NativeMethodChannelIncoming.invokeFlutter(Gson().toJson(msg), "invokeFCMRemoteMessageBody")
        }
    }*/

    /*object NativeMethodChannelIncoming {
        private const val CHANNEL_NAME = "flutter.native/incoming"
        private lateinit var methodChannel: MethodChannel
        fun configureChannel(flutterEngine: FlutterEngine) {
            methodChannel = MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL_NAME)
        }
        fun invokeFlutter(idea: String, method:String) {
            methodChannel.invokeMethod(method, idea)
        }
    }*/

    override fun registerWith(registry: PluginRegistry) {
        TODO("Not yet implemented")
        GeneratedPluginRegistrant.registerWith(FlutterEngine(applicationContext))
    }

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        GeneratedPluginRegistrant.registerWith(flutterEngine);
        //NativeMethodChannelIncoming.configureChannel(flutterEngine)

        //opentokVideoPlatformView = OpentokVideoFactory.getViewInstance(this)

        /*flutterEngine
                .platformViewsController
                .registry
                // opentok-video-container is a custom platform-view-type
                .registerViewFactory("opentok-video-container", OpentokVideoFactory())*/

        //addFlutterChannelListener()

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, "android_app_retain").apply {
            setMethodCallHandler { method, result ->
                if (method.method == "sendToBackground") {
                    moveTaskToBack(true)
                }
            }
        }
    }

    /*private val sessionListener: SessionListener = object: SessionListener {

        override fun onConnected(session: Session) {
            Log.d("MainActivity", "onConnected: Connected to session: ${session.sessionId}")
            publisher = Publisher.Builder(this@MainActivity).build()
            publisher?.setPublisherListener(publisherListener)
            publisher?.renderer?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL)
            //opentokVideoPlatformView.publisherContainer.addView(publisher?.view)
            if (publisher?.view is GLSurfaceView) {
                (publisher?.view as GLSurfaceView).setZOrderOnTop(true)
            }
            notifyFlutter(SdkState.LOGGED_IN)
            session.publish(publisher)
        }

        /*override fun onConnected2(session: Session) {
            // Connected to session
            Log.d("MainActivity", "Connected to session ${session.sessionId}")

            publisher = Publisher.Builder(this@MainActivity).build().apply {
                setPublisherListener(publisherListener)
                renderer?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL)

                view.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                opentokVideoPlatformView.publisherContainer.addView(view)
            }

            notifyFlutter(SdkState.LOGGED_IN)
            session.publish(publisher)
        }*/

        override fun onDisconnected(session: Session) {
            notifyFlutter(SdkState.LOGGED_OUT)
        }

        override fun onStreamReceived(session: Session, stream: Stream) {
            Log.d(
                "MainActivity",
                "onStreamReceived: New Stream Received " + stream.streamId + " in session: " + session.sessionId
            )
            if (subscriber == null) {
                subscriber = Subscriber.Builder(this@MainActivity, stream).build().apply {
                    renderer?.setStyle(BaseVideoRenderer.STYLE_VIDEO_SCALE, BaseVideoRenderer.STYLE_VIDEO_FILL)
                    setSubscriberListener(subscriberListener)
                    session.subscribe(this)

                    //opentokVideoPlatformView.subscriberContainer.addView(view)
                }
            }
        }

        override fun onStreamDropped(session: Session, stream: Stream) {
            Log.d(
                "MainActivity",
                "onStreamDropped: Stream Dropped: " + stream.streamId + " in session: " + session.sessionId
            )

            if (subscriber != null) {
                subscriber = null

                //opentokVideoPlatformView.subscriberContainer.removeAllViews()
            }
        }

        override fun onError(session: Session, opentokError: OpentokError) {
            Log.d("MainActivity", "Session error: " + opentokError.message)
            notifyFlutter(SdkState.ERROR)
        }
    }

    private val publisherListener: PublisherListener = object : PublisherListener {
        override fun onStreamCreated(publisherKit: PublisherKit, stream: Stream) {
            Log.d("MainActivity", "onStreamCreated: Publisher Stream Created. Own stream " + stream.streamId)
        }

        override fun onStreamDestroyed(publisherKit: PublisherKit, stream: Stream) {
            Log.d("MainActivity", "onStreamDestroyed: Publisher Stream Destroyed. Own stream " + stream.streamId)
        }

        override fun onError(publisherKit: PublisherKit, opentokError: OpentokError) {
            Log.d("MainActivity", "PublisherKit onError: " + opentokError.message)
            notifyFlutter(SdkState.ERROR)
        }
    }

    var subscriberListener: SubscriberListener = object : SubscriberListener {
        override fun onConnected(subscriberKit: SubscriberKit) {
            Log.d("MainActivity", "onConnected: Subscriber connected. Stream: " + subscriberKit.stream.streamId)
        }

        override fun onDisconnected(subscriberKit: SubscriberKit) {
            Log.d("MainActivity", "onDisconnected: Subscriber disconnected. Stream: " + subscriberKit.stream.streamId)
            notifyFlutter(SdkState.LOGGED_OUT)
        }

        override fun onError(subscriberKit: SubscriberKit, opentokError: OpentokError) {
            Log.d("MainActivity", "SubscriberKit onError: " + opentokError.message)
            notifyFlutter(SdkState.ERROR)
        }
    }

    private fun addFlutterChannelListener() {
        flutterEngine?.dartExecutor?.let {
            MethodChannel(it, "com.vonage").setMethodCallHandler { call, result ->

                when (call.method) {
                    "initSession" -> {
                        val apiKey = requireNotNull(call.argument<String>("apiKey"))
                        val sessionId = requireNotNull(call.argument<String>("sessionId"))
                        val token = requireNotNull(call.argument<String>("token"))

                        notifyFlutter(SdkState.WAIT)
                        initSession(apiKey, sessionId, token)
                        result.success("")
                    }
                    "makeCall" -> {
                        //swapCamera()
                        result.success("")
                    }
                    "swapCamera" -> {
                        swapCamera()
                        result.success("")
                    }
                    "toggleAudio" -> {
                        val publishAudio = requireNotNull(call.argument<Boolean>("publishAudio"))
                        toggleAudio(publishAudio)
                        result.success("")
                    }
                    "toggleVideo" -> {
                        val publishVideo = requireNotNull(call.argument<Boolean>("publishVideo"))
                        toggleVideo(publishVideo)
                        result.success("")
                    }
                    "dispose" -> {
                        session?.disconnect()
                        //publisher?.destroy()
                        //subscriber?.destroy()
                        //opentokVideoPlatformView.dispose()
                        //session = null
                        //publisher = null
                        //subscriber = null
                        //notifyFlutter(SdkState.LOGGED_OUT)
                    }
                    else -> {
                        result.notImplemented()
                    }
                }
            }
        }
    }

    private fun initSession(apiKey:String, sessionId:String, token:String) {
        session = Session.Builder(this, apiKey, sessionId).build()
        session?.setSessionListener(sessionListener)
        session?.connect(token)
    }

    private fun swapCamera() {
        publisher?.cycleCamera()
    }

    private fun toggleAudio(publishAudio: Boolean) {
        publisher?.publishAudio = publishAudio
    }

    private fun toggleVideo(publishVideo: Boolean) {
        publisher?.publishVideo = publishVideo
    }

    private fun notifyFlutter(state: SdkState) {
        Handler(Looper.getMainLooper()).post {
            flutterEngine?.dartExecutor?.let {
                MethodChannel(it, "com.vonage")
                    .invokeMethod("updateState", state.toString())
            }
        }
    }*/
}

enum class SdkState {
    LOGGED_OUT,
    LOGGED_IN,
    WAIT,
    ERROR
}