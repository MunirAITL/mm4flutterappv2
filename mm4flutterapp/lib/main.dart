import 'dart:io';

import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/helper/db_cus/tab_noti/NotiHelper.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/utils/Notifications/InitializeAwesomeNotifications.dart';
import 'package:aitl/utils/Notifications/NotificationData.dart';
import 'package:aitl/view/welcome/WelcomeScreen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';

import 'config/AppDefine.dart';
import 'config/Server.dart';
import 'controller/network/NetworkMgr.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  if (Platform.isAndroid) {
    await AndroidInAppWebViewController.setWebContentsDebuggingEnabled(true);
  }

  await Firebase.initializeApp();
  await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
  FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

  //  device settings
  await SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  //  error catching
  FlutterError.onError = (FlutterErrorDetails details) async {
    print("mail::FlutterErrorDetails" + details.toString());
  };

  //notification int
  await InitializeAwesomeNotifications.initAwesome();
  //allow notification
  // InitializeAwesomeNotifications.checkNotificationIsAllow();

  HttpOverrides.global = MyHttpOverrides();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();

    EasyLoading.instance
      ..displayDuration = const Duration(seconds: 3)
      ..loadingStyle = EasyLoadingStyle.custom
      ..textColor = MyTheme.bgColor
      //..textStyle = TextStyle(fontFamily: "Fieldwork", fontSize: 17)
      ..backgroundColor = MyTheme.bgColor2
      ..indicatorColor = MyTheme.brandColor
      ..maskColor = Colors.transparent
      ..indicatorType = EasyLoadingIndicatorType.threeBounce
      ..indicatorSize = 20
      ..maskType = EasyLoadingMaskType.clear
      ..userInteractions = false;

    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
    FirebaseMessaging.instance.getToken().then((value) async => {
          print("apns Token = ${value.toString()}"),
          await PrefMgr.shared.setPrefStr("fcmTokenKey", value.toString())
        });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      try {
        print(message);
        if (message.data != null) {
          if (message.data["EntityName"] == "TestPushNotitification") {
            InitializeAwesomeNotifications.generateNotification(
                title: message.notification.title,
                body: message.notification.body);
          } else {
            //  custom create apns via server -> go2 web dashboard -> create new case and get notify
            notiCreator(message);
          }
        }
      } catch (e) {}
      // debugPrint("Notification generation getting error ${message.data}");
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new GetMaterialApp(
        debugShowCheckedModeBanner: false,
        smartManagement: SmartManagement.full,
        enableLog: Server.isTest,
        defaultTransition: Transition.fade,
        title: AppDefine.APP_NAME,
        theme: MyTheme.themeData,
        builder: (context, widget) {
          // do your initialization here
          widget = EasyLoading.init()(context, widget);
          return widget;
        },
        home: WelcomeScreen());
  }
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  notiCreator(message);
}

notiCreator(message) {
  try {
    debugPrint("notification Message data get ${message.data}");

    NotificationData notiModel = new NotificationData.fromJson(message.data);

    Map<String, dynamic> notiMap =
        NotiHelper().getNotiMapLocalNotification(model: notiModel);

    String txt = notiMap['txt'] != null ? notiMap['txt'].toString() : "";
    if (notiModel.initiatorName != null) {
      InitializeAwesomeNotifications.generateNotification(
          title: notiModel.initiatorName ?? '', body: txt);
    } else {
      print(
          "APNS ************************** notiModel.initiatorName NOT FOUND");
    }
  } catch (e) {
    debugPrint("Notification generation getting error $e");
  }
}
