import 'package:aitl/view/widgets/dropdown/DropListModel.dart';

class W2NLocalData {
  static const listYesNoRB = {0: 'Yes', 1: 'No'};

  static final ddDoesWarranty = DropListModel([
    OptionItem(id: "1", title: "NHBC"),
    OptionItem(id: "2", title: "Zurich"),
    OptionItem(id: "3", title: "Architect certificate"),
  ]);
}
