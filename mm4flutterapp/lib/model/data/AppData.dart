import 'dart:io';

import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/model/json/db_cus/badge_counter/GetNotiBadgeAPIModel.dart';
import 'package:flutter/material.dart';

class AppData {
  static final AppData _appData = new AppData._internal();

  String udid;
  factory AppData() {
    return _appData;
  }

  TaskNotificationCountAndChatUnreadCountData
      taskNotificationCountAndChatUnreadCountData;

  AppData._internal();

  getUDID(BuildContext context) async {
    udid = await Common.getUDID(context);
  }
}

final appData = AppData();
