import 'package:aitl/model/json/web2native/case/requirements/13btl_portfolio/MortgageUserPortfolios.dart';

class PostBtlPortfolioAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  PostBtlPortfolioAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostBtlPortfolioAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> userPortfolioPost;
  Messages({this.userPortfolioPost});
  Messages.fromJson(Map<String, dynamic> json) {
    userPortfolioPost = json['UserPortfolio_post'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserPortfolio_post'] = this.userPortfolioPost;
    return data;
  }
}

class ResponseData {
  MortgageUserPortfolios mortgageUserPortfolio;
  ResponseData({this.mortgageUserPortfolio});

  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageUserPortfolio = json['MortgageUserPortfolio'] != null
        ? new MortgageUserPortfolios.fromJson(json['MortgageUserPortfolio'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserPortfolio != null) {
      data['MortgageUserPortfolio'] = this.mortgageUserPortfolio.toJson();
    }
    return data;
  }
}
