import 'MortgageUserOtherInfo.dart';

class PostES1APIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  PostES1APIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  PostES1APIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> otherinfoPost;
  Messages({this.otherinfoPost});

  Messages.fromJson(Map<String, dynamic> json) {
    otherinfoPost = json['otherinfo_post'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['otherinfo_post'] = this.otherinfoPost;
    return data;
  }
}

class ResponseData {
  MortgageUserOtherInfo mortgageUserOtherInfo;

  ResponseData({this.mortgageUserOtherInfo});

  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageUserOtherInfo = json['MortgageUserOtherInfo'] != null
        ? new MortgageUserOtherInfo.fromJson(json['MortgageUserOtherInfo'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserOtherInfo != null) {
      data['MortgageUserOtherInfo'] = this.mortgageUserOtherInfo.toJson();
    }
    return data;
  }
}
