class MortgageUserOtherInfo {
  int userId;
  dynamic user;
  int companyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  int taskId;
  String isItSharedOwnership;
  String detailsOfSharedOwnershipBodyYouAreBuyingFrom;
  String isItRightToBuy;
  double whatIsTheDiscountAmountReceived;
  String detailsOfTheCouncilAssociation;
  String attitudeToMortgageRepayment;
  String youPreferToAccumulateSavingsToRepayYourMortgageWhenItIsDue;
  String
      youPreferToHaveTheCertaintyThatYourMortgageLoanIsRepaidAtTheEndOfTheTerm;
  String
      youAreNotConcernedWithRepayingTheMortgageAsYouIntendToSellThePropertyBeforeTheEnd;
  String hasAnInterestOnlyLoanInFullOrPartBeenRequested;
  String ifYesWhatAreTheReasons;
  String methodOfCapitalRepaymentInvestmentVehiclesInheritance;
  String haveAnyInvestmentVehiclesBeenReviewedForSuitability;
  String paymentsIntoRetirement;
  String doPaymentsGoIntoRetirement;
  String ifYesWhyIsThis;
  String whatWillBeTheSourceOfIncomeAtThatTime;
  String doesFutureIncomeSupportRepaymentsAndExpectedFutureExpenditure;
  String debtConsolidation;
  String whyIsExistingUnsecuredLoanBeingAddedToTheMortgage;
  String
      isTheClientWillingToPotentiallyPayAGreaterAmountOverTheTermOfTheMortgage;
  String hasTheClientHadAnyDifficultiesWithPastRepaymentsOfTheLoan;
  String
      ifYesWhenAndHaveTheyNegotiatedAnySpecialArrangementsWithTheCreditorOrShouldTheyConsiderThis;
  String notes;
  String remarks;
  bool isAcceptTermAndConditions;
  String doYouWantToProvideBankDetailsForDirectDebitSetup;
  int id;

  MortgageUserOtherInfo(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.taskId,
      this.isItSharedOwnership,
      this.detailsOfSharedOwnershipBodyYouAreBuyingFrom,
      this.isItRightToBuy,
      this.whatIsTheDiscountAmountReceived,
      this.detailsOfTheCouncilAssociation,
      this.attitudeToMortgageRepayment,
      this.youPreferToAccumulateSavingsToRepayYourMortgageWhenItIsDue,
      this.youPreferToHaveTheCertaintyThatYourMortgageLoanIsRepaidAtTheEndOfTheTerm,
      this.youAreNotConcernedWithRepayingTheMortgageAsYouIntendToSellThePropertyBeforeTheEnd,
      this.hasAnInterestOnlyLoanInFullOrPartBeenRequested,
      this.ifYesWhatAreTheReasons,
      this.methodOfCapitalRepaymentInvestmentVehiclesInheritance,
      this.haveAnyInvestmentVehiclesBeenReviewedForSuitability,
      this.paymentsIntoRetirement,
      this.doPaymentsGoIntoRetirement,
      this.ifYesWhyIsThis,
      this.whatWillBeTheSourceOfIncomeAtThatTime,
      this.doesFutureIncomeSupportRepaymentsAndExpectedFutureExpenditure,
      this.debtConsolidation,
      this.whyIsExistingUnsecuredLoanBeingAddedToTheMortgage,
      this.isTheClientWillingToPotentiallyPayAGreaterAmountOverTheTermOfTheMortgage,
      this.hasTheClientHadAnyDifficultiesWithPastRepaymentsOfTheLoan,
      this.ifYesWhenAndHaveTheyNegotiatedAnySpecialArrangementsWithTheCreditorOrShouldTheyConsiderThis,
      this.notes,
      this.remarks,
      this.isAcceptTermAndConditions,
      this.doYouWantToProvideBankDetailsForDirectDebitSetup,
      this.id});

  MortgageUserOtherInfo.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    user = json['User'];
    companyId = json['CompanyId'];
    status = json['Status'];
    mortgageCaseInfoId = json['MortgageCaseInfoId'];
    creationDate = json['CreationDate'];
    taskId = json['TaskId'];
    isItSharedOwnership = json['IsItSharedOwnership'];
    detailsOfSharedOwnershipBodyYouAreBuyingFrom =
        json['DetailsOfSharedOwnershipBodyYouAreBuyingFrom'];
    isItRightToBuy = json['IsItRightToBuy'];
    whatIsTheDiscountAmountReceived = json['WhatIsTheDiscountAmountReceived'];
    detailsOfTheCouncilAssociation = json['DetailsOfTheCouncilAssociation'];
    attitudeToMortgageRepayment = json['AttitudeToMortgageRepayment'];
    youPreferToAccumulateSavingsToRepayYourMortgageWhenItIsDue =
        json['YouPreferToAccumulateSavingsToRepayYourMortgageWhenItIsDue'];
    youPreferToHaveTheCertaintyThatYourMortgageLoanIsRepaidAtTheEndOfTheTerm = json[
        'YouPreferToHaveTheCertaintyThatYourMortgageLoanIsRepaidAtTheEndOfTheTerm'];
    youAreNotConcernedWithRepayingTheMortgageAsYouIntendToSellThePropertyBeforeTheEnd =
        json[
            'YouAreNotConcernedWithRepayingTheMortgageAsYouIntendToSellThePropertyBeforeTheEnd'];
    hasAnInterestOnlyLoanInFullOrPartBeenRequested =
        json['HasAnInterestOnlyLoanInFullOrPartBeenRequested'];
    ifYesWhatAreTheReasons = json['IfYesWhatAreTheReasons'];
    methodOfCapitalRepaymentInvestmentVehiclesInheritance =
        json['MethodOfCapitalRepaymentInvestmentVehiclesInheritance'];
    haveAnyInvestmentVehiclesBeenReviewedForSuitability =
        json['HaveAnyInvestmentVehiclesBeenReviewedForSuitability'];
    paymentsIntoRetirement = json['PaymentsIntoRetirement'];
    doPaymentsGoIntoRetirement = json['DoPaymentsGoIntoRetirement'];
    ifYesWhyIsThis = json['IfYesWhyIsThis'];
    whatWillBeTheSourceOfIncomeAtThatTime =
        json['WhatWillBeTheSourceOfIncomeAtThatTime'];
    doesFutureIncomeSupportRepaymentsAndExpectedFutureExpenditure =
        json['DoesFutureIncomeSupportRepaymentsAndExpectedFutureExpenditure'];
    debtConsolidation = json['DebtConsolidation'];
    whyIsExistingUnsecuredLoanBeingAddedToTheMortgage =
        json['WhyIsExistingUnsecuredLoanBeingAddedToTheMortgage'];
    isTheClientWillingToPotentiallyPayAGreaterAmountOverTheTermOfTheMortgage = json[
        'IsTheClientWillingToPotentiallyPayAGreaterAmountOverTheTermOfTheMortgage'];
    hasTheClientHadAnyDifficultiesWithPastRepaymentsOfTheLoan =
        json['HasTheClientHadAnyDifficultiesWithPastRepaymentsOfTheLoan'];
    ifYesWhenAndHaveTheyNegotiatedAnySpecialArrangementsWithTheCreditorOrShouldTheyConsiderThis =
        json[
            'IfYesWhenAndHaveTheyNegotiatedAnySpecialArrangementsWithTheCreditorOrShouldTheyConsiderThis'];
    notes = json['Notes'];
    remarks = json['Remarks'];
    isAcceptTermAndConditions = json['IsAcceptTermAndConditions'];
    doYouWantToProvideBankDetailsForDirectDebitSetup =
        json['DoYouWantToProvideBankDetailsForDirectDebitSetup'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['TaskId'] = this.taskId;
    data['IsItSharedOwnership'] = this.isItSharedOwnership;
    data['DetailsOfSharedOwnershipBodyYouAreBuyingFrom'] =
        this.detailsOfSharedOwnershipBodyYouAreBuyingFrom;
    data['IsItRightToBuy'] = this.isItRightToBuy;
    data['WhatIsTheDiscountAmountReceived'] =
        this.whatIsTheDiscountAmountReceived;
    data['DetailsOfTheCouncilAssociation'] =
        this.detailsOfTheCouncilAssociation;
    data['AttitudeToMortgageRepayment'] = this.attitudeToMortgageRepayment;
    data['YouPreferToAccumulateSavingsToRepayYourMortgageWhenItIsDue'] =
        this.youPreferToAccumulateSavingsToRepayYourMortgageWhenItIsDue;
    data['YouPreferToHaveTheCertaintyThatYourMortgageLoanIsRepaidAtTheEndOfTheTerm'] =
        this.youPreferToHaveTheCertaintyThatYourMortgageLoanIsRepaidAtTheEndOfTheTerm;
    data['YouAreNotConcernedWithRepayingTheMortgageAsYouIntendToSellThePropertyBeforeTheEnd'] =
        this.youAreNotConcernedWithRepayingTheMortgageAsYouIntendToSellThePropertyBeforeTheEnd;
    data['HasAnInterestOnlyLoanInFullOrPartBeenRequested'] =
        this.hasAnInterestOnlyLoanInFullOrPartBeenRequested;
    data['IfYesWhatAreTheReasons'] = this.ifYesWhatAreTheReasons;
    data['MethodOfCapitalRepaymentInvestmentVehiclesInheritance'] =
        this.methodOfCapitalRepaymentInvestmentVehiclesInheritance;
    data['HaveAnyInvestmentVehiclesBeenReviewedForSuitability'] =
        this.haveAnyInvestmentVehiclesBeenReviewedForSuitability;
    data['PaymentsIntoRetirement'] = this.paymentsIntoRetirement;
    data['DoPaymentsGoIntoRetirement'] = this.doPaymentsGoIntoRetirement;
    data['IfYesWhyIsThis'] = this.ifYesWhyIsThis;
    data['WhatWillBeTheSourceOfIncomeAtThatTime'] =
        this.whatWillBeTheSourceOfIncomeAtThatTime;
    data['DoesFutureIncomeSupportRepaymentsAndExpectedFutureExpenditure'] =
        this.doesFutureIncomeSupportRepaymentsAndExpectedFutureExpenditure;
    data['DebtConsolidation'] = this.debtConsolidation;
    data['WhyIsExistingUnsecuredLoanBeingAddedToTheMortgage'] =
        this.whyIsExistingUnsecuredLoanBeingAddedToTheMortgage;
    data['IsTheClientWillingToPotentiallyPayAGreaterAmountOverTheTermOfTheMortgage'] =
        this.isTheClientWillingToPotentiallyPayAGreaterAmountOverTheTermOfTheMortgage;
    data['HasTheClientHadAnyDifficultiesWithPastRepaymentsOfTheLoan'] =
        this.hasTheClientHadAnyDifficultiesWithPastRepaymentsOfTheLoan;
    data['IfYesWhenAndHaveTheyNegotiatedAnySpecialArrangementsWithTheCreditorOrShouldTheyConsiderThis'] =
        this.ifYesWhenAndHaveTheyNegotiatedAnySpecialArrangementsWithTheCreditorOrShouldTheyConsiderThis;
    data['Notes'] = this.notes;
    data['Remarks'] = this.remarks;
    data['IsAcceptTermAndConditions'] = this.isAcceptTermAndConditions;
    data['DoYouWantToProvideBankDetailsForDirectDebitSetup'] =
        this.doYouWantToProvideBankDetailsForDirectDebitSetup;
    data['Id'] = this.id;
    return data;
  }
}
