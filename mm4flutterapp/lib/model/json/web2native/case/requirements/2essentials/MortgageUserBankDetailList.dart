class MortgageUserBankDetailList {
  int userId;
  dynamic user;
  int companyId;
  int status;
  int mortgageCaseInfoId;
  String creationDate;
  int taskId;
  String accountName;
  String bankName;
  String address;
  String postcode;
  String sortCode;
  String accountNumber;
  String timeWithBank;
  String remarks;
  int id;

  MortgageUserBankDetailList(
      {this.userId,
      this.user,
      this.companyId,
      this.status,
      this.mortgageCaseInfoId,
      this.creationDate,
      this.taskId,
      this.accountName,
      this.bankName,
      this.address,
      this.postcode,
      this.sortCode,
      this.accountNumber,
      this.timeWithBank,
      this.remarks,
      this.id});

  MortgageUserBankDetailList.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'];
    companyId = json['CompanyId'] ?? 0;
    status = json['Status'] ?? 0;
    mortgageCaseInfoId = json['MortgageCaseInfoId'] ?? 0;
    creationDate = json['CreationDate'] ?? '';
    taskId = json['TaskId'] ?? 0;
    accountName = json['AccountName'] ?? '';
    bankName = json['BankName'] ?? '';
    address = json['Address'] ?? '';
    postcode = json['Postcode'] ?? '';
    sortCode = json['SortCode'] ?? '';
    accountNumber = json['AccountNumber'] ?? '';
    timeWithBank = json['TimeWithBank'] ?? '';
    remarks = json['Remarks'] ?? '';
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['CompanyId'] = this.companyId;
    data['Status'] = this.status;
    data['MortgageCaseInfoId'] = this.mortgageCaseInfoId;
    data['CreationDate'] = this.creationDate;
    data['TaskId'] = this.taskId;
    data['AccountName'] = this.accountName;
    data['BankName'] = this.bankName;
    data['Address'] = this.address;
    data['Postcode'] = this.postcode;
    data['SortCode'] = this.sortCode;
    data['AccountNumber'] = this.accountNumber;
    data['TimeWithBank'] = this.timeWithBank;
    data['Remarks'] = this.remarks;
    data['Id'] = this.id;
    return data;
  }
}
