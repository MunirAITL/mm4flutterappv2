import 'MortgageUserRequirementModel.dart';

class MortgageRequirementsAPIModel {
  bool success;
  ErrorMessages errorMessages;
  Messages messages;
  ResponseData responseData;

  MortgageRequirementsAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  MortgageRequirementsAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new Messages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class Messages {
  List<String> requirementPost;
  Messages({this.requirementPost});
  Messages.fromJson(Map<String, dynamic> json) {
    requirementPost = json['requirement_post'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['requirement_post'] = this.requirementPost;
    return data;
  }
}

class ResponseData {
  MortgageUserRequirementModel mortgageUserRequirement;
  ResponseData({this.mortgageUserRequirement});
  ResponseData.fromJson(Map<String, dynamic> json) {
    mortgageUserRequirement = json['MortgageUserRequirement'] != null
        ? new MortgageUserRequirementModel.fromJson(
            json['MortgageUserRequirement'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageUserRequirement != null) {
      data['MortgageUserRequirement'] = this.mortgageUserRequirement.toJson();
    }
    return data;
  }
}
