class MortgageUserInCome {
  int userId;
  dynamic user;
  int status;
  int mortgageUserOccupationId;
  String inComeType;
  double amountPerYear;
  String remarks;
  int taskId;
  int companyId;
  int id;

  MortgageUserInCome(
      {this.userId,
      this.user,
      this.status,
      this.mortgageUserOccupationId,
      this.inComeType,
      this.amountPerYear,
      this.remarks,
      this.taskId,
      this.companyId,
      this.id});

  MortgageUserInCome.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'] ?? 0;
    user = json['User'];
    status = json['Status'] ?? 0;
    mortgageUserOccupationId = json['MortgageUserOccupationId'] ?? 0;
    inComeType = json['InComeType'] ?? '';
    amountPerYear = json['AmountPerYear'] ?? 0;
    remarks = json['Remarks'] ?? '';
    taskId = json['TaskId'] ?? 0;
    companyId = json['CompanyId'] ?? 0;
    id = json['Id'] ?? 0;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['Status'] = this.status;
    data['MortgageUserOccupationId'] = this.mortgageUserOccupationId;
    data['InComeType'] = this.inComeType;
    data['AmountPerYear'] = this.amountPerYear;
    data['Remarks'] = this.remarks;
    data['TaskId'] = this.taskId;
    data['CompanyId'] = this.companyId;
    data['Id'] = this.id;
    return data;
  }
}
