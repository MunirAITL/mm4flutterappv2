class CaseTaskBiddings {
  int userId;
  String status;
  String creationDate;
  String updatedDate;
  int versionNumber;
  int taskId;
  String coverLetter;
  String description;
  String deliveryDate;
  String deliveryTime;
  double fixedbiddigAmount;
  double hourlyBiddingAmount;
  double totalHourPerWeek;
  double totalHour;
  String ownerName;
  String thumbnailPath;
  String referenceType;
  String referenceId;
  String remarks;
  String imageServerUrl;
  String ownerImageUrl;
  String ownerProfileUrl;
  double taskCompletionRate;
  double averageRating;
  int ratingCount;
  bool isTaskOwner;
  int taskOwnerId;
  bool isReview;
  bool isReviewByClient;
  String referenceBiddingUserId;
  String referenceBiddingUserType;
  String paymentStatus;
  double discountAmount;
  double netTotalAmount;
  int userPromotionId;
  bool isInPersonOrOnline;
  int totalComments;
  String taskPaymentPaymentMethod;
  String taskPaymentAccountStatus;
  int taskPaymentId;
  String taskTitle;
  String taskTitleUrl;
  int communityId;
  int caseProcessorCompanyId;
  String caseProcessorCompanyName;
  String communityName;
  bool isOnline;
  int completedTaskerTaskCount;
  int totalTaskerAsignTask;
  bool isReviewTask;
  String isViewPermission;
  int caseInvitationStatus;
  String ownerEmail;
  String ownerMobileNumber;
  int id;

  CaseTaskBiddings(
      {this.userId,
      this.status,
      this.creationDate,
      this.updatedDate,
      this.versionNumber,
      this.taskId,
      this.coverLetter,
      this.description,
      this.deliveryDate,
      this.deliveryTime,
      this.fixedbiddigAmount,
      this.hourlyBiddingAmount,
      this.totalHourPerWeek,
      this.totalHour,
      this.ownerName,
      this.thumbnailPath,
      this.referenceType,
      this.referenceId,
      this.remarks,
      this.imageServerUrl,
      this.ownerImageUrl,
      this.ownerProfileUrl,
      this.taskCompletionRate,
      this.averageRating,
      this.ratingCount,
      this.isTaskOwner,
      this.taskOwnerId,
      this.isReview,
      this.isReviewByClient,
      this.referenceBiddingUserId,
      this.referenceBiddingUserType,
      this.paymentStatus,
      this.discountAmount,
      this.netTotalAmount,
      this.userPromotionId,
      this.isInPersonOrOnline,
      this.totalComments,
      this.taskPaymentPaymentMethod,
      this.taskPaymentAccountStatus,
      this.taskPaymentId,
      this.taskTitle,
      this.taskTitleUrl,
      this.communityId,
      this.caseProcessorCompanyId,
      this.caseProcessorCompanyName,
      this.communityName,
      this.isOnline,
      this.completedTaskerTaskCount,
      this.totalTaskerAsignTask,
      this.isReviewTask,
      this.isViewPermission,
      this.caseInvitationStatus,
      this.ownerEmail,
      this.ownerMobileNumber,
      this.id});

  CaseTaskBiddings.fromJson(Map<String, dynamic> json) {
    userId = json['UserId'];
    status = json['Status'];
    creationDate = json['CreationDate'];
    updatedDate = json['UpdatedDate'];
    versionNumber = json['VersionNumber'];
    taskId = json['TaskId'];
    coverLetter = json['CoverLetter'];
    description = json['Description'];
    deliveryDate = json['DeliveryDate'];
    deliveryTime = json['DeliveryTime'];
    fixedbiddigAmount = json['FixedbiddigAmount'];
    hourlyBiddingAmount = json['HourlyBiddingAmount'];
    totalHourPerWeek = json['TotalHourPerWeek'];
    totalHour = json['TotalHour'];
    ownerName = json['OwnerName'];
    thumbnailPath = json['ThumbnailPath'];
    referenceType = json['ReferenceType'];
    referenceId = json['ReferenceId'];
    remarks = json['Remarks'];
    imageServerUrl = json['ImageServerUrl'];
    ownerImageUrl = json['OwnerImageUrl'];
    ownerProfileUrl = json['OwnerProfileUrl'];
    taskCompletionRate = json['TaskCompletionRate'];
    averageRating = json['AverageRating'];
    ratingCount = json['RatingCount'];
    isTaskOwner = json['IsTaskOwner'];
    taskOwnerId = json['TaskOwnerId'];
    isReview = json['IsReview'];
    isReviewByClient = json['IsReviewByClient'];
    referenceBiddingUserId = json['ReferenceBiddingUserId'];
    referenceBiddingUserType = json['ReferenceBiddingUserType'];
    paymentStatus = json['PaymentStatus'];
    discountAmount = json['DiscountAmount'];
    netTotalAmount = json['NetTotalAmount'];
    userPromotionId = json['UserPromotionId'];
    isInPersonOrOnline = json['IsInPersonOrOnline'];
    totalComments = json['TotalComments'];
    taskPaymentPaymentMethod = json['TaskPaymentPaymentMethod'];
    taskPaymentAccountStatus = json['TaskPaymentAccountStatus'];
    taskPaymentId = json['TaskPaymentId'];
    taskTitle = json['TaskTitle'];
    taskTitleUrl = json['TaskTitleUrl'];
    communityId = json['CommunityId'];
    caseProcessorCompanyId = json['CaseProcessorCompanyId'];
    caseProcessorCompanyName = json['CaseProcessorCompanyName'];
    communityName = json['CommunityName'];
    isOnline = json['IsOnline'];
    completedTaskerTaskCount = json['CompletedTaskerTaskCount'];
    totalTaskerAsignTask = json['TotalTaskerAsignTask'];
    isReviewTask = json['IsReviewTask'];
    isViewPermission = json['IsViewPermission'];
    caseInvitationStatus = json['CaseInvitationStatus'];
    ownerEmail = json['OwnerEmail'];
    ownerMobileNumber = json['OwnerMobileNumber'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['UserId'] = this.userId;
    data['Status'] = this.status;
    data['CreationDate'] = this.creationDate;
    data['UpdatedDate'] = this.updatedDate;
    data['VersionNumber'] = this.versionNumber;
    data['TaskId'] = this.taskId;
    data['CoverLetter'] = this.coverLetter;
    data['Description'] = this.description;
    data['DeliveryDate'] = this.deliveryDate;
    data['DeliveryTime'] = this.deliveryTime;
    data['FixedbiddigAmount'] = this.fixedbiddigAmount;
    data['HourlyBiddingAmount'] = this.hourlyBiddingAmount;
    data['TotalHourPerWeek'] = this.totalHourPerWeek;
    data['TotalHour'] = this.totalHour;
    data['OwnerName'] = this.ownerName;
    data['ThumbnailPath'] = this.thumbnailPath;
    data['ReferenceType'] = this.referenceType;
    data['ReferenceId'] = this.referenceId;
    data['Remarks'] = this.remarks;
    data['ImageServerUrl'] = this.imageServerUrl;
    data['OwnerImageUrl'] = this.ownerImageUrl;
    data['OwnerProfileUrl'] = this.ownerProfileUrl;
    data['TaskCompletionRate'] = this.taskCompletionRate;
    data['AverageRating'] = this.averageRating;
    data['RatingCount'] = this.ratingCount;
    data['IsTaskOwner'] = this.isTaskOwner;
    data['TaskOwnerId'] = this.taskOwnerId;
    data['IsReview'] = this.isReview;
    data['IsReviewByClient'] = this.isReviewByClient;
    data['ReferenceBiddingUserId'] = this.referenceBiddingUserId;
    data['ReferenceBiddingUserType'] = this.referenceBiddingUserType;
    data['PaymentStatus'] = this.paymentStatus;
    data['DiscountAmount'] = this.discountAmount;
    data['NetTotalAmount'] = this.netTotalAmount;
    data['UserPromotionId'] = this.userPromotionId;
    data['IsInPersonOrOnline'] = this.isInPersonOrOnline;
    data['TotalComments'] = this.totalComments;
    data['TaskPaymentPaymentMethod'] = this.taskPaymentPaymentMethod;
    data['TaskPaymentAccountStatus'] = this.taskPaymentAccountStatus;
    data['TaskPaymentId'] = this.taskPaymentId;
    data['TaskTitle'] = this.taskTitle;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['CommunityId'] = this.communityId;
    data['CaseProcessorCompanyId'] = this.caseProcessorCompanyId;
    data['CaseProcessorCompanyName'] = this.caseProcessorCompanyName;
    data['CommunityName'] = this.communityName;
    data['IsOnline'] = this.isOnline;
    data['CompletedTaskerTaskCount'] = this.completedTaskerTaskCount;
    data['TotalTaskerAsignTask'] = this.totalTaskerAsignTask;
    data['IsReviewTask'] = this.isReviewTask;
    data['IsViewPermission'] = this.isViewPermission;
    data['CaseInvitationStatus'] = this.caseInvitationStatus;
    data['OwnerEmail'] = this.ownerEmail;
    data['OwnerMobileNumber'] = this.ownerMobileNumber;
    data['Id'] = this.id;
    return data;
  }
}
