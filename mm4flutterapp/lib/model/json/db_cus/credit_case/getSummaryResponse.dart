class GetSummaryResponse {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  GetSummaryResponseData responseData;

  GetSummaryResponse(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetSummaryResponse.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new GetSummaryResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();

  ErrorMessages.fromJson(Map<String, dynamic> json) {}

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class GetSummaryResponseData {
  SummaryReport summaryReport;

  GetSummaryResponseData({this.summaryReport});

  GetSummaryResponseData.fromJson(Map<String, dynamic> json) {
    summaryReport = json['SummaryReport'] != null
        ? new SummaryReport.fromJson(json['SummaryReport'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.summaryReport != null) {
      data['SummaryReport'] = this.summaryReport.toJson();
    }
    return data;
  }
}

class SummaryReport {
  GetSummaryReportResult getSummaryReportResult;

  SummaryReport({this.getSummaryReportResult});

  SummaryReport.fromJson(Map<String, dynamic> json) {
    getSummaryReportResult = json['GetSummaryReportResult'] != null
        ? new GetSummaryReportResult.fromJson(json['GetSummaryReportResult'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.getSummaryReportResult != null) {
      data['GetSummaryReportResult'] = this.getSummaryReportResult.toJson();
    }
    return data;
  }
}

class GetSummaryReportResult {
  Message message;
  String status;
  Metrics metrics;
  String reportDate;
  String reportId;
  String score;
  ScoreHistory scoreHistory;
  ScoreReasonCodes scoreReasonCodes;
  Message reportNextRefresh;
  String i;

  GetSummaryReportResult(
      {this.message,
      this.status,
      this.metrics,
      this.reportDate,
      this.reportId,
      this.score,
      this.scoreHistory,
      this.scoreReasonCodes,
      this.reportNextRefresh,
      this.i});

  GetSummaryReportResult.fromJson(Map<String, dynamic> json) {
    message =
        json['Message'] != null ? new Message.fromJson(json['Message']) : null;
    status = json['Status'];
    metrics =
        json['Metrics'] != null ? new Metrics.fromJson(json['Metrics']) : null;
    reportDate = json['ReportDate'];
    reportId = json['ReportId'];
    score = json['Score'];
    scoreHistory = json['ScoreHistory'] != null
        ? new ScoreHistory.fromJson(json['ScoreHistory'])
        : null;
    scoreReasonCodes = json['ScoreReasonCodes'] != null
        ? new ScoreReasonCodes.fromJson(json['ScoreReasonCodes'])
        : null;
    reportNextRefresh = json['ReportNextRefresh'] != null
        ? new Message.fromJson(json['ReportNextRefresh'])
        : null;
    i = json['I'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.message != null) {
      data['Message'] = this.message.toJson();
    }
    data['Status'] = this.status;
    if (this.metrics != null) {
      data['Metrics'] = this.metrics.toJson();
    }
    data['ReportDate'] = this.reportDate;
    data['ReportId'] = this.reportId;
    data['Score'] = this.score;
    if (this.scoreHistory != null) {
      data['ScoreHistory'] = this.scoreHistory.toJson();
    }
    if (this.scoreReasonCodes != null) {
      data['ScoreReasonCodes'] = this.scoreReasonCodes.toJson();
    }
    if (this.reportNextRefresh != null) {
      data['ReportNextRefresh'] = this.reportNextRefresh.toJson();
    }
    data['I'] = this.i;
    return data;
  }
}

class Message {
  String nil;

  Message({this.nil});

  Message.fromJson(Map<String, dynamic> json) {
    nil = json['Nil'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Nil'] = this.nil;
    return data;
  }
}

class Metrics {
  List<SummaryReportMetric> summaryReportMetric;

  Metrics({this.summaryReportMetric});

  Metrics.fromJson(Map<String, dynamic> json) {
    if (json['SummaryReportMetric'] != null) {
      summaryReportMetric = new List<SummaryReportMetric>();
      json['SummaryReportMetric'].forEach((v) {
        summaryReportMetric.add(new SummaryReportMetric.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.summaryReportMetric != null) {
      data['SummaryReportMetric'] =
          this.summaryReportMetric.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SummaryReportMetric {
  String title;
  String value;

  SummaryReportMetric({this.title, this.value});

  SummaryReportMetric.fromJson(Map<String, dynamic> json) {
    title = json['Title'];
    value = json['Value'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Title'] = this.title;
    data['Value'] = this.value;
    return data;
  }
}

class ScoreHistory {
  List<Score> score;

  ScoreHistory({this.score});

  ScoreHistory.fromJson(Map<String, dynamic> json) {
    if (json['Score'] != null) {
      score = [];
      json['Score'].forEach((v) {
        score.add(new Score.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.score != null) {
      data['Score'] = this.score.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Score {
  String scoreDate;
  String scoreValue;

  Score({this.scoreDate, this.scoreValue});

  Score.fromJson(Map<String, dynamic> json) {
    scoreDate = json['ScoreDate'];
    scoreValue = json['ScoreValue'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ScoreDate'] = this.scoreDate;
    data['ScoreValue'] = this.scoreValue;
    return data;
  }
}

class ScoreReasonCodes {
  List<ScoreReasonCode> scoreReasonCode;

  ScoreReasonCodes({this.scoreReasonCode});

  ScoreReasonCodes.fromJson(Map<String, dynamic> json) {
    if (json['ScoreReasonCode'] != null) {
      scoreReasonCode = new List<ScoreReasonCode>();
      json['ScoreReasonCode'].forEach((v) {
        scoreReasonCode.add(new ScoreReasonCode.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.scoreReasonCode != null) {
      data['ScoreReasonCode'] =
          this.scoreReasonCode.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ScoreReasonCode {
  String code;
  String message;
  String positive;

  ScoreReasonCode({this.code, this.message, this.positive});

  ScoreReasonCode.fromJson(Map<String, dynamic> json) {
    code = json['Code'];
    message = json['Message'];
    positive = json['Positive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Code'] = this.code;
    data['Message'] = this.message;
    data['Positive'] = this.positive;
    return data;
  }
}
