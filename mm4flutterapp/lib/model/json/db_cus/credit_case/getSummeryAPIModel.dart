import 'package:aitl/model/json/db_cus/credit_case/CreditInfoPostResponse.dart';
import 'package:aitl/model/json/db_cus/credit_case/KbaQuestionResponse.dart';
import 'package:aitl/model/json/db_cus/credit_case/getSummaryResponse.dart';

class GetSummeryAPIModel {
  bool success;
  dynamic errorMessages;
  dynamic messages;
  GetSummaryResponseData responseData;

  GetSummeryAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory GetSummeryAPIModel.fromJson(Map<String, dynamic> j) {
    return GetSummeryAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'] ?? {},
      messages: j['Messages'] ?? {},
      responseData: (j['ResponseData'] != null)
          ? GetSummaryResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}
