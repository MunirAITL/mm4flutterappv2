import 'package:aitl/model/json/db_cus/tab_newcase/MortgageCaseInfos.dart';

import '../tab_newcase/MortgageCaseRecomendationInfos.dart';

class GetMortgageCaseInfoTaskIdAPIModel {
  bool success;
  ErrorMessages errorMessages;
  ErrorMessages messages;
  ResponseData responseData;

  GetMortgageCaseInfoTaskIdAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  GetMortgageCaseInfoTaskIdAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    errorMessages = json['ErrorMessages'] != null
        ? new ErrorMessages.fromJson(json['ErrorMessages'])
        : null;
    messages = json['Messages'] != null
        ? new ErrorMessages.fromJson(json['Messages'])
        : null;
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.errorMessages != null) {
      data['ErrorMessages'] = this.errorMessages.toJson();
    }
    if (this.messages != null) {
      data['Messages'] = this.messages.toJson();
    }
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ErrorMessages {
  ErrorMessages();
  ErrorMessages.fromJson(Map<String, dynamic> json);
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    return data;
  }
}

class ResponseData {
  List<MortgageCaseInfos> mortgageCaseInfos;
  List<MortgageCaseRecomendationInfos> mortgageCaseRecomendationInfos;

  ResponseData({this.mortgageCaseInfos, this.mortgageCaseRecomendationInfos});

  ResponseData.fromJson(Map<String, dynamic> json) {
    if (json['MortgageCaseInfos'] != null) {
      mortgageCaseInfos = <MortgageCaseInfos>[];
      json['MortgageCaseInfos'].forEach((v) {
        mortgageCaseInfos.add(new MortgageCaseInfos.fromJson(v));
      });
    }
    if (json['MortgageCaseRecomendationInfos'] != null) {
      mortgageCaseRecomendationInfos = <MortgageCaseRecomendationInfos>[];
      json['MortgageCaseRecomendationInfos'].forEach((v) {
        mortgageCaseRecomendationInfos
            .add(new MortgageCaseRecomendationInfos.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.mortgageCaseInfos != null) {
      data['MortgageCaseInfos'] =
          this.mortgageCaseInfos.map((v) => v.toJson()).toList();
    }
    if (this.mortgageCaseRecomendationInfos != null) {
      data['MortgageCaseRecomendationInfos'] =
          this.mortgageCaseRecomendationInfos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
