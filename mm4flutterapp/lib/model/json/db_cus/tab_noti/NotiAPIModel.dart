import 'NotiModel.dart';

class NotiAPIModel {
  bool success;
  dynamic errorMessages;
  dynamic messages;
  _ResponseData responseData;

  NotiAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});

  factory NotiAPIModel.fromJson(Map<String, dynamic> j) {
    return NotiAPIModel(
      success: j['Success'] as bool,
      errorMessages: j['ErrorMessages'],
      messages: j['Messages'],
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ResponseData {
  List<dynamic> notifications;
  _ResponseData({this.notifications});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    var list_notifications = [];
    try {
      list_notifications = (j['Notifications'] != null)
          ? j['Notifications'].map((i) => NotiModel.fromJson(i)).toList()
          : [];
    } catch (e) {
      print(e.toString());
    }
    return _ResponseData(
      notifications: list_notifications ?? [],
    );
  }

  Map<String, dynamic> toMap() => {
        'Notifications': notifications,
      };
}
