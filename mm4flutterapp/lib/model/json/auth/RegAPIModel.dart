import 'UserModel.dart';

class RegAPIModel {
  bool success;
  _ErrorMessages errorMessages;
  _Messages messages;
  _ResponseData responseData;

  RegAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});
  //RegModel({this.success, this.errorMessages, this.messages});

  factory RegAPIModel.fromJson(Map<String, dynamic> j) {
    return RegAPIModel(
      success: j['Success'] as bool,
      errorMessages: _ErrorMessages.fromJson(j['ErrorMessages']),
      messages: _Messages.fromJson(j['Messages']),
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ErrorMessages {
  List<dynamic> register;
  _ErrorMessages({this.register});
  factory _ErrorMessages.fromJson(Map<String, dynamic> j) {
    return _ErrorMessages(
      register: j['register'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'register': register,
      };
}

class _Messages {
  List<dynamic> register;
  _Messages({this.register});
  factory _Messages.fromJson(Map<String, dynamic> j) {
    return _Messages(
      register: j['register'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'register': register,
      };
}

class _ResponseData {
  int otpId;
  String otpMobileNumber;
  String returnUrl;
  UserModel user;
  //List<dynamic> user;
  _ResponseData({this.otpId, this.otpMobileNumber, this.returnUrl, this.user});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    return _ResponseData(
        otpId: int.parse(j['OTPId'].toString()) ?? 0,
        otpMobileNumber: j['OTPMobileNumber'] ?? '',
        returnUrl: j['ReturnUrl'] ?? '',
        user: UserModel.fromJson(j['User']) ?? []);
  }

  Map<String, dynamic> toMap() => {
        'OTPId': otpId,
        'OTPMobileNumber': otpMobileNumber,
        'ReturnUrl': returnUrl,
        'User': user.toJson(),
      };
}
