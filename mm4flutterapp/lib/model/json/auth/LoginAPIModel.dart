import 'UserModel.dart';

class LoginAPIModel {
  bool success;
  _ErrorMessages errorMessages;
  _Messages messages;
  _ResponseData responseData;

  LoginAPIModel(
      {this.success, this.errorMessages, this.messages, this.responseData});
  //LoginModel({this.success, this.errorMessages, this.messages});

  factory LoginAPIModel.fromJson(Map<String, dynamic> j) {
    return LoginAPIModel(
      success: j['Success'] as bool,
      errorMessages: _ErrorMessages.fromJson(j['ErrorMessages']),
      messages: _Messages.fromJson(j['Messages']),
      responseData: (j['ResponseData'] != null)
          ? _ResponseData.fromJson(j['ResponseData'])
          : null,
    );
  }

  Map<String, dynamic> toMap() => {
        'Success': success,
        'ErrorMessages': errorMessages,
        'Messages': messages,
        'ResponseData': responseData,
      };
}

class _ErrorMessages {
  List<dynamic> login;
  _ErrorMessages({this.login});
  factory _ErrorMessages.fromJson(Map<String, dynamic> j) {
    return _ErrorMessages(
      login: j['login'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'login': login,
      };
}

class _Messages {
  List<dynamic> login;
  _Messages({this.login});
  factory _Messages.fromJson(Map<String, dynamic> j) {
    return _Messages(
      login: j['login'] ?? [],
    );
  }
  Map<String, dynamic> toMap() => {
        'login': login,
      };
}

class _ResponseData {
  String returnUrl;
  UserModel user;
  String accessToken;
  String refreshToken;
  //List<dynamic> user;
  _ResponseData(
      {this.returnUrl, this.user, this.accessToken, this.refreshToken});
  factory _ResponseData.fromJson(Map<String, dynamic> j) {
    return _ResponseData(
        returnUrl: j['ReturnUrl'] ?? '',
        user: UserModel.fromJson(j['User']) ?? [],
        accessToken: j['AccessToken'],
        refreshToken: j['RefreshToken']);
  }

  Map<String, dynamic> toMap() => {
        'ReturnUrl': returnUrl,
        'User': user.toJson(),
        'AccessToken': accessToken,
        'RefreshToken': refreshToken,
      };
}
