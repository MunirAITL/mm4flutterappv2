class Server {
  static const bool isTest = false;
  static const bool isOtp = true; // if you want otp skip than set to false

  static const String BASE_URL = (isTest)
      ? "https://app.mortgage-magic.co.uk"
      : "https://mortgage-magic.co.uk";

  static const String MISSING_IMG =
      BASE_URL + "/api/content/media/default_avatar.png";

  //  Device Info & APNS Stuff
  static const String FCM_DEVICE_INFO_URL =
      BASE_URL + "/api/fcmdeviceinfo/post";

  //  sendemailandnotification
  static const String CASE_EMAI_NOTI_GET_URL =
      BASE_URL + "/api/task/sendemailandnotification/#caseId#";

  static const String MORTGAGE_CASEINFO_LOCATIONTASKUSERID_GET = BASE_URL +
      "/api/mortgagecaseinfo/getbyuseridandcompanyidandclientagreementstatus?";

  static const String MORTGAGE_DIGITAL_SIGN_BY_CUSTOMER_URL =
      BASE_URL + "/apps/case-digital-sign-by-customer/-";

  static const String MORTGAGE_CLIENT_AGREEMENT_URL =
      BASE_URL + "/apps/client-agreement/-";

  //  Company Account
  static const String COMP_ACC_GET_URL =
      BASE_URL + "/api/users/get/getcompanyaccountsbyuserid?UserId=#userId#";

  //  Login
  static const String LOGIN_URL = BASE_URL + "/api/authentication/login";
  static const String GOOGLE_LOGIN_URL =
      BASE_URL + "/api/authentication/logingoogle";

  //  Login by Mobile OTP
  static const String LOGIN_MOBILE_OTP_POST_URL =
      BASE_URL + "/api/userotp/post";
  static const String SEND_OTP_NOTI_URL =
      BASE_URL + "/api/userotp/sendotpnotification?otpId=#otpId#";
  static const String LOGIN_MOBILE_OTP_PUT_URL = BASE_URL + "/api/userotp/put";
  static const String LOGIN_REG_OTP_FB_URL =
      BASE_URL + "/api/authentication/loginregistrationfacebookmobile";

  //  Login by FB Native

  //  Login by Gmail Native

  //  Login by Apple Native

  // case review
  static const String CASE_REVIEW = BASE_URL +
      "/api/mortgagecaseinfo/getbyuseridandcompanyidandclientagreementstatus?UserId=#UserId#&UserCompanyInfoId=#UserCompanyInfoId#&ClientAgreementStatus=#ClientAgreementStatus#";
  static const String TERMS_POLICY = BASE_URL +
      "/api/termsprivacynoticesetup/getbycompanyid?UserCompanyId=#UserCompanyId#";

  //  review rating
  static const String REVIEW_RATING_FORMSETUP_GET_URL = BASE_URL +
      "/api/reviewratingformsetup/getbycompanyid?UserCompanyId=#userCompanyId#";

  static const REVIEW_RATING_FORMSETUP_POST_URL =
      BASE_URL + "/api/userrating/post";

  //  Forgot
  static const String FORGOT_URL =
      BASE_URL + "/api/authentication/forgotpassword";

  // Change password
  static const String CHANGE_PWD_URL =
      BASE_URL + "/api/users/put/change-password";

  //  EmailVerify
  static const String EMAILVERIFY_URL =
      BASE_URL + "/api/authentication/verifyemail";

  //  Register
  static const String REG_URL = BASE_URL + "/api/authentication/register";

  //  Deactivate Profile
  static const String DEACTIVATE_PROFILE_URL =
      BASE_URL + "/api/users/deactivebyuserownerbyreason";

  //  Dashboard Action Alert and Cases
  static const String USERNOTEBYENTITY_URL = BASE_URL +
      "/api/usernote/getusernotebyentityidandentitynameanduseridandstatus?EntityId=#entityId#&EntityName=#entityName#&Status=#status#&UserId=#userId#";

  //  User Note Popup Put on Done
  static const String USERNOTE_PUT_URL = BASE_URL + "/api/usernote/put";

  //  New Case
  // static const String NEWCASE_URL = BASE_URL + "/api/task/taskinformationbysearch" + "/get?SearchText=&Distance=50&Location=Dhaka,%20Bangladesh&InPersonOrOnline=0&FromPrice=50&ToPrice=100000&IsHideAssignTask=0&Latitude=23.810469&Longitude=90.412918&UserId=#userId#&Page=#page#&Count=#count#&Status=#status#&UserCompanyId=#userCompanyId#";
  static const String NEWCASE_URL = BASE_URL +
      "/api/task/taskinformationbysearchformobileapplication" +
      "/get?SearchText=&Distance=50&Location=London, UK&InPersonOrOnline=0&FromPrice=50&ToPrice=100000&IsHideAssignTask=0&Latitude=23.810469&Longitude=90.412918&UserId=#userId#&Page=#page#&Count=#count#&Status=#status#&UserCompanyId=#userCompanyId#";

  //  Create Case
  static const String POSTCASE_URL = BASE_URL + "/api/task/post";

  //  create Case
  static const String SUBMITCASE_URL =
      BASE_URL + "/api/mortgagecasepaymentinfo/post";

  //  Edit Case
  static const String EDITCASE_URL = BASE_URL + "/api/task/put";

  //Case Infos
  static const String CASE_INFO_URL =
      BASE_URL + "/api/mortgagecaseinfo/getbytaskid?TaskId=#TaskId#";

  //  Notification
  static const String NOTI_URL =
      BASE_URL + "/api/notifications/get?userId=#userId#";

  static const String NOTI_DELETE_URL =
      BASE_URL + "/api/notifications/delete/#notiId#";

  static const GET_BADGE_COUNTER_URL = BASE_URL +
      "/api/notifications/get/gettasknotificationcountandchatunreadcountdata?CustomerId=#userId#";

  //  Timeline
  //static const String TIMELINE_URL = BASE_URL +
  // "/api/timeline/get?IsPrivate=#isPrivate#&ReceiverId=#receiverId#&SenderId=#senderId#&TaskId=#taskId#&Count=#count#&CustomerId=#customerId#&Page=#page#&timeLineId=#timeLineId#";
  static const String TIMELINE_ADVISOR_URL = BASE_URL +
      "/api/users/get/userbycomunityidandcompanyuseridforprivatemessage?UserId=#userId#&CommunityId=#communityId#&UserCompanyId=#companyId#";
  static const String TIMELINE_MESSAGE_TYPE_URL = BASE_URL +
      "/api/casereport/get/caselistforprivatemessagedata?UserCompanyId=#UserCompanyId#&CustomerId=#CustomerId#&AdviserOrIntroducerId=#AdviserOrIntroducerId#";
  static const String TASKBIDDING_URL =
      BASE_URL + "/api/taskbidding/get?taskId=#taskId#";
  static const String TIMELINE_URL = BASE_URL +
      "/api/timeline/gettimelinebyapp?Count=#count#&count=#count#&IsPrivate=#isPrivate#&ReceiverId=#receiverId#&SenderId=#senderId#&TaskId=#taskId#&customerId=#customerId#&page=#page#&timeLineId=#timeLineId#";
  static const String TIMELINE_POST_URL = BASE_URL + "/api/timeline/post";
  static const GETCASELISTGROUPCHAT_GET_URL = BASE_URL +
      "/api/casereport/get/getcaselistforgroupchatdata?UserCompanyId=#companyId#&CustomerId=#customerId#";

  //  More::Help->Support->Send Email  + Attachments
  static const String MEDIA_UPLOADFILES_URL =
      BASE_URL + "/api/media/uploadpictures";

  static const PUT_MEDIA_PROFILE_IMAGE_URL =
      BASE_URL + "/api/users/put/updateprofilepictureandcover";

  static const String RESOLUTION_URL = BASE_URL + "/api/resolution/post";

  //  More::Settings->Edit Profile
  static const String EDIT_PROFILE_URL = BASE_URL + "/api/users/put";
  static const String Get_PROFILE_INFO_URL =
      BASE_URL + "/api/users/get/#UserID#/basic";

  //  More::Settings->User Notification Settings
  //static const String NOTI_SETTINGS_URL =
  //BASE_URL + "/api/usernotificationsetting/get?userId=#userId#";
  static const String NOTI_SETTINGS_URL = BASE_URL +
      "/api/usernotificationsetting/getnotificationsettingbyusercompanyidanduserid?UserCompanyId=#userCompanyId#&UserId=#userId#";
  static const String NOTI_SETTINGS_POST_URL =
      BASE_URL + "/api/usernotificationsetting/post";
  static const String FCM_TEST_NOTI_URL =
      BASE_URL + "/api/notifications/sendtestpushnotificationtouser/#userId#";

  //  More::Badge
  static const String BADGE_USER_GET_URL =
      BASE_URL + "/api/userBadge/get?UserId=#userId#";
  static const String BADGE_EMAIL_URL =
      BASE_URL + "/api/userBadge/postemailbadge";
  static const String BADGE_PHOTOID_URL = BASE_URL + "/api/userBadge/post";
  static const String BADGE_PHOTOID_DEL_URL =
      BASE_URL + "/api/userBadge/delete/#badgeId#";

  //  WEBVIEW::
  //  Case Details WebView
  static const String CASEDETAILS_WEBVIEW_URL =
      BASE_URL + "/apps/about-me/#title#-#taskId#";
  static const String BASE_URL_NOTI_WEB = BASE_URL + "/apps/about-me";

  //  Misc
  static const String ABOUTUS_URL = BASE_URL + "/features";
  static const String HELP_INFO_URL = BASE_URL + "/contact";

  // static const String TC_URL = "https://www.mortgagemagic.com/privacy-policy/";
  static const String PRIVACY_URL =
      BASE_URL + "/assets/img/evernorth_privacy_policy.pdf";
  static const String FAQ_URL = BASE_URL + "/apps/faq-customer";
  static const String EID_URL = BASE_URL + "/mrg/mark-eid-mobile-verification";
  static const TC_CASE_URL =
      BASE_URL + "/company-term-of-business/MORTGAGE%20MAGIC%20PLATFORM";

  static const String CaseDigitalSignByCustomerURL =
      BASE_URL + "/apps/case-digital-sign-by-customer/-#CaseID#";
  static const String CLIENT_AGREEMENT_URL =
      BASE_URL + "/apps/client-agreement/-#CaseID#";

  //  credit report
  static const String CREDIT_DATA_DASHBOARD_URL =
      BASE_URL + "/api/crcreditdata/post/getreport";
  static const String CREDIT_USER_INFO_POST_URL =
      BASE_URL + "/api/crusermanage/post/validatenewuser";
  static const String GET_KBA_QUESTIONS_URL =
      BASE_URL + "/api/crusermanage/post/getkbaquestions";
  static const POST_KBA_QUESTIONS_URL =
      BASE_URL + "/api/crusermanage/post/answerkbaquestions";
  static const String GET_SUMMARY_REPORT_URL =
      BASE_URL + "/api/crcreditdata/post/getsummaryreport";
  static const GET_SIMSCOREWITHUSERID =
      BASE_URL + "/api/crcreditdata/post/getsimulatedscorewithuserid";
  static const String GET_USER_VALIDATION_FOR_OLD_USER_URL = BASE_URL +
      "/api/crusermanage/get/getuservalidationforolduser?UserId=#UserId#&UserCompanyId=#UserCompanyId#";

  //  Open Bank
  static const GET_INSIGHT4PERSONNELACCOUNTS_URL = BASE_URL +
      "/api/friendlyscore/getinsightsforpersonalaccounts?customerid=#customerId#";
  static const GET_FORECASTS4PERSONNELACCOUNT_URL = BASE_URL +
      "/api/friendlyscore/getforecastsforpersonalaccounts?customerid=#customerId#";

  //  href login
  static const LOGINACCEXISTSBYEMAIL_POST_URL =
      BASE_URL + "/api/authentication/post/loginaccountexistscheckbyEmail";
  static const POSTEMAILOTP_POST_URL = BASE_URL + "/api/userotp/postemailotp";
  static const SENDUSEREMAILOTP_GET_URL =
      BASE_URL + "/api/userotp/senduseremailotp?otpId=#otpId#";
  static const LOGINEMAILOTPBYMOBAPP_POST_URL =
      BASE_URL + "/api/authentication/loginemailotpbymobileapp";
}
