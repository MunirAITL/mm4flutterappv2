import 'package:aitl/config/Server.dart';

class SrvW2NCase {
  //  **************************  Dependents
  static const GET_DEPENDENTS_URL = Server.BASE_URL +
      "/api/mortgagefinancialdependants/getbyuseridandusercompanyid?UserId=#userId#&UserCompanyId=#userCompanyId#";
  static const POST_DEPENDENT_URL =
      Server.BASE_URL + "/api/mortgagefinancialdependants/post";
  static const PUT_DEPENDENT_URL =
      Server.BASE_URL + "/api/mortgagefinancialdependants/put";
  static const DEL_DEPENDENT_URL =
      Server.BASE_URL + "/api/mortgagefinancialdependants/delete/";

  //  **************************  Requirements ->  Step 1: Cases
  //
  static const GET_REQUIREMENTS_URL =
      Server.BASE_URL + "/api/mortgageuserrequirement/getbytaskid?TaskId=";
  static const POST_REQUIREMENTS_URL =
      Server.BASE_URL + "/api/mortgageuserrequirement/post";
  static const PUT_REQUIREMENTS_URL =
      Server.BASE_URL + "/api/mortgageuserrequirement/put";

  //  remortgage loan reasons
  static const GET_MORTGAGELOAN_REASONS_URL =
      Server.BASE_URL + "/api/mortgageloanreason/getbytaskid?TaskId=";
  static const POST_MORTGAGELOAN_REASONS_URL =
      Server.BASE_URL + "/api/mortgageloanreason/post";
  static const PUT_MORTGAGELOAN_REASONS_URL =
      Server.BASE_URL + "/api/mortgageloanreason/put";
  static const DEL_MORTGAGELOAN_REASONS_URL =
      Server.BASE_URL + "/api/mortgageloanreason/delete/";

  //  extra form e.g Equity Release case
  static const GET_EQUITYRELEASE_POWERATTORNEY_URL = Server.BASE_URL +
      "/api/mortgageequityreleasepowerofattorney/getbytaskid?TaskId=";
  static const POST_EQUITYRELEASE_POWERATTORNEY_URL =
      Server.BASE_URL + "/api/mortgageequityreleasepowerofattorney/post";
  static const PUT_EQUITYRELEASE_POWERATTORNEY_URL =
      Server.BASE_URL + "/api/mortgageequityreleasepowerofattorney/put";

  //
  static const GET_REQUIREMENTS_GEN_INC_URL = Server.BASE_URL +
      "/api/mortgagebuildingandcontentinsurance/getbytaskid?TaskId="; //
  static const POST_REQUIREMENTS_GEN_INC_URL =
      Server.BASE_URL + "/api/mortgagebuildingandcontentinsurance/post";
  static const PUT_REQUIREMENTS_GEN_INC_URL =
      Server.BASE_URL + "/api/mortgagebuildingandcontentinsurance/put";
  //
  static const GET_PERSONAL_PROCESSION_URL = Server.BASE_URL +
      "/api/mortgagebuildingandcontentinsuranceitem/getbytaskid?TaskId="; //
  static const POST_PERSONAL_PROCESSION_URL =
      Server.BASE_URL + "/api/mortgagebuildingandcontentinsuranceitem/post";
  static const PUT_PERSONAL_PROCESSION_URL =
      Server.BASE_URL + "/api/mortgagebuildingandcontentinsuranceitem/put";
  static const DEL_PERSONAL_PROCESSION_URL =
      Server.BASE_URL + "/api/mortgagebuildingandcontentinsuranceitem/delete/";

  //  **************************  Requirements ->  Step 2: Essential Informations
  //  main page ES api
  //  other
  static const GET_ES1_URL =
      Server.BASE_URL + "/api/mortgageuserotherinfo/getbytaskid?TaskId=";
  static const POST_ES1_URL =
      Server.BASE_URL + "/api/mortgageuserotherinfo/post";
  static const PUT_ES1_URL = Server.BASE_URL + "/api/mortgageuserotherinfo/put";
  //  key info
  static const GET_ES2_URL =
      Server.BASE_URL + "/api/mortgageuserkeyinformation/getbytaskid?TaskId=";
  static const POST_ES2_URL =
      Server.BASE_URL + "/api/mortgageuserkeyinformation/post";
  static const PUT_ES2_URL =
      Server.BASE_URL + "/api/mortgageuserkeyinformation/put";

  //  auto sug -> contact
  static const GET_CONTACT_AUTO_SUG_URL = Server.BASE_URL +
      "/api/mortgageuserothercontactinfo/getbyusercompanyinfoidandagenttypeandcontactname?";
  static const POST_CONTACT_AUTO_SUG_URL =
      Server.BASE_URL + "/api/mortgageuserothercontactinfo/post";
  static const PUT_CONTACT_AUTO_SUG_URL =
      Server.BASE_URL + "/api/mortgageuserothercontactinfo/put";
  static const GET_LIST_CONTACT_AUTO_SUG_URL =
      Server.BASE_URL + "/api/mortgageuserothercontactinfo/getbytaskid?TaskId=";
  static const DEL_CONTACT_AUTO_SUG_URL =
      Server.BASE_URL + "/api/mortgageuserothercontactinfo/delete/";
  //  bnk
  static const GET_ESBANK_URL = Server.BASE_URL +
      "/api/mortgageuserbankdetails/getmortgageuserbankdetailsbytaskid?TaskId=";
  static const POST_ESBANK_URL =
      Server.BASE_URL + "/api/mortgageuserbankdetails/post";
  static const PUT_ESBANK_URL =
      Server.BASE_URL + "/api/mortgageuserbankdetails/put";
  static const DEL_ESBANK_URL =
      Server.BASE_URL + "/api/mortgageuserbankdetails/delete/";

  //  **************************  Requirements ->  Step 3: Your Dependents
  static const PUT_MORT_CASE_URL =
      Server.BASE_URL + "/api/mortgagecaseinfo/put";

  //  **************************  Requirements ->  Step 4: Your Address History
  static const GET_ADDR_HISTORY_URL =
      Server.BASE_URL + "/api/useraddress/getbyuseridandusercompanyid?";
  static const POST_ADDR_HISTORY_URL =
      Server.BASE_URL + "/api/useraddress/post";
  static const PUT_ADDR_HISTORY_URL = Server.BASE_URL + "/api/useraddress/put";
  static const DEL_ADDR_HISTORY_URL =
      Server.BASE_URL + "/api/useraddress/delete/";

  //  **************************  Requirements ->  Step 6: Current Residential Mortgage
  static const GET_CUR_RES_MORT_URL = Server.BASE_URL +
      "/api/mortgageusercurrentresidentialmortgage/getbyuseridandusercompanyid?";
  static const POST_CUR_RES_MORT_URL =
      Server.BASE_URL + "/api/mortgageusercurrentresidentialmortgage/post";
  static const PUT_CUR_RES_MORT_URL =
      Server.BASE_URL + "/api/mortgageusercurrentresidentialmortgage/put";

  //  **************************  Requirements ->  Step 7: Emp & Income
  static const GET_EMP_URL = Server.BASE_URL +
      "/api/mortgageuseroccupation/getbyuseridandusercompanyid?";
  static const POST_EMP_URL =
      Server.BASE_URL + "/api/mortgageuseroccupation/post";
  static const PUT_EMP_URL =
      Server.BASE_URL + "/api/mortgageuseroccupation/put";
  static const DEL_EMP_URL =
      Server.BASE_URL + "/api/mortgageuseroccupation/delete/";

  static const GET_INCOME_URL =
      Server.BASE_URL + "/api/mortgageuserincome/getbyuseridandusercompanyid?";
  static const POST_INCOME_URL =
      Server.BASE_URL + "/api/mortgageuserincome/post";
  static const PUT_INCOME_URL = Server.BASE_URL + "/api/mortgageuserincome/put";
  static const DEL_INCOME_URL =
      Server.BASE_URL + "/api/mortgageuserincome/delete/";

  //  **************************  Requirements ->  Step 8: Existing Policy Item
  static const GET_EXISTING_POLICY_URL = Server.BASE_URL +
      "/api/mortgageuserexistingpolicyitem/getbyuseridandusercompanyid?";
  static const POST_EXISTING_POLICY_URL =
      Server.BASE_URL + "/api/mortgageuserexistingpolicyitem/post";
  static const PUT_EXISTING_POLICY_URL =
      Server.BASE_URL + "/api/mortgageuserexistingpolicyitem/put";
  static const DEL_EXISTING_POLICY_URL =
      Server.BASE_URL + "/api/mortgageuserexistingpolicyitem/delete/";

  //  **************************  Requirements ->  Step 9: Saving Investment
  static const GET_SAVING_INV_URL = Server.BASE_URL +
      "/api/mortgageusersavingorinvestmentitem/getbyuseridandusercompanyid?";
  static const POST_SAVING_INV_URL =
      Server.BASE_URL + "/api/mortgageusersavingorinvestmentitem/post";
  static const PUT_SAVING_INV_URL =
      Server.BASE_URL + "/api/mortgageusersavingorinvestmentitem/put";
  static const DEL_SAVING_INV_URL =
      Server.BASE_URL + "/api/mortgageusersavingorinvestmentitem/delete/";

  //  **************************  Requirements ->  Step 10: Credit Commitment
  static const GET_CREDIT_COMMITMENT_URL = Server.BASE_URL +
      "/api/mortgageuseraffordability/getbyuseridandusercompanyid?";
  static const POST_CREDIT_COMMITMENT_URL =
      Server.BASE_URL + "/api/mortgageuseraffordability/post";
  static const PUT_CREDIT_COMMITMENT_URL =
      Server.BASE_URL + "/api/mortgageuseraffordability/put";
  static const DEL_CREDIT_COMMITMENT_URL =
      Server.BASE_URL + "/api/mortgageuseraffordability/delete/";

  //  **************************  Requirements ->  Step 11: Credit History
  //  main
  static const GET_USERHISTORY_URL =
      Server.BASE_URL + "/api/mortgageusercredithistory/getbytaskid?TaskId=";
  static const GET_CREDIT_HISTORY_URL = Server.BASE_URL +
      "/api/mortgageusercredithistory/getbyuseridandusercompanyid?";
  static const POST_CREDIT_HISTORY_URL =
      Server.BASE_URL + "/api/mortgageusercredithistory/post";
  static const PUT_CREDIT_HISTORY_URL =
      Server.BASE_URL + "/api/mortgageusercredithistory/put";
  //  items
  static const GET_CREDIT_HISTORYITEMS_URL =
      Server.BASE_URL + "/api/mortgageusercredithistoryitem/getbyuserid?";
  static const POST_CREDIT_HISTORYITEMS_URL =
      Server.BASE_URL + "/api/mortgageusercredithistoryitem/post";
  static const PUT_CREDIT_HISTORYITEMS_URL =
      Server.BASE_URL + "/api/mortgageusercredithistoryitem/put";
  static const DEL_CREDIT_HISTORYITEMS_URL =
      Server.BASE_URL + "/api/mortgageusercredithistoryitem/delete/";

  // Assessment of Affordability
  static const GET_TASK_URL = Server.BASE_URL + "/api/task/get/";
  static const GET_ASSESS_AFFORD_URL = Server.BASE_URL +
      "/api/mortgageuserassesmentofaffordability/getbyuseridandusercompanyid?";
  static const GET_ASSESS_AFFORD_ITEMS_URL = Server.BASE_URL +
      "/api/mortgageuserassesmentofaffordabilityitem/getbyuseridandusercompanyid?";
  static const POST_ASSESS_AFFORD_URL =
      Server.BASE_URL + "/api/mortgageuserassesmentofaffordability/post";

  static const PUT_ASSESS_AFFORD_URL =
      Server.BASE_URL + "/api/mortgageuserassesmentofaffordability/put";
  //  ***items
  static const GET_ASSESS_AFFORD_ITEM_URL =
      Server.BASE_URL + "/api/mortgageuserassesmentofaffordabilityitem/get/";
  static const POST_ASSESS_AFFORD_ITEM_URL =
      Server.BASE_URL + "/api/mortgageuserassesmentofaffordabilityitem/post";

  static const PUT_ASSESS_AFFORD_ITEM_URL =
      Server.BASE_URL + "/api/mortgageuserassesmentofaffordabilityitem/put";

  static const DEL_ASSESS_AFFORD_ITEM_URL =
      Server.BASE_URL + "/api/mortgageuserassesmentofaffordabilityitem/delete/";

  // Btl Portfolio
  static const UPLOAD_XLS_FILE_URL =
      Server.BASE_URL + "/api/fileupload/uploadbtlportfoliofileexceel";
  static const GET_BTL_PORTFOLIO_URL = Server.BASE_URL +
      "/api/mortgageuserportfolio/getbyuseridandusercompanyid?";
  static const POST_BTL_PORTFOLIO_URL =
      Server.BASE_URL + "/api/mortgageuserportfolio/post";
  static const PUT_BTL_PORTFOLIO_URL =
      Server.BASE_URL + "/api/mortgageuserportfolio/put";
  static const DEL_BTL_PORTFOLIO_URL =
      Server.BASE_URL + "/api/mortgageuserportfolio/delete/";

  // Case Documents
  static const GET_DOC_CASE_URL =
      Server.BASE_URL + "/api/mortgagecasedocumentinfo/getbytaskid?TaskId=";
  static const POST_UPLOAD_DOC_URL =
      Server.BASE_URL + "/api/mortgagecasedocumentinfo/post";
  static const PUT_UPLOAD_DOC_URL =
      Server.BASE_URL + "/api/mortgagecasedocumentinfo/put";

  //  Case Application Submission
  static const GET_MORT_CASE_PAYMENT_URL =
      Server.BASE_URL + "/api/mortgagecasepaymentinfo/getbytaskid?TaskId=";
}
