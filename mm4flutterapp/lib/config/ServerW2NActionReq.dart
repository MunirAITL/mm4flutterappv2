import 'package:aitl/config/Server.dart';

class ServerW2NActionReq {
  static const GET_MORTCASEINFO_BYTASKID_URL = Server.BASE_URL +
      "/api/mortgagecaseinfo/getmortgagecaseinfowithlocationbytaskid?TaskId=";

  static const POST_ACCEPT_CLIENT_AGREEMENT_URL =
      Server.BASE_URL + "/api/mortgagecaseinfo/acceptclientagreement";
  static const GET_ACCEPTED_CLIENT_AGREEMENT_EMAIL_URL =
      Server.BASE_URL + "/api/users/sendclientagrementacceptedemail?";
}
