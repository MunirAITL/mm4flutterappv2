import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';

class BtnOutline extends StatelessWidget with Mixin {
  final String txt;
  final Color txtColor;
  double txtSize;
  final Color borderColor;
  final Function callback;
  final isBold;
  double radius;
  BtnOutline({
    Key key,
    @required this.txt,
    @required this.txtColor,
    @required this.borderColor,
    @required this.callback,
    this.txtSize,
    this.radius = 10,
    this.isBold = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (txtSize == null) {
      txtSize = MyTheme.txtSize;
    }
    return Container(
      child: OutlinedButton(
        //materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        child: new Txt(
            txt: txt,
            txtColor: txtColor,
            txtSize: txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: isBold),

        onPressed: () {
          callback();
        },
        //borderSide: BorderSide(color: borderColor),
        style: OutlinedButton.styleFrom(
          side: BorderSide(width: 0.5, color: Colors.black),
          minimumSize: Size.zero, // Set this
          padding: EdgeInsets.all(5), // and this
        ),
        /*shape: new OutlineInputBorder(
          borderRadius: new BorderRadius.circular(radius),
          borderSide: BorderSide(
              style: BorderStyle.solid, width: .7, color: borderColor),
        ),*/

        //color: Colors.black,
      ),
    );
  }
}
