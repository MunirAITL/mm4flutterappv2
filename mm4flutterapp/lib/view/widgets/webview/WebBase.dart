import 'dart:convert';
import 'dart:io';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/submit_case/submitCaseAPIMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/submit_application/submit_application.dart';
import 'package:aitl/model/json/db_cus/tab_dash/PrivacyPolicyAPIModel.dart';
import 'package:aitl/view/widgets/dialog/alert_dialog.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view_model/rx/WebviewController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:path/path.dart' as p;
//import 'package:webview_flutter/webview_flutter.dart'; //  FOR IOS
//import 'package:flutter_webview_pro/webview_flutter.dart'; //  FOR ANDROID
/*
abstract class WebBase<T extends StatefulWidget> extends State<T> with Mixin {
  final webviewController = Get.put(WebviewController());
  Map<String, String> header;

  onHideWebview();
  onShowWebview();

  //  HANDLERS
  onBack() {
    Navigator.of(context).pop();
  }

  onSubmitApplication(caseID, String message) {
    //if (message == "Back") {
    //onBack();
    //} else if (message == "SubmitApplication") {
    onHideWebview();
    AlertDialogCustom(
      context: context,
      onConfirmClick: () {
        SubmitApplication submitCaseAPIModel = new SubmitApplication(
            id: 0,
            userId: userData.userModel.id,
            status: 101,
            mortgageCaseInfoId: 0,
            taskId: int.parse(caseID),
            creationDate: DateTime.now().toString(),
            updatedDate: DateTime.now().toString(),
            versionNumber: 0,
            defineSolution: "",
            lenderProvider: "",
            term: "",
            loanAmount: 0,
            rate: 0,
            value: 0,
            fixedPeriodEnds: "",
            procFees: 0,
            adviceFee: 0,
            introducerFeeShare: 0,
            introducerPaymentMade: "",
            reasonForRecommendation: "",
            anyOtherComments: "",
            caseStatus: "",
            remarks: "",
            monthlyPaymentAmount: 0,
            otherChargeAmount: 0,
            rentalIncomeAmount: 0,
            valuationDate: "",
            fileUrl: "");

        SubmitCaseAPIMgr().wsOnPostCase(
            context: context,
            param: json.encode(submitCaseAPIModel),
            callback: (value) {
              onShowWebview();
              myLog("success === ${value.toString()}");
              try {
                Navigator.of(context).pop();
                //Get.to( () => MainCustomerScreen());
              } catch (e) {
                myLog("success === ${value.success}");
                myLog("success Message === ${value.messages}");
              }
            });
      },
      onCancelClick: () {
        onShowWebview();
        //Navigator.of(context).pop();
      },
    );
    //} else {
    //Get.offAll(MainCustomerScreen());
    //}
  }

  onDownloadFileModule(String message) async {
    await NetworkMgr.shared.downloadFile(
      context: context,
      url: message,
      callback: (f) {
        showToast(
            context: context, msg: "File downloaded successfully", which: 1);
      },
    );
  }

  onFullViewFileHandler(String message, Function(String) callbackReload) {
    final extension = p.extension(message);
    print(extension);
    if (extension.toLowerCase() == ".pdf") {
      onHideWebview();
      Get.to(
        () => PDFDocumentPage(
          title: "Document View",
          url: message,
        ),
      ).then((value) => onShowWebview());
    } else if (extension.toLowerCase() == ".doc" ||
        extension.toLowerCase() == ".docx" ||
        extension.toLowerCase() == ".xls") {
      callbackReload(
          "http://drive.google.com/viewerng/viewer?embedded=true&url=" +
              message);
    } else {
      onHideWebview();
      Get.to(() => PicFullView(
            url: message,
            title: "Document View",
          )).then((value) {
        onShowWebview();
      });
    }
  }

  onClickTermsOfBusiness(
      String message, Function(String) callbackReload) async {
    var url = Server.TERMS_POLICY;
    url = url.replaceAll(
        "#UserCompanyId#", userData.userModel.userCompanyID.toString());
    myLog("PrivacyPolicyHelper= " + url);
    onHideWebview();
    await NetworkMgr()
        .req<PrivacyPolicyAPIModel, Null>(
      context: context,
      reqType: ReqType.Get,
      url: url,
      isLoading: true,
    )
        .then((model) async {
      for (var m in model.responseData.termsPrivacyNoticeSetupsList) {
        if (m.type == "Terms of Business") {
          final extension = p.extension(m.webUrl);
          print(extension);
          if (extension.toLowerCase() == ".pdf") {
            callbackReload(
                "http://drive.google.com/viewerng/viewer?embedded=true&url=" +
                    m.webUrl);
          } else {
            callbackReload(m.webUrl);
          }
          webviewController.appTitle.value = "Terms of Business";
          break;
        }
      }
      Future.delayed(Duration(seconds: AppConfig.AlertDismisSec), () {
        onShowWebview();
      });
    });
  }

  onCallbackclientagreement() {
    Get.offAll(MainCustomerScreen());
  }

  onCallbackclientrecommendationagreement() {
    Get.offAll(MainCustomerScreen());
  }
}*/
