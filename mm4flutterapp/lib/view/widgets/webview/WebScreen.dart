import 'dart:io';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/webview/WebControl.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/WebviewController.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';

import 'AppWebView.dart';

class WebScreen extends StatefulWidget {
  final String url;
  final String title;
  final String caseID;

  const WebScreen({
    Key key,
    this.caseID,
    @required this.url,
    @required this.title,
  }) : super(key: key);

  @override
  State createState() => _WebScreenState();
}

class _WebScreenState extends State<WebScreen> with Mixin {
  final webviewController = Get.put(WebviewController());
  final CookieManager cookieManager = CookieManager.instance();

  var cookieStr;
  var listCookies;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    webviewController.dispose();
    cookieStr = null;
    //flutterWebviewPlugin.close();
    //flutterWebviewPlugin.dispose();
    //webView = null;
    super.dispose();
  }

  appInit() async {
    try {
      await Permission.camera.request();

      print(widget.caseID + "->" + widget.url);

      CookieJar cj = await CookieMgr().getCookiee();
      listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
      cookieStr = listCookies[0].toString();
      myLog(cookieStr);
      if (cookieStr.length > 0) {
        cookieManager.setCookie(
          url: Uri.parse(Server.BASE_URL),
          name: listCookies[0].name,
          value: listCookies[0].value,
          domain: listCookies[0].domain,
          path: listCookies[0].path,
          maxAge: listCookies[0].maxAge,
          //expiresDate: listCookies[0].expires,
          isSecure: true,
          //isHttpOnly: true,
        );
      }
      setState(() {});
    } catch (e) {
      cookieStr = "";
      if (mounted) setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      //resizeToAvoidBottomPadding :true,
      //resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: MyTheme.appbarElevation,
        backgroundColor: MyTheme.statusBarColor,
        automaticallyImplyLeading: false,
        leading: IconButton(
          icon: Icon(
              webviewController.appTitle.value == ""
                  ? Icons.arrow_back
                  : Icons.close,
              color: Colors.white),
          onPressed: () async {
            if (webviewController.appTitle.value == "")
              Get.back();
            else {
              StateProvider().notify(ObserverState.STATE_WEBVIEW_BACK);
              webviewController.appTitle.value = "";
            }
          },
        ),
        title: UIHelper().drawAppbarTitle(
            title: (webviewController.appTitle.value == "")
                ? widget.title
                : webviewController.appTitle.value),
        centerTitle: true,
        bottom: PreferredSize(
          preferredSize:
              Size.fromHeight((webviewController.isLoading.value) ? .5 : 0),
          child: Obx(() => (webviewController.isLoading.value)
              ? AppbarBotProgBar(
                  backgroundColor: MyTheme.appbarProgColor,
                )
              : SizedBox()),
        ),
      ),
      body: (cookieStr != null)
          ? openWebview()

          /*WebControl(
              url: widget.url,
              cookieStr: cookieStr,
              listCookies: listCookies,
              caseID: widget.caseID,
            )*/
          : SizedBox(),
    );
  }

  openWebview() {
    return OpenWebScreen(
        context: context,
        listCookies: listCookies,
        caseID: widget.caseID,
        title: widget.title,
        url: widget.url);
  }
}
