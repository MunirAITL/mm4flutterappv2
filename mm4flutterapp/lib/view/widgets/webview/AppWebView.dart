import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/submit_case/submitCaseAPIMgr.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/submit_application/submit_application.dart';
import 'package:aitl/model/json/db_cus/tab_dash/PrivacyPolicyAPIModel.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view/widgets/webview/PDFDocumentPage.dart';
import 'package:aitl/view/widgets/webview/WebControl.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/WebviewController.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:aitl/view/widgets/dialog/alert_dialog.dart';
import '../dialog/alert_dialog.dart';
import 'package:path/path.dart' as p;

OpenWebScreen({
  context,
  caseID,
  url,
  title,
  listCookies,
}) {
  InAppWebViewGroupOptions options = InAppWebViewGroupOptions(
      crossPlatform: InAppWebViewOptions(
        useShouldOverrideUrlLoading: true,
        mediaPlaybackRequiresUserGesture: false,
        transparentBackground: true,
      ),
      android: AndroidInAppWebViewOptions(
        useHybridComposition: true,
      ),
      ios: IOSInAppWebViewOptions(
        allowsInlineMediaPlayback: true,
      ));

  return InAppWebView(
    initialUrlRequest: URLRequest(url: Uri.parse(url)),
    initialOptions: options,
    //pullToRefreshController: pullToRefreshController,
    onWebViewCreated: (controller) {
      //webViewController = controller;
      controller.addJavaScriptHandler(
          handlerName: "Back",
          callback: (args) {
            Navigator.of(context).pop();
          });
      controller.addJavaScriptHandler(
          handlerName: "SubmitApplication",
          callback: (args) {
            //  hide
            AlertDialogCustom(
              context: context,
              onConfirmClick: () {
                SubmitApplication submitCaseAPIModel = new SubmitApplication(
                    id: 0,
                    userId: userData.userModel.id,
                    status: 101,
                    mortgageCaseInfoId: 0,
                    taskId: int.parse(caseID),
                    creationDate: DateTime.now().toString(),
                    updatedDate: DateTime.now().toString(),
                    versionNumber: 0,
                    defineSolution: "",
                    lenderProvider: "",
                    term: "",
                    loanAmount: 0,
                    rate: 0,
                    value: 0,
                    fixedPeriodEnds: "",
                    procFees: 0,
                    adviceFee: 0,
                    introducerFeeShare: 0,
                    introducerPaymentMade: "",
                    reasonForRecommendation: "",
                    anyOtherComments: "",
                    caseStatus: "",
                    remarks: "",
                    monthlyPaymentAmount: 0,
                    otherChargeAmount: 0,
                    rentalIncomeAmount: 0,
                    valuationDate: "",
                    fileUrl: "");

                SubmitCaseAPIMgr().wsOnPostCase(
                    context: context,
                    param: json.encode(submitCaseAPIModel),
                    callback: (value) {
                      //onShowWebview();
                      log("success === ${value.toString()}");
                      try {
                        Navigator.of(context).pop();
                        //Get.to( () => MainCustomerScreen());
                      } catch (e) {
                        log("success === ${value.success}");
                        log("success Message === ${value.messages}");
                      }
                    });
              },
              onCancelClick: () {
                //onShowWebview();
                //Navigator.of(context).pop();
              },
            );
          });
      controller.addJavaScriptHandler(
          handlerName: "DownloadFileModule",
          callback: (args) async {
            await NetworkMgr.shared.downloadFile(
              context: context,
              url: args[0],
              callback: (f) {
                Get.snackbar("File downloaded successfully", "",
                    colorText: Colors.white,
                    backgroundColor: MyTheme.statusBarColor);
              },
            );
          });

      controller.addJavaScriptHandler(
          handlerName: "onFullViewFileHandler",
          callback: (args) async {
            final extension = p.extension(args[0]);
            print(extension);
            if (extension.toLowerCase() == ".pdf") {
              //onHideWebview();
              Get.to(
                () => PDFDocumentPage(
                  title: "Document View",
                  url: args[0],
                ),
              ); //.then((value) => //onShowWebview());
            } else if (extension.toLowerCase() == ".doc" ||
                extension.toLowerCase() == ".docx" ||
                extension.toLowerCase() == ".xls") {
              final url2 =
                  "http://drive.google.com/viewerng/viewer?embedded=true&url=" +
                      args[0];
              //Controller.appTitle.value = "Document View";
              controller.loadUrl(urlRequest: URLRequest(url: Uri.parse(url2)));
            } else {
              //onHideWebview();
              Get.to(() => PicFullView(
                    url: args[0],
                    title: "Document View",
                  )).then((value) {
                //onShowWebview();
              });
            }
          });

      controller.addJavaScriptHandler(
          handlerName: "onClickTermsOfBusiness",
          callback: (args) async {
            var url = Server.TERMS_POLICY;
            url = url.replaceAll(
                "#UserCompanyId#", userData.userModel.userCompanyID.toString());
            log("PrivacyPolicyHelper= " + url);
            //onHideWebview();
            await NetworkMgr()
                .req<PrivacyPolicyAPIModel, Null>(
              context: context,
              reqType: ReqType.Get,
              url: url,
              isLoading: true,
            )
                .then((model) async {
              for (var m in model.responseData.termsPrivacyNoticeSetupsList) {
                if (m.type == "Terms of Business") {
                  final extension = p.extension(m.webUrl);
                  print(extension);
                  if (extension.toLowerCase() == ".pdf") {
                    final url2 =
                        "http://drive.google.com/viewerng/viewer?embedded=true&url=" +
                            m.webUrl;
                    controller.loadUrl(
                        urlRequest: URLRequest(url: Uri.parse(url2)));
                  } else {
                    controller.loadUrl(
                        urlRequest: URLRequest(url: Uri.parse(m.webUrl)));
                  }
                  //webviewController.appTitle.value = "Terms of Business";
                  break;
                }
              }
              //Future.delayed(Duration(seconds: AppConfig.AlertDismisSec), () {
              //onShowWebview();
              //});
            });
          });
      controller.addJavaScriptHandler(
          handlerName: "callbackclientrecommendationagreement",
          callback: (args) {
            Get.offAll(MainCustomerScreen());
          });
      controller.addJavaScriptHandler(
          handlerName: "callbackclientagreement",
          callback: (args) {
            Get.offAll(MainCustomerScreen());
          });
    },
    onLoadStart: (controller, url) {
      EasyLoading.show(status: "Loading...");
    },
    androidOnPermissionRequest: (controller, origin, resources) async {
      return PermissionRequestResponse(
          resources: resources, action: PermissionRequestResponseAction.GRANT);
    },
    onLoadStop: (controller, url) async {
      EasyLoading.dismiss();
    },
    onLoadError: (controller, url, code, message) {},
    onProgressChanged: (controller, progress) {},
    onUpdateVisitedHistory: (controller, url, androidIsReload) {},
    onConsoleMessage: (controller, consoleMessage) {
      print(consoleMessage);
    },
  );
}
