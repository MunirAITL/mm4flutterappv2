import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../Mixin.dart';

HrefLoginDialog({BuildContext context, Function callback}) {
  return Dialog(
    backgroundColor: Colors.transparent,
    child: Container(
      margin: EdgeInsets.only(left: 0.0, right: 0.0),
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Material(
            elevation: 0,
            color: Colors.transparent,
            child: Container(
              padding: EdgeInsets.only(
                top: 18.0,
              ),
              margin: EdgeInsets.only(top: 30, right: 10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    width: 1,
                    color: MyTheme.dBlueAirColor,
                    style: BorderStyle.solid,
                  ),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 0.0,
                      offset: Offset(0.0, 0.0),
                    ),
                  ]),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 10),
                    child: Txt(
                      txt: "Email not found",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize + .2,
                      txtAlign: TextAlign.center,
                      isBold: true,
                      //txtLineSpace: 1.5,
                    ),
                  ),
                  Txt(
                    txt:
                        "Sorry, we didn't recognise\nthis email address.\nDid you mistype it or do\nyou want to create a new\naccount?",
                    txtColor: Colors.black87,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                    //txtLineSpace: 1.5,
                  ),
                  SizedBox(height: 10),
                  Divider(color: Colors.grey, height: 10),
                  SizedBox(height: 5),
                  GestureDetector(
                    onTap: () {
                      Get.back();
                      callback();
                    },
                    child: Text(
                      "Create an account",
                      style: TextStyle(
                        color: Colors.cyan,
                        fontSize: 17,
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                  Divider(color: Colors.grey, height: 10),
                  SizedBox(height: 5),
                  GestureDetector(
                    onTap: () {
                      Get.back();
                    },
                    child: Text(
                      "Edit email",
                      style: TextStyle(
                        color: Colors.cyan,
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                ],
              ),
            ),
          ),
          Positioned(
            top: 5,
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        10), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                      border: Border.all(
                        width: .5,
                        color: MyTheme.dBlueAirColor,
                        style: BorderStyle.solid,
                      ),
                    ),
                    child: Icon(Icons.info,
                        color: MyTheme.dBlueAirColor, size: 30)),
              ),
            ),
          ),
          Positioned(
            top: 15,
            right: 0,
            child: Align(
              alignment: Alignment.topRight,
              child: GestureDetector(
                  child: Container(
                    //width: 50.0,
                    //height: 50.0,
                    padding: const EdgeInsets.all(
                        2), //I used some padding without fixed width and height
                    decoration: new BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: Colors.white,
                      border: Border.all(
                        width: 1,
                        color: MyTheme.dBlueAirColor,
                        style: BorderStyle.solid,
                      ),
                    ),
                    child: Icon(
                      Icons.close,
                      color: MyTheme.dBlueAirColor,
                    ),
                  ),
                  onTap: () {
                    Get.back();
                  }),
            ),
          ),
        ],
      ),
    ),
  );
}
