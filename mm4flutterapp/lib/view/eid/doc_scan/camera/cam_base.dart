import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:aitl/mixin.dart';
//import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
//import 'package:google_ml_kit/google_ml_kit.dart';
import 'package:http/http.dart' as http;

import 'dart:async';
import 'dart:io';

//import 'package:flutter_better_camera/camera.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';

abstract class CamBase<T extends StatefulWidget> extends State<T>
    with Mixin, WidgetsBindingObserver {
  CameraController controller;

  bool enableAudio = true;
  FlashMode flashMode = FlashMode.off;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void showInSnackBar(String message) {
    //scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
    showToast(context: context, msg: message);
  }

  /// Flash Toggle Button
  Widget flashButton() {
    IconData iconData = Icons.flash_off;
    Color color = Colors.black;
    if (flashMode == FlashMode.always) {
      iconData = Icons.flash_on;
      color = Colors.blue;
    } else if (flashMode == FlashMode.auto) {
      iconData = Icons.flash_auto;
      color = Colors.red;
    }
    return IconButton(
      icon: Icon(iconData),
      color: color,
      onPressed: controller != null && controller.value.isInitialized
          ? onFlashButtonPressed
          : null,
    );
  }

  /// Toggle Flash
  Future<void> onFlashButtonPressed() async {
    if (flashMode == FlashMode.off || flashMode == FlashMode.torch) {
      // Turn on the flash for capture
      flashMode = FlashMode.always;
    } else if (flashMode == FlashMode.always) {
      // Turn on the flash for capture if needed
      flashMode = FlashMode.auto;
    } else {
      // Turn off the flash
      flashMode = FlashMode.off;
    }
    // Apply the new mode
    await controller.setFlashMode(flashMode);

    // Change UI State
    setState(() {});
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(cameraDescription, ResolutionPreset.max);
    controller.initialize().then((_) {
      if (!mounted) {
        return;
      }
      setState(() {});
    }).catchError((Object e) {
      if (e is CameraException) {
        switch (e.code) {
          case 'CameraAccessDenied':
            print('User denied camera access.');
            break;
          default:
            print('Handle other errors.');
            break;
        }
      }
    });
    // If the controller is updated then update the UI.
    /*controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        showInSnackBar('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }*/
  }

  void onTakePictureButtonPressed() {
    takePicture().then((String filePath) {
      if (mounted) {
        if (filePath != null) {
          Get.back(result: filePath);
        }
      }
    });
  }

  Future<String> takePicture() async {
    if (!controller.value.isInitialized) {
      showInSnackBar('Error: select a camera first.');
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/eid';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    XFile f;
    try {
      f = await controller.takePicture();
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return f.path;
  }

  void _showCameraException(CameraException e) {
    log(e.code + e.description);
    showInSnackBar('Error: ${e.code}\n${e.description}');
  }
}
