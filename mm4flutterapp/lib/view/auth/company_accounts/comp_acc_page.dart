import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/auth/multiple_applicants/MultipleApplicantAPIModel.dart';
import 'package:aitl/view/NoAccess.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CompAccScreen2 extends StatefulWidget {
  const CompAccScreen2({Key key}) : super(key: key);

  @override
  State<CompAccScreen2> createState() => _CompAccScreenState();
}

class _CompAccScreenState extends State<CompAccScreen2> with Mixin {
  List<UserCompanyInfos> userCompanyInfos = [];

  var companyIndex = 0.obs;

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    userCompanyInfos = null;
    super.dispose();
  }

  initPage() async {
    try {
      await APIViewModel().req<MultipleApplicantAPIModel>(
          context: context,
          url: Server.COMP_ACC_GET_URL
              .replaceAll("#userId#", userData.userModel.id.toString()),
          reqType: ReqType.Get,
          callback: (model) async {
            if (mounted && model != null) {
              userCompanyInfos = model.responseData.userCompanyInfos;
              setState(() {});
            }
          });
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: MyTheme.appbarElevation,
          iconTheme: IconThemeData(color: Colors.white //change your color here
              ),
          backgroundColor: MyTheme.statusBarColor,
          title: UIHelper().drawAppbarTitle(title: "Your accounts"),
          centerTitle: true,
          /*leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () async {
                Navigator.pop(context);
              }),*/
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
              txt: "Use these accounts instead of creating a new one.",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false,
              //txtLineSpace: 1.2,
            ),
            SizedBox(height: 10),
            ListView.builder(
              shrinkWrap: true,
              primary: false,
              itemCount: userCompanyInfos.length,
              itemBuilder: (context, index) {
                final model = userCompanyInfos[index];
                //MMBtn btn = item['btn'] as MMBtn;
                return Card(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                    child: ListTile(
                      dense: true,
                      minLeadingWidth: 0,
                      title: Theme(
                          data: MyTheme.radioThemeData,
                          child: Obx(
                            () => Transform.translate(
                              offset: Offset(-30, 0),
                              child: RadioListTile(
                                value: index,
                                groupValue: companyIndex.value,
                                onChanged: (value) {
                                  companyIndex.value = value;
                                },
                                title: Txt(
                                    txt: model.companyName,
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .2,
                                    txtAlign: TextAlign.start,
                                    isBold: true),
                              ),
                            ),
                          )),
                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        //mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          _drawRow("Domain:", model.companyWebsite),
                          SizedBox(height: 10),
                          _drawRow("Phone:", model.officePhoneNumber),
                          SizedBox(height: 10),
                          MMBtn(
                                txt: model.userType,
                                bgColor: Colors.grey,
                                height: 40,
                                width: null,
                                callback: () {
                                  switch (model.id) {
                                    case 1003:
                                      break;
                                    default:
                                  }
                                },
                              ) ??
                              SizedBox(),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
            SizedBox(height: 20),
            (userCompanyInfos.length > 0)
                ? Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: MMBtn(
                      txt: "Continue with this account",
                      height: getHP(context, 7),
                      width: null,
                      callback: () async {
                        //
                        if (userData.userModel.communityID.toString() == "1") {
                          PrefMgr.shared.setPrefBool("isCompanyAccount", true);
                          Get.offAll(
                            () => MainCustomerScreen(),
                          ).then((value) {
                            //callback(route);
                          });

                          /*     if (userData.communityId == 2) {
                                                        navTo(context: context, isRep: true, page: () => MainIntroducerScreen()).then((value) {
                                                          //callback(route);
                                                        });
                                                      } else {
                                                        navTo(
                                                          context: context,
                                                          isRep: true,
                                                          page: () => MainCustomerScreenNew(),
                                                        ).then((value) {
                                                          //callback(route);
                                                        });
                                                      }*/

                        } else {
                          Get.off(() => NoAccess());
                        }
                      },
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }

  _drawRow(title, txt) => Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Flexible(
            child: Txt(
                txt: title,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                //txtLineSpace: 1.4,
                isBold: true),
          ),
          SizedBox(width: 10),
          Expanded(
            flex: 3,
            child: Txt(
                txt: txt,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                //txtLineSpace: 1.4,
                isBold: false),
          )
        ],
      );
}
