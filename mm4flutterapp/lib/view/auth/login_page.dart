import 'package:aitl/config/Server.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/NoAccess.dart';
import 'package:aitl/view/auth/ForgotScreen.dart';
import 'package:aitl/view/auth/company_accounts/comp_acc_page.dart';
import 'package:aitl/view/auth/otp/OtpByEmailPage.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../../config/MyTheme.dart';
import '../../controller/form_validator/UserProfileVal.dart';
import '../../controller/network/NetworkMgr.dart';
import '../../model/data/UserData.dart';
import '../../model/json/auth/LoginAPIModel.dart';
import '../../view_model/api/api_view_model.dart';
import '../db_cus/MainCustomerScreen.dart';
import '../widgets/input/InputTitleBox.dart';
import '../widgets/txt/Txt.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with Mixin {
  final email = TextEditingController();
  final pwd = TextEditingController();

  final focusEmail = FocusNode();
  final focusPwd = FocusNode();

  var isPwdVisable = true;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    email.dispose();
    pwd.dispose();
    super.dispose();
  }

  appInit() async {}

  validate() {
    //  Chowdhuryendu@gmail.com / 12345678
    //  papon+jh@aitl.co/123456
    // davidsen.cust001@help.mortgage-magic.co.uk/12345678
    if (UserProfileVal()
        .isEmpty(context, email, 'Please enter your email or mobile')) {
      return false;
    } else if (!UserProfileVal().isPwdOK(context, pwd)) {
      return false;
    }
    return true;
  }

  getParam({
    String email,
    String pwd,
    bool persist = false,
    bool checkSignUpMobileNumber = false,
    String countryCode = "880",
    String status = "101",
    String oTPCode = "",
    String birthDay = "",
    String birthMonth = "",
    String birthYear = "",
    String userCompanyId = "0",
  }) {
    return {
      "Email": email,
      "Password": pwd,
      "Persist": false,
      "CheckSignUpMobileNumber": false,
      "CountryCode": "880",
      "Status": "101",
      "OTPCode": "",
      "BirthDay": "",
      "BirthMonth": "",
      "BirthYear": "",
      "UserCompanyId": "0"
    };
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        resizeToAvoidBottomInset: false,
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        IconButton(
            onPressed: () {
              Get.back();
            },
            icon: Icon(
              Icons.arrow_back,
              color: Colors.black,
            )),
        SizedBox(height: 20),
        Align(
            alignment: Alignment.center,
            child: Image.asset(
              "assets/images/logo/mm.png",
              fit: BoxFit.cover,
              width: getWP(context, 60),
            )),
        SizedBox(height: 20),
        SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                drawInputBox(
                    context: context,
                    title: null,
                    input: email,
                    ph: "Email/Phone",
                    kbType: TextInputType.emailAddress,
                    inputAction: TextInputAction.next,
                    focusNode: focusEmail,
                    focusNodeNext: focusPwd,
                    autofocus: true,
                    len: 50),
                SizedBox(height: 10),
                drawInputBox(
                    context: context,
                    title: null,
                    input: pwd,
                    ph: "Password",
                    kbType: TextInputType.text,
                    inputAction: TextInputAction.done,
                    focusNode: focusPwd,
                    len: 20,
                    isPwd: true,
                    isobscureText: isPwdVisable,
                    callback: (v) {
                      isPwdVisable = v;
                      setState(() {});
                    }),
                SizedBox(height: 20),
                Align(
                  alignment: Alignment.centerRight,
                  child: InkWell(
                      child: new Text('Forgot password?',
                          style: TextStyle(color: Colors.black87)),
                      onTap: () {
                        Get.to(() => ForgotScreen());
                      }),
                ),
                SizedBox(height: 20),
                MMBtn(
                    txt: "Sign In",
                    txtColor: Colors.white,
                    width: getW(context),
                    height: getHP(context, 7),
                    radius: 10,
                    callback: () async {
                      if (validate()) {
                        APIViewModel().req<LoginAPIModel>(
                          context: context,
                          url: Server.LOGIN_URL,
                          param: getParam(
                              email: email.text.trim(), pwd: pwd.text.trim()),
                          reqType: ReqType.Post,
                          callback: (model) async {
                            if (mounted && model != null) {
                              if (model.success) {
                                if (model.responseData.user.communityID
                                        .toString() ==
                                    "1") {
                                  try {
                                    await PrefMgr.shared.setPrefStr(
                                        "accessToken",
                                        model.responseData.accessToken);
                                    await PrefMgr.shared.setPrefStr(
                                        "refreshToken",
                                        model.responseData.refreshToken);
                                    await DBMgr.shared.setUserProfile(
                                        user: model.responseData.user);
                                    await userData.setUserModel();
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                  if (!Server.isOtp) {
                                    //if (userData.userModel.companyCount > 1) {
                                    //Get.off(() => CompAccScreen());
                                    //} else {
                                    Get.off(() => MainCustomerScreen());
                                    //}
                                  } else {
                                    if (model.responseData.user
                                        .isMobileNumberVerified) {
                                      //if (userData.userModel.companyCount > 1) {
                                      //Get.off(() => CompAccScreen());
                                      //} else {
                                      Get.off(() => MainCustomerScreen());
                                      //}
                                    } else {
                                      final user = model.responseData.user;
                                      Get.to(() =>
                                          OtpByEmailPage(email: user.email));
                                    }
                                  }
                                } else {
                                  Get.to(() => NoAccess());
                                }
                              } else {
                                try {
                                  final err = model.errorMessages.login[0];
                                  showToast(context: context, msg: err);
                                } catch (e) {
                                  myLog(e.toString());
                                }
                              }
                            }
                          },
                          isCookie: true,
                        );
                      }
                    }),
                SizedBox(height: getHP(context, 10)),
              ],
            ),
          ),
        ),

        //_drawSocialLoginBtn(),
      ]),
    );
  }

  _drawSocialLoginBtn() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15),
          child: Row(
            children: [
              Expanded(child: Divider(color: Colors.grey)),
              Txt(
                  txt: "or you can use",
                  txtColor: Colors.black45,
                  txtSize: MyTheme.txtSize - .6,
                  txtAlign: TextAlign.center,
                  isBold: false),
              Expanded(child: Divider(color: Colors.grey)),
            ],
          ),
        ),
        SizedBox(height: 20),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 5, bottom: 5, left: 10, right: 10),
                child: Image.asset(
                  "assets/images/icons/google_btn.png",
                  //fit: BoxFit.fill,
                  width: 100,
                  height: 25,
                ),
              ),
            ),
            SizedBox(width: 20),
            Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 5, bottom: 5, left: 10, right: 10),
                child: Image.asset(
                  "assets/images/icons/fb_btn.png",
                  //fit: BoxFit.fill,
                  width: 100,
                  height: 25,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
