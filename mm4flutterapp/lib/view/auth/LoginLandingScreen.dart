import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/auth/RegScreen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import 'email/signin_page.dart';
import 'login_page.dart';

class LoginLandingScreen extends StatefulWidget {
  @override
  State createState() => _LoginLandingScreenState();
}

class _LoginLandingScreenState extends State<LoginLandingScreen> with Mixin {
  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {} catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    //SystemChrome.setEnabledSystemUIMode(SystemUiMode.leanBack);
    return SafeArea(
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: MyTheme.bgColor2,
            body: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout())));
  }

  drawLayout() {
    return Stack(
      children: [
        Positioned(
          top: 80,
          left: getWP(context, 18),
          child: Image.asset(
            "assets/images/logo/mm.png",
            fit: BoxFit.cover,
            width: getWP(context, 64),
          ),
        ),
        Positioned(
          top: getHP(context, 60),
          left: 40,
          right: 40,
          child: Container(
            //color: Colors.black,
            //height: getHP(context, 20),
            child: Padding(
              padding: const EdgeInsets.only(left: 0, right: 0),
              child: Column(children: [
                MMBtn(
                    txt: "Login with Email",
                    txtColor: Colors.white,
                    bgColor: null,
                    height: getHP(context, 6),
                    width: getW(context),
                    radius: 5,
                    callback: () {
                      Get.to(() => LoginPage());
                    }),
                SizedBox(height: 15),
                MMBtn(
                    txt: "Login with OTP Mobile",
                    txtColor: Colors.white,
                    bgColor: null,
                    height: getHP(context, 6),
                    width: getW(context),
                    radius: 5,
                    callback: () {
                      Get.to(() => SigninPage(isOptMobile: true));
                    }),
                SizedBox(height: 15),
                MMBtn(
                    txt: "Login with OTP Email",
                    txtColor: Colors.white,
                    bgColor: null,
                    height: getHP(context, 6),
                    width: getW(context),
                    radius: 5,
                    callback: () {
                      Get.to(() => SigninPage(isOptMobile: false));
                    }),
              ]),
            ),
          ),
        ),
        Positioned(
          bottom: 40,
          left: 20,
          right: 20,
          child: GestureDetector(
            onTap: () {
              Get.to(() => RegScreen());
            },
            child: Text(
              "Open an account",
              textAlign: TextAlign.center,
              style: TextStyle(
                  decoration: TextDecoration.underline,
                  color: HexColor.fromHex("#06152B"),
                  fontSize: 16,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ],
    );
  }
}
