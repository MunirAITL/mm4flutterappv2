import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/hreflogin/PostEmailOtpAPIModel.dart';
import 'package:aitl/model/json/auth/hreflogin/SendUserEmailOtpAPIModel.dart';
import 'package:aitl/model/json/auth/otp/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/view/NoAccess.dart';
import 'package:aitl/view/auth/LoginLandingScreen.dart';
import 'package:aitl/view/auth/company_accounts/comp_acc_page.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class OtpByEmailPage extends StatefulWidget {
  final bool isBack;
  final String email;

  const OtpByEmailPage({
    Key key,
    @required this.email,
    this.isBack = false,
  }) : super(key: key);

  @override
  State createState() => _OtpByEmailPageState();
}

class _OtpByEmailPageState extends State<OtpByEmailPage>
    with SingleTickerProviderStateMixin, Mixin {
  // final _scaffoldKey = GlobalKey<ScaffoldState>();

  String countryCode = "+44";
  String countryName = "GB";

  bool isLoading = false;

  // Variables
  static const int SMS_AUTH_CODE_LEN = 6;
  int _currentDigit;
  int _firstDigit;
  int _secondDigit;
  int _thirdDigit;
  int _fourthDigit;
  int _fiveDigit;
  int _sixDigit;

  var userId = 0;
  var otpCode = "";

  onOptSend() async {
    otpCode = "";
    await APIViewModel().req<PostEmailOtpAPIModel>(
      context: context,
      url: Server.POSTEMAILOTP_POST_URL,
      reqType: ReqType.Post,
      param: {
        "Email": widget.email,
        "Status": "101",
      },
      callback: (model2) async {
        if (mounted) {
          if (model2 != null) {
            if (model2.success) {
              await APIViewModel().req<SendUserEmailOtpAPIModel>(
                  context: context,
                  url: Server.SENDUSEREMAILOTP_GET_URL.replaceAll(
                      "#otpId#", model2.responseData.userOTP.id.toString()),
                  reqType: ReqType.Get,
                  callback: (model3) async {
                    if (mounted) {
                      if (model3 != null) {
                        if (model3.success) {
                          showToast(
                              context: context, msg: 'OTP sent successfully');
                        } else {
                          final err =
                              model3.messages.getSenduseremailotplogin[0];
                          showToast(context: context, msg: err, which: 0);
                        }
                        clearOtp();
                      }
                    }
                  });
            } else {
              final err = model2.messages.postUserotp[0];
              showToast(context: context, msg: err, which: 0);
            }
          }
        }
      },
    );
  }

  getFullPhoneNumber({String countryCodeTxt, String number}) {
    var fullNumber = countryCodeTxt + number;

    return fullNumber;
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {} catch (e) {}
    super.dispose();
  }

  void clearOtp() {
    _sixDigit = null;
    _fiveDigit = null;
    _fourthDigit = null;
    _thirdDigit = null;
    _secondDigit = null;
    _firstDigit = null;
    if (mounted) {
      setState(() {});
    }
  }

  appInit() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;

          countryCode = cd;
        });
      }
    } catch (e) {
      print("Sms2 screen country code and name problem ");
    }
    try {
      userId = userData.userModel.id;
    } catch (e) {
      debugPrint("User id getting problem $userId = " + e.toString());
    }

    onOptSend();
  }

  // Return "OTP" input field
  get _getInputField {
    return new Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        _otpTextField(_firstDigit),
        _otpTextField(_secondDigit),
        _otpTextField(_thirdDigit),
        _otpTextField(_fourthDigit),
        _otpTextField(_fiveDigit),
        _otpTextField(_sixDigit),
      ],
    );
  }

  // Returns "Resend" button
  get _getResendButton {
    return !isLoading
        ? Center(
            child: Padding(
              padding: const EdgeInsets.only(
                  left: 20, right: 20, top: 20, bottom: 20),
              child: MMBtn(
                  //bgColor: ,
                  //txtColor: Colors.black,
                  txt: (!widget.isBack) ? "Continue" : "Back",
                  height: getHP(context, 7),
                  width: getW(context) / 3,
                  radius: 5,
                  callback: () {
                    if (!widget.isBack) {
                      Get.offAll(() => LoginLandingScreen());
                    } else {
                      Get.back();
                    }
                  }),
            ),

            /* child: Txt(
        txt: "Resend code",
        txtColor: Colors.black,
        txtSize: MyTheme.txtSize - .2,
        txtAlign: TextAlign.center,
        isBold: false,
      ),*/
          )
        : SizedBox();
  }

  // Returns "Otp" keyboard
  get _getOtpKeyboard {
    return new Container(
        height: getW(context) - 140,
        child: new Column(
          children: <Widget>[
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "1",
                      onPressed: () {
                        _setCurrentDigit(1);
                      }),
                  _otpKeyboardInputButton(
                      label: "2",
                      onPressed: () {
                        _setCurrentDigit(2);
                      }),
                  _otpKeyboardInputButton(
                      label: "3",
                      onPressed: () {
                        _setCurrentDigit(3);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "4",
                      onPressed: () {
                        _setCurrentDigit(4);
                      }),
                  _otpKeyboardInputButton(
                      label: "5",
                      onPressed: () {
                        _setCurrentDigit(5);
                      }),
                  _otpKeyboardInputButton(
                      label: "6",
                      onPressed: () {
                        _setCurrentDigit(6);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardInputButton(
                      label: "7",
                      onPressed: () {
                        _setCurrentDigit(7);
                      }),
                  _otpKeyboardInputButton(
                      label: "8",
                      onPressed: () {
                        _setCurrentDigit(8);
                      }),
                  _otpKeyboardInputButton(
                      label: "9",
                      onPressed: () {
                        _setCurrentDigit(9);
                      }),
                ],
              ),
            ),
            new Expanded(
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _otpKeyboardActionButton(
                      label: new Icon(
                        Icons.backspace,
                        color: Color(0xFF8C8C8C),
                        size: 30,
                      ),
                      onPressed: () {
                        if (mounted) {
                          setState(() {
                            otpCode = "";
                            if (_sixDigit != null) {
                              _sixDigit = null;
                            } else if (_fiveDigit != null) {
                              _fiveDigit = null;
                            } else if (_fourthDigit != null) {
                              _fourthDigit = null;
                            } else if (_thirdDigit != null) {
                              _thirdDigit = null;
                            } else if (_secondDigit != null) {
                              _secondDigit = null;
                            } else if (_firstDigit != null) {
                              _firstDigit = null;
                            }
                          });
                        }
                      }),
                  _otpKeyboardInputButton(
                      label: "0",
                      onPressed: () {
                        _setCurrentDigit(0);
                      }),
                  _otpKeyboardActionButton(
                      label: Container(
                        decoration: BoxDecoration(
                          color: Color(0xFF8C8C8C),
                          shape: BoxShape.circle,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(5),
                          child: new Icon(
                            Icons.keyboard_return,
                            color: Colors.white,
                            size: 25,
                          ),
                        ),
                      ),
                      onPressed: () {
                        if (mounted) {
                          if (otpCode.length == SMS_AUTH_CODE_LEN) {
                            callOTPApi();
                          }
                        }
                      }),
                ],
              ),
            ),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        /*appBar: AppBar(
          automaticallyImplyLeading: false,
          elevation: 0,
          iconTheme: IconThemeData(color: MyTheme.titleColor),
          backgroundColor: MyTheme.bgColor2,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          centerTitle: true,
        ),*/
        bottomNavigationBar: BottomAppBar(
          color: Colors.white,
          child: _getOtpKeyboard,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
      child: Container(
        // width: getW(context),
        // height: getH(context),
        child: Column(
          // mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.center,
          // mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 40, left: 10, right: 10),
              child: Column(
                children: [
                  Txt(
                    txt: "Verify yourself",
                    txtColor: MyTheme.titleColor,
                    txtSize: MyTheme.txtSize + 1.2,
                    txtAlign: TextAlign.center,
                    isBold: true,
                  ),
                  SizedBox(height: 20),
                  Text(
                      "We have sent a code in your email.\nPlease enter the code here. ",
                      textAlign: TextAlign.center,
                      style: new TextStyle(
                          fontSize: getTxtSize(
                              context: context, txtSize: MyTheme.txtSize),
                          color: MyTheme.inputColor,
                          fontWeight: FontWeight.normal,
                          height: 1.5)),
                  SizedBox(height: 10),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Txt(
                            txt: "${widget.email.toString()}",
                            txtColor: Colors.black,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.center,
                            isBold: false),
                      ),
                      SizedBox(width: 5),
                      Flexible(
                        child: TextButton(
                          onPressed: () async {
                            onOptSend();
                          },
                          child: Text(
                            "Resend code",
                            style: TextStyle(
                              color: Colors.blue,
                              fontSize: 17,
                              decoration: TextDecoration.underline,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            SizedBox(height: 20),
            _getInputField,
            _getResendButton,
            /*SizedBox(height: 20),
            Center(
              child: CupertinoButton(
                onPressed: () => {Get.offAll(() => RegScreen())},
                color: MyTheme.brandColor,
                borderRadius: new BorderRadius.circular(20.0),
                child: new Text(
                  "Continue",
                  textAlign: TextAlign.center,
                  style: new TextStyle(color: Colors.white),
                ),
              ),
            ),
            SizedBox(height: 20),*/
          ],
        ),
      ),
    );
  }

  // Returns "Otp custom text field"
  Widget _otpTextField(int digit) {
    int boxSpace = 2 * 6;
    double boxW = (getWP(context, 100) / SMS_AUTH_CODE_LEN) - boxSpace;
    return new Container(
      width: boxW,
      height: boxW,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        //borderRadius: BorderRadius.circular(10),
        border: Border.all(color: Colors.black, width: .5),
        //color: MyTheme.brandColor,
      ),
      child: Text(
        digit != null ? digit.toString() : "",
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.black,
          fontSize: 30,
          fontWeight: FontWeight.bold,
          fontFamily: "Dosis Regular",
        ),
      ),
    );
  }

  // Returns "Otp keyboard input Button"
  Widget _otpKeyboardInputButton({String label, VoidCallback onPressed}) {
    return new Material(
      color: Colors.transparent,
      child: new InkWell(
        onTap: onPressed,
        borderRadius: new BorderRadius.circular(40.0),
        child: new Container(
          height: 80.0,
          width: 80.0,
          decoration: new BoxDecoration(shape: BoxShape.rectangle),
          child: new Center(
            child: Text(
              label,
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Color(0xFF8C8C8C),
                fontSize: 30,
                fontWeight: FontWeight.bold,
                fontFamily: "Dosis Bold",
              ),
            ),
          ),
        ),
      ),
    );
  }

  // Returns "Otp keyboard action Button"
  _otpKeyboardActionButton({Widget label, VoidCallback onPressed}) {
    return new InkWell(
      onTap: onPressed,
      borderRadius: new BorderRadius.circular(40.0),
      child: new Container(
        height: 80.0,
        width: 80.0,
        decoration: new BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: new Center(
          child: label,
        ),
      ),
    );
  }

  // Current digit
  void _setCurrentDigit(int i) {
    if (mounted) {
      setState(() {
        _currentDigit = i;
        if (_firstDigit == null) {
          _firstDigit = _currentDigit;
        } else if (_secondDigit == null) {
          _secondDigit = _currentDigit;
        } else if (_thirdDigit == null) {
          _thirdDigit = _currentDigit;
        } else if (_fourthDigit == null) {
          _fourthDigit = _currentDigit;
        } else if (_fiveDigit == null) {
          _fiveDigit = _currentDigit;
        } else if (_sixDigit == null) {
          _sixDigit = _currentDigit;
          final otpCode2 = _firstDigit.toString() +
              _secondDigit.toString() +
              _thirdDigit.toString() +
              _fourthDigit.toString() +
              _fiveDigit.toString() +
              _sixDigit.toString();
          if (otpCode2.length == SMS_AUTH_CODE_LEN) {
            otpCode = otpCode2;
            callOTPApi();
          }
        }
      });
    }
  }

  void callOTPApi() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
      await APIViewModel().req<LoginRegOtpFBAPIModel>(
        context: context,
        url: Server.LOGINEMAILOTPBYMOBAPP_POST_URL,
        reqType: ReqType.Post,
        param: {
          "Email": widget.email,
          "OTPCode": otpCode,
        },
        callback: (model) async {
          if (mounted) {
            if (model != null) {
              if (model.success) {
                if (model.responseData.user.communityID.toString() == "1") {
                  await PrefMgr.shared.setPrefStr(
                      "accessToken", model.responseData.accessToken);
                  await PrefMgr.shared.setPrefStr(
                      "refreshToken", model.responseData.refreshToken);

                  await DBMgr.shared
                      .setUserProfile(user: model.responseData.user);
                  await userData.setUserModel();

                  //if (userData.userModel.companyCount > 1) {
                  //Get.offAll(() => CompAccScreen());
                  //} else {
                  Get.offAll(() => MainCustomerScreen());
                  //}
                } else {
                  final err = model.errorMessages.login[0];
                  showToast(context: context, msg: err, which: 0);
                }
              }
              Future.delayed(Duration(seconds: 4), () {
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              });
            }
          } else {
            Get.to(() => NoAccess());
          }
        },
      );
    }
  }
}
