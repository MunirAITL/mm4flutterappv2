import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/api/auth/LoginApiMgr.dart';
import 'package:aitl/controller/api/auth/RegAPIMgr.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/NoAccess.dart';
import 'package:aitl/view/auth/LoginLandingScreen.dart';
import 'package:aitl/view/auth/company_accounts/comp_acc_page.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/DatePickerView.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/input/InputTitleBoxfWithCountryCode.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/txt/TxtBox.dart';
import 'package:aitl/view/widgets/views/TCView.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

import 'email/check_email_page.dart';

class RegScreen extends StatefulWidget {
  final bool isOTPComeThrough;
  String firstName = "";
  String lastName = "";
  String emailAddress = "";
  String phoneNumber = "";

  RegScreen(
      {this.firstName,
      this.lastName,
      this.emailAddress,
      this.phoneNumber,
      this.isOTPComeThrough = false});

  @override
  State createState() => _RegScreenState();
}

enum genderEnum { male, female }

class _RegScreenState extends State<RegScreen> with Mixin {
  final _fname = TextEditingController();
  final _lname = TextEditingController();
  final _email = TextEditingController();
  final _mobile = TextEditingController();
  final _pwd = TextEditingController();
  //
  final focusFname = FocusNode();
  final focusLname = FocusNode();
  final focusEmail = FocusNode();
  final focusMobile = FocusNode();
  final focusPwd = FocusNode();

  String countryCode = "+44";
  String countryName = "GB";

  bool isobscureText = true;

  String dob = "";

  String dobDD = "";

  String dobMM = "";

  String dobYY = "";

  genderEnum _gender = genderEnum.male;

  bool isStep2 = false;
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  getFullPhoneNumber({String countryCodeTxt, String number}) {
    var fullNumber = countryCodeTxt + number;
    return fullNumber;
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    _fname.dispose();
    _lname.dispose();
    _mobile.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    getMobCode();
    try {
      _fname.text = widget.firstName;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      _lname.text = widget.lastName;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      _email.text = widget.emailAddress;
    } catch (e) {
      myLog(e.toString());
    }

    try {
      _mobile.text = widget.phoneNumber;
    } catch (e) {
      myLog(e.toString());
    }
  }

  getMobCode() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;
          countryCode = cd;
        });
      }
    } catch (e) {
      print("sms2 Screen country code and name problem ");
    }
  }

  validate1() {
    if (!UserProfileVal().isFNameOK(context, _fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(context, _lname)) {
      return false;
    } else if (!UserProfileVal()
        .isEmailOK(context, _email, "Please enter your valid email address")) {
      return false;
    } else if (!UserProfileVal().isPhoneOK(context, _mobile)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(context, _pwd)) {
      return false;
    }
    return true;
  }

  /*validate2() {
    if (!UserProfileVal().isDOBOK(context, dob)) {
      return false;
    }
    return true;
  }*/

  refreshStep1() {
    setState(() {
      isStep2 = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          automaticallyImplyLeading: (widget.isOTPComeThrough) ? true : false,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: 0,
          //titleSpacing: 0,
          title: (isStep2)
              ? UIHelper().drawAppbarTitle(
                  title: "Create an account", txtColor: Colors.white)
              : GestureDetector(
                  onTap: () {
                    Get.offAll(() => LoginLandingScreen());
                  },
                  child: Container(
                    color: Colors.transparent,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Icon(Icons.arrow_back),
                        SizedBox(width: 5),
                        Txt(
                            txt: "Sign in",
                            txtColor: Colors.white,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                        //shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
                      ],
                    ),
                  ),
                ),
          centerTitle: false,
          leading: (isStep2)
              ? IconButton(
                  onPressed: () {
                    setState(() {
                      isStep2 = false;
                    });
                  },
                  icon: Icon(Icons.arrow_back),
                )
              : null,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: (!isStep2) ? drawRegView1() : drawRegView2(),
        ),
      ),
    );
  }

  drawRegView1() {
    return Container(
      //decoration: MyTheme.bgBlueColor,
      width: getW(context),
      child: Padding(
        padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                child: Column(
                  //mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    //SizedBox(height: 30),
                    Txt(
                      txt: "Let's Get Started",
                      txtColor: MyTheme.titleColor,
                      txtSize: MyTheme.txtSize + 1,
                      txtAlign: TextAlign.start,
                      isBold: true,
                    ),
                    //SizedBox(height: 10),
                    Txt(
                      txt: "Create an account to get started",
                      txtColor: MyTheme.title2Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.left,
                      fontWeight: FontWeight.w300,
                      txtLineSpace: 1.4,
                      isBold: false,
                    ),
                    SizedBox(height: 20),
                    drawInputBox(
                      context: context,
                      title: "First Name",
                      input: _fname,
                      ph: "",
                      kbType: TextInputType.name,
                      inputAction: TextInputAction.next,
                      focusNode: focusFname,
                      focusNodeNext: focusLname,
                      len: 20,
                    ),
                    SizedBox(height: 20),
                    drawInputBox(
                      context: context,
                      title: "Last Name",
                      input: _lname,
                      ph: "",
                      kbType: TextInputType.name,
                      inputAction: TextInputAction.next,
                      focusNode: focusLname,
                      focusNodeNext: focusEmail,
                      len: 20,
                    ),
                    SizedBox(height: 20),
                    drawInputBox(
                      context: context,
                      title: "Email",
                      input: _email,
                      ph: "",
                      kbType: TextInputType.emailAddress,
                      inputAction: TextInputAction.next,
                      focusNode: focusEmail,
                      focusNodeNext:
                          widget.phoneNumber == null ? focusMobile : focusPwd,
                      len: 50,
                    ),
                    SizedBox(height: 20),
                    widget.phoneNumber == null
                        ? drawInputBoxWithCountryCode(
                            context: context,
                            title: "Phone Number",
                            input: _mobile,
                            ph: "",
                            kbType: TextInputType.phone,
                            inputAction: TextInputAction.next,
                            focusNode: focusMobile,
                            focusNodeNext: focusPwd,
                            len: 15,
                            isWhiteBG: false,
                            countryCode: countryCode,
                            countryName: countryName,
                            getCountryCode: (value) {
                              countryCode = value.toString();
                              print("Country Code Clik = " + countryCode);
                              PrefMgr.shared
                                  .setPrefStr("countryName", value.code);
                              PrefMgr.shared
                                  .setPrefStr("countryCode", value.toString());
                            })
                        : Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Txt(
                                  txt: "Mobile Number",
                                  txtColor: MyTheme.inputColor,
                                  txtSize: MyTheme.txtSize - .2,
                                  txtAlign: TextAlign.start,
                                  fontWeight: FontWeight.w400,
                                  isBold: false),
                              SizedBox(height: 10),
                              TxtBox(txt: widget.phoneNumber),
                            ],
                          ),
                    /*  Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: drawInputBox(
                        title: "Phone Number",
                        input: _mobile,
                        ph: "0898215324232",
                        kbType: TextInputType.phone,
                        len: 20,
                      ),
                    ),*/
                    SizedBox(height: 20),
                    drawInputBox(
                        context: context,
                        title: "Password",
                        input: _pwd,
                        ph: "",
                        kbType: TextInputType.text,
                        inputAction: TextInputAction.done,
                        focusNode: focusPwd,
                        len: 20,
                        isPwd: true,
                        isobscureText: isobscureText,
                        callback: (v) {
                          isobscureText = v;
                          setState(() {});
                        }),
                  ],
                ),
              ),
              /*SizedBox(height: 20),
              Txt(
                txt: "or",
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                child: ListTile(
                  tileColor: Colors.blue.shade800,
                  leading: Image(
                    image: AssetImage(
                      "assets/images/icons/fb_icon.png",
                    ),
                    //color: null,
                    width: 20,
                    height: 20,
                  ),
                  minLeadingWidth: 0,
                  title: MaterialButton(
                    child: Container(
                      width: double.infinity,
                      child: Align(
                        alignment: Alignment(-1, 0),
                        child: Text(
                          "Continue with Facebook",
                        ),
                      ),
                    ),
                    onPressed: () async {
                      //
                    },
                  ),
                ),
              ),*/
              SizedBox(height: 30),
              TCView(screenName: 'Continue'),
              SizedBox(height: 30),
              MMBtn(
                  txt: "Continue",
                  height: getHP(context, 7),
                  width: getW(context),
                  radius: 8,
                  callback: () {
                    if (validate1()) {
                      isStep2 = true;
                      getMobCode();
                      setState(() {});
                    }
                  }),
              SizedBox(height: 40),
            ],
          ),
        ),
      ),
    );
  }

  //  ***************** RegView2  **********************

  drawRegView2() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);
    return Container(
      //decoration: MyTheme.bgBlueColor,
      width: getW(context),
      height: getH(context),
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                // drawCompSwitch(),
                SizedBox(height: 10),
                DatePickerView(
                  cap: "Select Date of Birth",
                  dt: (dob == '') ? 'Select Date' : dob,
                  txtColor: MyTheme.inputColor,
                  initialDate: dateDOBlast,
                  firstDate: dateDOBfirst,
                  lastDate: dateDOBlast,
                  callback: (value) {
                    if (mounted) {
                      setState(() {
                        try {
                          dob = DateFormat('dd-MMM-yyyy')
                              .format(value)
                              .toString();
                          final dobArr = dob.toString().split('-');
                          this.dobDD = dobArr[0];
                          this.dobMM = dobArr[1];
                          this.dobYY = dobArr[2];
                        } catch (e) {
                          myLog(e.toString());
                        }
                      });
                    }
                  },
                ),
                SizedBox(height: 40),
                drawGender(),
                SizedBox(height: 40),
                MMBtn(
                    txt: "Get Started",
                    height: getHP(context, 7),
                    width: getW(context),
                    callback: () {
                      //if (validate2()) {
                      setState(() {
                        isLoading = true;
                      });
                      RegAPIMgr().wsRegAPI(
                          context: context,
                          email: _email.text.trim(),
                          pwd: _pwd.text.trim(),
                          fname: _fname.text.trim(),
                          lname: _lname.text.trim(),
                          mobile: getFullPhoneNumber(
                                  countryCodeTxt: countryCode,
                                  number: _mobile.text.toString())
                              .trim(),
                          countryCode: countryCode,
                          dob: dob,
                          dobDD: dobDD,
                          dobMM: dobMM,
                          dobYY: dobYY,
                          gender:
                              _gender == genderEnum.male ? 'Male' : 'Female',
                          callback: (model) async {
                            if (model != null && mounted) {
                              try {
                                if (model.success) {
                                  //  recall login to get cookie
                                  LoginAPIMgr().wsLoginAPI(
                                      context: context,
                                      email: _email.text.trim(),
                                      pwd: _pwd.text.trim(),
                                      callback: (model2) async {
                                        if (model2 != null && mounted) {
                                          if (model2
                                                  .responseData.user.communityID
                                                  .toString() ==
                                              "1") {
                                            try {
                                              setState(() {
                                                isLoading = false;
                                              });
                                              if (model2.success) {
                                                try {
                                                  await DBMgr.shared
                                                      .setUserProfile(
                                                          user: model2
                                                              .responseData
                                                              .user);
                                                  await userData.setUserModel();

                                                  if (!Server.isOtp) {
                                                    //if (userData.userModel
                                                    //      .companyCount >
                                                    //1) {
                                                    //Get.offAll(() =>
                                                    //  CompAccScreen());
                                                    //} else {
                                                    Get.offAll(() =>
                                                        MainCustomerScreen());
                                                    //}
                                                  } else {
                                                    if (model.responseData.user
                                                        .isMobileNumberVerified) {
                                                      //if (userData.userModel
                                                      //      .companyCount >
                                                      //1) {
                                                      //Get.offAll(() =>
                                                      //  CompAccScreen());
                                                      //} else {
                                                      Get.offAll(() =>
                                                          MainCustomerScreen());
                                                      //}
                                                    } else {
                                                      Get.off(() =>
                                                          CheckEmailPage(
                                                              email: _email.text
                                                                  .trim()));
                                                    }
                                                  }
                                                } catch (e) {
                                                  myLog(e.toString());
                                                }
                                              } else {
                                                try {
                                                  if (mounted) {
                                                    final err = model2
                                                        .errorMessages.login[0]
                                                        .toString();
                                                    showToast(
                                                        context: context,
                                                        msg: err);
                                                  }
                                                } catch (e) {
                                                  myLog(e.toString());
                                                }
                                              }
                                            } catch (e) {
                                              myLog(e.toString());
                                            }
                                          }
                                        } else {
                                          Get.to(() => NoAccess());
                                        }
                                      });
                                } else {
                                  try {
                                    setState(() {
                                      isLoading = false;
                                    });
                                    if (mounted) {
                                      final err = model
                                          .errorMessages.register[0]
                                          .toString();
                                      showToast(context: context, msg: err);
                                    }
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                }
                              } catch (e) {
                                myLog(e.toString());
                              }
                            }
                          });
                    }
                    //}
                    ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  drawGender() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Txt(
            txt: "Gender",
            txtColor: MyTheme.inputColor,
            txtSize: MyTheme.txtSize - .2,
            isBold: false,
            fontWeight: FontWeight.w400,
            txtAlign: TextAlign.start,
          ),
          //SizedBox(height: 10),
          Theme(
            data: MyTheme.radioThemeData,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.male;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.male,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Male",
                  txtColor: MyTheme.inputColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
                InkWell(
                  onTap: () {
                    if (mounted) {
                      setState(() {
                        _gender = genderEnum.female;
                      });
                    }
                  },
                  child: Radio(
                    value: genderEnum.female,
                    groupValue: _gender,
                    onChanged: (genderEnum value) {
                      if (mounted) {
                        setState(() {
                          _gender = value;
                        });
                      }
                    },
                  ),
                ),
                Txt(
                  txt: "Female",
                  txtColor: MyTheme.inputColor,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
/*
  drawCompSwitch() {
    return Container(
      //color: MyTheme.brandColor,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.only(right: 50),
                      child: Txt(
                        txt: "Do you have an adviser to assist you with your case? If not we can help you find one.",
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: true,
                        //txtLineSpace: 1.5,
                      ),
                    ),
                  ),
                  //SizedBox(width: 10),
                  SwitchView(
                    onTxt: "Yes",
                    offTxt: "No",
                    value: _isSwitch,
                    onChanged: (value) {
                      _isSwitch = value;
                      if (mounted) {
                        setState(() {});
                      }
                    },
                  ),
                ],
              ),
            ),
            (_isSwitch)
                ? Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 30),
                        Txt(txt: "Company Name", txtColor: Colors.black, txtSize: MyTheme.txtSize, txtAlign: TextAlign.start, isBold: true),
                        SizedBox(height: 20),
                        InputBox(
                          ctrl: _compName,
                          lableTxt: "Birilliant Company",
                          kbType: TextInputType.text,
                          len: 100,
                          isPwd: false,
                        ),
                      ],
                    ),
                  )
                : SizedBox(),
          ],
        ),
      ),
    );
  }*/
}
