import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/auth/otp/Sms2APIMgr.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/view/auth/otp/OtpByMobilePage.dart';
import 'package:aitl/view/base_app.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/input/InputTitleBoxfWithCountryCode.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'check_email_page.dart';

class SigninPage extends StatefulWidget {
  final hrefLoginData;
  final bool isOptMobile;
  const SigninPage({Key key, this.hrefLoginData, @required this.isOptMobile})
      : super(key: key);

  @override
  _SigninPageState createState() => _SigninPageState();
}

class _SigninPageState extends BaseApp<SigninPage> {
  final email = TextEditingController();

  String countryCode = "+44";
  String countryName = "GB";

  getMobCode() async {
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryName = cn;
          countryCode = cd;
        });
      }
    } catch (e) {
      print("sms2 Screen country code and name problem ");
    }
  }

  callOtpMobileAPI() async {
    var countryCode = "+44";
    try {
      var cn = await PrefMgr.shared.getPrefStr("countryName");
      var cd = await PrefMgr.shared.getPrefStr("countryCode");
      if (cn != null &&
          cd != null &&
          cn.toString().isNotEmpty &&
          cd.toString().isNotEmpty) {
        setState(() {
          countryCode = cd;
        });
      }
    } catch (e) {
      print("Sms2 screen country code and name problem ");
    }
    Sms2APIMgr().wsLoginMobileOtpPostAPI(
      context: context,
      countryCode: countryCode,
      mobile: email.text.trim(),
      callback: (model) {
        if (model != null && mounted) {
          try {
            if (model.success) {
              Sms2APIMgr().wsSendOtpNotiAPI(
                  context: context,
                  otpId: model.responseData.userOTP.id,
                  callback: (model) {
                    if (model != null && mounted) {
                      try {
                        if (model.success) {
                          Get.to(() => OtpByMobilePage(
                              mobile: model.responseData.userOTP.mobileNumber));
                        } else {
                          try {
                            if (mounted) {
                              final err =
                                  model.messages.postUserotp[0].toString();
                              showToast(context: context, msg: err);
                            }
                          } catch (e) {
                            myLog(e.toString());
                          }
                        }
                      } catch (e) {
                        myLog(e.toString());
                      }
                    }
                  });
            } else {
              try {
                if (mounted) {
                  final err = model.messages.postUserotp[0].toString();
                  showToast(context: context, msg: err);
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          } catch (e) {
            myLog(e.toString());
          }
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    //testLogin();
    getMobCode();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        resizeToAvoidBottomInset: false,
        body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(
                Icons.arrow_back,
                color: Colors.black,
              )),
          SizedBox(height: 20),
          Align(
              alignment: Alignment.center,
              child: Image.asset(
                "assets/images/logo/mm.png",
                fit: BoxFit.cover,
                width: getWP(context, 60),
              )),
          SizedBox(height: 20),
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Txt(
                      txt: "Enter the " +
                          (widget.isOptMobile
                              ? "mobile number"
                              : "email address") +
                          " associated with your account.",
                      txtColor: MyTheme.inputColor,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      fontWeight: FontWeight.w500,
                      isBold: false,
                    ),
                  ),
                  SizedBox(height: 20),
                  widget.isOptMobile ? drawOtpMobile() : drawOtpEmail(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawOtpMobile() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          drawInputBoxWithCountryCode(
              context: context,
              title: "Mobile number",
              input: email,
              ph: "",
              kbType: TextInputType.phone,
              inputAction: TextInputAction.next,
              focusNode: FocusNode(),
              len: 15,
              isWhiteBG: false,
              countryCode: countryCode,
              countryName: countryName,
              getCountryCode: (value) {
                countryCode = value.toString();
                print("Country Code Clik = " + countryCode);
                PrefMgr.shared.setPrefStr("countryName", value.code);
                PrefMgr.shared.setPrefStr("countryCode", value.toString());
              }),
          SizedBox(height: 30),
          MMBtn(
              txt: "Next",
              txtColor: Colors.white,
              height: getHP(context, 7),
              width: getW(context),
              radius: 10,
              callback: () {
                try {
                  if (email.text.length > UserProfileVal.PHONE_LIMIT) {
                    callOtpMobileAPI();
                  } else {
                    showToast(
                        context: context, msg: "Invalid mobile number entered");
                  }
                } catch (e) {
                  showToast(
                      context: context, msg: "Invalid mobile number entered");
                }
              }),
        ],
      );

  drawOtpEmail() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Txt(
            txt: "Email address:",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize,
            txtAlign: TextAlign.center,
            fontWeight: FontWeight.w400,
            isBold: false,
          ),
          TextField(
              controller: email,
              autofocus: true,
              maxLength: 50,
              autocorrect: false,
              style: TextStyle(
                color: Colors.black,
                fontSize:
                    getTxtSize(context: context, txtSize: MyTheme.txtSize + .5),
                height: MyTheme.txtLineSpace,
              ),
              keyboardType: TextInputType.emailAddress,
              decoration: new InputDecoration(
                isDense: true,
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                counterText: '',
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black, width: .5),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black, width: .5),
                ),
                border: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.white, width: .5),
                ),
              )),
          SizedBox(height: 30),
          MMBtn(
              txt: "Next",
              txtColor: Colors.white,
              height: getHP(context, 7),
              width: getW(context),
              radius: 10,
              callback: () {
                if (email.text.trim().isEmpty) {
                  showToast(
                      context: context, msg: "Please enter your email address");
                  return;
                }

                if (RegExp(UserProfileVal.EMAIL_REG)
                    .hasMatch(email.text.trim())) {
                  Get.off(() => CheckEmailPage(email: email.text.trim()));
                } else {
                  showToast(
                      context: context, msg: "Invalid email address entered");
                }
              }),
        ],
      );
}
