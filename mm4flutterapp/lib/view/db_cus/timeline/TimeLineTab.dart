import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/db_cus/timeline/GroupChatAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/timeline/TimelineAPIMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/AppData.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimelineUserModel.dart';
import 'package:aitl/view/widgets/progress/AppbarBotProgbar.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'TimeLineBase.dart';

class TimeLineTab extends StatefulWidget {
  @override
  State createState() => _TimeLineTabState();
}

class _TimeLineTabState extends BaseTimeline<TimeLineTab>
    with StateListener, TickerProviderStateMixin {
  static const platform = const MethodChannel('flutter.native/tokbox');

  StateProvider _stateProvider;
  TabController tabController;

  @override
  void onStateChangedWithData(state, data) async {
    try {
      if (state == ObserverState.STATE_BADGE_MESSAGE_COUNT && data == 2) {
        if (mounted) {
          final unreadMessageCount =
              appData.taskNotificationCountAndChatUnreadCountData != null
                  ? appData.taskNotificationCountAndChatUnreadCountData
                      .numberOfUnReadMessage
                  : 0;
          if (unreadMessageCount > 0) {
            appData.taskNotificationCountAndChatUnreadCountData
                .numberOfUnReadMessage = 0;
            await PrefMgr.shared
                .setPrefInt("NumberOfUnReadMessage", unreadMessageCount);
          }
          StateProvider().notifyWithData(ObserverState.STATE_BOTNAV, null);
          //setState(() {});
        }
      }
    } catch (e) {}
  }

  @override
  onStateChanged(ObserverState state) async {
    if (state == ObserverState.STATE_CHANGED_tabbar3_reload_case_api) {
      getRefreshDataChat();
    }
  }
  /*onPageLoad() async {
    try {
      final j =
          '{"Success":true,"ErrorMessages":{},"Messages":{},"ResponseData":{"Users":[{"FirstName":"Ben","LastName":"Stokes","Name":"Ben Stokes","UserName":"benstoke5001","ProfileImageUrl":"https://financemagic.co.uk/api/content/media/default_avatar.png","CoverImageUrl":"~/Content/Media/d_cover.jpg","ProfileUrl":null,"DateCreatedUtc":"2019-12-19T10:48:00.86","DateCreatedLocal":"2019-12-19T10:48:00.86Z","Active":true,"LastLoginDateUtc":null,"LastLoginDateLocal":null,"FollowerCount":0,"FollowingCount":0,"FriendCount":0,"CanFollow":true,"FollowStatus":0,"FriendStatus":0,"UnreadNotificationCount":0,"UnreadMessageCount":0,"IsOnline":true,"SeName":"ben-stokes","StanfordWorkplaceURL":"","Headline":"","BriefBio":"","ReferenceId":null,"ReferenceType":null,"Remarks":null,"Community":null,"Locations":null,"Cohort":null,"CommunityId":"9","IsFirstLogin":false,"MobileNumber":"824287682424","DateofBirth":"01-Dec-2019","IsMobileNumberVerified":false,"IsProfileImageVerified":false,"IsDateOfBirthVerified":true,"IsElectronicIdVerified":false,"IsEmailVerified":false,"LastLoginDate":"33 Minutes ago","UserProfileUrl":"Ben-Stokes-115773","UserAccesType":0,"Address":null,"Email":"benstoke5001@gmail.com","Latitude":null,"Longitude":null,"BankVerifiedStatus":0,"NationalIDVerifiedStatus":0,"BillingAccountVerifiedStatus":0,"IsCustomerPrivacy":0,"ProfileImageId":0,"WorkHistory":null,"UserTaskCategoryId":0,"DeviceName":null,"SkillName":null,"UserTaskCategoryName":null,"Status":"","GroupId":0,"NamePrefix":null,"MiddleName":null,"AreYouASmoker":null,"AddressLine1":null,"AddressLine2":null,"AddressLine3":null,"Town":null,"County":null,"Postcode":null,"TelNumber":null,"NationalInsuranceNumber":null,"Nationality":null,"CountryofBirth":null,"CountryofResidency":null,"PassportNumber":null,"MaritalStatus":null,"UserCompanyId":0,"CompanyName":null,"OccupantType":null,"LivingDate":"0001-01-01T00:00:00","VisaExpiryDate":"0001-01-01T00:00:00","PassportExpiryDate":"0001-01-01T00:00:00","VisaName":null,"OtherVisaName":null,"ReportLogoUrl":null,"UserCompanyInfo":null,"CompanyType":null,"IsActiveCustomerPrivacy":null,"IsActiveCustomerAgreement":null,"CompanyWebsite":null,"IsCallCenterActive":null,"CompanyCount":0,"Id":115773},{"FirstName":"Bilkis","LastName":"khanom","Name":"Bilkis khanom","UserName":"mk.adv001","ProfileImageUrl":"http://localhost:59082/api//media/uploadpictures/c13632f7dcf04e6c84f1ca3041a56b86_128_128.jpg","CoverImageUrl":"//http://localhost:59082/api","ProfileUrl":null,"DateCreatedUtc":"2020-11-07T08:39:34.067","DateCreatedLocal":"2020-11-07T08:39:34.067Z","Active":true,"LastLoginDateUtc":null,"LastLoginDateLocal":null,"FollowerCount":0,"FollowingCount":0,"FriendCount":0,"CanFollow":true,"FollowStatus":0,"FriendStatus":0,"UnreadNotificationCount":0,"UnreadMessageCount":0,"IsOnline":true,"SeName":"bilkis-khanom","StanfordWorkplaceURL":"","Headline":"","BriefBio":"","ReferenceId":"115765","ReferenceType":"User","Remarks":null,"Community":null,"Locations":null,"Cohort":"","CommunityId":"9","IsFirstLogin":false,"MobileNumber":"8801717225888526","DateofBirth":"11-Nov-2000","IsMobileNumberVerified":false,"IsProfileImageVerified":true,"IsDateOfBirthVerified":true,"IsElectronicIdVerified":false,"IsEmailVerified":false,"LastLoginDate":"Online","UserProfileUrl":"Bilkis-khanom-117897","UserAccesType":0,"Address":null,"Email":"mk.adv001@yopmail.com","Latitude":null,"Longitude":null,"BankVerifiedStatus":0,"NationalIDVerifiedStatus":0,"BillingAccountVerifiedStatus":0,"IsCustomerPrivacy":0,"ProfileImageId":90948,"WorkHistory":null,"UserTaskCategoryId":0,"DeviceName":null,"SkillName":null,"UserTaskCategoryName":null,"Status":"","GroupId":0,"NamePrefix":null,"MiddleName":"","AreYouASmoker":"Yes","AddressLine1":null,"AddressLine2":null,"AddressLine3":null,"Town":null,"County":null,"Postcode":null,"TelNumber":null,"NationalInsuranceNumber":null,"Nationality":null,"CountryofBirth":null,"CountryofResidency":null,"PassportNumber":null,"MaritalStatus":null,"UserCompanyId":0,"CompanyName":null,"OccupantType":null,"LivingDate":"0001-01-01T00:00:00","VisaExpiryDate":"0001-01-01T00:00:00","PassportExpiryDate":"0001-01-01T00:00:00","VisaName":null,"OtherVisaName":null,"ReportLogoUrl":null,"UserCompanyInfo":null,"CompanyType":null,"IsActiveCustomerPrivacy":null,"IsActiveCustomerAgreement":null,"CompanyWebsite":null,"IsCallCenterActive":null,"CompanyCount":0,"Id":117897},{"FirstName":"haf.adv102","LastName":"Rahman","Name":"haf.adv102 Rahman","UserName":"haf.adv102","ProfileImageUrl":"https://financemagic.co.uk/api/content/media/default_avatar.png","CoverImageUrl":"~/Content/Media/d_cover.jpg","ProfileUrl":null,"DateCreatedUtc":"2021-02-13T09:06:11.853","DateCreatedLocal":"2021-02-13T09:06:11.853Z","Active":true,"LastLoginDateUtc":null,"LastLoginDateLocal":null,"FollowerCount":0,"FollowingCount":0,"FriendCount":0,"CanFollow":true,"FollowStatus":0,"FriendStatus":0,"UnreadNotificationCount":0,"UnreadMessageCount":0,"IsOnline":true,"SeName":"hafadv102-rahman","StanfordWorkplaceURL":"","Headline":"","BriefBio":"","ReferenceId":"116043","ReferenceType":"User","Remarks":null,"Community":null,"Locations":null,"Cohort":"Male","CommunityId":"9","IsFirstLogin":false,"MobileNumber":"88017142258556","DateofBirth":"11-Nov-1985","IsMobileNumberVerified":false,"IsProfileImageVerified":false,"IsDateOfBirthVerified":true,"IsElectronicIdVerified":false,"IsEmailVerified":false,"LastLoginDate":"Online","UserProfileUrl":"haf.adv102-Rahman-117965","UserAccesType":0,"Address":null,"Email":"haf.adv102@yopmail.com","Latitude":null,"Longitude":null,"BankVerifiedStatus":0,"NationalIDVerifiedStatus":0,"BillingAccountVerifiedStatus":0,"IsCustomerPrivacy":0,"ProfileImageId":0,"WorkHistory":null,"UserTaskCategoryId":0,"DeviceName":null,"SkillName":null,"UserTaskCategoryName":null,"Status":"","GroupId":0,"NamePrefix":"Mrs","MiddleName":"","AreYouASmoker":"","AddressLine1":null,"AddressLine2":null,"AddressLine3":null,"Town":null,"County":null,"Postcode":null,"TelNumber":null,"NationalInsuranceNumber":null,"Nationality":null,"CountryofBirth":null,"CountryofResidency":null,"PassportNumber":null,"MaritalStatus":null,"UserCompanyId":0,"CompanyName":null,"OccupantType":null,"LivingDate":"0001-01-01T00:00:00","VisaExpiryDate":"0001-01-01T00:00:00","PassportExpiryDate":"0001-01-01T00:00:00","VisaName":null,"OtherVisaName":null,"ReportLogoUrl":null,"UserCompanyInfo":null,"CompanyType":null,"IsActiveCustomerPrivacy":null,"IsActiveCustomerAgreement":null,"CompanyWebsite":null,"IsCallCenterActive":null,"CompanyCount":0,"Id":117965},{"FirstName":"Sabita","LastName":"Chowdhury","Name":"Sabita Chowdhury","UserName":"sc.adv001","ProfileImageUrl":"https://financemagic.co.uk/api/content/media/default_avatar.png","CoverImageUrl":"~/Content/Media/d_cover.jpg","ProfileUrl":null,"DateCreatedUtc":"2020-11-06T10:02:59.923","DateCreatedLocal":"2020-11-06T10:02:59.923Z","Active":true,"LastLoginDateUtc":null,"LastLoginDateLocal":null,"FollowerCount":0,"FollowingCount":0,"FriendCount":0,"CanFollow":true,"FollowStatus":0,"FriendStatus":0,"UnreadNotificationCount":0,"UnreadMessageCount":0,"IsOnline":true,"SeName":"sabita-chowdhury","StanfordWorkplaceURL":"","Headline":"","BriefBio":"","ReferenceId":"115765","ReferenceType":"User","Remarks":null,"Community":null,"Locations":null,"Cohort":"","CommunityId":"9","IsFirstLogin":false,"MobileNumber":"55785756666","DateofBirth":"11-Nov-2000","IsMobileNumberVerified":false,"IsProfileImageVerified":false,"IsDateOfBirthVerified":true,"IsElectronicIdVerified":false,"IsEmailVerified":false,"LastLoginDate":"25-Mar-2021 07:14 ","UserProfileUrl":"Sabita-Chowdhury-117880","UserAccesType":0,"Address":null,"Email":"sc.adv001@yopmail.com","Latitude":null,"Longitude":null,"BankVerifiedStatus":0,"NationalIDVerifiedStatus":0,"BillingAccountVerifiedStatus":0,"IsCustomerPrivacy":0,"ProfileImageId":0,"WorkHistory":null,"UserTaskCategoryId":0,"DeviceName":null,"SkillName":null,"UserTaskCategoryName":null,"Status":"","GroupId":0,"NamePrefix":"Mr","MiddleName":"","AreYouASmoker":"Yes","AddressLine1":null,"AddressLine2":null,"AddressLine3":null,"Town":null,"County":null,"Postcode":null,"TelNumber":null,"NationalInsuranceNumber":null,"Nationality":null,"CountryofBirth":null,"CountryofResidency":null,"PassportNumber":null,"MaritalStatus":null,"UserCompanyId":0,"CompanyName":null,"OccupantType":null,"LivingDate":"0001-01-01T00:00:00","VisaExpiryDate":"0001-01-01T00:00:00","PassportExpiryDate":"0001-01-01T00:00:00","VisaName":null,"OtherVisaName":null,"ReportLogoUrl":null,"UserCompanyInfo":null,"CompanyType":null,"IsActiveCustomerPrivacy":null,"IsActiveCustomerAgreement":null,"CompanyWebsite":null,"IsCallCenterActive":null,"CompanyCount":0,"Id":117880}]}}';
      final model = TimeLineAdvisorAPIModel.fromJson(json.decode(j));
      if (model != null && mounted) {
        try {
          if (model.success) {
            try {
              final List<dynamic> timelineUserModel = model.responseData.users;
              if (timelineUserModel != null && mounted) {
                //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                if (timelineUserModel.length != pageCount) {
                  isPageDone = true;
                }
                try {
                  for (TimelineUserModel user in timelineUserModel) {
                    listTimelineUserModel.add(user);
                  }
                } catch (e) {
                  myLog(e.toString());
                }
                myLog(listTimelineUserModel.toString());
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              } else {
                if (mounted) {
                  setState(() {
                    isLoading = false;
                  });
                }
              }
            } catch (e) {
              myLog(e.toString());
            }
          } else {
            try {
              //final err = model.errorMessages.login[0].toString();
              if (mounted) {
                showToast(context: context,txtColor: Colors.white, bgColor: MyTheme.brandColor,msg: "Timelines not found");
              }
            } catch (e) {
              myLog(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          }
        } catch (e) {
          myLog(e.toString());
          if (mounted) {
            setState(() {
              isLoading = false;
            });
          }
        }
      } else {
        myLog("not in");
        if (mounted) {
          setState(() {
            isLoading = false;
          });
        }
      }
    } catch (e) {
      myLog(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }*/

  onPageLoadChat() async {
    try {
      setState(() {
        isLoading = true;
      });
      TimelineAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        status: caseStatus,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                try {
                  final List<dynamic> timelineUserModel =
                      model.responseData.users;
                  if (timelineUserModel != null && mounted) {
                    //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
                    if (timelineUserModel.length != pageCount) {
                      isPageDone = true;
                    }
                    try {
                      for (TimelineUserModel user in timelineUserModel) {
                        listTimelineUserModelChat.add(user);
                      }
                    } catch (e) {
                      myLog(e.toString());
                    }

                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  } else {
                    if (mounted) {
                      setState(() {
                        isLoading = false;
                      });
                    }
                  }
                } catch (e) {
                  myLog(e.toString());
                }
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    showToast(context: context, msg: "Timelines not found");
                  }
                } catch (e) {
                  myLog(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              myLog(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          } else {
            myLog("Time line tab screen not in");
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        },
      );
    } catch (e) {
      myLog(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  onPageLoadGroup() async {
    try {
      setState(() {
        isLoading = true;
      });
      GroupChatAPIMgr().wsOnPageLoad(
        context: context,
        pageStart: pageStart,
        pageCount: pageCount,
        caseStatus: caseStatus,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {
                listCaseGroupMessageData =
                    model.responseData.caseGroupMessageData;
                setState(() {
                  isLoading = false;
                });
              } else {
                try {
                  //final err = model.errorMessages.login[0].toString();
                  if (mounted) {
                    isLoading = false;
                    showToast(context: context, msg: "Cases not found");
                  }
                } catch (e) {
                  myLog(e.toString());
                  if (mounted) {
                    setState(() {
                      isLoading = false;
                    });
                  }
                }
              }
            } catch (e) {
              myLog(e.toString());
              if (mounted) {
                setState(() {
                  isLoading = false;
                });
              }
            }
          } else {
            myLog("my case tab new not in");
            if (mounted) {
              setState(() {
                isLoading = false;
              });
            }
          }
        },
      );
    } catch (e) {
      myLog(e.toString());
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> getRefreshDataChat() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listTimelineUserModelChat.clear();
    onPageLoadChat();
  }

  Future<void> getRefreshDataGroup() async {
    pageStart = 0;
    isPageDone = false;
    isLoading = true;
    listCaseGroupMessageData.clear();
    onPageLoadGroup();
  }

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    tabController.dispose();
    tabController = null;
    listTimelineUserModelChat = null;
    listCaseGroupMessageData = null;
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}
    tabController = TabController(length: 2, vsync: this, initialIndex: 0);
    tabController.addListener(() {
      if (tabController.indexIsChanging) {
      } else {
        if (tabController.index == 0) {
          print("0");
          getRefreshDataChat();
        } else if (tabController.index == 1) {
          print("1");
          getRefreshDataGroup();
        }
      }
    });
    try {
      getRefreshDataChat();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          centerTitle: false,
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: "Messages"),
          /*actions: [
            Server.isTest
                ? GestureDetector(
                    onTap: () async {
                      //OpenTokConfig().set(
                      //"T1==cGFydG5lcl9pZD00NzQzNzg1MSZzaWc9ZDc5ODI0MGM4NmM2MjE4Y2ZjZGIwMTE2OGJhZmZmNzdhNGVlOWNkNzpzZXNzaW9uX2lkPTJfTVg0ME56UXpOemcxTVg1LU1UWTBNek0zTVRReU56QXlOSDVaWXpZNFJUZDBRVVEwZEZwRVdDOVNlV0l2ZVhWSFNrOS1mZyZjcmVhdGVfdGltZT0xNjQzMzcxNTkxJm5vbmNlPTAuMjI5MjAzMTk1MDkwNzkyOSZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNjQ1OTYzNTkzJmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9");
                      OpenTokConfig().set((userData.userModel.email ==
                              "munir@aitl.net") //?publisher : subscriber
                          ? "T1==cGFydG5lcl9pZD00NzQzNzg1MSZzaWc9NWMwNTAwMGVhNjFjMDliNjY0ODExYmVjYmU5NjljZGE3MTA0M2ZiZDpzZXNzaW9uX2lkPTJfTVg0ME56UXpOemcxTVg1LU1UWTBNek0zTVRReU56QXlOSDVaWXpZNFJUZDBRVVEwZEZwRVdDOVNlV0l2ZVhWSFNrOS1mZyZjcmVhdGVfdGltZT0xNjQzODAyMTA1Jm5vbmNlPTAuMzc4NTI4MDIwNzA3NTQ2NzYmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTY0NjM5NDEwNCZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ=="
                          : "T1==cGFydG5lcl9pZD00NzQzNzg1MSZzaWc9ZGY2NTZiOWEyODFmZDljMGYwMjFlMjNlYjUzNDhlOTgzMWVkNzg4YzpzZXNzaW9uX2lkPTJfTVg0ME56UXpOemcxTVg1LU1UWTBNek0zTVRReU56QXlOSDVaWXpZNFJUZDBRVVEwZEZwRVdDOVNlV0l2ZVhWSFNrOS1mZyZjcmVhdGVfdGltZT0xNjQzODAyMTUwJm5vbmNlPTAuOTM2NzgyNDI1MTYxNzQzOCZyb2xlPXN1YnNjcmliZXImZXhwaXJlX3RpbWU9MTY0NjM5NDE0OSZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==");

                      if (Platform.isIOS) {
                        // iOS-specific code
                        final param = {
                          'apiKey': OpenTokConfig.API_KEY,
                          'sessionId': OpenTokConfig.SESSION_ID,
                          'token': OpenTokConfig.TOKEN
                        };
                        await platform.invokeMethod('start', param);
                      } else {
                        Get.to(() => CallWidget());
                      }
                    },
                    child: Container(
                      width: getWP(context, 25),
                      color: Colors.transparent,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Flexible(
                                child: Text("Support",
                                    style: TextStyle(color: Colors.white))),
                            Flexible(
                                child: Icon(Icons.video_call,
                                    color: Color(0xFF66FF00), size: 30)),
                          ],
                        ),
                      ),
                    ))
                : SizedBox(),
            SizedBox(width: 5)
          ],*/
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(getHP(context, 7)),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                (isLoading)
                    ? AppbarBotProgBar(
                        backgroundColor: MyTheme.appbarProgColor,
                      )
                    : SizedBox(),
                Container(
                  color: MyTheme.bgColor2,
                  height: getHP(context, 7),
                  child: DecoratedBox(
                    //This is responsible for the background of the tabbar, does the magic
                    decoration: BoxDecoration(
                      //This is for background color
                      color: Colors.white.withOpacity(0.0),
                      //This is for bottom border that is needed
                      border: Border(
                          bottom: BorderSide(color: Colors.black, width: 1.4)),
                    ),
                    child: TabBar(
                      controller: tabController,
                      //labelColor: Colors.deepOrange,
                      unselectedLabelColor: Colors.black45,
                      labelColor: Colors.black,
                      indicatorColor: Color(0xFF00A368),
                      indicatorSize: TabBarIndicatorSize.tab,
                      indicatorWeight: 3,
                      tabs: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.chat_outlined,
                                color: MyTheme.titleColor, size: 22),
                            SizedBox(width: 5),
                            Flexible(
                              child: Txt(
                                txt: "Chat",
                                txtColor: MyTheme.titleColor,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                fontWeight: FontWeight.w500,
                                isBold: false,
                              ),
                            )
                          ],
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.group_rounded,
                                color: MyTheme.titleColor),
                            SizedBox(width: 5),
                            Flexible(
                              child: Txt(
                                txt: "Group",
                                txtColor: MyTheme.titleColor,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.center,
                                fontWeight: FontWeight.w500,
                                isBold: false,
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: TabBarView(
            controller: tabController,
            children: [
              drawChatView(),
              drawGroupChatView(),
            ],
          ),
        ),
      ),
    );
  }
}
