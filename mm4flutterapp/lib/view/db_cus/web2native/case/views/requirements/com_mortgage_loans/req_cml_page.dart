import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/2other_essentials/es_other_page.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/com_mortgage_loans/req_cml_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../../config/SrvW2NCase.dart';
import '../../../../../../../controller/network/NetworkMgr.dart';
import '../../../../../../../model/json/web2native/case/requirements/mortgage/MortgageRequirementsAPIModel.dart';
import '../../../../../../../model/json/web2native/case/requirements/remortgage/reasons/MortgageLoanReasonsModel.dart';
import '../../../../../../../model/json/web2native/case/requirements/remortgage/reasons/PostMortgageLoandReasonAPIModel.dart';
import '../../../../../../../view_model/api/api_view_model.dart';
import '../../../../../../../view_model/helper/ui_helper.dart';
import '../../../../../../widgets/dropdown/DropListModel.dart';
import '../../../dialogs/requirements/req_remortgage_dialog.dart';

class ReqCMLPage extends StatefulWidget {
  @override
  State<ReqCMLPage> createState() => _ReqCMLState();
}

class _ReqCMLState extends ReqCMLBase<ReqCMLPage> {
  final amount = TextEditingController();

  //  API start...
  onAddReasonDialog() {
    resetData();
    Get.dialog(reqReMortgageDialog(
        context: context,
        amount: amount,
        title: "Additional Loan",
        isNew: true,
        callbackSuccess: (otherTxt) async {
          try {
            var url = SrvW2NCase.POST_MORTGAGELOAN_REASONS_URL;
            await APIViewModel().req<PostMortgageLoandReasonAPIModel>(
                context: context,
                url: url,
                param: {
                  "Id": 0,
                  "UserId": userData.userModel.id,
                  "CreationDate": DateTime.now().toString(),
                  "Status": 0,
                  "TaskId": caseModelCtrl.locModel.value.id,
                  "LoanReasonName": requirementController.optReq.value.title,
                  "Amount": amount.text.trim(),
                  "Remarks": otherTxt,
                },
                reqType: ReqType.Post,
                callback: (model) async {
                  if (mounted && model != null) {
                    if (model.success) {
                      requirementsCtrl.listMortgageReasons
                          .add(model.responseData.mortgageLoanReason);
                      requirementsCtrl.update();
                      StateProvider()
                          .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
                      setState(() {});
                    }
                  }
                });
          } catch (e) {}
          setState(() {});
        },
        callbackFailed: (err) {
          showToast(context: context, msg: err);
        }));
  }

  onEditReasonDialog(MortgageLoanReasonsModel modelEdit) {
    amount.text = modelEdit.amount.toStringAsFixed(0);
    requirementController.optReq.value =
        OptionItem(id: modelEdit.remarks, title: modelEdit.loanReasonName);
    Get.dialog(reqReMortgageDialog(
        context: context,
        amount: amount,
        title: "Edit Mortgage Loan Reason",
        isNew: false,
        callbackSuccess: (otherTxt) async {
          try {
            var url = SrvW2NCase.PUT_MORTGAGELOAN_REASONS_URL;
            await APIViewModel().req<PostMortgageLoandReasonAPIModel>(
                context: context,
                url: url,
                param: {
                  "Id": modelEdit.id,
                  "UserId": userData.userModel.id,
                  "CreationDate": DateTime.now().toString(),
                  "Status": 0,
                  "TaskId": caseModelCtrl.locModel.value.id,
                  "LoanReasonName": requirementController.optReq.value.title,
                  "Amount": amount.text.trim(),
                  "Remarks": otherTxt,
                },
                reqType: ReqType.Put,
                callback: (model) async {
                  if (mounted && model != null) {
                    if (model.success) {
                      requirementsCtrl.listMortgageReasons.remove(modelEdit);
                      requirementsCtrl.listMortgageReasons
                          .add(model.responseData.mortgageLoanReason);
                      requirementsCtrl.update();
                      StateProvider()
                          .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
                      setState(() {});
                    }
                  }
                });
          } catch (e) {}
          setState(() {});
        },
        callbackFailed: (err) {
          showToast(context: context, msg: err);
        }));
  }

  onDelReasonDialog(MortgageLoanReasonsModel modelOld) async {
    try {
      await APIViewModel().req<PostMortgageLoandReasonAPIModel>(
          context: context,
          url: SrvW2NCase.DEL_MORTGAGELOAN_REASONS_URL + modelOld.id.toString(),
          reqType: ReqType.Delete,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                requirementsCtrl.listMortgageReasons.remove(modelOld);
                requirementsCtrl.update();
                StateProvider()
                    .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
                setState(() {});
                showToast(
                    context: context,
                    msg: "Mortgage loan reason deleted successfully.",
                    which: 1);
              }
            }
          });
    } catch (e) {}
  }

  onRequirementAPI() async {
    var id;
    try {
      if (requirementsCtrl.requirementsModelAPIModel != null)
        id = requirementsCtrl.requirementsModelAPIModel.value.id;
    } catch (e) {}
    var _loanAmount = 0;
    var _depositAmount = 0;
    var _noBedrooms = 0;
    var _incentiveAmount = 0;
    var _prefMortTermsYY = 0;
    var _prefMortTermsMM = 0;
    var _noKitchens = 0;
    var _noBathrooms = 0;
    var _noWC = 0;
    var _purchasePrice = 0;
    var _currentValProperty = 0;
    var _groundRent = 0;
    var _srvCharges = 0;

    var _mortgageRedeemed = 0;
    var _monthlyMortgagePayment = 0;
    var _capitalRaising = 0;
    var _currentMortOutstanding = 0;
    //var _ltvAmount = 0;
    var _howmuchFund = 0;
    var _incomeFromProperty = 0;

    try {
      _loanAmount = int.parse(loanAmount.text.trim());
    } catch (e) {}
    try {
      _depositAmount = int.parse(depositAmount.text.trim());
    } catch (e) {}
    try {
      _noBedrooms = int.parse(noBedrooms.text.trim());
    } catch (e) {}
    try {
      _incentiveAmount = int.parse(incentiveAmount.text.trim());
    } catch (e) {}
    try {
      _prefMortTermsYY = int.parse(prefMortTermsYY.text.trim());
    } catch (e) {}
    try {
      _prefMortTermsMM = int.parse(prefMortTermsMM.text.trim());
    } catch (e) {}
    try {
      _noKitchens = int.parse(noKitchens.text.trim());
    } catch (e) {}
    try {
      _noBathrooms = int.parse(noBathrooms.text.trim());
    } catch (e) {}
    try {
      _noWC = int.parse(noWC.text.trim());
    } catch (e) {}
    try {
      _purchasePrice = int.parse(purchasePrice.text.trim());
    } catch (e) {}
    try {
      _currentValProperty = int.parse(currentValProperty.text.trim());
    } catch (e) {}
    try {
      _groundRent = int.parse(groundRent.text.trim());
    } catch (e) {}
    try {
      _srvCharges = int.parse(srvCharges.text.trim());
    } catch (e) {}

    try {
      _mortgageRedeemed = int.parse(mortgageRedeemed.text.trim());
    } catch (e) {}
    try {
      _monthlyMortgagePayment = int.parse(monthlyMortgagePayment.text.trim());
    } catch (e) {}
    try {
      _capitalRaising = int.parse(capitalRaising.text.trim());
    } catch (e) {}
    try {
      _currentMortOutstanding = int.parse(currentMortOutstanding.text.trim());
    } catch (e) {}
    try {
      //_ltvAmount = int.parse(ltvAmount.text.trim());
    } catch (e) {}
    try {
      _howmuchFund = int.parse(howmuchFund.text.trim());
    } catch (e) {}
    try {
      _incomeFromProperty = int.parse(incomeFromProperty.text.trim());
    } catch (e) {}
    try {
      var param = {
        "Id": id != null ? id : 0,
        "UserId": userData.userModel.id,
        "User": null,
        "CompanyId": userData.userModel.userCompanyID,
        "Status": 101,
        "MortgageCaseInfoId": 0,
        "CreationDate": DateTime.now().toString(),
        "MortgageType": "",
        "AddressOfPropertyToBeMortgaged": addrOfProperty,
        "PriceOfPropertyBeingPurchasedCurrentValuationOfProperty":
            _currentValProperty,
        "HowMuchDoYouWishToBorrow": _loanAmount,
        "AmountOfDepositEquity": _depositAmount,
        "SourceOfDeposit": "",
        "Savings": 0,
        "OtherStateWhat": 0,
        "AreFundsAvailableToPayFeesInConnectionWithMortgage":
            W2NLocalData.listYesNoRB[fundsAvailableRBIndex],
        "DoesExistingLenderFacilitateFurtherAdvances": "No",
        "CurrentLenderAccountNumber": currentLenderAccNo.text.trim(),
        "PropertyType": listPropertyTypeRB[propertyTypeIndex],
        "PropertyTenure": listPropertyTenureRB[propertyTenureIndex],
        "IfLeaseholdHowLongIsLeftOnTheLease": yearLeftLease.text.trim(),
        "NumberOfBedrooms": _noBedrooms,
        "FloorsInTheBuilding": floorBuilding.text.trim(),
        "WhichFloorIsTheProperty": whichFloorProperty.text.trim(),
        "YearPropertyWasBuilt": yearBuilt.text.trim(),
        "AnyExtensionOrLoftConversionDone":
            W2NLocalData.listYesNoRB[extLoftConvRBIndex],
        "IsThePropertyOfANonStandardConstruction":
            W2NLocalData.listYesNoRB[nonStandardConstrRBIndex],
        "IsItExCouncil": W2NLocalData.listYesNoRB[exCouncilRBIndex],
        "IsThisANewBuiltProperty":
            W2NLocalData.listYesNoRB[newBuiltPropRBIndex],
        "HasTheVendorOwnedThePropertyOverSixMonths":
            W2NLocalData.listYesNoRB[vendorOwned6mRBIndex],
        "AreYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesOrGoods":
            W2NLocalData.listYesNoRB[incentivesRBIndex],
        "IsThisAnAuctionOrRepossessionPurchase":
            W2NLocalData.listYesNoRB[repossessionRBIndex],
        "HasThisPropertyBeenFoundWithTheAssistanceOfAPropertyClub":
            W2NLocalData.listYesNoRB[propertyClubRBIndex],
        "Notes": notes.text.trim(),
        "Remarks": "",
        "FinanceType": "",
        "AuctionHouseLotDetails": "",
        "RepaymentType": "",
        "ExitStrategy": "",
        "Details": "",
        "GVD": "",
        "AreYouLooking": listLookingForRB[lookingForRBIndex],
        "BalanceOutstanding": _currentMortOutstanding,
        "LTVAmount": 0, //_ltvAmount,
        "AmountofLoanRequired": 0,
        "TypeofLoan": "",
        "DoYouHaveAccountsForTheBusiness": "",
        "DoYouHaveManagementAccounts": "",
        "NonstandardConstructionDetails": nonStandardConstrDetails.text.trim(),
        "AreYouRaisingAnyMoneyfromAboveProperty":
            W2NLocalData.listYesNoRB[capitalRaisingRBIndex],
        "AreYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesValue":
            _incentiveAmount,
        "AreFundsAvailableToPayFeesInConnectionWithMortgageValue": _howmuchFund,
        "PreferredMortgageTermYear": _prefMortTermsYY,
        "PreferredMortgageTermMonth": _prefMortTermsMM,
        "NumberOfKoichens": _noKitchens,
        "NumberOfBaahroom": _noBathrooms,
        "NumberOfWC": _noWC,
        "IsTheFlatAboveShopRestaurant":
            W2NLocalData.listYesNoRB[flatHasShopRBIndex],
        "DoesTheFlatHaveDeckORBalconyAccess":
            W2NLocalData.listYesNoRB[xAccessRBIndex],
        "AreYouLookingToRemortgageYourCurrentResidentialProperty":
            W2NLocalData.listYesNoRB[reMortgagePropertyRBIndex],
        "DidYouPurchaseThePropertyDirectFromTheCouncilHousingAssociation":
            W2NLocalData.listYesNoRB[purchaseDirecltlyRBIndex],
        "AreThereAnyRightToBuyOrSimilarRestrictionImposed":
            W2NLocalData.listYesNoRB[restrictionRBIndex],
        "RestrictionExpireDate": restrictionDate,
        "AreYouAllowedToRemortgageThePropertyByTheCouncilHousingAssociation":
            W2NLocalData.listYesNoRB[housingAssociationRBIndex],
        "DoYouHaveExistingMortgageOnTheProperty":
            W2NLocalData.listYesNoRB[existingMortgagePropertyRBIndex],
        "CurrentLender": currentLender.text.trim(),
        "DatePropertyPurchased": datePropertyPurchase == ""
            ? AppConfig.date1970
            : datePropertyPurchase ?? AppConfig.date1970,
        "CurrentMortgageToBeRedeemed": _mortgageRedeemed,
        "PurchasePrice": _purchasePrice,
        "AmountRaisingOverCurrentMortgage": _capitalRaising,
        "DoYouWantToApplyForInterestOnlyLoan":
            W2NLocalData.listYesNoRB[loanInterestOnlyRBIndex],
        "MonthlyMortgagePayment": _monthlyMortgagePayment,
        "RentalIncomeAchievableAmount": 0,
        "IsThePropertyAlreadyTenanted": "No",
        "TenancyType": "",
        "AreYouCurrentlyResidingInThisProperty": "No",
        "AreYouRetainingThisPropertyAsABuyToLetPropertyToPurchaseANewResidentialProperty":
            "No",
        "AreYouAFirstTimeLandLord": "No",
        "HaveYouBeenAnExperiencedLandlordForMoreThan12Months": "No",
        "HaveYouEverLivedInThisProperty": "No",
        "AreYouLookingToTakeASecondChargeLoanOnYourInvestmentProperty": "Yes",
        "PropertyDescription": propDesc.text.trim(),
        "IncomeFromTheProperty": _incomeFromProperty.toString(),
        "WhatIsYourLoanRepaymentStrategy": loanRepayStrategy.text.trim(),
        "EquityAmount": 0,
        "PreferredRepaymentType": listPrefRepayTypeRB[prefRepayTypeRBIndex],
        "ServiceChargeAmount": _srvCharges,
        "GroundRentAmount": _groundRent,
        "ClientExperience": "",
        "Plan": "",
        "CostofRefurb": "0",
        "PostCode": "",
        "PropertyEPCRating": null,
        "SharedHolderPercentage": 0,
        "DatePropertyPurchasedDD": "",
        "DatePropertyPurchasedMM": "",
        "DatePropertyPurchasedYY": "",
        "RestrictionExpireDateDD": "",
        "RestrictionExpireDateMM": "",
        "RestrictionExpireDateYY": "",
        "TaskId": caseModelCtrl.locModel.value.id
      };

      await APIViewModel().req<MortgageRequirementsAPIModel>(
          context: context,
          url: id == null
              ? SrvW2NCase.POST_REQUIREMENTS_URL
              : SrvW2NCase.PUT_REQUIREMENTS_URL,
          reqType: id == null ? ReqType.Post : ReqType.Put,
          param: param,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                requirementsCtrl.requirementsModelAPIModel.value =
                    model.responseData.mortgageUserRequirement;
                requirementsCtrl.update();
                StateProvider()
                    .notify(ObserverState.STATE_W2N_REQUIREMENTS_LISTNER);
                Get.off(() => ESOtherPage());
              }
            }
          });
    } catch (e) {}
  }

  //  API end...

  resetData() {
    try {
      amount.clear();
      requirementController.optReq = OptionItem(id: null, title: "Select").obs;
    } catch (e) {}
  }

  updateFields() {
    final model = requirementsCtrl.requirementsModelAPIModel.value;

    try {
      lookingForRBIndex =
          Common.getMapKeyByVal(listLookingForRB, model.areYouLooking);
    } catch (e) {}
    try {
      reMortgagePropertyRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB,
          model.areYouLookingToRemortgageYourCurrentResidentialProperty);
    } catch (e) {}
    try {
      existingMortgagePropertyRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB,
          model.doYouHaveExistingMortgageOnTheProperty);
    } catch (e) {}
    try {
      addrOfProperty = model.addressOfPropertyToBeMortgaged ?? '';
    } catch (e) {}
    try {
      flatHasShopRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.isTheFlatAboveShopRestaurant);
    } catch (e) {}
    try {
      fundsAvailableRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
          model.areFundsAvailableToPayFeesInConnectionWithMortgage);
    } catch (e) {}
    try {
      howmuchFund.text = model
          .areFundsAvailableToPayFeesInConnectionWithMortgageValue
          .toStringAsFixed(0);
    } catch (e) {}
    try {
      mortgageRedeemed.text =
          model.currentMortgageToBeRedeemed.toStringAsFixed(0);
    } catch (e) {}
    try {
      monthlyMortgagePayment.text =
          model.monthlyMortgagePayment.toStringAsFixed(0);
    } catch (e) {}
    try {
      propDesc.text = model.propertyDescription ?? '';
    } catch (e) {}
    try {
      incomeFromProperty.text = model.incomeFromTheProperty ?? '';
    } catch (e) {}

    try {
      currentLender.text = model.currentLender ?? '';
    } catch (e) {}
    try {
      currentLenderAccNo.text = model.currentLenderAccountNumber ?? '';
    } catch (e) {}
    try {
      if (model != null)
        datePropertyPurchase =
            DateFun.getDate(model.datePropertyPurchased, 'dd-MMM-yyyy');
    } catch (e) {}
    try {
      currentValProperty.text = model
          .priceOfPropertyBeingPurchasedCurrentValuationOfProperty
          .toStringAsFixed(0);
    } catch (e) {}
    try {
      purchasePrice.text = model.purchasePrice.toStringAsFixed(0);
    } catch (e) {}
    try {
      loanAmount.text = model.howMuchDoYouWishToBorrow.toStringAsFixed(0);
    } catch (e) {}
    try {
      depositAmount.text = model.amountOfDepositEquity.toStringAsFixed(0);
    } catch (e) {}
    try {
      currentMortOutstanding.text = model.balanceOutstanding.toStringAsFixed(0);
    } catch (e) {}
    try {
      //ltvAmount.text = model.lTVAmount.toStringAsFixed(0);
    } catch (e) {}
    try {
      incentivesRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB,
          model
              .areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesOrGoods);
    } catch (e) {}
    try {
      incentiveAmount.text = model
          .areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesValue
          .toStringAsFixed(0);
    } catch (e) {}
    try {
      prefMortTermsYY.text = model.preferredMortgageTermYear.toString();
    } catch (e) {}
    try {
      prefMortTermsMM.text = model.preferredMortgageTermMonth.toString();
    } catch (e) {}
    try {
      prefRepayTypeRBIndex = Common.getMapKeyByVal(
          listPrefRepayTypeRB, model.preferredRepaymentType);
    } catch (e) {}
    try {
      propertyTypeIndex =
          Common.getMapKeyByVal(listPropertyTypeRB, model.propertyType);
    } catch (e) {}
    try {
      propertyTenureIndex =
          Common.getMapKeyByVal(listPropertyTenureRB, model.propertyTenure);
    } catch (e) {}
    try {
      yearLeftLease.text = model.ifLeaseholdHowLongIsLeftOnTheLease ?? '';
    } catch (e) {}
    try {
      capitalRaisingRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
          model.areYouRaisingAnyMoneyfromAboveProperty);
    } catch (e) {}
    try {
      capitalRaising.text =
          model.amountRaisingOverCurrentMortgage.toStringAsFixed(0);
    } catch (e) {}
    try {
      noBedrooms.text = model.numberOfBedrooms.toString();
    } catch (e) {}
    try {
      noKitchens.text = model.numberOfKoichens.toString();
    } catch (e) {}
    try {
      noBathrooms.text = model.numberOfBaahroom.toString();
    } catch (e) {}
    try {
      noWC.text = model.numberOfWC.toString();
    } catch (e) {}
    try {
      floorBuilding.text = model.floorsInTheBuilding ?? '';
    } catch (e) {}
    try {
      groundRent.text = model.groundRentAmount.toStringAsFixed(0);
    } catch (e) {}
    try {
      srvCharges.text = model.serviceChargeAmount.toStringAsFixed(0);
    } catch (e) {}
    try {
      loanInterestOnlyRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.doYouWantToApplyForInterestOnlyLoan);
    } catch (e) {}
    try {
      loanRepayStrategy.text = model.whatIsYourLoanRepaymentStrategy ?? '';
    } catch (e) {}
    try {
      yearBuilt.text = model.yearPropertyWasBuilt ?? '';
    } catch (e) {}
    try {
      whichFloorProperty.text = model.whichFloorIsTheProperty ?? '';
    } catch (e) {}
    try {
      extLoftConvRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.anyExtensionOrLoftConversionDone);
    } catch (e) {}
    try {
      purchaseDirecltlyRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB,
          model
              .didYouPurchaseThePropertyDirectFromTheCouncilHousingAssociation);
    } catch (e) {}
    try {
      restrictionRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
          model.areThereAnyRightToBuyOrSimilarRestrictionImposed);
    } catch (e) {}

    try {
      if (model != null)
        restrictionDate =
            DateFun.getDate(model.restrictionExpireDate, 'dd-MMM-yyyy');
      else {
        restrictionDate =
            DateFun.getDate(DateTime.now().toString(), 'dd-MMM-yyyy');
      }
    } catch (e) {}
    try {
      housingAssociationRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB,
          model
              .areYouAllowedToRemortgageThePropertyByTheCouncilHousingAssociation);
    } catch (e) {}
    try {
      nonStandardConstrRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
          model.isThePropertyOfANonStandardConstruction);
    } catch (e) {}
    try {
      nonStandardConstrDetails.text = model.nonstandardConstructionDetails;
    } catch (e) {}
    try {
      exCouncilRBIndex =
          Common.getMapKeyByVal(W2NLocalData.listYesNoRB, model.isItExCouncil);
    } catch (e) {}
    try {
      xAccessRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.doesTheFlatHaveDeckORBalconyAccess);
    } catch (e) {}
    try {
      newBuiltPropRBIndex = Common.getMapKeyByVal(
          W2NLocalData.listYesNoRB, model.isThisANewBuiltProperty);
    } catch (e) {}
    try {
      vendorOwned6mRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
          model.hasTheVendorOwnedThePropertyOverSixMonths);
    } catch (e) {}
    try {
      repossessionRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
          model.isThisAnAuctionOrRepossessionPurchase);
    } catch (e) {}
    try {
      propertyClubRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
          model.hasThisPropertyBeenFoundWithTheAssistanceOfAPropertyClub);
    } catch (e) {}
    try {
      notes.text = model.notes ?? '';
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    amount.dispose();
    currentValProperty.dispose();
    currentMortOutstanding.dispose();
    mortgageRedeemed.dispose();
    monthlyMortgagePayment.dispose();
    currentLender.dispose();
    currentLenderAccNo.dispose();
    purchasePrice.dispose();
    loanAmount.dispose();
    depositAmount.dispose();
    incentiveAmount.dispose();
    prefMortTermsYY.dispose();
    prefMortTermsMM.dispose();
    noBedrooms.dispose();
    noKitchens.dispose();
    noBathrooms.dispose();
    groundRent.dispose();
    srvCharges.dispose();
    noWC.dispose();
    floorBuilding.dispose();
    whichFloorProperty.dispose();
    yearBuilt.dispose();
    notes.dispose();
    nonStandardConstrDetails.dispose();
    yearLeftLease.dispose();
    capitalRaising.dispose();
    loanRepayStrategy.dispose();

    focusCurrentValProperty.dispose();
    focusCurrentMortOutstanding.dispose();
    focusMortgageRedeemed.dispose();
    focusMonthlyMortgagePayment.dispose();
    focusCurrentLender.dispose();
    focusCurrentLenderAccNo.dispose();
    focusPurchasePrice.dispose();
    focusLoanAmount.dispose();
    focusDepositAmount.dispose();
    focusIncentivesAmount.dispose();
    focusPrefMortTermsYY.dispose();
    focusPrefMortTermsMM.dispose();
    focusNoBedrooms.dispose();
    focusNoKitchens.dispose();
    focusNoBathrooms.dispose();
    focusGroundRent.dispose();
    focusSrvCharges.dispose();
    focusNoWC.dispose();
    focusFloorBuilding.dispose();
    focusYearBuilt.dispose();
    focusNonStandardConstrDetails.dispose();
    focusYearLeftLease.dispose();
    focusWhichFloorProperty.dispose();
    focusCapitalRaising.dispose();
    focusLoanRepayStrategy.dispose();
    super.dispose();
  }

  initPage() async {
    try {
      //print(getSubCase());
      //onGetReasonsAPI();
      updateFields();
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    //print(widget.mortgageUserRequirementModel);
    //updateFields();
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(
                    title: caseModelCtrl.locModel.value.title +
                        " | " +
                        caseModelCtrl.locModel.value.id.toString()),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    drawProgressView(5),
                    drawForm(),
                    SizedBox(height: 30),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Flexible(
                          child: MMBtn(
                              txt: "Back",
                              height: getHP(context, 7),
                              width: getW(context),
                              bgColor: Colors.grey,
                              radius: 10,
                              callback: () async {
                                Get.back();
                              }),
                        ),
                        SizedBox(width: 10),
                        Flexible(
                          child: MMBtn(
                              txt: "Save & continue",
                              height: getHP(context, 7),
                              width: getW(context),
                              radius: 10,
                              callback: () {
                                if (onValidate()) {
                                  onRequirementAPI();
                                }
                              }),
                        ),
                      ],
                    ),
                    SizedBox(height: 40),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
