import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/PostAssessAffordItemAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/dialogs/requirements/12assess_afford_items/assess_afford_items_dialog.dart';
import 'package:aitl/view/db_cus/web2native/case_base.dart';
import 'package:aitl/view/widgets/btn/BtnOutline.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view/widgets/txt/SignText.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class AssessAffordBase<T extends StatefulWidget> extends CaseBase<T> {
  calculation();

  //  house hold
  final monthlyRent = TextEditingController();
  final mortgageRent = TextEditingController();
  final electricity = TextEditingController();
  final councilTax = TextEditingController();
  final tv = TextEditingController();
  final insurance = TextEditingController();
  final gas = TextEditingController();
  final water = TextEditingController();
  final other = TextEditingController();
  final groceries = TextEditingController();
  final clothing = TextEditingController();
  final homeHelp = TextEditingController();
  final laundry = TextEditingController();
  final mobilePhone = TextEditingController();
  final homePhone = TextEditingController();
  final internet = TextEditingController();
  final investments = TextEditingController();
  final maintenancePayments = TextEditingController();
  final entertainment = TextEditingController();
  final holiday = TextEditingController();
  final sports = TextEditingController();
  final cigarettes = TextEditingController();
  final alcohol = TextEditingController();
  final pension = TextEditingController();
  final leisure = TextEditingController();
  final travel = TextEditingController();
  final regSubs = TextEditingController();
  final pets = TextEditingController();
  final insurancePhone = TextEditingController();
  final regSaving = TextEditingController();
  final petrol = TextEditingController();
  final comCost = TextEditingController();
  final carCost = TextEditingController();
  final carFuelCost = TextEditingController();
  final motorInsurance = TextEditingController();
  final otherTransCost = TextEditingController();
  final dayCare = TextEditingController();
  final schooling = TextEditingController();
  final outing = TextEditingController();
  final clothes = TextEditingController();
  final notes = TextEditingController();

  getVal(TextEditingController tf) {
    try {
      return double.parse(tf.text);
    } catch (e) {}
    return 0;
  }

  drawForm() {
    //if (assessAffordCtrl.assessAffordModel.value == null) return SizedBox();

    return Container(
      margin: const EdgeInsets.all(10),
      padding: const EdgeInsets.all(5),
      decoration: BoxDecoration(
          border: Border.all(color: MyTheme.statusBarColor, width: .5)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Card(
            elevation: 2,
            color: Colors.grey[200],
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Flexible(
                      child: Txt(
                    txt: "CURRENT OR EXPECTED FUTURE EXPENDITURE",
                    txtColor: MyTheme.dBlueAirColor,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.start,
                    isBold: true,
                  )),
                  Flexible(
                      child: Txt(
                    txt: "AMOUNT - Monthly or Annual",
                    txtColor: MyTheme.dBlueAirColor,
                    txtSize: MyTheme.txtSize - .4,
                    txtAlign: TextAlign.end,
                    isBold: true,
                  )),
                ],
              ),
            ),
          ),
          SizedBox(height: 20),
          _drawRow(
            title:
                "Total “MONTHLY PAYMENT” of continuing loans/credit from previous page",
            isEdit: false,
            tf: monthlyRent,
          ),
          SizedBox(height: 20),
          Txt(
            txt: "Household",
            txtColor: MyTheme.statusBarColor,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: true,
          ),
          /*SizedBox(height: 10),
          _drawRow(
            title: "Mortgage/Rent",
            tf: mortgageRent,
          ),*/
          SizedBox(height: 10),
          _drawRow(
            title: "Electric",
            tf: electricity,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Council Tax",
            tf: councilTax,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Television",
            tf: tv,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Insurance",
            tf: insurance,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Gas",
            tf: gas,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Water",
            tf: water,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Other",
            tf: other,
          ),
          SizedBox(height: 20),
          Txt(
            txt: "Living Cost",
            txtColor: MyTheme.statusBarColor,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: true,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Groceries",
            tf: groceries,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Clothing",
            tf: clothing,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Home Help",
            tf: homeHelp,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Laundry",
            tf: laundry,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Mobile Phone",
            tf: mobilePhone,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Home Phone",
            tf: homePhone,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Internet",
            tf: internet,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Investments",
            tf: investments,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Maintenance Payments",
            tf: maintenancePayments,
          ),
          SizedBox(height: 20),
          Txt(
            txt: "Life Styles",
            txtColor: MyTheme.statusBarColor,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: true,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Entertainment",
            tf: entertainment,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Holiday",
            tf: holiday,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Sports",
            tf: sports,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Cigarettes",
            tf: cigarettes,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Alcohol",
            tf: alcohol,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Pension",
            tf: pension,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Leisure",
            tf: leisure,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Travel",
            tf: travel,
          ),
          SizedBox(height: 10),
          _drawRow(
            title:
                "Regular subscriptions ie. newspaper, magazine, films, health club, golf, tennis, football, etc",
            tf: regSubs,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Pets - food, insurance, grooming, etc",
            tf: pets,
          ),
          SizedBox(height: 10),
          _drawRow(
            title:
                "Insurances (other than B&C) ie, life, health, medical, dental, phone, etc",
            tf: insurancePhone,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Regular savings",
            tf: regSaving,
          ),
          SizedBox(height: 20),
          Txt(
            txt: "Transport Costs",
            txtColor: MyTheme.statusBarColor,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: true,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Petrol",
            tf: petrol,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Commuting Costs",
            tf: comCost,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Car Costs",
            tf: carCost,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Car fuel costs",
            tf: carFuelCost,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Motor Insurance",
            tf: motorInsurance,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Other Transport Cost",
            tf: otherTransCost,
          ),
          SizedBox(height: 20),
          Txt(
            txt: "Children",
            txtColor: MyTheme.statusBarColor,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: true,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Day Care",
            tf: dayCare,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Schooling",
            tf: schooling,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Other schooling costs ie. meals, uniform, outings, etc",
            tf: outing,
          ),
          SizedBox(height: 10),
          _drawRow(
            title: "Clothes",
            tf: clothes,
          ),
          SizedBox(height: 10),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Card(
                elevation: 2,
                color: Colors.grey[200],
                child: Padding(
                  padding: const EdgeInsets.only(left: 5, right: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Txt(
                          txt: "Others",
                          txtColor: MyTheme.statusBarColor,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.start,
                          isBold: true,
                        ),
                      ),
                      BtnOutline(
                          txt: "Add Category",
                          txtColor: Colors.black,
                          txtSize: 1.6,
                          isBold: true,
                          borderColor: MyTheme.statusBarColor,
                          callback: () {
                            Get.dialog(reqAccessAffordDialog(
                                context: context,
                                taskId: caseModelCtrl.locModel.value.id,
                                model2: null,
                                callbackSuccess: (model) {
                                  assessAffordCtrl.listAssessAffordItemModel
                                      .add(model);
                                  assessAffordCtrl.update();
                                  showToast(
                                      context: context,
                                      msg:
                                          "Category has been added successfully");
                                  Future.delayed(Duration(seconds: 1),
                                      () => calculation());
                                  setState(() {});
                                },
                                callbackFailed: (err) {
                                  showToast(context: context, msg: err);
                                }));
                          }),
                    ],
                  ),
                ),
              ),
              drawAssessAffordItems(
                  context, true, assessAffordCtrl.listAssessAffordItemModel,
                  (model2, isEdit) async {
                if (isEdit) {
                  Get.dialog(reqAccessAffordDialog(
                      context: context,
                      taskId: caseModelCtrl.locModel.value.id,
                      model2: model2,
                      callbackSuccess: (model) {
                        assessAffordCtrl.listAssessAffordItemModel
                            .remove(model2);
                        assessAffordCtrl.listAssessAffordItemModel.add(model);
                        assessAffordCtrl.update();
                        showToast(
                            context: context,
                            msg: "Category has been updated successfully");
                        Future.delayed(
                            Duration(seconds: 1), () => calculation());
                        setState(() {});
                      },
                      callbackFailed: (err) {
                        showToast(context: context, msg: err);
                      }));
                } else {
                  try {
                    await APIViewModel().req<PostAssessAffordItemAPIModel>(
                        context: context,
                        url: SrvW2NCase.DEL_ASSESS_AFFORD_ITEM_URL +
                            model2.id.toString(),
                        reqType: ReqType.Delete,
                        callback: (model) async {
                          if (mounted && model != null) {
                            if (model.success) {
                              assessAffordCtrl.listAssessAffordItemModel
                                  .remove(model2);
                              assessAffordCtrl.update();
                              Future.delayed(
                                  Duration(seconds: 1), () => calculation());
                              setState(() {});
                            }
                          }
                        });
                  } catch (e) {}
                }
              })
            ],
          ),
        ],
      ),
    );
  }

  _drawRow({
    @required TextEditingController tf,
    String title,
    bool isEdit = true,
  }) {
    return IntrinsicHeight(
      child: Row(
        children: [
          Expanded(
              flex: 2,
              child: Txt(
                txt: title,
                txtColor: Colors.black,
                txtSize: MyTheme.txtSize - .5,
                txtAlign: TextAlign.start,
                isBold: true,
              )),
          SizedBox(width: 5),
          Flexible(
              child: isEdit
                  ? drawInputCurrencyBox(
                      context: context,
                      tf: tf,
                      hintTxt: null,
                      labelColor: Colors.black,
                      labelTxt: null,
                      isBold: true,
                      len: 3,
                      focusNode: null,
                      focusNodeNext: null,
                      onChange: (v) {
                        try {
                          calculation();
                        } catch (e) {}
                      })
                  : drawSignText(
                      context: context,
                      title: null,
                      txt: tf.text,
                    )),
        ],
      ),
    );
  }

  setData(TextEditingController tf, String v) {
    try {
      tf.text = v.replaceAll(".00", "");
    } catch (e) {}
  }
}
