import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/10credit_commitment/req_cr_commit_mixin.dart';
import 'package:aitl/view/db_cus/web2native/case_base.dart';
import 'package:flutter/material.dart';

abstract class CrCommitBase<T extends StatefulWidget> extends CaseBase<T> {}
