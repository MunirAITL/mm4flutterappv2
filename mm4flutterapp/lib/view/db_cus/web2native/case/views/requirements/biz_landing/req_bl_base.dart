import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';
import '../../../../../../../config/MyTheme.dart';
import '../../../../../../../model/json/web2native/case/requirements/remortgage/reasons/MortgageLoanReasonsModel.dart';
import '../../../../../../../view_model/rx/web2native/requirements/req_remortgage_ctrl.dart';
import '../../../../../../widgets/dialog/DatePickerView.dart';
import '../../../../../../widgets/gplaces/GPlacesView.dart';
import '../../../../../../widgets/txt/Txt.dart';
import '../../../../case_base.dart';
import '../../../mixin/requirements/req_scr_mixin.dart';

abstract class ReqBLBase<T extends StatefulWidget> extends CaseBase<T>
    with ReqSCRMixin {
  onAddReasonDialog();
  onEditReasonDialog(MortgageLoanReasonsModel model);
  onDelReasonDialog(MortgageLoanReasonsModel model);

  final requirementController = Get.put(ReqReMortgageCtrl());

  final listLookingForRB = {0: 'Purchase Mortgage', 1: 'Remortgage'};
  int lookingForRBIndex = 1;

  int reMortgagePropertyRBIndex = 1;

  int existingMortgagePropertyRBIndex = 1;

  int incentivesRBIndex = 1;

  int capitalRaisingRBIndex = 1;

  int loanInterestOnlyRBIndex = 1;

  final listPrefRepayTypeRB = {
    0: 'Capital interest',
    1: 'Interest only',
    2: 'Part & part',
    3: 'No preference'
  };
  int prefRepayTypeRBIndex = 0;

  final listPropertyTypeRB = {
    0: 'Fully commercial',
    1: 'Land',
    2: 'Warehouse',
    3: 'Hotel',
    4: 'Patrol Station',
    5: 'Sem-Commercial'
  };
  int propertyTypeIndex = 0;

  final listPropertyTenureRB = {0: 'Freehold', 1: 'Leasehold', 2: 'Feudal'};
  int propertyTenureIndex = 0;

  int extLoftConvRBIndex = 1;

  int fundsAvailableRBIndex = 1;

  int nonStandardConstrRBIndex = 1;

  int exCouncilRBIndex = 1;

  int xAccessRBIndex = 1;

  int purchaseDirecltlyRBIndex = 1;

  int restrictionRBIndex = 1;

  int housingAssociationRBIndex = 1;

  int newBuiltPropRBIndex = 1;

  int flatHasShopRBIndex = 1;

  int repossessionRBIndex = 1;

  int vendorOwned6mRBIndex = 1;

  int propertyClubRBIndex = 1;

  final currentValProperty = TextEditingController();
  final currentMortOutstanding = TextEditingController();
  final mortgageRedeemed = TextEditingController();
  final monthlyMortgagePayment = TextEditingController();
  final currentLender = TextEditingController();
  final currentLenderAccNo = TextEditingController();
  final purchasePrice = TextEditingController();
  final loanAmount = TextEditingController();
  final depositAmount = TextEditingController();
  final incentiveAmount = TextEditingController();
  final prefMortTermsYY = TextEditingController();
  final prefMortTermsMM = TextEditingController();
  final noBedrooms = TextEditingController();
  final noKitchens = TextEditingController();
  final noBathrooms = TextEditingController();
  final groundRent = TextEditingController();
  final srvCharges = TextEditingController();
  final noWC = TextEditingController();
  final floorBuilding = TextEditingController();
  final whichFloorProperty = TextEditingController();
  final yearBuilt = TextEditingController();
  final notes = TextEditingController();
  final nonStandardConstrDetails = TextEditingController();
  final yearLeftLease = TextEditingController();
  final capitalRaising = TextEditingController();
  final loanRepayStrategy = TextEditingController();
  final howmuchFund = TextEditingController();
  final propDesc = TextEditingController();
  final incomeFromProperty = TextEditingController();

  final focusCurrentValProperty = FocusNode();
  final focusCurrentMortOutstanding = FocusNode();
  final focusMortgageRedeemed = FocusNode();
  final focusMonthlyMortgagePayment = FocusNode();
  final focusCurrentLender = FocusNode();
  final focusCurrentLenderAccNo = FocusNode();
  final focusPurchasePrice = FocusNode();
  final focusLoanAmount = FocusNode();
  final focusDepositAmount = FocusNode();
  final focusIncentivesAmount = FocusNode();
  final focusPrefMortTermsYY = FocusNode();
  final focusPrefMortTermsMM = FocusNode();
  final focusNoBedrooms = FocusNode();
  final focusNoKitchens = FocusNode();
  final focusNoBathrooms = FocusNode();
  final focusGroundRent = FocusNode();
  final focusSrvCharges = FocusNode();
  final focusNoWC = FocusNode();
  final focusFloorBuilding = FocusNode();
  final focusYearBuilt = FocusNode();
  final focusNonStandardConstrDetails = FocusNode();
  final focusYearLeftLease = FocusNode();
  final focusWhichFloorProperty = FocusNode();
  final focusCapitalRaising = FocusNode();
  final focusLoanRepayStrategy = FocusNode();
  final focusHowmuchFund = FocusNode();
  final focusIncomeFromProperty = FocusNode();

  String datePropertyPurchase = "";
  String restrictionDate = "";
  String addrOfProperty = "";

  onValidate() {
    return true;
  }

  drawForm() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          UIHelper().drawRadioTitle(
              context: context,
              title: "Are you looking for:",
              list: listLookingForRB,
              index: lookingForRBIndex,
              callback: (index) {
                setState(() => lookingForRBIndex = index);
              }),
          SizedBox(height: 10),
          GPlacesView(
              title: "Address of property to be mortgaged?",
              titleColor: Colors.black,
              isBold: true,
              txtSize: MyTheme.txtSize - .2,
              address: addrOfProperty,
              callback: (String address, String postCode, Location loc) {
                addrOfProperty = address;
                setState(() {});
              }),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Current Valuation of Property?",
              "If you're not sure, your best guess is fine at this point.",
              currentValProperty,
              focusCurrentValProperty,
              lookingForRBIndex == 1
                  ? focusCurrentMortOutstanding
                  : focusPurchasePrice,
              (v) {}),
          SizedBox(height: 10),
          DatePickerView(
            cap: "Date property purchased",
            dt: (datePropertyPurchase == '')
                ? 'Select Date'
                : datePropertyPurchase,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            initialDate: DateTime.now(),
            firstDate: DateTime(1900, 1, 1),
            lastDate: DateTime.now(),
            padding: 5,
            radius: 5,
            callback: (value) {
              if (mounted) {
                setState(() {
                  try {
                    datePropertyPurchase =
                        DateFormat('dd-MM-yyyy').format(value).toString();
                  } catch (e) {
                    myLog(e.toString());
                  }
                });
              }
            },
          ),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Purchase price",
              "If you're not sure, your best guess is fine at this point.",
              purchasePrice,
              focusPurchasePrice,
              focusLoanAmount, (v) {
            try {
              final v1 = v.trim().isEmpty ? '0' : v.trim();
              final v2 =
                  loanAmount.text.trim().isEmpty ? '0' : loanAmount.text.trim();
              final purPrice = int.parse(v1);
              final lonAmt = int.parse(v2);
              depositAmount.text = lonAmt > purPrice
                  ? (lonAmt - purPrice).toString()
                  : (purPrice - lonAmt).toString();
              setState(() {});
            } catch (e) {}
          }),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Loan Amount",
              "If you're not sure, your best guess is fine at this point.",
              loanAmount,
              focusLoanAmount,
              focusDepositAmount, (v) {
            try {
              final v1 = v.trim().isEmpty ? '0' : v.trim();
              final v2 = purchasePrice.text.trim().isEmpty
                  ? '0'
                  : purchasePrice.text.trim();
              final lonAmt = int.parse(v1);
              final purPrice = int.parse(v2);
              depositAmount.text = lonAmt > purPrice
                  ? (lonAmt - purPrice).toString()
                  : (purPrice - lonAmt).toString();
              setState(() {});
            } catch (e) {}
          }),
          SizedBox(height: 10),
          _drawPrefMortgageTermsView(),
          _drawPrefRepayTypeView(),
          SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              title:
                  "Are funds available to pay fees in connection with mortgage?",
              list: W2NLocalData.listYesNoRB,
              index: fundsAvailableRBIndex,
              callback: (index) {
                setState(() => fundsAvailableRBIndex = index);
              }),
          fundsAvailableRBIndex == 0
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: drawCurrencyBox(
                      "How much funds available to pay fees in connection with mortgage?",
                      "If you're not sure, your best guess is fine at this point.",
                      howmuchFund,
                      focusHowmuchFund,
                      null,
                      (v) {}))
              : SizedBox(),
          _drawPropertyTypeView(),
          SizedBox(height: 10),
          drawTextArea(title: "Property Description", tf: propDesc),
          SizedBox(height: 10),
          drawCurrencyBox("Income from the property", null, incomeFromProperty,
              focusIncomeFromProperty, null, (v) {}),
          lookingForRBIndex == 1
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: UIHelper().drawRadioTitle(
                      context: context,
                      title: "Has the vendor owned the property over 6 Months",
                      list: W2NLocalData.listYesNoRB,
                      index: vendorOwned6mRBIndex,
                      callback: (index) {
                        setState(() => vendorOwned6mRBIndex = index);
                      }))
              : SizedBox(),
          SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              title: "Is this an auction or repossession purchase?",
              list: W2NLocalData.listYesNoRB,
              index: repossessionRBIndex,
              callback: (index) {
                setState(() => repossessionRBIndex = index);
              }),
          SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              title:
                  "Has this property been found with the assistance of a property club, finder or similar?",
              list: W2NLocalData.listYesNoRB,
              index: propertyClubRBIndex,
              callback: (index) {
                setState(() => propertyClubRBIndex = index);
              }),
          SizedBox(height: 10),
          drawTextArea(title: "Notes", tf: notes),
        ],
      ),
    );
  }

  _drawPropertyTypeView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Security Property Details",
            subTitle: "What type of property is this?",
            list: listPropertyTypeRB,
            index: propertyTypeIndex,
            callback: (index) {
              setState(() => propertyTypeIndex = index);
            }),
      ],
    );
  }

  _drawPrefRepayTypeView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Preferred repayment type",
            list: listPrefRepayTypeRB,
            index: prefRepayTypeRBIndex,
            callback: (index) {
              setState(() => prefRepayTypeRBIndex = index);
            }),
      ],
    );
  }

  _drawPrefMortgageTermsView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: incentivesRBIndex == 0 ? 20 : 0),
        Txt(
            txt: "Preferred mortgage term",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: true),
        SizedBox(height: 5),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
                child: drawInputCurrencyBox(
                    sign: "Year",
                    context: context,
                    tf: prefMortTermsYY,
                    hintTxt: null,
                    len: 4,
                    focusNode: focusPrefMortTermsYY,
                    focusNodeNext: focusPrefMortTermsMM)),
            SizedBox(width: 10),
            Flexible(
                child: drawInputCurrencyBox(
                    sign: "Month",
                    context: context,
                    tf: prefMortTermsMM,
                    hintTxt: null,
                    len: 2,
                    focusNode: focusPrefMortTermsMM,
                    focusNodeNext: null)),
          ],
        )
      ],
    );
  }
}
