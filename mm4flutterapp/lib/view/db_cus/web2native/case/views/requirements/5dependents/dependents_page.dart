import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/model/json/web2native/case/requirements/5dependents/MortgageFinancialDependantsModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/5dependents/PostDependentsAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/4address_history/addr_history_page.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../../config/MyTheme.dart';
import '../../../../../../../controller/network/NetworkMgr.dart';
import '../../../../../../../model/data/UserData.dart';
import '../../../../../../../view_model/api/api_view_model.dart';
import '../../../../../../../view_model/helper/ui_helper.dart';
import '../../../../../../../view_model/rx/web2native/requirements/main/req_step5_dependent_ctrl.dart';
import '../../../../../../widgets/dropdown/DropListModel.dart';
import '../../../dialogs/requirements/5dependent/dependent_dialog.dart';
import 'dependents_base.dart';

class DependentsPage extends StatefulWidget {
  @override
  State<DependentsPage> createState() => _DependentsPageState();
}

class _DependentsPageState extends DependentsBase<DependentsPage> {
  final dependentController = Get.put(ReqStep5DependentCtrl());
  final scrollController = ScrollController();
  final name = TextEditingController();
  final livingWith = TextEditingController();

  //  API start...
  onPostDependentAPI() async {
    try {
      var dd = '', mm = '', yy = '';
      try {
        final dobArr = dependentController.dob.value.split("-");
        dd = dobArr[0];
        mm = dobArr[1];
        yy = dobArr[2];
      } catch (e) {}
      await APIViewModel().req<PostDependentsAPIModel>(
          context: context,
          url: SrvW2NCase.POST_DEPENDENT_URL,
          reqType: ReqType.Post,
          param: {
            "Id": 0,
            "UserId": userData.userModel.id,
            "CompanyId": userData.userModel.userCompanyID,
            "Status": 101,
            "MortgageCaseInfoId": 0,
            "TaskId": caseModelCtrl.locModel.value.id,
            "CreationDate": DateTime.now().toString(),
            "CustomerId": 0,
            "CustmerRelationShipWithAnotherCustomer": "",
            "Name": name.text.trim(),
            "DateOfBirth": dependentController.dob.value,
            "Relationship": dependentController.optRelationShip.value.title,
            "LivingWith": livingWith.text.trim(),
            "Remarks": "",
            "IsTheDependantSameAsFirstApplicant": "No",
            "Age": 0,
            "DateofBirthDD": dd,
            "DateofBirthMM": mm,
            "DateofBirthYY": yy
          },
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                dependentCtrl.listDependentsModelAPIModel
                    .add(model.responseData.mortgageFinancialDependant);
                dependentCtrl.update();
                setState(() {});
                Future.delayed(
                    Duration(milliseconds: 500),
                    () => scrollController.animateTo(
                        scrollController.position.maxScrollExtent,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.ease));
                showToast(
                    context: context,
                    msg: "Dependent added successfully.",
                    which: 1);
              }
            }
          });
    } catch (e) {}
  }

  onPutDependentAPI(MortgageFinancialDependantsModel modelOld) async {
    try {
      var dd = '', mm = '', yy = '';
      try {
        final dobArr = dependentController.dob.value.split("-");
        dd = dobArr[0];
        mm = dobArr[1];
        yy = dobArr[2];
      } catch (e) {}
      await APIViewModel().req<PostDependentsAPIModel>(
          context: context,
          url: SrvW2NCase.PUT_DEPENDENT_URL,
          reqType: ReqType.Put,
          param: {
            "Id": modelOld.id,
            "UserId": userData.userModel.id,
            "CompanyId": userData.userModel.userCompanyID,
            "Status": 101,
            "MortgageCaseInfoId": 0,
            "TaskId": caseModelCtrl.locModel.value.id,
            "CreationDate": DateTime.now().toString(),
            "CustomerId": 0,
            "CustmerRelationShipWithAnotherCustomer": "",
            "Name": name.text.trim(),
            "DateOfBirth": dependentController.dob.value,
            "Relationship": dependentController.optRelationShip.value.title,
            "LivingWith": livingWith.text.trim(),
            "Remarks": "",
            "IsTheDependantSameAsFirstApplicant": "No",
            "Age": 0,
            "DateofBirthDD": dd,
            "DateofBirthMM": mm,
            "DateofBirthYY": yy
          },
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                dependentCtrl.listDependentsModelAPIModel.remove(modelOld);
                dependentCtrl.listDependentsModelAPIModel
                    .add(model.responseData.mortgageFinancialDependant);
                dependentCtrl.update();
                setState(() {});
                Future.delayed(
                    Duration(milliseconds: 500),
                    () => scrollController.animateTo(
                        scrollController.position.maxScrollExtent,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.ease));
                showToast(
                    context: context,
                    msg: "Dependent updated successfully.",
                    which: 1);
              }
            }
          });
    } catch (e) {}
  }

  onDelDependentAPI(MortgageFinancialDependantsModel modelOld) async {
    try {
      await APIViewModel().req<PostDependentsAPIModel>(
          context: context,
          url: SrvW2NCase.DEL_DEPENDENT_URL + modelOld.id.toString(),
          reqType: ReqType.Delete,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                dependentCtrl.listDependentsModelAPIModel.remove(modelOld);
                dependentCtrl.update();
                setState(() {});
                showToast(
                    context: context,
                    msg: "Dependent deleted successfully.",
                    which: 1);
              }
            }
          });
    } catch (e) {}
  }

  //  API end...

  onAddDependentDialog() {
    resetData();
    Get.dialog(dependentDialog(
        context: context,
        livingWith: livingWith,
        name: name,
        title: "Add Financial Dependents",
        callbackSuccess: () {
          onPostDependentAPI();
        },
        callbackFailed: (err) {
          showToast(context: context, msg: err);
        }));
  }

  onEditDependentDialog(MortgageFinancialDependantsModel model) {
    try {
      name.text = model.name ?? '';
      livingWith.text = model.livingWith ?? '';
      dependentController.dob.value = model.dateOfBirth ?? '';
      dependentController.optRelationShip.value =
          OptionItem(id: "0", title: model.relationship);
      Get.dialog(dependentDialog(
          context: context,
          livingWith: livingWith,
          name: name,
          title: "Edit Financial Dependents",
          callbackSuccess: () {
            onPutDependentAPI(model);
          },
          callbackFailed: (err) {
            showToast(context: context, msg: err);
          }));
    } catch (e) {}
  }

  resetData() {
    try {
      name.clear();
      livingWith.clear();
      dependentController.dob.value = "";
      dependentController.optRelationShip =
          OptionItem(id: null, title: "Select Relationship").obs;
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    scrollController.dispose();
    name.dispose();
    livingWith.dispose();
    super.dispose();
  }

  initPage() async {}

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(title: "Your dependants"),
              ),
              bottomNavigationBar: BottomAppBar(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: MMBtn(
                            txt: "Back",
                            height: getHP(context, 7),
                            width: getW(context),
                            bgColor: Colors.grey,
                            radius: 10,
                            callback: () async {
                              Get.back();
                            }),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        child: MMBtn(
                            txt: "Save & continue",
                            height: getHP(context, 7),
                            width: getW(context),
                            radius: 10,
                            callback: () async {
                              /*var id = 0;
                      try {
                        id = mainReqStep2ESCtrl.getES1Model.value.id;
                      } catch (e) {}*/
                              Get.off(() => AddrHistoryPage());
                              /*await APIViewModel()
                          .req<GetMortgageCaseInfoLocTaskidUseridAPIModel>(
                              context: context,
                              url: SrvW2NCase.PUT_MORT_CASE_URL,
                              reqType: ReqType.Put,
                              param: {
                                "Id": id, //dependentController.,
                                "UserId": userData.userModel.id,
                                "CompanyId": userData.userModel.userCompanyInfo.id,
                                "Status": 101,
                                "CreationDate": DateTime.now().toString(),
                                "UpdatedDate": DateTime.now().toString(),
                                "VersionNumber": 1,
                                "CaseType": null,
                                "CustomerType": "",
                                "IsSmoker": userData.userModel.areYouASmoker,
                                "IsDoYouHaveAnyFinancialDependants":
                                    listDependentsRB[dependentsRBIndex],
                                "Remarks": "",
                                "IsAnyOthers": "No",
                                "TaskId": caseModelCtrl.locModel.value.id,
                                "CoapplicantUserId": 0,
                                "CustomerName": userData.userModel.name,
                                "CustomerEmail": userData.userModel.email,
                                "CustomerMobileNumber":
                                    userData.userModel.mobileNumber,
                                "CustomerAddress": "",
                                "ProfileImageUrl":
                                    userData.userModel.profileImageURL,
                                "NamePrefix": userData.userModel.namePrefix,
                                "AreYouBuyingThePropertyInNameOfASPV": "",
                                "CompanyName": "",
                                "RegisteredAddress": "",
                                "DateRegistered": DateTime.now().toString(),
                                "CompanyRegistrationNumber": "",
                                "ApplicationNumber": 1,
                                "CustomerEmail1": "",
                                "CustomerEmail2": "",
                                "CustomerEmail3": "",
                                "IsDoYouHaveAnyBTLPortfulio": "",
                                "IsClientAgreement": "",
                                "AdminFee": 0,
                                "AdminFeeWhenPayable": "",
                                "AdviceFee": 0,
                                "AdviceFeeWhenPayable": "",
                                "IsFeesRefundable": "",
                                "FeesRefundable": "",
                                "IPAdddress": "116.71.5.140",
                                "IPLocation": null,
                                "DeviceType": null,
                                "ReportLogo": null,
                                "OfficePhoneNumber": null,
                                "ReportFooter": null,
                                "AdviceFeeType": "",
                                "ClientAgreementStatus": "",
                                "RecommendationAgreementStatus": "",
                                "RecommendationAgreementSignature": "",
                                "IsThereAnySavingsOrInvestments": "",
                                "IsThereAnyExistingPolicy": "",
                                "TaskTitleUrl": "",
                                "CustomerAddress1": "",
                                "CustomerAddress2": "",
                                "CustomerAddress3": "",
                                "CustomerPostcode": "",
                                "CustomerTown": "",
                                "CustomerLastName": userData.userModel.lastName,
                                "CustomerDateofBirth":
                                    userData.userModel.dateofBirth,
                                "CustomerGender": userData.userModel.cohort,
                                "CustomerAreYouASmoker":
                                    userData.userModel.areYouASmoker,
                                "OccupationCode": null,
                                "IsTheCompanyATradingCompany": null
                              },
                              callback: (model) async {
                                if (mounted && model != null) {
                                  if (model.success) {
                                    Get.to(() => AddrHistoryPage());
                                  }
                                }
                              });*/
                            }),
                      ),
                    ],
                  ),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        controller: scrollController,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    drawProgressView(40),
                    drawAddDependentView(),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
