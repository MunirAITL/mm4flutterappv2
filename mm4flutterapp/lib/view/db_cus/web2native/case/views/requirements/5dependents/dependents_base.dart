import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/model/json/web2native/case/requirements/5dependents/MortgageFinancialDependantsModel.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';

import '../../../../case_base.dart';

abstract class DependentsBase<T extends StatefulWidget> extends CaseBase<T> {
  int dependentsRBIndex = 0;

  onAddDependentDialog();
  onEditDependentDialog(MortgageFinancialDependantsModel model);
  onDelDependentAPI(MortgageFinancialDependantsModel modelOld);

  drawAddDependentView() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          Txt(
              txt: "Do you have any dependant?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: true),
          SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              subTitle:
                  "People who depend on you financially. For example, children or adults in your care.",
              list: W2NLocalData.listYesNoRB,
              index: dependentsRBIndex,
              callback: (index) {
                setState(() => dependentsRBIndex = index);
              }),
          dependentsRBIndex == 0 ? _drawAddDependent() : SizedBox(),
        ],
      ),
    );
  }

  _drawAddDependent() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(top: 20),
        child: Column(
          children: [
            drawDottedBox("Add Dependent", () {
              onAddDependentDialog();
            }),
            Padding(
                padding: const EdgeInsets.only(top: 20),
                child: drawDependentItems(
                    context, true, dependentCtrl.listDependentsModelAPIModel,
                    (model) {
                  onEditDependentDialog(model);
                }, (model) {
                  onDelDependentAPI(model);
                })),
          ],
        ),
      ),
    );
  }
}
