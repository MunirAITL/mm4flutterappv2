import 'dart:convert';

import 'package:aitl/config/AppConfig.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/SrvW2NCase.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/web2native/case/requirements/9saving_investment/MortgageUserSavingOrInvestmentItem.dart';
import 'package:aitl/model/json/web2native/case/requirements/9saving_investment/PostSavingInvAPIModel.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/DatePickerView.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/input/InputW2N.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:json_string/json_string.dart';

import 'saving_inv_base.dart';

class SavingInvDetailsPage extends StatefulWidget {
  final MortgageUserSavingOrInvestmentItem savingInvModel;
  const SavingInvDetailsPage({Key key, this.savingInvModel}) : super(key: key);
  @override
  State<SavingInvDetailsPage> createState() => _SavingInvDetailsState();
}

class _SavingInvDetailsState extends SavingInvBase<SavingInvDetailsPage> {
  DropListModel ddProvider = DropListModel([
    OptionItem(id: "1", title: "Abbey Life"),
    OptionItem(id: "1", title: "Aegon Scottish Equitable"),
    OptionItem(id: "1", title: "AIG Life"),
    OptionItem(id: "1", title: "AJ Bell"),
    OptionItem(id: "1", title: "Alba Life"),
    OptionItem(id: "1", title: "AMP National Provident Ins"),
    OptionItem(id: "1", title: "Aviva"),
    OptionItem(id: "1", title: "AXA"),
    OptionItem(id: "1", title: "Black Horse Life"),
    OptionItem(id: "1", title: "Bright Grey"),
    OptionItem(id: "1", title: "Britannic Money"),
    OptionItem(id: "1", title: "British Life"),
    OptionItem(id: "1", title: "Bupa"),
    OptionItem(id: "1", title: "Canada Life"),
    OptionItem(id: "1", title: "Century Life"),
    OptionItem(id: "1", title: "Cirencester FS"),
    OptionItem(id: "1", title: "Clerical Medical"),
    OptionItem(id: "1", title: "Engage Mutual"),
    OptionItem(id: "1", title: "Equitable Life"),
    OptionItem(id: "1", title: "Forester Life"),
    OptionItem(id: "1", title: "Friends Provident"),
    OptionItem(id: "1", title: "GE Life"),
    OptionItem(id: "1", title: "Groupama"),
    OptionItem(id: "1", title: "Guardian Financial Services"),
    OptionItem(id: "1", title: "Hamilton Life Assurance"),
    OptionItem(id: "1", title: "Hartford Life"),
    OptionItem(id: "1", title: "Irish Permanent"),
    OptionItem(id: "1", title: "Legal &amp; General"),
    OptionItem(id: "1", title: "London Life"),
    OptionItem(id: "1", title: "LV="),
    OptionItem(id: "1", title: "Merchant Investors"),
    OptionItem(id: "1", title: "National Friendly"),
    OptionItem(id: "1", title: "Partnership"),
    OptionItem(id: "1", title: "Payment Shield"),
    OptionItem(id: "1", title: "Pearl"),
    OptionItem(id: "1", title: "Pru Health"),
    OptionItem(id: "1", title: "Pru-Protrect"),
    OptionItem(id: "1", title: "Royal London"),
    OptionItem(id: "1", title: "Scottish Friendly"),
    OptionItem(id: "1", title: "Scottish Provident"),
    OptionItem(id: "1", title: "Scottish Window"),
    OptionItem(id: "1", title: "Standard Life"),
    OptionItem(id: "1", title: "Suffolk Life"),
    OptionItem(id: "1", title: "Swiss Life"),
    OptionItem(id: "1", title: "Unum"),
    OptionItem(id: "1", title: "Vitality"),
    OptionItem(id: "1", title: "Windsor Life"),
    OptionItem(id: "1", title: "Winterthur Life"),
    OptionItem(id: "1", title: "Zurich"),
    OptionItem(id: "1", title: "Other"),
  ]);
  var optProvider = OptionItem(id: null, title: "Select Provider").obs;

  DropListModel ddObj = DropListModel([
    OptionItem(id: "1", title: "Capital Growth"),
    OptionItem(id: "1", title: "Emergency Funds"),
    OptionItem(id: "1", title: "Final Salary"),
    OptionItem(id: "1", title: "Future Income"),
    OptionItem(id: "1", title: "Immediate Income"),
    OptionItem(id: "1", title: "Long Term Care"),
    OptionItem(id: "1", title: "Money Purchase"),
    OptionItem(id: "1", title: "No Purpose"),
    OptionItem(id: "1", title: "Ready Cash"),
    OptionItem(id: "1", title: "Retirement"),
    OptionItem(id: "1", title: "Target Purchase"),
    OptionItem(id: "1", title: "Other"),
  ]);
  var optObj = OptionItem(id: null, title: "Select objective").obs;

  DropListModel ddFreq = DropListModel([
    OptionItem(id: "1", title: "Monthly"),
    OptionItem(id: "1", title: "Bi-Annual"),
    OptionItem(id: "1", title: "Quarterly"),
    OptionItem(id: "1", title: "Annual"),
    OptionItem(id: "1", title: "Single Premium"),
    OptionItem(id: "1", title: "4 Weekly"),
    OptionItem(id: "1", title: "Weekly"),
  ]);
  var optFreqDeposit = OptionItem(id: null, title: "Select Frequency").obs;
  var optFreqInv = OptionItem(id: null, title: "Select Frequency").obs;
  var optFreqIncome = OptionItem(id: null, title: "Select Frequency").obs;

  final owner = TextEditingController();
  final balance = TextEditingController();
  final intrRate = TextEditingController();
  final incomeTaken = TextEditingController();
  final accNo = TextEditingController();
  final regDepositAmount = TextEditingController();
  final regInvAmount = TextEditingController();
  final regIncomeAmount = TextEditingController();
  final platform = TextEditingController();
  final initInv = TextEditingController();
  final valuation = TextEditingController();
  final assetDesc = TextEditingController();
  final purchasePrice = TextEditingController();
  final furtherDetails = TextEditingController();
  final cash12Mdetails = TextEditingController();
  final emergencyFundDetails = TextEditingController();

  //
  final listAssetTypeRB = {
    0: "Savings",
    1: "Investments",
    2: "Other Assets",
    3: "Emergency Fund"
  };
  int assetTypeRBIndex = 0;

  //  *********** asset types start...
  Map<int, String> listSavingTypeRB = {
    0: "Bank Account",
    1: "Cash",
    2: "Shares",
  };
  int savingTypeRBIndex = 0;

  final listEmergencyFundRB = {0: "Yes", 1: "No"};
  int emergencyFundRBIndex = 1;

  final listRegDepositRB = {0: "Yes", 1: "No"};
  int regDepositRBIndex = 1;

  final listRegInvRB = {0: "Yes", 1: "No"};
  int regInvRBIndex = 1;

  final listRegIncomeRB = {0: "Yes", 1: "No"};
  int regIncomeRBIndex = 1;

  final listEstateDeathRB = {0: "Yes", 1: "No"};
  int estateDeathRBIndex = 1;

  final listSpouseDeathRB = {0: "Yes", 1: "No"};
  int spouseDeathRBIndex = 1;

  final listCashed12MRB = {0: "Yes", 1: "No"};
  int cashed12MRBIndex = 1;

  var dtBalanceDate = "".obs;
  var dtStartInvDate = "".obs;
  var dtValuationDate = "".obs;
  var dtMatDate = "".obs;
  var dtPurchaseDate = "".obs;

  //  *********** asset types end...

  onExistingPolicyAPI() async {
    try {
      final model2 = widget.savingInvModel;

      var id;
      var _balance = 0;
      var _intrRate = 0;
      var _depositInvAmount = 0;
      var _initInv = 0;
      var _regIncomeAmount = 0;
      var _valuation = 0;
      var _purchasePrice = 0;

      try {
        id = widget.savingInvModel.id;
      } catch (e) {}
      try {
        _balance = int.parse(balance.text);
      } catch (e) {}
      try {
        _intrRate = int.parse(intrRate.text);
      } catch (e) {}
      try {
        _depositInvAmount = int.parse(regDepositAmount.text);
      } catch (e) {}
      try {
        _initInv = int.parse(initInv.text);
      } catch (e) {}
      try {
        _regIncomeAmount = int.parse(regIncomeAmount.text);
      } catch (e) {}
      try {
        _valuation = int.parse(valuation.text);
      } catch (e) {}
      try {
        _purchasePrice = int.parse(purchasePrice.text);
      } catch (e) {}

      final now = DateFun.getDate(DateTime.now().toString(), "dd-MMM-yyyy");
      final param = {
        "Id": id ?? 0,
        "UserId": userData.userModel.id,
        "UserCompanyId": userData.userModel.userCompanyInfo.id,
        "Status": 101,
        "MortgageUserSavingOrInvestmentId": 0,
        "MortgageCaseInfoId": 0,
        "TaskId": caseModelCtrl.locModel.value.id,
        "CreationDate": now,
        "UpdatedDate": now,
        "TypeofAsset": listAssetTypeRB[assetTypeRBIndex],
        "SavingsType": listSavingTypeRB[savingTypeRBIndex],
        "Owners": owner.text.trim(),
        "Provider": optProvider.value.title,
        "ProviderOtherName": "",
        "Objective": optObj.value.title,
        "Balance": _balance,
        "BalanceDate": dtBalanceDate.value.isEmpty
            ? AppConfig.date1970
            : dtBalanceDate.value ?? AppConfig.date1970,
        "InterestRate": _intrRate,
        "MaturityDate": dtMatDate.value.isEmpty
            ? AppConfig.date1970
            : dtMatDate.value ?? AppConfig.date1970,
        "IncomeTakenOrReinvested": incomeTaken.text.trim(),
        "AccountNumber": accNo.text.trim(),
        "AreRegularDepositOrOrInvestmentBeingMade":
            listRegDepositRB[regDepositRBIndex],
        "DepositOrInvestmentAmount": _depositInvAmount,
        "DepositOrInvestmentFrequency": optFreqDeposit.value.title,
        "WillThisFormPartOfTheEstateOnDeath":
            listEstateDeathRB[estateDeathRBIndex],
        "FurtherDetails": furtherDetails.text,
        "Platform": platform.text.trim(),
        "InitialInvestmentAmount": _initInv,
        "InitialInvestmentStartDate": dtStartInvDate.value.isEmpty
            ? AppConfig.date1970
            : dtStartInvDate.value ?? AppConfig.date1970,
        "IsRegularIncomeCurrentlyBeingDrawn": listRegIncomeRB[regIncomeRBIndex],
        "IncomeCurrentlyBeingDrawnAmount": _regIncomeAmount,
        "IncomeCurrentlyBeingDrawnFrequency": optFreqIncome.value.title,
        "WillThisFormPartOfTheSpousesEstateOnDeath":
            listSpouseDeathRB[spouseDeathRBIndex],
        "AssetDescription": assetDesc.text.trim(),
        "ValuationAmount": _valuation,
        "ValuationDate": dtValuationDate.value.isEmpty
            ? AppConfig.date1970
            : dtValuationDate.value ?? AppConfig.date1970,
        "Notes": "",
        "PurchasePrice": _purchasePrice,
        "PurchaseDate": dtPurchaseDate.value.isEmpty
            ? AppConfig.date1970
            : dtPurchaseDate.value ?? AppConfig.date1970,
        "Remarks": "",
        "DoesTheClientHaveAnEmergencyFund":
            listEmergencyFundRB[emergencyFundRBIndex],
        "DoesTheClientHaveAnEmergencyFundDetails":
            emergencyFundDetails.text.trim(),
        "WereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12Months":
            listCashed12MRB[cashed12MRBIndex],
        "WereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12MonthsDetails":
            cash12Mdetails.text.trim()
      };

      myLog(JsonString(json.encode(param)).source);

      await APIViewModel().req<PostSavingInvAPIModel>(
          context: context,
          url: model2 == null
              ? SrvW2NCase.POST_SAVING_INV_URL
              : SrvW2NCase.PUT_SAVING_INV_URL,
          reqType: model2 == null ? ReqType.Post : ReqType.Put,
          param: param,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                if (model2 != null) {
                  savingInvCtrl.savingInvCtrl.remove(model2);
                }
                savingInvCtrl.savingInvCtrl
                    .add(model.responseData.mortgageUserSavingOrInvestmentItem);
                savingInvCtrl.update();
                if (model2 != null) {
                  showToast(
                      context: context,
                      msg: "Saving investment updated successfully");
                } else {
                  showToast(
                      context: context,
                      msg: "Saving investment added successfully");
                }
                Future.delayed(Duration(seconds: 3), () {
                  Get.back();
                });
              }
            }
          });
    } catch (e) {}
  }

  updateAssetTypo(int index) {
    switch (index) {
      case 0:
        listSavingTypeRB = {
          0: "Bank Account",
          1: "Cash",
          2: "Shares",
        };
        break;
      case 1:
        listSavingTypeRB = {
          0: "Bank Account",
          1: "Bond",
          2: "Cash",
          3: "Guaranteed Investment Cash",
          4: "ISA",
          5: "ISA Transfer",
          6: "Shares",
          7: "Unit Trust",
        };
        break;
      case 2:
        listSavingTypeRB = {
          0: "Antiques",
          1: "Cars",
          2: "Contents",
          3: "Works of Art",
          4: "Other",
        };
        break;
      case 3:
        listSavingTypeRB = {};
        break;
      default:
    }
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  initPage() async {
    try {
      if (widget.savingInvModel != null) {
        final model = widget.savingInvModel;
        try {
          assetTypeRBIndex =
              Common.getMapKeyByVal(listAssetTypeRB, model.typeofAsset);
        } catch (e) {}

        updateAssetTypo(assetTypeRBIndex);

        try {
          savingTypeRBIndex =
              Common.getMapKeyByVal(listSavingTypeRB, model.savingsType);
        } catch (e) {}
        //
        try {
          emergencyFundRBIndex = Common.getMapKeyByVal(
              listEmergencyFundRB, model.doesTheClientHaveAnEmergencyFund);
        } catch (e) {}
        try {
          cashed12MRBIndex = Common.getMapKeyByVal(
              listCashed12MRB, model.doesTheClientHaveAnEmergencyFund);
        } catch (e) {}
        try {
          emergencyFundDetails.text =
              model.doesTheClientHaveAnEmergencyFundDetails.trim();
        } catch (e) {}
        try {
          cash12Mdetails.text = model
              .wereAnyInvestmentsSurrenderedOrCashedInDuringTheLast12MonthsDetails
              .trim();
        } catch (e) {}
        //
        try {
          owner.text = model.owners.trim();
        } catch (e) {}
        try {
          optProvider = OptionItem(id: "1", title: model.provider).obs;
        } catch (e) {}
        try {
          optObj = OptionItem(id: "1", title: model.objective).obs;
        } catch (e) {}
        try {
          balance.text = model.balance.toStringAsFixed(0);
        } catch (e) {}
        //
        try {
          dtBalanceDate.value =
              DateFun.getDate(model.balanceDate, "dd-MMM-yyyy");
        } catch (e) {}
        try {
          dtStartInvDate.value =
              DateFun.getDate(model.initialInvestmentStartDate, "dd-MMM-yyyy");
        } catch (e) {}
        try {
          dtMatDate.value = DateFun.getDate(model.maturityDate, "dd-MMM-yyyy");
        } catch (e) {}
        try {
          dtValuationDate.value =
              DateFun.getDate(model.valuationDate, "dd-MMM-yyyy");
        } catch (e) {}
        try {
          dtPurchaseDate.value =
              DateFun.getDate(model.purchaseDate, "dd-MMM-yyyy");
        } catch (e) {}
        //
        try {
          intrRate.text = model.interestRate.toStringAsFixed(0);
        } catch (e) {}
        try {
          incomeTaken.text = model.incomeTakenOrReinvested.trim();
        } catch (e) {}
        try {
          accNo.text = model.accountNumber.trim();
        } catch (e) {}
        //
        try {
          regDepositRBIndex = Common.getMapKeyByVal(
              listRegDepositRB, model.areRegularDepositOrOrInvestmentBeingMade);
        } catch (e) {}
        try {
          regDepositAmount.text =
              model.depositOrInvestmentAmount.toStringAsFixed(0);
        } catch (e) {}
        try {
          optFreqDeposit =
              OptionItem(id: "1", title: model.depositOrInvestmentFrequency)
                  .obs;
        } catch (e) {}
        //
        try {
          estateDeathRBIndex = Common.getMapKeyByVal(
              listEstateDeathRB, model.willThisFormPartOfTheEstateOnDeath);
        } catch (e) {}
        try {
          spouseDeathRBIndex = Common.getMapKeyByVal(listSpouseDeathRB,
              model.willThisFormPartOfTheSpousesEstateOnDeath);
        } catch (e) {}
        try {
          furtherDetails.text = model.furtherDetails;
        } catch (e) {}
        //  **************
        try {
          platform.text = model.platform.trim();
        } catch (e) {}
        try {
          initInv.text = model.initialInvestmentAmount.toStringAsFixed(0);
        } catch (e) {}
        try {
          valuation.text = model.valuationAmount.toStringAsFixed(0);
        } catch (e) {}
        //
        try {
          regInvRBIndex = Common.getMapKeyByVal(
              listRegInvRB, model.areRegularDepositOrOrInvestmentBeingMade);
        } catch (e) {}
        try {
          regInvAmount.text =
              model.depositOrInvestmentAmount.toStringAsFixed(0);
        } catch (e) {}
        try {
          optFreqInv =
              OptionItem(id: "1", title: model.depositOrInvestmentFrequency)
                  .obs;
        } catch (e) {}
        //
        try {
          regIncomeRBIndex = Common.getMapKeyByVal(
              listRegIncomeRB, model.isRegularIncomeCurrentlyBeingDrawn);
        } catch (e) {}
        try {
          regIncomeAmount.text =
              model.incomeCurrentlyBeingDrawnAmount.toStringAsFixed(0);
        } catch (e) {}
        try {
          optFreqIncome = OptionItem(
                  id: "1", title: model.incomeCurrentlyBeingDrawnFrequency)
              .obs;
        } catch (e) {}
        //  **************
        try {
          assetDesc.text = model.assetDescription.trim();
        } catch (e) {}
        try {
          owner.text = model.owners.trim();
        } catch (e) {}
        try {
          purchasePrice.text = model.purchasePrice.toString();
        } catch (e) {}
      }
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
              //resizeToAvoidBottomInset: false,
              backgroundColor: MyTheme.bgColor2,
              appBar: AppBar(
                centerTitle: false,
                iconTheme: IconThemeData(color: Colors.white),
                backgroundColor: MyTheme.statusBarColor,
                elevation: MyTheme.appbarElevation,
                title: UIHelper().drawAppbarTitle(
                    title: widget.savingInvModel == null
                        ? "Add Saving & Investment"
                        : "Update Saving & Investment"),
              ),
              bottomNavigationBar: BottomAppBar(
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 20, top: 5, bottom: 5),
                  child: MMBtn(
                      txt: widget.savingInvModel == null ? "Save" : "Update",
                      height: getHP(context, 7),
                      width: getW(context),
                      radius: 5,
                      callback: () async {
                        if (assetTypeRBIndex != 3 && owner.text.isEmpty) {
                          showToast(
                              context: context, msg: "Please enter owner");
                          return;
                        }
                        if ((assetTypeRBIndex == 0 || assetTypeRBIndex == 1) &&
                            optProvider.value.id == null) {
                          showToast(
                              context: context, msg: "Please choose provider");
                          return;
                        }
                        if ((assetTypeRBIndex == 0 || assetTypeRBIndex == 1) &&
                            optObj.value.id == null) {
                          showToast(
                              context: context, msg: "Please choose objective");
                          return;
                        }
                        if (assetTypeRBIndex == 0 && balance.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter balance amount");
                          return;
                        }
                        if (assetTypeRBIndex == 1 && initInv.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter initial investment");
                          return;
                        }
                        if ((assetTypeRBIndex == 1) && valuation.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter valuation amount");
                          return;
                        }
                        if ((assetTypeRBIndex == 0 || assetTypeRBIndex == 1) &&
                            intrRate.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter Interest rate");
                          return;
                        }
                        if ((assetTypeRBIndex == 0 || assetTypeRBIndex == 1) &&
                            accNo.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter account number");
                          return;
                        }
                        if (assetTypeRBIndex == 2 && assetDesc.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter asset description");
                          return;
                        }
                        if (assetTypeRBIndex == 2 &&
                            purchasePrice.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter purchase price");
                          return;
                        }
                        if (assetTypeRBIndex == 3 &&
                            emergencyFundRBIndex == 0 &&
                            emergencyFundDetails.text.isEmpty) {
                          showToast(
                              context: context,
                              msg: "Please enter emergency fund details");
                          return;
                        }
                        if (assetTypeRBIndex == 3 &&
                            cashed12MRBIndex == 0 &&
                            cash12Mdetails.text.isEmpty) {
                          showToast(
                              context: context,
                              msg:
                                  "Please enter details of any investments surrendered or cashed in during the last 12 months");
                          return;
                        }
                        onExistingPolicyAPI();
                      }),
                ),
              ),
              body:
                  drawLayout() /*GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),*/
              ),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    Widget wid = SizedBox();
    switch (assetTypeRBIndex) {
      case 0:
        wid = drawSavings();
        break;
      case 1:
        wid = drawInvestments();
        break;
      case 2:
        wid = drawOtherAssets();
        break;
      case 3:
        wid = drawEmergencyFund();
        break;
      default:
    }
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              UIHelper().drawRadioTitle(
                  context: context,
                  title: "Type of Asset",
                  list: listAssetTypeRB,
                  index: assetTypeRBIndex,
                  callback: (index) {
                    updateAssetTypo(index);
                    setState(() => assetTypeRBIndex = index);
                  }),
              wid,
            ],
          ),
        ),
      ),
    );
  }

  drawSavings() {
    final DateTime now = DateTime.now();
    final old = DateTime(now.year - 50, now.month, now.day);
    final next = DateTime(now.year + 10, now.month, now.day);
    return Obx(() => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 15),
            UIHelper().drawRadioTitle(
                context: context,
                title: "Savings Type",
                list: listSavingTypeRB,
                index: savingTypeRBIndex,
                callback: (index) {
                  setState(() => savingTypeRBIndex = index);
                }),
            SizedBox(height: 10),
            InputW2N(
              title: "Owner",
              input: owner,
              ph: "Owner",
              kbType: TextInputType.text,
              len: 50,
              isBold: true,
            ),
            SizedBox(height: 5),
            DropDownListDialog(
              context: context,
              title: optProvider.value.title,
              h1: "Select provider",
              heading: "Provider",
              ddTitleList: ddProvider,
              vPadding: 3,
              callback: (optionItem) {
                optProvider.value = optionItem;
              },
            ),
            SizedBox(height: 10),
            DropDownListDialog(
              context: context,
              title: optObj.value.title,
              h1: "Select objective",
              heading: "Objective",
              ddTitleList: ddObj,
              vPadding: 3,
              callback: (optionItem) {
                optObj.value = optionItem;
              },
            ),
            SizedBox(height: 10),
            drawInputCurrencyBox(
                context: context,
                tf: balance,
                hintTxt: null,
                labelColor: Colors.black,
                labelTxt: "Balance",
                isBold: true,
                len: 10,
                focusNode: null,
                focusNodeNext: null),
            SizedBox(height: 10),
            DatePickerView(
              cap: "Date",
              dt: (dtBalanceDate.value == '')
                  ? 'Select Date'
                  : dtBalanceDate.value,
              txtColor: Colors.black,
              fontWeight: FontWeight.bold,
              initialDate: now,
              firstDate: old,
              lastDate: next,
              padding: 5,
              radius: 5,
              callback: (value) {
                if (mounted) {
                  dtBalanceDate.value =
                      DateFormat('dd-MMM-yyyy').format(value).toString();
                }
              },
            ),
            SizedBox(height: 10),
            drawInputCurrencyBox(
                sign: "%",
                context: context,
                tf: intrRate,
                hintTxt: null,
                labelColor: Colors.black,
                labelTxt: "Interest Rate (%)",
                isBold: true,
                len: 3,
                focusNode: null,
                focusNodeNext: null,
                onChange: (v) {
                  if (v.isNotEmpty) {
                    try {
                      var rate = int.parse(v);
                      if (rate > 100) {
                        showAlert(
                            context: context,
                            msg: "The rate must be between 0 to 100");
                        intrRate.text = "100";
                      }
                    } catch (e) {}
                  }
                }),
            SizedBox(height: 10),
            DatePickerView(
              cap: "Maturity Date",
              dt: (dtMatDate.value == '') ? 'Select Date' : dtMatDate.value,
              txtColor: Colors.black,
              fontWeight: FontWeight.bold,
              initialDate: now,
              firstDate: old,
              lastDate: next,
              padding: 5,
              radius: 5,
              callback: (value) {
                if (mounted) {
                  dtMatDate.value =
                      DateFormat('dd-MMM-yyyy').format(value).toString();
                }
              },
            ),
            SizedBox(height: 10),
            InputW2N(
              title: "Income taken/Reinvested",
              input: incomeTaken,
              ph: "Income taken/Reinvested",
              kbType: TextInputType.text,
              len: 50,
              isBold: true,
            ),
            SizedBox(height: 5),
            InputW2N(
              title: "Account Number",
              input: accNo,
              ph: "Account Number",
              kbType: TextInputType.number,
              len: 20,
              isBold: true,
            ),
            SizedBox(height: 5),
            UIHelper().drawRadioTitle(
                context: context,
                title: "Repay before mortgage completes?",
                list: listRegDepositRB,
                index: regDepositRBIndex,
                callback: (index) {
                  setState(() => regDepositRBIndex = index);
                }),
            regDepositRBIndex == 0
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      drawInputCurrencyBox(
                          context: context,
                          tf: regDepositAmount,
                          hintTxt: null,
                          labelColor: Colors.black,
                          labelTxt: "Amount",
                          isBold: true,
                          len: 3,
                          focusNode: null,
                          focusNodeNext: null),
                      SizedBox(height: 10),
                      DropDownListDialog(
                        context: context,
                        title: optFreqDeposit.value.title,
                        h1: "Select frequency",
                        heading: "Frequency",
                        ddTitleList: ddFreq,
                        vPadding: 3,
                        callback: (optionItem) {
                          optFreqDeposit.value = optionItem;
                        },
                      ),
                      SizedBox(height: 10),
                    ],
                  )
                : SizedBox(),
            SizedBox(height: 10),
            UIHelper().drawRadioTitle(
                context: context,
                title: "Will this form part of the estate on death?",
                list: listEstateDeathRB,
                index: estateDeathRBIndex,
                callback: (index) {
                  setState(() => estateDeathRBIndex = index);
                }),
            SizedBox(height: 10),
            UIHelper().drawRadioTitle(
                context: context,
                title: "Will this form part of the Spouse’s estate on death?",
                list: listSpouseDeathRB,
                index: spouseDeathRBIndex,
                callback: (index) {
                  setState(() => spouseDeathRBIndex = index);
                }),
            SizedBox(height: 10),
            drawTextArea(title: "Further Details", tf: furtherDetails),
          ],
        ));
  }

  drawInvestments() {
    final DateTime now = DateTime.now();
    final old = DateTime(now.year - 50, now.month, now.day);
    final next = DateTime(now.year + 10, now.month, now.day);
    return Obx(() => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 15),
            UIHelper().drawRadioTitle(
                context: context,
                title: "Investment Type",
                list: listSavingTypeRB,
                index: savingTypeRBIndex,
                callback: (index) {
                  setState(() => savingTypeRBIndex = index);
                }),
            SizedBox(height: 10),
            InputW2N(
              title: "Platform",
              input: platform,
              ph: "Platform",
              kbType: TextInputType.text,
              len: 50,
              isBold: true,
            ),
            SizedBox(height: 5),
            InputW2N(
              title: "Owners",
              input: owner,
              ph: "Owners",
              kbType: TextInputType.text,
              len: 50,
              isBold: true,
            ),
            SizedBox(height: 5),
            DropDownListDialog(
              context: context,
              title: optProvider.value.title,
              h1: "Select provider",
              heading: "Provider",
              ddTitleList: ddProvider,
              vPadding: 3,
              callback: (optionItem) {
                optProvider.value = optionItem;
              },
            ),
            SizedBox(height: 10),
            DropDownListDialog(
              context: context,
              title: optObj.value.title,
              h1: "Select objective",
              heading: "Objective",
              ddTitleList: ddObj,
              vPadding: 3,
              callback: (optionItem) {
                optObj.value = optionItem;
              },
            ),
            SizedBox(height: 10),
            drawInputCurrencyBox(
                context: context,
                tf: initInv,
                hintTxt: null,
                labelColor: Colors.black,
                isBold: true,
                labelTxt: "Initial Investment",
                len: 10,
                focusNode: null,
                focusNodeNext: null),
            SizedBox(height: 10),
            DatePickerView(
              cap: "Start Date",
              dt: (dtStartInvDate.value == '')
                  ? 'Select Date'
                  : dtStartInvDate.value,
              txtColor: Colors.black,
              fontWeight: FontWeight.bold,
              initialDate: now,
              firstDate: old,
              lastDate: now,
              padding: 5,
              radius: 5,
              callback: (value) {
                if (mounted) {
                  dtStartInvDate.value =
                      DateFormat('dd-MMM-yyyy').format(value).toString();
                }
              },
            ),
            SizedBox(height: 10),
            drawInputCurrencyBox(
                context: context,
                tf: valuation,
                hintTxt: null,
                labelColor: Colors.black,
                isBold: true,
                labelTxt: "Valuation",
                len: 10,
                focusNode: null,
                focusNodeNext: null),
            SizedBox(height: 10),
            DatePickerView(
              cap: "Valuation Date",
              dt: (dtValuationDate.value == '')
                  ? 'Select Date'
                  : dtValuationDate.value,
              txtColor: Colors.black,
              fontWeight: FontWeight.bold,
              initialDate: now,
              firstDate: old,
              lastDate: next,
              padding: 5,
              radius: 5,
              callback: (value) {
                if (mounted) {
                  dtValuationDate.value =
                      DateFormat('dd-MMM-yyyy').format(value).toString();
                }
              },
            ),
            SizedBox(height: 10),
            drawInputCurrencyBox(
                sign: "%",
                context: context,
                tf: intrRate,
                hintTxt: null,
                labelColor: Colors.black,
                labelTxt: "Interest rate/yield (%)",
                isBold: true,
                len: 3,
                focusNode: null,
                focusNodeNext: null,
                onChange: (v) {
                  if (v.isNotEmpty) {
                    try {
                      var rate = int.parse(v);
                      if (rate > 100) {
                        showAlert(
                            context: context,
                            msg: "The rate must be between 0 to 100");
                        intrRate.text = "100";
                      }
                    } catch (e) {}
                  }
                }),
            SizedBox(height: 10),
            DatePickerView(
              cap: "Maturity Date",
              dt: (dtMatDate.value == '') ? 'Select Date' : dtMatDate.value,
              txtColor: Colors.black,
              fontWeight: FontWeight.bold,
              initialDate: now,
              firstDate: old,
              lastDate: next,
              padding: 5,
              radius: 5,
              callback: (value) {
                if (mounted) {
                  dtMatDate.value =
                      DateFormat('dd-MMM-yyyy').format(value).toString();
                }
              },
            ),
            SizedBox(height: 10),
            InputW2N(
              title: "Account Number",
              input: accNo,
              ph: "Account Number",
              kbType: TextInputType.number,
              len: 20,
              isBold: true,
            ),
            SizedBox(height: 5),
            UIHelper().drawRadioTitle(
                context: context,
                title: "Repay before mortgage completes?",
                list: listRegInvRB,
                index: regInvRBIndex,
                callback: (index) {
                  setState(() => regInvRBIndex = index);
                }),
            regInvRBIndex == 0
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      drawInputCurrencyBox(
                          context: context,
                          tf: regInvAmount,
                          hintTxt: null,
                          labelColor: Colors.black,
                          labelTxt: "Amount",
                          isBold: true,
                          len: 3,
                          focusNode: null,
                          focusNodeNext: null),
                      SizedBox(height: 10),
                      DropDownListDialog(
                        context: context,
                        title: optFreqInv.value.title,
                        h1: "Select frequency",
                        heading: "Frequency",
                        ddTitleList: ddFreq,
                        vPadding: 3,
                        callback: (optionItem) {
                          optFreqInv.value = optionItem;
                        },
                      ),
                      SizedBox(height: 10),
                    ],
                  )
                : SizedBox(),
            SizedBox(height: 5),
            UIHelper().drawRadioTitle(
                context: context,
                title: "Is regular income currently being drawn?",
                list: listRegIncomeRB,
                index: regIncomeRBIndex,
                callback: (index) {
                  setState(() => regIncomeRBIndex = index);
                }),
            regIncomeRBIndex == 0
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(height: 10),
                      drawInputCurrencyBox(
                          context: context,
                          tf: regIncomeAmount,
                          hintTxt: null,
                          labelColor: Colors.black,
                          labelTxt: "Amount",
                          isBold: true,
                          len: 3,
                          focusNode: null,
                          focusNodeNext: null),
                      SizedBox(height: 10),
                      DropDownListDialog(
                        context: context,
                        title: optFreqIncome.value.title,
                        h1: "Select frequency",
                        heading: "Frequency",
                        ddTitleList: ddFreq,
                        vPadding: 3,
                        callback: (optionItem) {
                          optFreqIncome.value = optionItem;
                        },
                      ),
                      SizedBox(height: 10),
                    ],
                  )
                : SizedBox(),
            SizedBox(height: 10),
            UIHelper().drawRadioTitle(
                context: context,
                title: "Will this form part of the estate on death?",
                list: listEstateDeathRB,
                index: estateDeathRBIndex,
                callback: (index) {
                  setState(() => estateDeathRBIndex = index);
                }),
            SizedBox(height: 10),
            UIHelper().drawRadioTitle(
                context: context,
                title: "Will this form part of the Spouse’s estate on death?",
                list: listSpouseDeathRB,
                index: spouseDeathRBIndex,
                callback: (index) {
                  setState(() => spouseDeathRBIndex = index);
                }),
            SizedBox(height: 10),
            drawTextArea(title: "Further Details", tf: furtherDetails),
          ],
        ));
  }

  drawOtherAssets() {
    final DateTime now = DateTime.now();
    final old = DateTime(now.year - 50, now.month, now.day);
    final next = DateTime(now.year + 10, now.month, now.day);
    return Obx(() => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 15),
            UIHelper().drawRadioTitle(
                context: context,
                title: "Other Asset Type",
                list: listSavingTypeRB,
                index: savingTypeRBIndex,
                callback: (index) {
                  setState(() => savingTypeRBIndex = index);
                }),
            SizedBox(height: 10),
            drawTextArea(title: "Asset description", tf: assetDesc),
            SizedBox(height: 10),
            InputW2N(
              title: "Asset Owners",
              input: owner,
              ph: "Asset Owners",
              kbType: TextInputType.text,
              len: 50,
              isBold: true,
            ),
            SizedBox(height: 5),
            drawInputCurrencyBox(
                context: context,
                tf: purchasePrice,
                hintTxt: null,
                labelColor: Colors.black,
                isBold: true,
                labelTxt: "Purchase Price",
                len: 10,
                focusNode: null,
                focusNodeNext: null,
                onChange: (v) {}),
            SizedBox(height: 10),
            DatePickerView(
              cap: "Purchase Date",
              dt: (dtPurchaseDate.value == '')
                  ? 'Select Date'
                  : dtPurchaseDate.value,
              txtColor: Colors.black,
              fontWeight: FontWeight.bold,
              initialDate: now,
              firstDate: old,
              lastDate: now,
              padding: 5,
              radius: 5,
              callback: (value) {
                if (mounted) {
                  dtPurchaseDate.value =
                      DateFormat('dd-MMM-yyyy').format(value).toString();
                }
              },
            ),
            SizedBox(height: 10),
            drawInputCurrencyBox(
                context: context,
                tf: valuation,
                hintTxt: null,
                labelColor: Colors.black,
                isBold: true,
                labelTxt: "Valuation",
                len: 10,
                focusNode: null,
                focusNodeNext: null),
            SizedBox(height: 10),
            DatePickerView(
              cap: "Valuation Date",
              dt: (dtValuationDate.value == '')
                  ? 'Select Date'
                  : dtValuationDate.value,
              txtColor: Colors.black,
              fontWeight: FontWeight.bold,
              initialDate: now,
              firstDate: old,
              lastDate: next,
              padding: 5,
              radius: 5,
              callback: (value) {
                if (mounted) {
                  dtValuationDate.value =
                      DateFormat('dd-MMM-yyyy').format(value).toString();
                }
              },
            ),
          ],
        ));
  }

  drawEmergencyFund() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 15),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Does the client have an emergency fund?",
            list: listEmergencyFundRB,
            index: emergencyFundRBIndex,
            callback: (index) {
              setState(() => emergencyFundRBIndex = index);
            }),
        emergencyFundRBIndex == 0
            ? Column(
                children: [
                  SizedBox(height: 10),
                  drawTextArea(title: "Details", tf: emergencyFundDetails),
                ],
              )
            : SizedBox(),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title:
                "Were any investments surrendered or cashed in during the last 12 months?",
            list: listCashed12MRB,
            index: cashed12MRBIndex,
            callback: (index) {
              setState(() => cashed12MRBIndex = index);
            }),
        cashed12MRBIndex == 0
            ? Column(
                children: [
                  SizedBox(height: 10),
                  drawTextArea(title: "Details", tf: cash12Mdetails),
                ],
              )
            : SizedBox(),
      ],
    );
  }
}
