import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/GetContactAutoSugAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserBankDetailList.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserOtherContactInfos.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/PostES1APIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/PostES2APIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/PostESBnkAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case/dialogs/requirements/2essentials/es_add_bnk_dialog.dart';
import 'package:aitl/view/db_cus/web2native/case/views/requirements/3profile/profile_details_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../../../../config/MyTheme.dart';
import '../../../../../../../config/SrvW2NCase.dart';
import '../../../../../../../controller/network/NetworkMgr.dart';
import '../../../../../../../model/data/UserData.dart';
import '../../../../../../../view_model/api/api_view_model.dart';
import '../../../../../../../view_model/helper/ui_helper.dart';
import '../../../../../../widgets/btn/MMBtn.dart';
import 'es_mort_base.dart';
import 'prof_contact_page.dart';

class ESMortPage extends StatefulWidget {
  @override
  State<ESMortPage> createState() => _ESMortPageState();
}

class _ESMortPageState extends ESMortBase<ESMortPage> {
  //  *********************************** ES DATA

  onPostPutESAPI() async {
    try {
      var id1 = 0;
      var id2 = 0;
      try {
        id1 = otherESCtrl.getES1Model.value.id ?? 0;
        id2 = otherESCtrl.getES2Model.value.id ?? 0;
      } catch (e) {}
      final now = DateTime.now().toString();
      //  ES: Others

      var _r2bDisAmount = 0;
      try {
        _r2bDisAmount = int.parse(r2bDisAmount.text);
      } catch (e) {}

      await APIViewModel().req<PostES1APIModel>(
          context: context,
          url: id1 == 0 ? SrvW2NCase.POST_ES1_URL : SrvW2NCase.PUT_ES1_URL,
          reqType: id1 == 0 ? ReqType.Post : ReqType.Put,
          param: {
            "Id": id1,
            "UserId": userData.userModel.id,
            "CompanyId": userData.userModel.userCompanyInfo.id,
            "CreationDate": now,
            "TaskId": caseModelCtrl.locModel.value.id,
            "IsItSharedOwnership": W2NLocalData.listYesNoRB[sharedOwnerRBIndex],
            "DetailsOfSharedOwnershipBodyYouAreBuyingFrom":
                sharedOwner.text.trim(),
            "IsItRightToBuy": W2NLocalData.listYesNoRB[r2BRBIndex],
            "WhatIsTheDiscountAmountReceived": _r2bDisAmount,
            "DetailsOfTheCouncilAssociation": r2bDetailsCouncil.text.trim(),
            "AttitudeToMortgageRepayment": "",
            "YouPreferToAccumulateSavingsToRepayYourMortgageWhenItIsDue": "",
            "YouPreferToHaveTheCertaintyThatYourMortgageLoanIsRepaidAtTheEndOfTheTerm":
                "",
            "YouAreNotConcernedWithRepayingTheMortgageAsYouIntendToSellThePropertyBeforeTheEnd":
                "",
            "HasAnInterestOnlyLoanInFullOrPartBeenRequested":
                W2NLocalData.listYesNoRB[interestOnlyRBIndex],
            "IfYesWhatAreTheReasons": amountOfInterest.text.trim(),
            "MethodOfCapitalRepaymentInvestmentVehiclesInheritance":
                exitStrategry.text.trim(),
            "HaveAnyInvestmentVehiclesBeenReviewedForSuitability": "",
            "PaymentsIntoRetirement": "",
            "DoPaymentsGoIntoRetirement":
                W2NLocalData.listYesNoRB[retirementRBIndex],
            "IfYesWhyIsThis": "",
            "WhatWillBeTheSourceOfIncomeAtThatTime": retirementDesc.text.trim(),
            "DoesFutureIncomeSupportRepaymentsAndExpectedFutureExpenditure": "",
            "DebtConsolidation": "",
            "WhyIsExistingUnsecuredLoanBeingAddedToTheMortgage": "",
            "IsTheClientWillingToPotentiallyPayAGreaterAmountOverTheTermOfTheMortgage":
                W2NLocalData.listYesNoRB[greaterAmountRBIndex],
            "HasTheClientHadAnyDifficultiesWithPastRepaymentsOfTheLoan":
                W2NLocalData.listYesNoRB[difficultiesRBIndex],
            "IfYesWhenAndHaveTheyNegotiatedAnySpecialArrangementsWithTheCreditorOrShouldTheyConsiderThis":
                difficultiesDesc.text.trim(),
            "Notes": desc.text.trim(),
            "Remarks": "",
            "IsAcceptTermAndConditions": false,
            "DoYouWantToProvideBankDetailsForDirectDebitSetup":
                W2NLocalData.listYesNoRB[bnkDirectDebitRBIndex]
          },
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                otherESCtrl.getES1Model.value =
                    model.responseData.mortgageUserOtherInfo;
                otherESCtrl.update();
              }
            }
          });
      //  ES: Key Info
      var _payoffAmount = 0;
      try {
        _payoffAmount = int.parse(payoffAmount.text.trim());
      } catch (e) {}
      await APIViewModel().req<PostES2APIModel>(
          context: context,
          url: id2 == 0 ? SrvW2NCase.POST_ES2_URL : SrvW2NCase.PUT_ES2_URL,
          reqType: id2 == 0 ? ReqType.Post : ReqType.Put,
          param: {
            "Id": id2,
            "UserId": userData.userModel.id,
            "CompanyId": userData.userModel.userCompanyInfo.id,
            "CreationDate": now,
            "TaskId": caseModelCtrl.locModel.value.id,
            "MightYourIncomeOrExpenditureChangeSignificantlyWithinTheForeseeableFuture":
                "",
            "IsIncome": W2NLocalData.listYesNoRB[incomeChangeRBIndex],
            "ApproximateTimescaleAmountReasonIncome": incomeChange.text.trim(),
            "IsExpenditure": W2NLocalData.listYesNoRB[expenditureChangeRBIndex],
            "ApproximateTimescaleAmountReasonExpenditure":
                expenditureChange.text.trim(),
            "DoYouHaveAnyPlansToPayOffSomeOrAllOfTheMortgageInTheForeseeableFuture":
                W2NLocalData.listYesNoRB[payOffRBIndex],
            "ApproximateAmount": _payoffAmount,
            "ApproximatetimescaleAmountReasonForeseeableFuture":
                payoffReason.text.trim(),
            "AreYouLikelyToMoveHomeWithinTheForeseeableFutureOtherThanThisTransaction":
                W2NLocalData.listYesNoRB[moveHomeRBIndex],
            "IsLargerThanApproximateAmount":
                listMoveHomeSizeRB[moveHomeRBIndex],
            "IsSmallerThanApproximateAmount": "",
            "ApproximatetimescaleAmountReasonTheForeseeableFutureOtherThanThisTransaction":
                moveHomeReason.text.trim(),
            "MortgageRequirements": "",
            "AnUpperLimitOnYourMortgageCostsForASpecificPeriod":
                W2NLocalData.listYesNoRB[mortgageReqUpperLimitRBIndex],
            "ReasonAndForHowLongAnUpperLimit":
                mortgageReqUpperLimitReason.text.trim(),
            "ToFixYourMortgageCostsForASpecificPeriod":
                W2NLocalData.listYesNoRB[mortgageReqFixRBIndex],
            "ReasonAndForHowLongToFixYourMortgageCosts":
                mortgageReqFixReason.text.trim(),
            "ARateLinkedToTheBankOfEnglandBaseRate":
                W2NLocalData.listYesNoRB[mortgageReqBaseRateRBIndex],
            "ReasonAndForHowLongARateLinkedToTheBank":
                mortgageReqBaseRateReason.text.trim(),
            "ADiscountOnYourMortgageRepaymentsInTheEarlyYears":
                W2NLocalData.listYesNoRB[mortgageReqDisRBIndex],
            "ReasonAndForHowLongADiscountOnYourMortgageRepayments":
                mortgageReqDisReason.text.trim(),
            "AccessToAnInitialCashSum":
                W2NLocalData.listYesNoRB[mortgageReqCashbackRBIndex],
            "AccessToAnInitialCashSumReasonAndForHowLong":
                mortgageReqCashbackReason.text.trim(),
            "WhichOfTheFollowingAreImportantToYou": "",
            "NoEarlyRepaymentChargeOnYourMortgageAtAnyPoint":
                W2NLocalData.listYesNoRB[earlyRepaymentRBIndex],
            "NoEarlyRepaymentChargeOverhangAfterSelectedRateEnds":
                W2NLocalData.listYesNoRB[earlyRepaymentOverhangRBIndex],
            "NoHighLendingCharge": W2NLocalData.listYesNoRB[highLandingRBIndex],
            "SpeedOfMortgageCompletion":
                W2NLocalData.listYesNoRB[speedMortRBIndex],
            "AbilityToAddFeesToTheLoan":
                W2NLocalData.listYesNoRB[addFeeLoanRBIndex],
            "AbilityToTakeRepaymentHolidays":
                W2NLocalData.listYesNoRB[repaymentHolidayRBIndex],
            "AbilityToMakeUnderpaymentsOrOverpayments":
                W2NLocalData.listYesNoRB[overPaymentRBIndex],
            "FreeLegalFees": W2NLocalData.listYesNoRB[freeLegalFeeRBIndex],
            "NovaluationFee": W2NLocalData.listYesNoRB[noValFeeRBIndex],
            "HaveValuationsFeesRefunded":
                W2NLocalData.listYesNoRB[valFeeRefundedRBIndex],
            "IsAcceptTermsAndCondition": false,
            "WHATMORTGAGEFEATURESDOYOUREQUIRE":
                W2NLocalData.listYesNoRB[reqMortFeaturesRBIndex],
            "WouldYouLikeAnyOtherMortgageFeaturesSuchAsOverpaymentsOrPortability":
                portability.text.trim(),
            "Notes": notes.text.trim(),
            "Remarks": "",
            "MortgageFeatureDetails": mortgageFeatureDetails.text.trim()
          },
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                otherESCtrl.getES2Model.value =
                    model.responseData.mortgageUserKeyInformation;
                otherESCtrl.update();
              }
            }
          });
      if (mounted) Get.off(() => ProfileDetailsPage());
    } catch (e) {}
  }

  //  *********************************** CONTACT DETAILS

  delContactAPI(MortgageUserOtherContactInfos modelDel) async {
    try {
      await APIViewModel().req<GetContactAutoSugAPIModel>(
          context: context,
          url: SrvW2NCase.DEL_CONTACT_AUTO_SUG_URL + modelDel.id.toString(),
          reqType: ReqType.Delete,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                otherESCtrl.listContactModel.remove(modelDel);
                otherESCtrl.update();
                setState(() {});
              }
            }
          });
    } catch (e) {}
  }

  editContactAPI(MortgageUserOtherContactInfos modelEdit) {
    Get.to(() => ProfContactPage(contactModel: modelEdit))
        .then((mortgageUserOtherContactInfo) {
      if (mortgageUserOtherContactInfo != null) {
        otherESCtrl.listContactModel.remove(modelEdit);
        otherESCtrl.listContactModel.add(mortgageUserOtherContactInfo);
        otherESCtrl.update();
        setState(() {});
      }
    });
  }

  @override
  addContactAPI() {
    Get.to(() => ProfContactPage()).then((mortgageUserOtherContactInfo) {
      if (mortgageUserOtherContactInfo != null) {
        otherESCtrl.listContactModel.add(mortgageUserOtherContactInfo);
        otherESCtrl.update();
        setState(() {});
      }
    });
  }

  //  *********************************** BANK DETAILS

  delBnkAPI(MortgageUserBankDetailList modelDel) async {
    try {
      await APIViewModel().req<PostESBnkAPIModel>(
          context: context,
          url: SrvW2NCase.DEL_ESBANK_URL + modelDel.id.toString(),
          reqType: ReqType.Delete,
          callback: (model) async {
            if (mounted && model != null) {
              if (model.success) {
                otherESCtrl.listBnkModel.remove(modelDel);
                otherESCtrl.update();
                setState(() {});
              }
            }
          });
    } catch (e) {}
  }

  openBnkDialog(MortgageUserBankDetailList modelEdit) {
    Get.dialog(reqStep2ESBankDialog(
        context: context,
        taskId: caseModelCtrl.locModel.value.id,
        model2: modelEdit,
        callbackSuccess: (model) async {
          if (mounted) {
            await APIViewModel().req<PostESBnkAPIModel>(
                context: context,
                url: modelEdit == null
                    ? SrvW2NCase.POST_ESBANK_URL
                    : SrvW2NCase.PUT_ESBANK_URL,
                param: model.toJson(),
                reqType: modelEdit == null ? ReqType.Post : ReqType.Put,
                callback: (model) async {
                  if (mounted && model != null) {
                    if (model.success) {
                      if (modelEdit != null) {
                        otherESCtrl.listBnkModel.remove(modelEdit);
                      }
                      otherESCtrl.listBnkModel
                          .add(model.responseData.mortgageUserBankDetail);
                      otherESCtrl.update();
                      setState(() {});
                    }
                  }
                });
          }
        },
        callbackFailed: (err) {
          showToast(context: context, msg: err);
        }));
  }

  //
  updateES1() {
    try {
      final model1 = otherESCtrl.getES1Model.value;
      try {
        bnkDirectDebitRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
            model1.doYouWantToProvideBankDetailsForDirectDebitSetup);
      } catch (e) {}

      try {
        sharedOwnerRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model1.isItSharedOwnership);
      } catch (e) {}
      try {
        sharedOwner.text =
            model1.detailsOfSharedOwnershipBodyYouAreBuyingFrom ?? '';
      } catch (e) {}

      try {
        r2BRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model1.isItRightToBuy);
      } catch (e) {}
      try {
        r2bDisAmount.text =
            model1.whatIsTheDiscountAmountReceived.toStringAsFixed(0);
      } catch (e) {}
      try {
        r2bDetailsCouncil.text = model1.detailsOfTheCouncilAssociation ?? '';
      } catch (e) {}

      try {
        interestOnlyRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
            model1.hasAnInterestOnlyLoanInFullOrPartBeenRequested);
      } catch (e) {}
      try {
        amountOfInterest.text = model1.ifYesWhatAreTheReasons ?? '';
      } catch (e) {}
      try {
        exitStrategry.text =
            model1.methodOfCapitalRepaymentInvestmentVehiclesInheritance ?? '';
      } catch (e) {}
      try {
        retirementRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model1.doPaymentsGoIntoRetirement);
      } catch (e) {}
      try {
        retirementDesc.text =
            model1.whatWillBeTheSourceOfIncomeAtThatTime ?? '';
      } catch (e) {}
      try {
        greaterAmountRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB,
            model1
                .isTheClientWillingToPotentiallyPayAGreaterAmountOverTheTermOfTheMortgage);
      } catch (e) {}
      try {
        difficultiesRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
            model1.hasTheClientHadAnyDifficultiesWithPastRepaymentsOfTheLoan);
      } catch (e) {}
      try {
        difficultiesDesc.text = model1
                .ifYesWhenAndHaveTheyNegotiatedAnySpecialArrangementsWithTheCreditorOrShouldTheyConsiderThis ??
            '';
      } catch (e) {}
      try {
        desc.text = model1.notes ?? '';
      } catch (e) {}
    } catch (e) {}
  }

  updateES2() {
    try {
      final model2 = otherESCtrl.getES2Model.value;
      try {
        incomeChangeRBIndex =
            Common.getMapKeyByVal(W2NLocalData.listYesNoRB, model2.isIncome);
      } catch (e) {}
      try {
        incomeChange.text = model2.approximateTimescaleAmountReasonIncome ?? '';
      } catch (e) {}
      try {
        expenditureChangeRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.isExpenditure);
      } catch (e) {}
      try {
        expenditureChange.text =
            model2.approximateTimescaleAmountReasonExpenditure ?? '';
      } catch (e) {}
      try {
        payOffRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB,
            model2
                .doYouHaveAnyPlansToPayOffSomeOrAllOfTheMortgageInTheForeseeableFuture);
      } catch (e) {}
      try {
        payoffAmount.text = model2.approximateAmount.toStringAsFixed(0) ?? '';
      } catch (e) {}
      try {
        payoffReason.text =
            model2.approximatetimescaleAmountReasonForeseeableFuture ?? '';
      } catch (e) {}
      try {
        moveHomeRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB,
            model2
                .areYouLikelyToMoveHomeWithinTheForeseeableFutureOtherThanThisTransaction);
      } catch (e) {}
      try {
        moveHomeSizeRBIndex = Common.getMapKeyByVal(
            listMoveHomeSizeRB, model2.isLargerThanApproximateAmount);
      } catch (e) {}
      try {
        moveHomeReason.text = model2
                .approximatetimescaleAmountReasonTheForeseeableFutureOtherThanThisTransaction ??
            '';
      } catch (e) {}
      try {
        mortgageReqUpperLimitRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB,
            model2.anUpperLimitOnYourMortgageCostsForASpecificPeriod);
      } catch (e) {}
      try {
        mortgageReqUpperLimitReason.text =
            model2.reasonAndForHowLongAnUpperLimit ?? '';
      } catch (e) {}
      try {
        mortgageReqFixRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
            model2.toFixYourMortgageCostsForASpecificPeriod);
      } catch (e) {}
      try {
        mortgageReqFixReason.text =
            model2.reasonAndForHowLongToFixYourMortgageCosts ?? '';
      } catch (e) {}
      try {
        mortgageReqBaseRateRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB,
            model2.aRateLinkedToTheBankOfEnglandBaseRate);
      } catch (e) {}
      try {
        mortgageReqBaseRateReason.text =
            model2.reasonAndForHowLongARateLinkedToTheBank ?? '';
      } catch (e) {}
      try {
        mortgageReqDisRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
            model2.aDiscountOnYourMortgageRepaymentsInTheEarlyYears);
      } catch (e) {}
      try {
        mortgageReqDisReason.text =
            model2.reasonAndForHowLongADiscountOnYourMortgageRepayments ?? '';
      } catch (e) {}
      try {
        mortgageReqCashbackRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.accessToAnInitialCashSum);
      } catch (e) {}
      try {
        mortgageReqCashbackReason.text =
            model2.accessToAnInitialCashSumReasonAndForHowLong ?? '';
      } catch (e) {}

      try {
        earlyRepaymentRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
            model2.noEarlyRepaymentChargeOnYourMortgageAtAnyPoint);
      } catch (e) {}
      try {
        earlyRepaymentOverhangRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB,
            model2.noEarlyRepaymentChargeOverhangAfterSelectedRateEnds);
      } catch (e) {}
      try {
        highLandingRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.noHighLendingCharge);
      } catch (e) {}
      try {
        speedMortRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.speedOfMortgageCompletion);
      } catch (e) {}
      try {
        addFeeLoanRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.abilityToAddFeesToTheLoan);
      } catch (e) {}
      try {
        repaymentHolidayRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.abilityToTakeRepaymentHolidays);
      } catch (e) {}
      try {
        overPaymentRBIndex = Common.getMapKeyByVal(W2NLocalData.listYesNoRB,
            model2.abilityToMakeUnderpaymentsOrOverpayments);
      } catch (e) {}
      try {
        freeLegalFeeRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.freeLegalFees);
      } catch (e) {}
      try {
        noValFeeRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.novaluationFee);
      } catch (e) {}
      try {
        valFeeRefundedRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.haveValuationsFeesRefunded);
      } catch (e) {}
      try {
        reqMortFeaturesRBIndex = Common.getMapKeyByVal(
            W2NLocalData.listYesNoRB, model2.wHATMORTGAGEFEATURESDOYOUREQUIRE);
      } catch (e) {}
      try {
        mortgageFeatureDetails.text = model2.mortgageFeatureDetails ?? '';
      } catch (e) {}
      try {
        portability.text = model2
                .wouldYouLikeAnyOtherMortgageFeaturesSuchAsOverpaymentsOrPortability ??
            '';
      } catch (e) {}
      try {
        notes.text = model2.notes ?? '';
      } catch (e) {}
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    initPage();
  }

  @override
  void dispose() {
    amountOfInterest.dispose();
    exitStrategry.dispose();
    retirementDesc.dispose();
    difficultiesDesc.dispose();
    desc.dispose();
    incomeChange.dispose();
    expenditureChange.dispose();
    payoffAmount.dispose();
    payoffReason.dispose();
    moveHomeReason.dispose();
    mortgageReqUpperLimitReason.dispose();
    mortgageReqFixReason.dispose();
    mortgageReqBaseRateReason.dispose();
    mortgageReqDisReason.dispose();
    mortgageReqCashbackReason.dispose();
    mortgageFeatureDetails.dispose();
    portability.dispose();
    notes.dispose();
    focusPortability.dispose();
    super.dispose();
  }

  initPage() async {
    updateES1();
    updateES2();
  }

  @override
  Widget build(BuildContext context) {
    //print(widget.mortgageUserRequirementModel);
    //updateFields();
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: SafeArea(
        child: WillPopScope(
          onWillPop: () async {
            final currentFocus = FocusScope.of(context);
            if (!currentFocus.hasPrimaryFocus &&
                currentFocus.focusedChild != null) {
              FocusManager.instance.primaryFocus?.unfocus();
            }
            return true;
          },
          child: Scaffold(
            //resizeToAvoidBottomInset: false,
            backgroundColor: MyTheme.bgColor2,
            appBar: AppBar(
              centerTitle: false,
              iconTheme: IconThemeData(color: Colors.white),
              backgroundColor: MyTheme.statusBarColor,
              elevation: MyTheme.appbarElevation,
              title: UIHelper()
                  .drawAppbarTitle(title: "Other Essential Information"),
            ),
            body: GestureDetector(
                behavior: HitTestBehavior.opaque,
                onPanDown: (detail) {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                onTap: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                },
                child: drawLayout()),
          ),
        ),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              drawProgressView(25),
              SizedBox(height: 20),
              drawForm(),
              SizedBox(height: 30),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: MMBtn(
                        txt: "Back",
                        height: getHP(context, 7),
                        width: getW(context),
                        bgColor: Colors.grey,
                        radius: 10,
                        callback: () async {
                          Get.back();
                        }),
                  ),
                  SizedBox(width: 10),
                  Flexible(
                    child: MMBtn(
                        txt: "Save & continue",
                        height: getHP(context, 7),
                        width: getW(context),
                        radius: 10,
                        callback: () {
                          onPostPutESAPI();
                        }),
                  ),
                ],
              ),
              SizedBox(height: 40),
            ],
          ),
        ),
      ),
    );
  }
}
