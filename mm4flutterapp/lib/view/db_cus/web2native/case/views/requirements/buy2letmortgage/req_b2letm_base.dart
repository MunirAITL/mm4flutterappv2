import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/view/db_cus/web2native/case/mixin/requirements/req_mortgage_mixin.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';

import '../../../../../../../config/MyTheme.dart';
import '../../../../../../../model/json/web2native/case/requirements/mortgage/mortgage_model.dart';
import '../../../../../../../view_model/rx/web2native/requirements/req_mortgage_ctrl.dart';
import '../../../../../../widgets/gplaces/GPlacesView.dart';
import '../../../../../../widgets/input/InputTitleBox.dart';
import '../../../../../../widgets/radio/draw_radio_group.dart';
import '../../../../../../widgets/txt/Txt.dart';
import '../../../../case_base.dart';
import '../../../dialogs/requirements/req_mortgage_dialog.dart';

enum eB2LetMSubList {
  Experienced_Landlord,
  First_time_Landlord,
  Consumer_buy_to_Let,
}

abstract class ReqB2LetMBase<T extends StatefulWidget> extends CaseBase<T>
    with ReqMortgageMixin {
  getSubCase();
  onAddAmountDialog();
  onEditAmountDialog(MortgageDepositByModel model);

  final requirementController = Get.put(ReqMortgageCtrl());

  //final listMortgageTypeRB = {0: 'Home Owner', 1: 'First Time Buyer'};
  //int mortgageTypeRBIndex = 0;

  int incentivesRBIndex = 1;

  final listPrefRepayTypeRB = {
    0: 'Capital interest',
    1: 'Interest only',
    2: 'Part & part',
    3: 'No preference'
  };
  int prefRepayTypeRBIndex = 0;

  int propertyTenantedRBIndex = 1;

  final listPropertyTenantedTypeRB = {
    0: 'Standard Tenancy',
    1: 'HMO (House in Multiple Occupation)',
    2: 'Multi Unit Block',
    3: 'Periodic Tenancy',
    4: 'Corporate Let'
  };
  int propertyTenantedTypeRBIndex = 0;

  final listPropertyTypeRB = {0: 'House', 1: 'Flat'};
  int propertyTypeIndex = 0;

  final listPropertyTenureRB = {0: 'Freehold', 1: 'Leasehold', 2: 'Feudal'};
  int propertyTenureIndex = 0;

  int extLoftConvRBIndex = 1;

  int nonStandardConstrRBIndex = 1;

  int exCouncilRBIndex = 1;

  int xAccessRBIndex = 1;

  int newBuiltPropRBIndex = 1;

  int flatHasShopRBIndex = 1;

  int doesWarantyRBIndex = 1;

  int haveLiftRBIndex = 1;

  var optDoesWarranty = OptionItem(id: null, title: "Select warranty").obs;

  final currentValProperty = TextEditingController();
  final purchasePrice = TextEditingController();
  final loanAmount = TextEditingController();
  final depositAmount = TextEditingController();
  final incentiveAmount = TextEditingController();
  final prefMortTermsYY = TextEditingController();
  final prefMortTermsMM = TextEditingController();
  final noBedrooms = TextEditingController();
  final noKitchens = TextEditingController();
  final noBathrooms = TextEditingController();
  final groundRent = TextEditingController();
  final srvCharges = TextEditingController();
  final noWC = TextEditingController();
  final floorBuilding = TextEditingController();
  final whichFloorProperty = TextEditingController();
  final yearBuilt = TextEditingController();
  final notes = TextEditingController();
  final nonStandardConstrDetails = TextEditingController();
  final yearLeftLease = TextEditingController();
  final rentalIncome = TextEditingController();

  final focusCurrentValProperty = FocusNode();
  final focusPurchasePrice = FocusNode();
  final focusLoanAmount = FocusNode();
  final focusDepositAmount = FocusNode();
  final focusIncentivesAmount = FocusNode();
  final focusPrefMortTermsYY = FocusNode();
  final focusPrefMortTermsMM = FocusNode();
  final focusNoBedrooms = FocusNode();
  final focusNoKitchens = FocusNode();
  final focusNoBathrooms = FocusNode();
  final focusGroundRent = FocusNode();
  final focusSrvCharges = FocusNode();
  final focusNoWC = FocusNode();
  final focusFloorBuilding = FocusNode();
  final focusYearBuilt = FocusNode();
  final focusNonStandardConstrDetails = FocusNode();
  final focusYearLeftLease = FocusNode();
  final focusWhichFloorProperty = FocusNode();
  final focusRentalIncome = FocusNode();

  final List<MortgageDepositByModel> listDepositedBy = [];

  String addrOfProperty = "";

  onValidate() {
    return true;
  }

  drawForm() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          GPlacesView(
              title: "Address of property to be mortgaged?",
              titleColor: Colors.black,
              isBold: true,
              txtSize: MyTheme.txtSize - .2,
              address: addrOfProperty,
              callback: (String address, String postCode, Location loc) {
                addrOfProperty = address;
                setState(() {});
              }),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Current Valuation of Property?",
              "If you're not sure, your best guess is fine at this point.",
              currentValProperty,
              focusCurrentValProperty,
              focusPurchasePrice,
              (v) {}),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Purchase price",
              "If you're not sure, your best guess is fine at this point.",
              purchasePrice,
              focusPurchasePrice,
              focusLoanAmount, (v) {
            setState(() {
              try {
                depositAmount.text =
                    calDepositAmount(loanAmt: loanAmount.text, purchasePrice: v)
                        .toString();
              } catch (e) {}
            });
          }),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Loan Amount",
              "If you're not sure, your best guess is fine at this point.",
              loanAmount,
              focusLoanAmount,
              focusDepositAmount, (v) {
            setState(() {
              try {
                depositAmount.text = calDepositAmount(
                        loanAmt: v, purchasePrice: purchasePrice.text)
                    .toString();
              } catch (e) {}
            });
          }),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Deposit Amount",
              "If you're not sure, your best guess is fine at this point.",
              depositAmount,
              focusDepositAmount,
              focusRentalIncome,
              (v) {}),
          SizedBox(height: 10),
          Txt(
              txt: "Where does your deposit come from?",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: true),
          Txt(
              txt: "(e.g. savings, a gift) You can add more than one source",
              txtColor: Colors.grey,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: false),
          SizedBox(height: 5),
          drawDottedBox("Add Amount", () {
            onAddAmountDialog();
          }),
          drawDepositAmountItems(context, listDepositedBy, (modelEdit) {
            onEditAmountDialog(modelEdit);
          }, (modelDel) {
            listDepositedBy.remove(modelDel);
            setState(() {});
          }),
          SizedBox(height: 10),
          drawCurrencyBox(
              "What is the rental income achievable",
              "Please add Monthly",
              rentalIncome,
              focusRentalIncome,
              null,
              (v) {}),
          SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              title: "Is the property already tenanted?",
              list: W2NLocalData.listYesNoRB,
              index: propertyTenantedRBIndex,
              callback: (index) {
                setState(() => propertyTenantedRBIndex = index);
              }),
          propertyTenantedRBIndex == 0
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10),
                    UIHelper().drawRadioTitle(
                        context: context,
                        title: "Please select the tenancy type",
                        list: listPropertyTenantedTypeRB,
                        index: propertyTenantedTypeRBIndex,
                        callback: (index) {
                          setState(() => propertyTenantedTypeRBIndex = index);
                        }),
                  ],
                )
              : SizedBox(),
          _drawIncentivesView(),
          incentivesRBIndex == 0
              ? drawCurrencyBox(
                  "Please state how much?",
                  "If you're not sure, your best guess is fine at this point.",
                  incentiveAmount,
                  focusIncentivesAmount,
                  null,
                  (v) {})
              : SizedBox(),
          _drawPrefMortgageTermsView(),
          _drawPrefRepayTypeView(),
          _drawPropertyTypeView(),
          _drawPropertyTenureView(),
          SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: drawInputBox(
                  context: context,
                  title: "Number of Bedrooms",
                  ph: "0",
                  input: noBedrooms,
                  len: 2,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusNoBedrooms,
                  focusNodeNext: focusNoKitchens,
                ),
              ),
              SizedBox(width: 10),
              Flexible(
                child: drawInputBox(
                  context: context,
                  title: "Number of Kitchens",
                  ph: "0",
                  input: noKitchens,
                  len: 2,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusNoKitchens,
                  focusNodeNext: focusGroundRent,
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: drawInputBox(
                  context: context,
                  title: "Number of Bathrooms",
                  ph: "0",
                  input: noBathrooms,
                  len: 2,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusNoBathrooms,
                  focusNodeNext: focusNoWC,
                ),
              ),
              SizedBox(width: 10),
              Flexible(
                child: drawInputBox(
                  context: context,
                  title: "Number of W/C",
                  ph: "0",
                  input: noWC,
                  len: 2,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusNoWC,
                  focusNodeNext: focusFloorBuilding,
                ),
              ),
            ],
          ),
          propertyTypeIndex == 1
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: drawInputBox(
                          context: context,
                          title: "How much is the ground rent?",
                          ph: "0",
                          input: groundRent,
                          len: 10,
                          txtColor: Colors.black,
                          isBold: true,
                          kbType: TextInputType.number,
                          inputAction: TextInputAction.next,
                          focusNode: focusGroundRent,
                          focusNodeNext: focusSrvCharges,
                        ),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        child: drawInputBox(
                          context: context,
                          title: "How much is the service charge?",
                          ph: "0",
                          input: srvCharges,
                          len: 10,
                          txtColor: Colors.black,
                          isBold: true,
                          kbType: TextInputType.number,
                          inputAction: TextInputAction.next,
                          focusNode: focusSrvCharges,
                          focusNodeNext: focusNoBathrooms,
                        ),
                      ),
                    ],
                  ))
              : SizedBox(),
          propertyTypeIndex == 1
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Column(
                    children: [
                      drawInputBox(
                        context: context,
                        title: "Floors in the building",
                        ph: "Floors in the building",
                        input: floorBuilding,
                        len: 20,
                        txtColor: Colors.black,
                        isBold: true,
                        kbType: TextInputType.text,
                        inputAction: TextInputAction.next,
                        focusNode: focusFloorBuilding,
                        focusNodeNext: focusWhichFloorProperty,
                      ),
                      SizedBox(height: 10),
                      UIHelper().drawRadioTitle(
                          context: context,
                          title: "Is the flat above shop/ restaurant?",
                          list: W2NLocalData.listYesNoRB,
                          index: flatHasShopRBIndex,
                          callback: (index) {
                            setState(() => flatHasShopRBIndex = index);
                          }),
                      SizedBox(height: 10),
                      UIHelper().drawRadioTitle(
                          context: context,
                          title: "Does the property have a lift?",
                          list: W2NLocalData.listYesNoRB,
                          index: haveLiftRBIndex,
                          callback: (index) {
                            setState(() => haveLiftRBIndex = index);
                          }),
                      SizedBox(height: 10),
                      drawInputBox(
                        context: context,
                        title: "Which floor is the property",
                        ph: "Which floor is the property",
                        input: whichFloorProperty,
                        len: 50,
                        txtColor: Colors.black,
                        isBold: true,
                        kbType: TextInputType.text,
                        inputAction: TextInputAction.next,
                        focusNode: focusWhichFloorProperty,
                        focusNodeNext: focusYearBuilt,
                      )
                    ],
                  ),
                )
              : SizedBox(),
          drawInputBox(
            context: context,
            title: "Year property was built",
            ph: "YYYY",
            input: yearBuilt,
            len: 4,
            txtColor: Colors.black,
            isBold: true,
            kbType: TextInputType.number,
            inputAction: TextInputAction.next,
            focusNode: focusYearBuilt,
            focusNodeNext: null,
          ),
          SizedBox(height: 10),
          _drawRadioQ(),
        ],
      ),
    );
  }

  _drawRadioQ() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        UIHelper().drawRadioTitle(
            context: context,
            title: "Any extension or loft conversion done?",
            list: W2NLocalData.listYesNoRB,
            index: extLoftConvRBIndex,
            callback: (index) {
              setState(() => extLoftConvRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title:
                "Is the property of a non-standard construction (ie. thatched roof, barn conversion etc)",
            list: W2NLocalData.listYesNoRB,
            index: nonStandardConstrRBIndex,
            callback: (index) {
              setState(() => nonStandardConstrRBIndex = index);
            }),
        SizedBox(height: 10),
        nonStandardConstrRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: drawInputBox(
                  context: context,
                  title: "Non-standard Construction Details",
                  ph: "Construction Details",
                  input: nonStandardConstrDetails,
                  len: 255,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  focusNode: focusNonStandardConstrDetails,
                  focusNodeNext: null,
                ))
            : SizedBox(),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is it Ex Council? Or purchased from council directly",
            list: W2NLocalData.listYesNoRB,
            index: exCouncilRBIndex,
            callback: (index) {
              setState(() => exCouncilRBIndex = index);
            }),
        propertyTypeIndex == 1
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: UIHelper().drawRadioTitle(
                    context: context,
                    title: "Does the flat have deck or balcony access?",
                    list: W2NLocalData.listYesNoRB,
                    index: xAccessRBIndex,
                    callback: (index) {
                      setState(() => xAccessRBIndex = index);
                    }))
            : SizedBox(),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is this a new built property?",
            list: W2NLocalData.listYesNoRB,
            index: newBuiltPropRBIndex,
            callback: (index) {
              setState(() => newBuiltPropRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Dose this property have warranty?",
            list: W2NLocalData.listYesNoRB,
            index: doesWarantyRBIndex,
            callback: (index) {
              setState(() => doesWarantyRBIndex = index);
            }),
        doesWarantyRBIndex == 0
            ? Obx(() => Padding(
                padding: const EdgeInsets.only(top: 10),
                child: DropDownListDialog(
                  context: context,
                  title: optDoesWarranty.value.title,
                  h1: "Select warranty",
                  heading: "If so what warranty",
                  ddTitleList: W2NLocalData.ddDoesWarranty,
                  vPadding: 3,
                  callback: (optionItem) {
                    optDoesWarranty.value = optionItem;
                  },
                )))
            : SizedBox(),
        SizedBox(height: 10),
        drawTextArea(title: "Notes", tf: notes),
      ],
    );
  }

  _drawPropertyTenureView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Property Tenure",
            list: listPropertyTenureRB,
            index: propertyTenureIndex,
            radioType: eRadioType.HORIZONTAL,
            callback: (index) {
              setState(() => propertyTenureIndex = index);
            }),
        SizedBox(height: 5),
        propertyTenureIndex == 1
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: drawInputBox(
                  context: context,
                  title: "Year left on lease",
                  ph: "Year left on lease",
                  input: yearLeftLease,
                  len: 4,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  focusNode: focusYearLeftLease,
                  focusNodeNext: null,
                ))
            : SizedBox()
      ],
    );
  }

  _drawPropertyTypeView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Security Property Details",
            subTitle: "What type of property is this?",
            list: listPropertyTypeRB,
            index: propertyTypeIndex,
            callback: (index) {
              setState(() => propertyTypeIndex = index);
            }),
        SizedBox(height: 5),
      ],
    );
  }

  _drawPrefRepayTypeView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 20),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Preferred repayment type",
            list: listPrefRepayTypeRB,
            index: prefRepayTypeRBIndex,
            callback: (index) {
              setState(() => prefRepayTypeRBIndex = index);
            }),
      ],
    );
  }

  _drawPrefMortgageTermsView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: incentivesRBIndex == 0 ? 20 : 0),
        Txt(
            txt: "Preferred mortgage term",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: true),
        SizedBox(height: 5),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
                child: drawInputCurrencyBox(
                    sign: "Year",
                    context: context,
                    tf: prefMortTermsYY,
                    hintTxt: null,
                    len: 4,
                    focusNode: focusPrefMortTermsYY,
                    focusNodeNext: focusPrefMortTermsMM)),
            SizedBox(width: 10),
            Flexible(
                child: drawInputCurrencyBox(
                    sign: "Month",
                    context: context,
                    tf: prefMortTermsMM,
                    hintTxt: null,
                    len: 2,
                    focusNode: focusPrefMortTermsMM,
                    focusNodeNext: null)),
          ],
        )
      ],
    );
  }

  _drawIncentivesView() {
    return Container(
      child: Column(
        children: [
          SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              title:
                  "Are you receiving any incentives for buying this property such as discounts, cash back, free services or goods?",
              list: W2NLocalData.listYesNoRB,
              index: incentivesRBIndex,
              callback: (index) {
                setState(() => incentivesRBIndex = index);
              }),
          SizedBox(height: 10),
        ],
      ),
    );
  }
}
