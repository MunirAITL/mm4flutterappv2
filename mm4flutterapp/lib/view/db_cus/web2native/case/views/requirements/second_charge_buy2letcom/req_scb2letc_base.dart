import 'package:aitl/model/data/w2n_data.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/input/drawInputCurrencyBox.dart';
import 'package:aitl/view/widgets/txt/SignText.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geolocation.dart';
import 'package:intl/intl.dart';

import '../../../../../../../config/MyTheme.dart';
import '../../../../../../../model/json/web2native/case/requirements/remortgage/reasons/MortgageLoanReasonsModel.dart';
import '../../../../../../../view_model/rx/web2native/requirements/req_remortgage_ctrl.dart';
import '../../../../../../widgets/dialog/DatePickerView.dart';
import '../../../../../../widgets/gplaces/GPlacesView.dart';
import '../../../../../../widgets/input/InputTitleBox.dart';
import '../../../../../../widgets/radio/draw_radio_group.dart';
import '../../../../../../widgets/txt/Txt.dart';
import '../../../../case_base.dart';
import '../../../dialogs/requirements/req_remortgage_dialog.dart';
import '../../../mixin/requirements/req_scr_mixin.dart';

abstract class ReqSCB2LetCBase<T extends StatefulWidget> extends CaseBase<T>
    with ReqSCRMixin {
  onAddReasonDialog();
  onEditReasonDialog(MortgageLoanReasonsModel model);
  onDelReasonDialog(MortgageLoanReasonsModel model);

  final requirementController = Get.put(ReqReMortgageCtrl());

  int reMortgagePropertyRBIndex = 0;

  int existingMortgagePropertyRBIndex = 1;

  int incentivesRBIndex = 1;

  int capitalRaisingRBIndex = 1;

  int loanInterestOnlyRBIndex = 1;

  final listPrefRepayTypeRB = {
    0: 'Capital interest',
    1: 'Interest only',
    2: 'Part & part',
    3: 'No preference'
  };
  int prefRepayTypeRBIndex = 0;

  final listPropertyTypeRB = {0: 'House', 1: 'Flat'};
  int propertyTypeIndex = 0;

  final listPropertyTenureRB = {0: 'Freehold', 1: 'Leasehold', 2: 'Feudal'};
  int propertyTenureIndex = 0;

  int extLoftConvRBIndex = 1;

  int nonStandardConstrRBIndex = 1;

  int exCouncilRBIndex = 1;

  int xAccessRBIndex = 1;

  int purchaseDirecltlyRBIndex = 1;

  int restrictionRBIndex = 1;

  int housingAssociationRBIndex = 1;

  int newBuiltPropRBIndex = 1;

  int flatHasShopRBIndex = 1;

  int doesWarantyRBIndex = 1;

  int haveLiftRBIndex = 1;

  var optDoesWarranty = OptionItem(id: null, title: "Select warranty").obs;

  final currentValProperty = TextEditingController();
  final currentMortOutstanding = TextEditingController();
  final mortgageRedeemed = TextEditingController();
  final monthlyMortgagePayment = TextEditingController();
  final currentLender = TextEditingController();
  final currentLenderAccNo = TextEditingController();
  final purchasePrice = TextEditingController();
  final loanAmount = TextEditingController();
  final depositAmount = TextEditingController();
  final incentiveAmount = TextEditingController();
  final prefMortTermsYY = TextEditingController();
  final prefMortTermsMM = TextEditingController();
  final noBedrooms = TextEditingController();
  final noKitchens = TextEditingController();
  final noBathrooms = TextEditingController();
  final groundRent = TextEditingController();
  final srvCharges = TextEditingController();
  final noWC = TextEditingController();
  final floorBuilding = TextEditingController();
  final whichFloorProperty = TextEditingController();
  final yearBuilt = TextEditingController();
  final notes = TextEditingController();
  final nonStandardConstrDetails = TextEditingController();
  final yearLeftLease = TextEditingController();
  final capitalRaising = TextEditingController();
  final loanRepayStrategy = TextEditingController();

  final focusCurrentValProperty = FocusNode();
  final focusCurrentMortOutstanding = FocusNode();
  final focusLtvAmount = FocusNode();
  final focusMortgageRedeemed = FocusNode();
  final focusMonthlyMortgagePayment = FocusNode();
  final focusCurrentLender = FocusNode();
  final focusCurrentLenderAccNo = FocusNode();
  final focusPurchasePrice = FocusNode();
  final focusLoanAmount = FocusNode();
  final focusDepositAmount = FocusNode();
  final focusIncentivesAmount = FocusNode();
  final focusPrefMortTermsYY = FocusNode();
  final focusPrefMortTermsMM = FocusNode();
  final focusNoBedrooms = FocusNode();
  final focusNoKitchens = FocusNode();
  final focusNoBathrooms = FocusNode();
  final focusGroundRent = FocusNode();
  final focusSrvCharges = FocusNode();
  final focusNoWC = FocusNode();
  final focusFloorBuilding = FocusNode();
  final focusYearBuilt = FocusNode();
  final focusNonStandardConstrDetails = FocusNode();
  final focusYearLeftLease = FocusNode();
  final focusWhichFloorProperty = FocusNode();
  final focusCapitalRaising = FocusNode();
  final focusLoanRepayStrategy = FocusNode();

  String datePropertyPurchase = "";
  String restrictionDate = "";
  String addrOfProperty = "";

  var ltv = "0";

  onValidate() {
    return true;
  }

  drawForm() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          UIHelper().drawRadioTitle(
              context: context,
              title:
                  "Are you looking to take a second charge loan on your investment property?",
              list: W2NLocalData.listYesNoRB,
              index: reMortgagePropertyRBIndex,
              callback: (index) {
                setState(() => reMortgagePropertyRBIndex = index);
              }),
          SizedBox(height: 10),
          GPlacesView(
              title: "Address of security property?",
              titleColor: Colors.black,
              isBold: true,
              txtSize: MyTheme.txtSize - .2,
              address: addrOfProperty,
              callback: (String address, String postCode, Location loc) {
                addrOfProperty = address;
                setState(() {});
              }),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Current Valuation of Property?",
              "If you're not sure, your best guess is fine at this point.",
              currentValProperty,
              focusCurrentValProperty,
              focusCurrentMortOutstanding, (v) {
            setState(() {
              try {
                ltv = calLTVAmount(
                        curValProperty: v,
                        balOutstanding: currentMortOutstanding.text)
                    .toString();
              } catch (e) {}
            });
          }),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Current mortgage outstanding",
              "If you're not sure, your best guess is fine at this point.",
              currentMortOutstanding,
              focusCurrentMortOutstanding,
              focusLtvAmount, (v) {
            setState(() {
              try {
                ltv = calLTVAmount(
                        curValProperty: currentValProperty.text,
                        balOutstanding: v)
                    .toString();
              } catch (e) {}
            });
          }),
          SizedBox(height: 10),
          drawSignText(
              context: context,
              sign: "%",
              title: "Your current LTV(%)",
              txtColor: Colors.black,
              isBold: true,
              txt: ltv,
              padding: 7),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Monthly Mortgage Payment",
              "If you're not sure, your best guess is fine at this point.",
              monthlyMortgagePayment,
              focusMonthlyMortgagePayment,
              focusCurrentLender,
              (v) {}),
          SizedBox(height: 10),
          drawInputBox(
            context: context,
            title: "Current Mortgage Lender",
            ph: "Current Lender",
            input: currentLender,
            len: 20,
            txtColor: Colors.black,
            isBold: true,
            kbType: TextInputType.text,
            inputAction: TextInputAction.next,
            focusNode: focusCurrentLender,
            focusNodeNext: focusCurrentLenderAccNo,
          ),
          SizedBox(height: 10),
          drawInputBox(
            context: context,
            title: "Mortgage Account Number",
            ph: "Account Number",
            input: currentLenderAccNo,
            len: 20,
            txtColor: Colors.black,
            isBold: true,
            kbType: TextInputType.number,
            inputAction: TextInputAction.next,
            focusNode: focusCurrentLenderAccNo,
            focusNodeNext: focusPurchasePrice,
          ),
          SizedBox(height: 10),
          DatePickerView(
            cap: "Date property purchased",
            dt: (datePropertyPurchase == '')
                ? 'Select Date'
                : datePropertyPurchase,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            initialDate: DateTime.now(),
            firstDate: DateTime(1900, 1, 1),
            lastDate: DateTime.now(),
            padding: 5,
            radius: 5,
            callback: (value) {
              if (mounted) {
                setState(() {
                  try {
                    datePropertyPurchase =
                        DateFormat('dd-MM-yyyy').format(value).toString();
                  } catch (e) {
                    myLog(e.toString());
                  }
                });
              }
            },
          ),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Purchase price",
              "If you're not sure, your best guess is fine at this point.",
              purchasePrice,
              focusPurchasePrice,
              focusLoanAmount, (v) {
            setState(() {
              try {
                depositAmount.text =
                    calDepositAmount(loanAmt: loanAmount.text, purchasePrice: v)
                        .toString();
              } catch (e) {}
            });
          }),
          SizedBox(height: 10),
          drawCurrencyBox(
              "Loan Amount",
              "If you're not sure, your best guess is fine at this point.",
              loanAmount,
              focusLoanAmount,
              focusDepositAmount, (v) {
            setState(() {
              try {
                depositAmount.text = calDepositAmount(
                        loanAmt: v, purchasePrice: purchasePrice.text)
                    .toString();
              } catch (e) {}
            });
          }),
          SizedBox(height: 10),
          /*drawCurrencyBox(
              "Deposit Amount",
              "If you're not sure, your best guess is fine at this point.",
              depositAmount,
              focusDepositAmount,
              null),
          SizedBox(height: 10),*/

          /*_drawIncentivesView(),
          incentivesRBIndex == 0
              ? drawCurrencyBox(
                  "Please state how much?",
                  "If you're not sure, your best guess is fine at this point.",
                  incentiveAmount,
                  focusIncentivesAmount,
                  null)
              : SizedBox(),*/
          _drawPrefMortgageTermsView(),
          _drawPrefRepayTypeView(),
          /*SizedBox(height: 10),
          UIHelper().drawRadioTitle(
              context: context,
              title: "Do you want to apply for “Interest Only” loan?",
              list: W2NLocalData.listYesNoRB,
              index: loanInterestOnlyRBIndex,
              callback: (index) {
                setState(() => loanInterestOnlyRBIndex = index);
              }),
          loanInterestOnlyRBIndex == 0
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: drawInputBox(
                    context: context,
                    title: "What is your loan repayment strategy?",
                    ph: "Loan repayment strategy",
                    input: loanRepayStrategy,
                    len: 20,
                    txtColor: Colors.black,
                    isBold: true,
                    kbType: TextInputType.text,
                    inputAction: TextInputAction.next,
                    focusNode: focusLoanRepayStrategy,
                    focusNodeNext: null,
                  ),
                )
              : SizedBox(),*/
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              Txt(
                  txt: "Reason for additional loan:",
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.start,
                  isBold: true),
              Txt(
                  txt:
                      "Please note that if you apply for interest only mortgage, your loan balance will still be outstanding at the end of the loan term. You need to be absolutely certain as to how you will be pay the loan back. You may have to sell the property and downsize or arrange sufficient funds to pay off the loan.",
                  txtColor: Colors.black54,
                  txtSize: MyTheme.txtSize - .4,
                  txtAlign: TextAlign.start,
                  isBold: false),
              SizedBox(height: 5),
              drawDottedBox("Add Reason", () {
                onAddReasonDialog();
              })
            ],
          ),
          drawMortgageReasonsItems(
              context, requirementsCtrl.listMortgageReasons, (modelEdit) {
            onEditReasonDialog(modelEdit);
          }, (modelDel) {
            onDelReasonDialog(modelDel);
          }),
          _drawPropertyTypeView(),
          _drawPropertyTenureView(),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: drawInputBox(
                  context: context,
                  title: "Number of Bedrooms",
                  ph: "0",
                  input: noBedrooms,
                  len: 2,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusNoBedrooms,
                  focusNodeNext: focusNoKitchens,
                ),
              ),
              SizedBox(width: 10),
              Flexible(
                child: drawInputBox(
                  context: context,
                  title: "Number of Kitchens",
                  ph: "0",
                  input: noKitchens,
                  len: 2,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusNoKitchens,
                  focusNodeNext: focusGroundRent,
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: drawInputBox(
                  context: context,
                  title: "Number of Bathrooms",
                  ph: "0",
                  input: noBathrooms,
                  len: 2,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusNoBathrooms,
                  focusNodeNext: focusNoWC,
                ),
              ),
              SizedBox(width: 10),
              Flexible(
                child: drawInputBox(
                  context: context,
                  title: "Number of W/C",
                  ph: "0",
                  input: noWC,
                  len: 2,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusNoWC,
                  focusNodeNext: focusFloorBuilding,
                ),
              ),
            ],
          ),
          propertyTypeIndex == 1
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: drawInputBox(
                          context: context,
                          title: "How much is the ground rent?",
                          ph: "0",
                          input: groundRent,
                          len: 10,
                          txtColor: Colors.black,
                          isBold: true,
                          kbType: TextInputType.number,
                          inputAction: TextInputAction.next,
                          focusNode: focusGroundRent,
                          focusNodeNext: focusSrvCharges,
                        ),
                      ),
                      SizedBox(width: 10),
                      Flexible(
                        child: drawInputBox(
                          context: context,
                          title: "How much is the service charge?",
                          ph: "0",
                          input: srvCharges,
                          len: 10,
                          txtColor: Colors.black,
                          isBold: true,
                          kbType: TextInputType.number,
                          inputAction: TextInputAction.next,
                          focusNode: focusSrvCharges,
                          focusNodeNext: focusNoBathrooms,
                        ),
                      ),
                    ],
                  ))
              : SizedBox(),
          propertyTypeIndex == 1
              ? Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: Column(
                    children: [
                      drawInputBox(
                        context: context,
                        title: "Floors in the building",
                        ph: "Floors in the building",
                        input: floorBuilding,
                        len: 20,
                        txtColor: Colors.black,
                        isBold: true,
                        kbType: TextInputType.text,
                        inputAction: TextInputAction.next,
                        focusNode: focusFloorBuilding,
                        focusNodeNext: focusWhichFloorProperty,
                      ),
                      SizedBox(height: 10),
                      UIHelper().drawRadioTitle(
                          context: context,
                          title: "Is the flat above shop/ restaurant?",
                          list: W2NLocalData.listYesNoRB,
                          index: flatHasShopRBIndex,
                          callback: (index) {
                            setState(() => flatHasShopRBIndex = index);
                          }),
                      SizedBox(height: 10),
                      UIHelper().drawRadioTitle(
                          context: context,
                          title: "Does the property have a lift?",
                          list: W2NLocalData.listYesNoRB,
                          index: haveLiftRBIndex,
                          callback: (index) {
                            setState(() => haveLiftRBIndex = index);
                          }),
                      SizedBox(height: 10),
                      drawInputBox(
                        context: context,
                        title: "Which floor is the property",
                        ph: "Which floor is the property",
                        input: whichFloorProperty,
                        len: 50,
                        txtColor: Colors.black,
                        isBold: true,
                        kbType: TextInputType.text,
                        inputAction: TextInputAction.next,
                        focusNode: focusWhichFloorProperty,
                        focusNodeNext: focusYearBuilt,
                      )
                    ],
                  ),
                )
              : SizedBox(),
          SizedBox(height: 10),
          drawInputBox(
            context: context,
            title: "Year property was built",
            ph: "YYYY",
            input: yearBuilt,
            len: 4,
            txtColor: Colors.black,
            isBold: true,
            kbType: TextInputType.number,
            inputAction: TextInputAction.next,
            focusNode: focusYearBuilt,
            focusNodeNext: null,
          ),
          SizedBox(height: 10),
          _drawRadioQ(),
        ],
      ),
    );
  }

  _drawRadioQ() {
    final DateTime now = DateTime.now();
    final next = DateTime(now.year + 10, now.month, now.day);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        UIHelper().drawRadioTitle(
            context: context,
            title: "Any extension or loft conversion done?",
            list: W2NLocalData.listYesNoRB,
            index: extLoftConvRBIndex,
            callback: (index) {
              setState(() => extLoftConvRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title:
                "Is the property of a non-standard construction (ie. thatched roof, barn conversion etc)",
            list: W2NLocalData.listYesNoRB,
            index: nonStandardConstrRBIndex,
            callback: (index) {
              setState(() => nonStandardConstrRBIndex = index);
            }),
        SizedBox(height: 10),
        nonStandardConstrRBIndex == 0
            ? Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: drawInputBox(
                  context: context,
                  title: "Non-standard Construction Details",
                  ph: "Construction Details",
                  input: nonStandardConstrDetails,
                  len: 255,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  focusNode: focusNonStandardConstrDetails,
                  focusNodeNext: null,
                ))
            : SizedBox(),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is it Ex Council? Or purchased from council directly",
            list: W2NLocalData.listYesNoRB,
            index: exCouncilRBIndex,
            callback: (index) {
              setState(() => exCouncilRBIndex = index);
            }),
        propertyTypeIndex == 1
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: UIHelper().drawRadioTitle(
                    context: context,
                    title: "Does the flat have deck or balcony access?",
                    list: W2NLocalData.listYesNoRB,
                    index: xAccessRBIndex,
                    callback: (index) {
                      setState(() => xAccessRBIndex = index);
                    }))
            : SizedBox(),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title:
                "Did you purchase the property direct from the council/ housing association?",
            list: W2NLocalData.listYesNoRB,
            index: purchaseDirecltlyRBIndex,
            callback: (index) {
              setState(() => purchaseDirecltlyRBIndex = index);
            }),
        purchaseDirecltlyRBIndex == 0
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 10),
                  UIHelper().drawRadioTitle(
                      context: context,
                      title:
                          "Are there any Right to Buy or similar restriction imposed?",
                      list: W2NLocalData.listYesNoRB,
                      index: restrictionRBIndex,
                      callback: (index) {
                        setState(() => restrictionRBIndex = index);
                      }),
                  restrictionRBIndex == 0
                      ? Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: DatePickerView(
                            cap: "When does the restriction expire?",
                            dt: (restrictionDate == '')
                                ? 'Select Date'
                                : restrictionDate,
                            txtColor: Colors.black,
                            fontWeight: FontWeight.bold,
                            initialDate: now,
                            firstDate: now,
                            lastDate: next,
                            padding: 5,
                            radius: 5,
                            callback: (value) {
                              if (mounted) {
                                setState(() {
                                  try {
                                    restrictionDate = DateFormat('dd-MM-yyyy')
                                        .format(value)
                                        .toString();
                                  } catch (e) {
                                    myLog(e.toString());
                                  }
                                });
                              }
                            },
                          ),
                        )
                      : SizedBox(),
                  SizedBox(height: 10),
                  UIHelper().drawRadioTitle(
                      context: context,
                      title:
                          "Are you allowed to Re-mortgage the property by the council/ housing association?",
                      list: W2NLocalData.listYesNoRB,
                      index: housingAssociationRBIndex,
                      callback: (index) {
                        setState(() => housingAssociationRBIndex = index);
                      }),
                ],
              )
            : SizedBox(),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Is this a new built property?",
            list: W2NLocalData.listYesNoRB,
            index: newBuiltPropRBIndex,
            callback: (index) {
              setState(() => newBuiltPropRBIndex = index);
            }),
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Dose this property have warranty?",
            list: W2NLocalData.listYesNoRB,
            index: doesWarantyRBIndex,
            callback: (index) {
              setState(() => doesWarantyRBIndex = index);
            }),
        doesWarantyRBIndex == 0
            ? Obx(() => Padding(
                padding: const EdgeInsets.only(top: 10),
                child: DropDownListDialog(
                  context: context,
                  title: optDoesWarranty.value.title,
                  h1: "Select warranty",
                  heading: "If so what warranty",
                  ddTitleList: W2NLocalData.ddDoesWarranty,
                  vPadding: 3,
                  callback: (optionItem) {
                    optDoesWarranty.value = optionItem;
                  },
                )))
            : SizedBox(),
        SizedBox(height: 10),
        drawTextArea(title: "Notes", tf: notes),
      ],
    );
  }

  _drawPropertyTenureView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Property Tenure",
            list: listPropertyTenureRB,
            index: propertyTenureIndex,
            radioType: eRadioType.HORIZONTAL,
            callback: (index) {
              setState(() => propertyTenureIndex = index);
            }),
        SizedBox(height: 10),
        propertyTenureIndex == 1
            ? Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: drawInputBox(
                  context: context,
                  title: "Year left on lease",
                  ph: "Year left on lease",
                  input: yearLeftLease,
                  len: 4,
                  txtColor: Colors.black,
                  isBold: true,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  focusNode: focusYearLeftLease,
                  focusNodeNext: null,
                ))
            : SizedBox()
      ],
    );
  }

  _drawPropertyTypeView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Security Property Details",
            subTitle: "What type of property is this?",
            list: listPropertyTypeRB,
            index: propertyTypeIndex,
            callback: (index) {
              setState(() => propertyTypeIndex = index);
            }),
        SizedBox(height: 5),
      ],
    );
  }

  _drawPrefRepayTypeView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: 10),
        UIHelper().drawRadioTitle(
            context: context,
            title: "Preferred repayment type",
            list: listPrefRepayTypeRB,
            index: prefRepayTypeRBIndex,
            callback: (index) {
              setState(() => prefRepayTypeRBIndex = index);
            }),
      ],
    );
  }

  _drawPrefMortgageTermsView() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(height: incentivesRBIndex == 0 ? 20 : 0),
        Txt(
            txt: "Preferred mortgage term",
            txtColor: Colors.black,
            txtSize: MyTheme.txtSize - .2,
            txtAlign: TextAlign.start,
            isBold: true),
        SizedBox(height: 5),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
                child: drawInputCurrencyBox(
                    sign: "Year",
                    context: context,
                    tf: prefMortTermsYY,
                    hintTxt: null,
                    len: 4,
                    focusNode: focusPrefMortTermsYY,
                    focusNodeNext: focusPrefMortTermsMM)),
            SizedBox(width: 10),
            Flexible(
                child: drawInputCurrencyBox(
                    sign: "Month",
                    context: context,
                    tf: prefMortTermsMM,
                    hintTxt: null,
                    len: 2,
                    focusNode: focusPrefMortTermsMM,
                    focusNodeNext: null)),
          ],
        )
      ],
    );
  }

  _drawIncentivesView() {
    return Container(
      child: Column(
        children: [
          SizedBox(height: 20),
          UIHelper().drawRadioTitle(
              context: context,
              title:
                  "Are you receiving any incentives for buying this property such as discounts, cash back, free services or goods?",
              list: W2NLocalData.listYesNoRB,
              index: incentivesRBIndex,
              callback: (index) {
                setState(() => incentivesRBIndex = index);
              }),
          SizedBox(height: 10),
        ],
      ),
    );
  }
}
