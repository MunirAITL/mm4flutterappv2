import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/model/json/web2native/case/requirements/10cr_commit/MortgageUserAffordAbility.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/web2native/requirements/main/req_step10_cr_commit_ctrl.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

mixin ReqStep10CrComMixin {
  drawCrCommitList(
    BuildContext context,
    bool isShowAction,
    ReqStep10CrCommitCtrl ctrl,
    Function(MortgageUserAffordAbility model, bool isEdit) callback,
  ) {
    if (ctrl.crCommitCtrl == null) return SizedBox();
    ctrl.balOutstanding = 0.0.obs;
    ctrl.creditLimit = 0.0.obs;
    ctrl.monthlyPayment = 0.0.obs;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ctrl.crCommitCtrl.length > 0
            ? Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    //color: Colors.grey.shade200,
                    decoration: BoxDecoration(
                      color: MyTheme.bgColor,
                      border: Border.all(width: .5),
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: 2,
                            child: Txt(
                                txt: "Finance Type",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                maxLines: 50,
                                isBold: true),
                          ),
                          Expanded(
                            child: Txt(
                                txt: "Balance Outstanding",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                maxLines: 2,
                                isBold: true),
                          ),
                          Expanded(
                            child: Txt(
                                txt: "Monthly Payment",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                maxLines: 2,
                                isBold: true),
                          ),
                          Expanded(
                            child: Txt(
                                txt: "Credit Limit",
                                txtColor: Colors.white,
                                txtSize: MyTheme.txtSize - .6,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                maxLines: 2,
                                isBold: true),
                          ),
                          isShowAction
                              ? Flexible(
                                  flex: 2,
                                  child: Txt(
                                      txt: "Action",
                                      txtColor: Colors.white,
                                      txtSize: MyTheme.txtSize - .4,
                                      txtAlign: TextAlign.start,
                                      fontWeight: FontWeight.w500,
                                      isBold: true))
                              : SizedBox(),
                          SizedBox(width: isShowAction ? 40 : 20),
                        ],
                      ),
                    ),
                  ),
                ),
              )
            : SizedBox(),
        ...ctrl.crCommitCtrl.map(
          (e) {
            if (e.isAddItemForAffordabilityCalculation == "true") {
              ctrl.balOutstanding.value += e.outstandingBalance;
              ctrl.creditLimit.value += e.creditLimit;
              ctrl.monthlyPayment.value += e.monthlyPayment;
            }

            return ListTileTheme(
              contentPadding: EdgeInsets.all(0),
              iconColor: Colors.black,
              child: Theme(
                data: ThemeData.light().copyWith(
                  accentColor: Colors.black,
                ),
                child: Card(
                  color: e.isAddItemForAffordabilityCalculation == "true"
                      ? Colors.greenAccent
                      : MyTheme.bgColor2,
                  child: ExpansionTile(
                    //trailing: SizedBox.shrink(),

                    title: Padding(
                      padding: const EdgeInsets.only(left: 5),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                              flex: isShowAction ? 2 : 3,
                              child: Txt(
                                  txt: e.typeOfFinance,
                                  txtColor: MyTheme.statusBarColor,
                                  txtSize: MyTheme.txtSize - .5,
                                  txtAlign: TextAlign.start,
                                  fontWeight: FontWeight.w500,
                                  isBold: true)),
                          SizedBox(width: 5),
                          Expanded(
                            child: Text(
                              AppDefine.CUR_SIGN +
                                  e.outstandingBalance
                                      .toStringAsFixed(2)
                                      .replaceAll(".00", ""),
                              textAlign: TextAlign.left,
                              style:
                                  TextStyle(color: Colors.black, fontSize: 12),
                              softWrap: true,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Expanded(
                            child: Text(
                              AppDefine.CUR_SIGN +
                                  e.monthlyPayment
                                      .toStringAsFixed(2)
                                      .replaceAll(".00", ""),
                              textAlign: TextAlign.left,
                              style:
                                  TextStyle(color: Colors.black, fontSize: 12),
                              softWrap: true,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          Expanded(
                            child: Text(
                              AppDefine.CUR_SIGN +
                                  e.creditLimit
                                      .toStringAsFixed(2)
                                      .replaceAll(".00", ""),
                              textAlign: TextAlign.left,
                              style:
                                  TextStyle(color: Colors.black, fontSize: 12),
                              softWrap: true,
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          isShowAction
                              ? Flexible(
                                  child: Row(
                                    children: [
                                      Flexible(
                                        child: GestureDetector(
                                            onTap: () {
                                              callback(e, true);
                                            },
                                            child: Icon(Icons.edit_outlined,
                                                color: Colors.grey, size: 20)),
                                      ),
                                      SizedBox(width: 10),
                                      Flexible(
                                        child: GestureDetector(
                                            onTap: () {
                                              confirmDialog(
                                                  context: context,
                                                  title: "Confirmation!!",
                                                  msg:
                                                      "Are you sure to delete this item?",
                                                  callbackYes: () {
                                                    callback(e, false);
                                                  });
                                            },
                                            child: Icon(Icons.delete_outline,
                                                color: Colors.grey, size: 20)),
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox()
                        ],
                      ),
                    ),
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 5, bottom: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            UIHelper().draw2Row(
                                "Finance Provider", e.financeProvider),
                            SizedBox(height: 5),
                            UIHelper().draw2Row(
                                "Repay before mortgage completes?",
                                e.repayBeforeMortgage),
                            SizedBox(height: 5),
                            UIHelper().draw2Row("End date",
                                DateFun.getDate(e.endDate, "dd-MMM-yyyy")),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            );
          },
        ),
        (ctrl.balOutstanding.value > 0 ||
                ctrl.monthlyPayment.value > 0 ||
                ctrl.creditLimit.value > 0)
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Card(
                    child: Padding(
                      padding: const EdgeInsets.all(10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Flexible(
                            //flex: 2,
                            child: Txt(
                                txt: "Total:",
                                txtColor: MyTheme.statusBarColor,
                                txtSize: MyTheme.txtSize - .2,
                                txtAlign: TextAlign.center,
                                isBold: true),
                          ),
                          Expanded(
                            child: Txt(
                                txt: AppDefine.CUR_SIGN +
                                    ctrl.balOutstanding.value
                                        .toStringAsFixed(2)
                                        .replaceAll(".00", ""),
                                txtColor: MyTheme.statusBarColor,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.center,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                          Expanded(
                            child: Txt(
                                txt: AppDefine.CUR_SIGN +
                                    ctrl.monthlyPayment.value
                                        .toStringAsFixed(2)
                                        .replaceAll(".00", ""),
                                txtColor: MyTheme.statusBarColor,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                          Expanded(
                            child: Txt(
                                txt: AppDefine.CUR_SIGN +
                                    ctrl.creditLimit.value
                                        .toStringAsFixed(2)
                                        .replaceAll(".00", ""),
                                txtColor: MyTheme.statusBarColor,
                                txtSize: MyTheme.txtSize - .4,
                                txtAlign: TextAlign.start,
                                fontWeight: FontWeight.w500,
                                isBold: true),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              )
            : SizedBox()
      ],
    );
  }
}
