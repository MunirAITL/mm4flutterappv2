import 'package:aitl/model/json/web2native/case/requirements/12access_afford/Task.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:flutter/material.dart';
import '../../../../../../view_model/helper/ui_helper.dart';
import '../../../../../../view_model/rx/web2native/requirements/requirements_ctrl.dart';

mixin ReqDFMixin {
  drawDFDetailsView({
    BuildContext context,
    RequirementsCtrl requirementsCtrl,
    Task caseModel,
  }) {
    final model = requirementsCtrl.requirementsModelAPIModel;
    if (model.value == null) return SizedBox();
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIHelper().draw2Row("Finance Type", model.value.financeType ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Auction house & Lot details, if auction purchase",
                model.value.auctionHouseLotDetails ?? ''),
            SizedBox(height: 10),
            UIHelper()
                .draw2Row("Repayment type", model.value.repaymentType ?? ''),
            SizedBox(height: 10),
            UIHelper()
                .draw2Row("Exit Strategy", model.value.exitStrategy ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Details", model.value.details ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("GDV", model.value.gVD ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Address of property?",
                model.value.addressOfPropertyToBeMortgaged ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Current approximate Valuation of Property?",
                UIHelper().addCurSign(model.value
                    .priceOfPropertyBeingPurchasedCurrentValuationOfProperty)),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Date property purchased",
                DateFun.getDate(
                    model.value.datePropertyPurchased, 'dd MMMM yyyy')),
            SizedBox(height: 10),
            UIHelper().draw2Row("Purchase price",
                UIHelper().addCurSign(model.value.purchasePrice)),
            SizedBox(height: 10),
            UIHelper().draw2Row("Loan Amount",
                UIHelper().addCurSign(model.value.howMuchDoYouWishToBorrow)),
            /*SizedBox(height: 10),
            UIHelper().draw2Row(
                "Client Experience", model.value.clientExperience ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Plan", model.value.plan ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Cost of refurb",
                UIHelper().addCurSign(model.value.costofRefurb)),*/
            SizedBox(height: 10),
            UIHelper().draw2Row("Deposit Amount",
                UIHelper().addCurSign(model.value.amountOfDepositEquity)),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Preferred mortgage term",
                model.value.preferredMortgageTermYear.toString() +
                    ' Year ' +
                    model.value.preferredMortgageTermMonth.toString() +
                    ' Month'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Preferred Repayment type",
                model.value.preferredRepaymentType ?? ''),
            SizedBox(height: 10),
            /*UIHelper().draw2Row(
                "Security Property Details\nWhat type of property is this?",
                model.value.propertyType ?? ''),
            SizedBox(height: 10),
            UIHelper()
                .draw2Row("Property Tenure", model.value.propertyTenure ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Year left on lease",
                model.value.ifLeaseholdHowLongIsLeftOnTheLease ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Number of bedrooms",
                model.value.numberOfBedrooms.toString() ?? 0),
            SizedBox(height: 10),
            UIHelper().draw2Row("Number of Kitchens",
                model.value.numberOfKoichens.toString() ?? 0),
            SizedBox(height: 10),
            UIHelper().draw2Row("Number of Bathroom",
                model.value.numberOfBaahroom.toString() ?? 0),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "How much is the ground rent?",
                model.value.groundRentAmount != null
                    ? model.value.groundRentAmount > 0
                        ? UIHelper().addCurSign(model.value.groundRentAmount)
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "How much is the service charge?",
                model.value.serviceChargeAmount != null
                    ? model.value.serviceChargeAmount > 0
                        ? UIHelper().addCurSign(model.value.serviceChargeAmount)
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Number of W/C", model.value.numberOfWC.toString() ?? '0'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Floors in the building",
                model.value.floorsInTheBuilding ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Is the flat above shop/ restaurant?",
                model.value.isTheFlatAboveShopRestaurant ?? 'No'),
                SizedBox(height: 10),
            UIHelper().draw2Row("Does the property have a lift?",
                model.value.doesThePropertyHaveALift ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Which floor is the property",
                model.value.whichFloorIsTheProperty ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Year property was built",
                model.value.yearPropertyWasBuilt ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Any extension or Loft Conversion done?",
                model.value.anyExtensionOrLoftConversionDone ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Is the property of a non-standard construction (ie. thatched roof, barn conversion etc)",
                model.value.isThePropertyOfANonStandardConstruction ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Non-standard Construction Details",
                model.value.nonstandardConstructionDetails ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Is it Ex Council? Or purchased from council directly",
                model.value.isItExCouncil ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Does the flat have deck or balcony access?",
                model.value.doesTheFlatHaveDeckORBalconyAccess ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Is this a new built property?",
                model.value.isThisANewBuiltProperty ?? 'No'),
                SizedBox(height: 10),
            UIHelper().draw2Row("Dose this property have warranty?",
                model.value.doesThisPropertyHaveWarranty ?? ''),
                SizedBox(height: 10),
            UIHelper().draw2Row("If so what warranty",
                model.value.warrantyType ?? ''),
            SizedBox(height: 10),*/
            UIHelper().draw2Row("Notes", model.value.notes ?? ''),
          ],
        ),
      ),
    );
  }
}
