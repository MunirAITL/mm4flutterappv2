import 'package:aitl/model/json/web2native/case/requirements/12access_afford/Task.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:flutter/material.dart';
import '../../../../../../config/MyTheme.dart';
import '../../../../../../model/json/web2native/case/requirements/mortgage/mortgage_model.dart';
import '../../../../../../view_model/helper/ui_helper.dart';
import '../../../../../../view_model/rx/web2native/requirements/requirements_ctrl.dart';
import '../../../../../../view_model/rx/web2native/requirements/req_mortgage_ctrl.dart';
import '../../../../../widgets/txt/Txt.dart';
import '../../dialogs/requirements/req_mortgage_dialog.dart';
import 'package:get/get.dart';

mixin ReqMortgageMixin {
  drawMortgageDetailsView({
    BuildContext context,
    RequirementsCtrl requirementsCtrl,
    Task caseModel,
  }) {
    final model = requirementsCtrl.requirementsModelAPIModel;
    if (model.value == null) return SizedBox();
    final requirementController = Get.put(ReqMortgageCtrl());
    final List<MortgageDepositByModel> listDepositedBy = [];
    for (final j in model.toJson().entries) {
      if (j.value != null) {
        requirementController.ddReq.value.listOptionItems.forEach((opt) {
          if (j.key.toLowerCase() == opt.id.toLowerCase() && j.value > 0) {
            listDepositedBy.add(MortgageDepositByModel(
                key: j.key, from: opt.title, amount: j.value));
          }
        });
      }
    }
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            UIHelper()
                .draw2Row("Mortgage Type", model.value.mortgageType ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Address of property to be mortgaged?",
                model.value.addressOfPropertyToBeMortgaged ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Current approximate Valuation of Property?",
                UIHelper().addCurSign(model.value
                    .priceOfPropertyBeingPurchasedCurrentValuationOfProperty)),
            SizedBox(height: 10),
            /*UIHelper().draw2Row("Current mortgage outstanding",
                UIHelper().addCurSign(model.value.balanceOutstanding)),
            SizedBox(height: 10),
            /*UIHelper().draw2Row("Your current LTV(%)",
                UIHelper().addCurSign(model.value.lTVAmount)),
            SizedBox(height: 10),*/
            UIHelper().draw2Row("Monthly Mortgage Payment",
                UIHelper().addCurSign(model.value.monthlyMortgagePayment)),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Current Lender Name", model.value.currentLender ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Current Lender Account Number",
                model.value.currentLenderAccountNumber ?? ''),
            SizedBox(height: 10),*/
            UIHelper().draw2Row(
                "Date property purchased",
                DateFun.getDate(
                    model.value.datePropertyPurchased, 'dd MMMM yyyy')),
            SizedBox(height: 10),
            UIHelper().draw2Row("Purchase price",
                UIHelper().addCurSign(model.value.purchasePrice)),
            SizedBox(height: 10),
            UIHelper().draw2Row("Loan Amount",
                UIHelper().addCurSign(model.value.howMuchDoYouWishToBorrow)),
            SizedBox(height: 10),
            UIHelper().draw2Row("Deposit Amount",
                UIHelper().addCurSign(model.value.amountOfDepositEquity)),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Savings", UIHelper().addCurSign(model.value.savings)),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Are you receiving any incentives for buying this property such as discounts, cash back, free services or goods?",
                model.value
                        .areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesOrGoods ??
                    'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Please state how much?",
                UIHelper().addCurSign(model.value
                    .areYouReceivingAnyIncentivesForBuyingThisPropertySuchAsDiscountsCashBackFreeServicesValue)),
            SizedBox(height: 10),
            /*UIHelper().draw2Row(
                "How much rent do you receive from this property",
                UIHelper()
                    .addCurSign(model.value.rentalIncomeAchievableAmount)),
            SizedBox(height: 10),
            UIHelper().draw2Row("Is the property already tenanted?",
                model.value.isThePropertyAlreadyTenanted ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Are you currently residing in this property?",
                model.value.areYouCurrentlyResidingInThisProperty ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Are you retaining this property as a Buy to Let property to purchase a new residential property?",
                model.value
                        .areYouRetainingThisPropertyAsABuyToLetPropertyToPurchaseANewResidentialProperty ??
                    'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Are you a first time land lord?",
                model.value.areYouAFirstTimeLandLord ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Have you been an experienced landlord for more than 12 months?",
                model.value
                        .haveYouBeenAnExperiencedLandlordForMoreThan12Months ??
                    'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Have you ever lived in this property?",
                model.value.haveYouEverLivedInThisProperty ?? 'No'),
            SizedBox(height: 10),*/
            UIHelper().draw2Row(
                "Preferred mortgage term",
                model.value.preferredMortgageTermYear.toString() +
                    ' Year ' +
                    model.value.preferredMortgageTermMonth.toString() +
                    ' Month'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Preferred Repayment type",
                model.value.preferredRepaymentType ?? ''),
            SizedBox(height: 10),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                listDepositedBy.length > 0
                    ? Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black,
                            width: .5,
                          ),
                          borderRadius: BorderRadius.circular(5),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Txt(
                                  txt: "Added Amounts",
                                  txtColor: MyTheme.statusBarColor,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.start,
                                  style: TextStyle(
                                      decoration: TextDecoration.underline,
                                      fontSize: 14,
                                      color: MyTheme.statusBarColor),
                                  isBold: false),
                              drawDepositAmountItems(
                                  context, listDepositedBy, null, null),
                            ],
                          ),
                        ),
                      )
                    : SizedBox(),
              ],
            ),
            SizedBox(height: 10),

            /*UIHelper().draw2Row("Are you capital raising?",
                model.value.areYouRaisingAnyMoneyfromAboveProperty ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Amount raising over current mortgage",
                UIHelper()
                    .addCurSign(model.value.amountRaisingOverCurrentMortgage)),
            SizedBox(height: 10),*/
            /*UIHelper().draw2Row(
                "Are funds available to pay fees in connection with mortgage?",
                model.value
                        .areFundsAvailableToPayFeesInConnectionWithMortgage ??
                    ''),
            SizedBox(height: 10),*/
            UIHelper().draw2Row(
                "Security Property Details\nWhat type of property is this?",
                model.value.propertyType ?? ''),
            SizedBox(height: 10),
            UIHelper()
                .draw2Row("Property Tenure", model.value.propertyTenure ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Year left on lease",
                model.value.ifLeaseholdHowLongIsLeftOnTheLease ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Number of bedrooms",
                model.value.numberOfBedrooms.toString() ?? 0),
            SizedBox(height: 10),
            UIHelper().draw2Row("Number of Kitchens",
                model.value.numberOfKoichens.toString() ?? 0),
            SizedBox(height: 10),
            UIHelper().draw2Row("Number of Bathroom",
                model.value.numberOfBaahroom.toString() ?? 0),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "How much is the ground rent?",
                model.value.groundRentAmount != null
                    ? model.value.groundRentAmount > 0
                        ? UIHelper().addCurSign(model.value.groundRentAmount)
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "How much is the service charge?",
                model.value.serviceChargeAmount != null
                    ? model.value.serviceChargeAmount > 0
                        ? UIHelper().addCurSign(model.value.serviceChargeAmount)
                        : 'N/A'
                    : 'N/A'),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Number of W/C", model.value.numberOfWC.toString() ?? '0'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Floors in the building",
                model.value.floorsInTheBuilding ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Is the flat above shop/ restaurant?",
                model.value.isTheFlatAboveShopRestaurant ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Does the property have a lift?",
                model.value.doesThePropertyHaveALift ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Which floor is the property",
                model.value.whichFloorIsTheProperty ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Year property was built",
                model.value.yearPropertyWasBuilt ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Any extension or Loft Conversion done?",
                model.value.anyExtensionOrLoftConversionDone ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Is the property of a non-standard construction (ie. thatched roof, barn conversion etc)",
                model.value.isThePropertyOfANonStandardConstruction ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Non-standard Construction Details",
                model.value.nonstandardConstructionDetails ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "Is it Ex Council? Or purchased from council directly",
                model.value.isItExCouncil ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Does the flat have deck or balcony access?",
                model.value.doesTheFlatHaveDeckORBalconyAccess ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Is this a new built property?",
                model.value.isThisANewBuiltProperty ?? 'No'),
            SizedBox(height: 10),
            UIHelper().draw2Row("Dose this property have warranty?",
                model.value.doesThisPropertyHaveWarranty ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row(
                "If so what warranty", model.value.warrantyType ?? ''),
            SizedBox(height: 10),
            UIHelper().draw2Row("Notes", model.value.notes ?? ''),
          ],
        ),
      ),
    );
  }
}
