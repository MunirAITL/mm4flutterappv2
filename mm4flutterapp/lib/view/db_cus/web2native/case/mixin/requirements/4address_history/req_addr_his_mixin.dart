import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/model/json/web2native/case/requirements/4address_history/UserAddresss.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

mixin ReqStep4AddrHisMixin {
  drawAddrHistoryView(
    BuildContext context,
    List<UserAddresss> listUserAddresss,
    bool isShowAction,
    Function(UserAddresss) callbackEdit,
    Function(UserAddresss) callbackDel,
  ) {
    if (listUserAddresss == null) return SizedBox();
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ...listUserAddresss.map((model) => Card(
                color: MyTheme.bgColor2,
                elevation: 2,
                child: Padding(
                  padding: const EdgeInsets.only(
                      top: 10, left: 5, right: 5, bottom: 5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(
                            flex: isShowAction ? 5 : 6,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Txt(
                                    txt: (model.addressLine1 +
                                            " " +
                                            model.addressLine2 +
                                            " " +
                                            model.addressLine3 +
                                            " " +
                                            model.postcode)
                                        .trim(),
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.start,
                                    fontWeight: FontWeight.w500,
                                    isBold: true),
                                model.livingDate != ""
                                    ? Padding(
                                        padding: const EdgeInsets.only(top: 5),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Flexible(
                                              child: Txt(
                                                  txt: "Living Start Date",
                                                  txtColor: Colors.black,
                                                  txtSize: MyTheme.txtSize - .6,
                                                  txtAlign: TextAlign.start,
                                                  isBold: false),
                                            ),
                                            SizedBox(width: 10),
                                            Flexible(
                                              child: Txt(
                                                  txt: DateFun.getDate(
                                                      model.livingDate,
                                                      "dd-MMM-yyyy"),
                                                  txtColor: Colors.black87,
                                                  txtSize: MyTheme.txtSize - .6,
                                                  txtAlign: TextAlign.start,
                                                  fontWeight: FontWeight.w500,
                                                  isBold: true),
                                            ),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                                model.areYouLivingAtThisAddress != "Yes"
                                    ? Padding(
                                        padding: const EdgeInsets.only(top: 5),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Flexible(
                                              child: Txt(
                                                  txt: "Living End Date",
                                                  txtColor: Colors.black87,
                                                  txtSize: MyTheme.txtSize - .6,
                                                  txtAlign: TextAlign.start,
                                                  isBold: false),
                                            ),
                                            SizedBox(width: 10),
                                            Flexible(
                                              child: Txt(
                                                  txt: DateFun.getDate(
                                                      model.livingEndDate,
                                                      "dd-MMM-yyyy"),
                                                  txtColor: Colors.black,
                                                  txtSize: MyTheme.txtSize - .6,
                                                  txtAlign: TextAlign.start,
                                                  fontWeight: FontWeight.w500,
                                                  isBold: true),
                                            ),
                                          ],
                                        ),
                                      )
                                    : SizedBox(),
                              ],
                            ),
                          ),
                          isShowAction
                              ? Flexible(
                                  child: Row(
                                    children: [
                                      Flexible(
                                        child: GestureDetector(
                                            onTap: () {
                                              callbackEdit(model);
                                            },
                                            child: Icon(Icons.edit_outlined,
                                                color: Colors.grey, size: 20)),
                                      ),
                                      SizedBox(width: 10),
                                      Flexible(
                                        child: GestureDetector(
                                            onTap: () {
                                              confirmDialog(
                                                  context: context,
                                                  title: "Confirmation!!",
                                                  msg:
                                                      "Are you sure to delete this item?",
                                                  callbackYes: () {
                                                    callbackDel(model);
                                                  });
                                            },
                                            child: Icon(Icons.delete_outline,
                                                color: Colors.grey, size: 20)),
                                      ),
                                    ],
                                  ),
                                )
                              : SizedBox()
                        ],
                      ),
                      //Divider(color: MyTheme.statusBarColor)
                    ],
                  ),
                ),
              )),
        ],
      ),
    );
  }
}
