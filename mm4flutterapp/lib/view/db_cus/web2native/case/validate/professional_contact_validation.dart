import 'package:aitl/model/json/web2native/case/requirements/2essentials/MortgageUserOtherContactInfos.dart';

class ProfessionalContactValidation {
  static String onValidate(
      {MortgageUserOtherContactInfos modelContact,
      String search,
      String companyName}) {
    if (search.isEmpty || modelContact == null) {
      return "Please enter valid Contact Name Agent type already exist. Please select from contact drop-down.";
    } else if (companyName.isEmpty) {
      return "Please enter valid Company Name";
    }
    return "";
  }
}
