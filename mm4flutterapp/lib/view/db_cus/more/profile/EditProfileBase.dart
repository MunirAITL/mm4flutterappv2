import 'dart:convert';

import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/api/db_cus/more/settings/EditProfileAPIMgr.dart';
import 'package:aitl/controller/api/db_cus/more/settings/GetProfileAPIMgr.dart';
import 'package:aitl/controller/classes/Common.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/settings/EditProfileHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/model/json/auth/UserModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/edit_profile/UserProfileAPIModel.dart';
import 'package:aitl/view/db_cus/web2native/case_base.dart';
import 'package:aitl/view/widgets/dialog/DatePickerView.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/input/InputTitleBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/txt/TxtBox.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../../../../mixin.dart';

enum smokerEnum { yes, no }

abstract class EditProfileBase<T extends StatefulWidget> extends CaseBase<T> {
  drawLayout();

  bool isExtAccess = false;
  bool isUpdateProfile = false;

  EditProfileHelper editProfileHelper;

  //  personal info
  final fname = TextEditingController();
  final mname = TextEditingController();
  final lname = TextEditingController();
  final pwd = TextEditingController();
  final mobile = TextEditingController();
  final tel = TextEditingController();
  final nationalInsuranceNumber = TextEditingController();
  final refSource = TextEditingController();
  final passport = TextEditingController();
  final TextEditingController deactivateReason = TextEditingController();
  //
  final focusFname = FocusNode();
  final focusMname = FocusNode();
  final focusLname = FocusNode();
  final focusEmail = FocusNode();
  final focusMobile = FocusNode();
  final focusTel = FocusNode();
  final focusPwd = FocusNode();
  final focusNIN = FocusNode();
  final focusPP = FocusNode();
  final focusRef = FocusNode();

  //  Email
  String email;
  String profileImageId = "";
  String coverImageId = "";

  //  DOB
  String dob = "";

  //  Passport
  String passportExp = "";

  smokerEnum radiosmoker = smokerEnum.no;

  bool isLoading = false;

  updateProfileAPI(
      {String profileImageId = '',
      String coverImageId = '',
      Function(UserModel model) callback}) async {
    try {
      if (validate()) {
        isUpdateProfile = true;
        final param = EditProfileHelper().getParam(
          firstName: fname.text.trim(),
          lastName: lname.text.trim(),
          email: email,
          userName: userData.userModel.userName,
          profileImageId: profileImageId.toString(),
          coverImageId: coverImageId.toString(),
          referenceId: userData.userModel.referenceID.toString(),
          referenceType: "",
          //_ref.text.trim(),
          stanfordWorkplaceURL: refSource.text.toString().trim(),
          //_ref.text.trim(),
          remarks: userData.userModel.remarks,
          cohort: editProfileHelper.optGender.title.trim(),
          communityId: userData.userModel.communityID.toString(),
          isFirstLogin: false,
          mobileNumber: mobile.text.trim(),
          dateofBirth: dob,
          middleName: mname.text.trim(),
          namePrefix: editProfileHelper.optTitle.title.trim(),
          areYouASmoker: (radiosmoker == smokerEnum.no) ? 'No' : 'Yes',
          countryCode: '',
          addressLine1: userData.userModel.addressLine1,
          addressLine2: userData.userModel.addressLine2,
          addressLine3: userData.userModel.addressLine3,
          town: userData.userModel.town,
          county: userData.userModel.county,
          postcode: userData.userModel.postcode,
          telNumber: tel.text.trim(),
          nationalInsuranceNumber: nationalInsuranceNumber.text.trim(),
          nationality: editProfileHelper.optNationalities.title,
          countryofBirth: editProfileHelper.optCountriesBirth.title,
          countryofResidency: editProfileHelper.optCountriesResidential.title,
          passportNumber: passport.text.trim(),
          maritalStatus: editProfileHelper.optMaritalStatus.title,
          occupantType: userData.userModel.occupantType,
          livingDate: userData.userModel.livingDate,
          password: pwd.text.trim(),
          userCompanyId: userData.userModel.userCompanyID,
          visaExpiryDate: userData.userModel.visaExpiryDate,
          passportExpiryDate: passportExp,
          visaName: userData.userModel.visaName,
          otherVisaName: userData.userModel.otherVisaName,
          id: userData.userModel.id,
        );
        myLog("profile update data passportExp = " + passportExp);
        myLog("profile update data = " + json.encode(param));
        EditProfileAPIMgr().wsUpdateProfileAPI(
          context: context,
          param: param,
          callback: (model) async {
            if (model != null && mounted) {
              try {
                if (model.success) {
                  try {
                    await DBMgr.shared
                        .setUserProfile(user: model.responseData.user);
                    await userData.setUserModel();
                    if (callback != null) {
                      callback(model.responseData.user);
                    } else {
                      final msg = model.messages.post_user[0].toString();
                      showToast(context: context, msg: msg, which: 1);
                      setState(() {});
                    }
                  } catch (e) {
                    myLog(e.toString());
                  }
                } else {
                  try {
                    if (mounted) {
                      final err = model.errorMessages.post_user[0].toString();
                      showToast(context: context, msg: err);
                    }
                  } catch (e) {
                    myLog(e.toString());
                  }
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          },
        );
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  updateData() async {
    isLoading = true;
    GetProfileAPIMgr().wsGetProfileAPI(
      context: context,
      callback: (model) async {
        if (model != null && mounted) {
          try {
            if (model.success) {
              try {
                UserModel userModel = model.responseData.user;

                //  personal info
                email = userModel.email;
                profileImageId = userModel.profileImageID.toString();
                fname.text = userModel.firstName;
                mname.text = userModel.middleName;
                lname.text = userModel.lastName;
                mobile.text =
                    Common.stripCountryCodePhone(userModel.mobileNumber);
                tel.text = Common.stripCountryCodePhone(userModel.telNumber);

                //  visa info
                nationalInsuranceNumber.text =
                    userModel.nationalInsuranceNumber;
                refSource.text = userModel.stanfordWorkplaceURL;
                passport.text = userModel.passportNumber;
                if (userModel.passportExpiryDate == "0001-01-01T00:00:00" ||
                    userModel.passportExpiryDate == "1970-01-01T00:00:00") {
                  passportExp = '';
                } else {
                  passportExp = DateFormat('dd-MMM-yyyy')
                      .format(DateTime.parse(userModel.passportExpiryDate));
                }

                radiosmoker = (userModel.areYouASmoker == "No")
                    ? smokerEnum.no
                    : smokerEnum.yes;

                dob = userModel.dateofBirth;

                //  populate countries drop down all 3
                editProfileHelper = EditProfileHelper();
                await editProfileHelper.getCountriesBirth(
                    context: context, cap: userModel.countryofBirth);
                await editProfileHelper.getCountriesResidential(
                    context: context, cap: userModel.countryofResidency);
                await editProfileHelper.getCountriesNationaity(
                    context: context, cap: userModel.nationality);

                //  set data into drop down
                if (userModel.namePrefix != '') {
                  editProfileHelper.optTitle.id = "1";
                  editProfileHelper.optTitle.title = userModel.namePrefix;
                }

                if (userModel.cohort != '') {
                  editProfileHelper.optGender.id = "1";
                  editProfileHelper.optGender.title = userModel.cohort;
                }

                if (userModel.maritalStatus != '') {
                  editProfileHelper.optMaritalStatus.id = "1";
                  editProfileHelper.optMaritalStatus.title =
                      userModel.maritalStatus;
                }

                if (userModel.countryofBirth != '') {
                  editProfileHelper.optCountriesBirth.id = "1";
                  editProfileHelper.optCountriesBirth.title =
                      userModel.countryofBirth;
                }

                if (userModel.countryofResidency != '') {
                  editProfileHelper.optCountriesResidential.id = "1";
                  editProfileHelper.optCountriesResidential.title =
                      userModel.countryofResidency;
                }

                if (userModel.nationality != '') {
                  editProfileHelper.optNationalities.id = "1";
                  editProfileHelper.optNationalities.title =
                      userModel.nationality;
                }

                model.responseData.user.userCompanyID =
                    userData.userModel.userCompanyID;

                await DBMgr.shared
                    .setUserProfile(user: model.responseData.user);
                await userData.setUserModel();

                setState(() {
                  isLoading = false;
                });
              } catch (e) {
                myLog("Error UserInfo data parse = " + e.toString());
                setState(() {
                  isLoading = false;
                });
              }
            } else {
              setState(() {
                isLoading = false;
              });

              try {
                if (mounted) {
                  final err = model.errorMessages.post_user[0].toString();
                  showToast(context: context, msg: err);
                }
              } catch (e) {
                myLog(e.toString());
              }
            }
          } catch (e) {
            myLog(e.toString());
          }
        }
      },
    );
  }

  validate() {
    if (!UserProfileVal().isFNameOK(context, fname)) {
      return false;
    } else if (!UserProfileVal().isLNameOK(context, lname)) {
      return false;
    } else if (editProfileHelper.optGender.id == null) {
      showToast(context: context, msg: "Please choose gender from the list");
      return false;
    } else if (dob == "") {
      showToast(context: context, msg: "Please select date of birth");
      return false;
    } else if (!UserProfileVal().isPhoneOK(context, mobile)) {
      return false;
    } else if (editProfileHelper.optNationalities.id == null) {
      showToast(context: context, msg: "Please choose nationality");
      return false;
    }
    return true;
  }

  @override
  void dispose() {
    editProfileHelper = null;
    //  personal info
    pwd.dispose();
    fname.dispose();
    mname.dispose();
    lname.dispose();
    mobile.dispose();
    tel.dispose();
    nationalInsuranceNumber.dispose();
    refSource.dispose();
    passport.dispose();

    //  deactivate
    deactivateReason.dispose();

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  drawPersonalInfoView() {
    final DateTime dateNow = DateTime.now();
    final dateDOBlast = DateTime(dateNow.year - 18, dateNow.month, dateNow.day);
    final dateDOBfirst =
        DateTime(dateNow.year - 100, dateNow.month, dateNow.day);

    return Container(
      //width: getW(context),
      child: Column(
        children: [
          isExtAccess
              ? Container(
                  width: getW(context),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Flexible(
                            child: Txt(
                                txt: "Personal Information",
                                txtColor: MyTheme.titleColor,
                                txtSize: MyTheme.txtSize,
                                txtAlign: TextAlign.start,
                                isBold: true),
                          ),
                          SizedBox(width: 5),
                          Container(
                            child: Image.asset(
                                "assets/images/ico/checked_blue_ico.png"),
                          ),
                        ],
                      ),
                      SizedBox(height: 10),
                      Container(color: Colors.black, height: 0.5)
                    ],
                  ),
                )
              : SizedBox(),
          SizedBox(height: 20),
          editProfileHelper != null
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        child: Txt(
                      txt: "Choose Title",
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize - .2,
                      isBold: true,
                      txtAlign: TextAlign.start,
                    )),
                    SizedBox(height: 10.0),
                    DropDownListDialog(
                      context: context,
                      title: editProfileHelper.optTitle.title,
                      ddTitleList: editProfileHelper.ddTitle,
                      callback: (optionItem) {
                        editProfileHelper.optTitle = optionItem;
                        setState(() {});
                      },
                    ),
                  ],
                )
              : SizedBox(),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "First Name",
            input: fname,
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusFname,
            focusNodeNext: focusMname,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Middle Name",
            input: mname,
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusMname,
            focusNodeNext: focusLname,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Last Name",
            input: lname,
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusLname,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          editProfileHelper != null
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        width: getW(context),
                        child: Txt(
                          txt: "Choose Gender",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          isBold: false,
                          fontWeight: FontWeight.bold,
                          txtAlign: TextAlign.start,
                        )),
                    SizedBox(height: 10),
                    DropDownListDialog(
                      context: context,
                      title: editProfileHelper.optGender.title,
                      ddTitleList: editProfileHelper.ddGender,
                      callback: (optionItem) {
                        editProfileHelper.optGender = optionItem;
                        setState(() {});
                      },
                    ),
                  ],
                )
              : SizedBox(),
          SizedBox(height: 20),
          editProfileHelper != null
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        width: getW(context),
                        child: Txt(
                          txt: "Choose Marital Status",
                          txtColor: Colors.black,
                          txtSize: MyTheme.txtSize - .2,
                          isBold: false,
                          fontWeight: FontWeight.bold,
                          txtAlign: TextAlign.start,
                        )),
                    SizedBox(height: 10),
                    DropDownListDialog(
                      context: context,
                      title: editProfileHelper.optMaritalStatus.title,
                      ddTitleList: editProfileHelper.ddMaritalStatus,
                      callback: (optionItem) {
                        editProfileHelper.optMaritalStatus = optionItem;
                        setState(() {});
                      },
                    ),
                  ],
                )
              : SizedBox(),
          SizedBox(height: 20),
          DatePickerView(
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            cap: 'Select date of birth',
            dt: (dob == '') ? 'Select date of birth' : dob,
            initialDate: dateDOBlast,
            firstDate: dateDOBfirst,
            lastDate: dateDOBlast,
            callback: (value) {
              if (mounted) {
                setState(() {
                  try {
                    dob = DateFormat('dd-MM-yyyy').format(value).toString();
                  } catch (e) {
                    myLog(e.toString());
                  }
                });
              }
            },
          ),
          SizedBox(height: 20),
          editProfileHelper != null
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        width: getW(context),
                        child: Txt(
                          txt: "Country of birth",
                          txtColor: Colors.black,
                          fontWeight: FontWeight.bold,
                          txtSize: MyTheme.txtSize - .2,
                          isBold: false,
                          txtAlign: TextAlign.start,
                        )),
                    SizedBox(height: 10),
                    DropDownListDialog(
                      context: context,
                      title: editProfileHelper.optCountriesBirth.title,
                      ddTitleList: editProfileHelper.ddMaritalCountriesBirth,
                      callback: (optionItem) {
                        editProfileHelper.optCountriesBirth = optionItem;
                        setState(() {});
                      },
                    ),
                  ],
                )
              : SizedBox(),
          SizedBox(height: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Txt(
                txt: "Email",
                txtColor: Colors.black,
                fontWeight: FontWeight.bold,
                txtSize: MyTheme.txtSize - .2,
                isBold: false,
                txtAlign: TextAlign.start,
              ),
              SizedBox(height: 10),
              TxtBox(txt: email),
            ],
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Mobile Number",
            input: mobile,
            kbType: TextInputType.phone,
            inputAction: TextInputAction.next,
            focusNode: focusMobile,
            focusNodeNext: focusTel,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Telephone Number",
            input: tel,
            kbType: TextInputType.phone,
            inputAction: TextInputAction.next,
            focusNode: focusTel,
            focusNodeNext: focusNIN,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "National Insurance Number",
            input: nationalInsuranceNumber,
            kbType: TextInputType.text,
            inputAction: TextInputAction.next,
            focusNode: focusNIN,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          editProfileHelper != null
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        width: getW(context),
                        child: Txt(
                          txt: "Country of Residence",
                          txtColor: Colors.black,
                          fontWeight: FontWeight.bold,
                          txtSize: MyTheme.txtSize - .2,
                          isBold: false,
                          txtAlign: TextAlign.start,
                        )),
                    SizedBox(height: 10),
                    DropDownListDialog(
                      context: context,
                      title: editProfileHelper.optCountriesResidential.title,
                      ddTitleList:
                          editProfileHelper.ddMaritalCountriesResidential,
                      callback: (optionItem) {
                        editProfileHelper.optCountriesResidential = optionItem;
                        setState(() {});
                      },
                    ),
                  ],
                )
              : SizedBox(),
          SizedBox(height: 20),
          editProfileHelper != null
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        width: getW(context),
                        child: Txt(
                          txt: "Nationality",
                          txtColor: Colors.black,
                          fontWeight: FontWeight.bold,
                          txtSize: MyTheme.txtSize - .2,
                          isBold: false,
                          txtAlign: TextAlign.start,
                        )),
                    SizedBox(height: 10),
                    DropDownListDialog(
                      context: context,
                      title: editProfileHelper.optNationalities.title,
                      ddTitleList: editProfileHelper.ddMaritalNationalities,
                      callback: (optionItem) {
                        editProfileHelper.optNationalities = optionItem;
                        setState(() {});
                      },
                    ),
                  ],
                )
              : SizedBox(),
          drawsmoker(),
        ],
      ),
    );
  }

  drawVisaInfoView() {
    final DateTime dateNow = DateTime.now();
    final dateExplast = DateTime(dateNow.year + 50, dateNow.month, dateNow.day);
    final dateExpfirst = DateTime(dateNow.year, dateNow.month + 1, dateNow.day);
    return Container(
      //width: getW(context),
      child: Column(
        children: [
          Container(
            width: getW(context),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Txt(
                          txt: "Visa Information",
                          txtColor: MyTheme.titleColor,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: true),
                    ),
                    SizedBox(width: 5),
                    Container(
                      child:
                          Image.asset("assets/images/ico/checked_blue_ico.png"),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Container(color: Colors.black, height: 0.5)
              ],
            ),
          ),
          SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Passport Number",
            input: passport,
            kbType: TextInputType.name,
            inputAction: TextInputAction.next,
            focusNode: focusPP,
            focusNodeNext: focusRef,
            len: 50,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),
          SizedBox(height: 20),
          DatePickerView(
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
            cap: 'Passport Expiry Date',
            dt: (passportExp == '') ? 'Passport Expiry Date' : passportExp,
            initialDate: dateExpfirst,
            firstDate: dateExpfirst,
            lastDate: dateExplast,
            callback: (value) {
              if (mounted) {
                setState(() {
                  try {
                    passportExp =
                        DateFormat('dd-MMM-yyyy').format(value).toString();
                  } catch (e) {
                    myLog(e.toString());
                  }
                });
              }
            },
          ),
          /*SizedBox(height: 20),
          drawInputBox(
            context: context,
            title: "Reference Source",
            input: _refSource,
            kbType: TextInputType.text,
            inputAction: TextInputAction.done,
            focusNode: focusRef,
            len: 20,
            txtColor: Colors.black,
            fontWeight: FontWeight.bold,
          ),*/
          /*
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 20),
            child: DatePickerView(
              cap: 'Visa Expiry Date',
              dt: (visaExp == '') ? 'Visa Expiry Date' : visaExp,
              initialDate: dateExpfirst,
              firstDate: dateExpfirst,
              lastDate: dateExplast,
              callback: (value) {
                if (mounted) {
                  setState(() {
                    try {
                      visaExp =
                          DateFormat('dd-MM-yyyy').format(value).toString();
                    } catch (e) {
                      myLog(e.toString());
                    }
                  });
                }
              },
            ),
          ),*/
        ],
      ),
    );
  }

  drawsmoker() {
    return Padding(
      padding: const EdgeInsets.only(left: 10, top: 20),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Txt(
                txt: "Are you a smoker?",
                txtColor: Colors.black,
                fontWeight: FontWeight.bold,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.start,
                isBold: false),
            SizedBox(height: 5),
            Theme(
              data: MyTheme.radioThemeData,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          radiosmoker = smokerEnum.no;
                        });
                      }
                    },
                    child: Radio(
                      visualDensity: VisualDensity(horizontal: 0, vertical: 0),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      value: smokerEnum.no,
                      groupValue: radiosmoker,
                      onChanged: (smokerEnum value) {
                        if (mounted) {
                          setState(() {
                            radiosmoker = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "No",
                    txtColor: Colors.black,
                    fontWeight: FontWeight.w400,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                  SizedBox(width: 20),
                  InkWell(
                    onTap: () {
                      if (mounted) {
                        setState(() {
                          radiosmoker = smokerEnum.yes;
                        });
                      }
                    },
                    child: Radio(
                      visualDensity: VisualDensity(horizontal: 0, vertical: 0),
                      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      value: smokerEnum.yes,
                      groupValue: radiosmoker,
                      onChanged: (smokerEnum value) {
                        if (mounted) {
                          setState(() {
                            radiosmoker = value;
                          });
                        }
                      },
                    ),
                  ),
                  Txt(
                    txt: "Yes",
                    txtColor: Colors.black,
                    fontWeight: FontWeight.w400,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.center,
                    isBold: false,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
