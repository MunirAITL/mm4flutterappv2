import 'dart:convert';
import 'dart:io';

import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/PubNubCfg.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/config/utils/tokbox_cfg.dart';
import 'package:aitl/controller/api/misc/DeviceInfoAPIMgr.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/AppData.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/json/db_cus/badge_counter/GetNotiBadgeAPIModel.dart';
import 'package:aitl/view/auth/LoginLandingScreen.dart';
import 'package:aitl/view/db_cus/timeline/tokbox/video_call_page.dart';
import 'package:aitl/view/widgets/dialog/ConfirmationDialog.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/incoming/incoming_channel.dart';
import 'package:aitl/view_model/helper/incoming/incoming_handler.dart';
import 'package:aitl/view_model/rx/viewController.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/db_cus/more/MoreTab.dart';
import 'package:aitl/view/db_cus/my_cases/MyCaseTab.dart';
import 'package:aitl/view/db_cus/new_case/DashBoradScreen.dart';
import 'package:aitl/view/db_cus/noti/NotiTab.dart';
import 'package:aitl/view/db_cus/timeline/TimeLineTab.dart';
import 'package:aitl/view/widgets/dialog/HelpTutDialog.dart';
import 'package:aitl/view/widgets/tabs_nav/bottomNavigation.dart';
import 'package:aitl/view/widgets/tabs_nav/tabItem.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:aitl/Mixin.dart';
//import 'package:flutter_incoming_call/flutter_incoming_call.dart';
import 'package:pubnub/pubnub.dart';

//  next on https://pub.dev/packages/persistent_bottom_nav_bar

class MainCustomerScreen extends StatefulWidget {
  MainCustomerScreen();

  @override
  State createState() => MainCustomerScreenState();
}

class MainCustomerScreenState extends State<MainCustomerScreen>
    with Mixin, StateListener, TickerProviderStateMixin {
  static const platform = const MethodChannel('flutter.native/tokbox');
  var _androidAppRetain = MethodChannel("android_app_retain");

  final _scaffoldKey = GlobalKey<ScaffoldState>();

  StateProvider _stateProvider;
  bool isDialogHelpOpenned = false;
  static int currentTab = 0;
  var viewController = Get.put(ViewController());

  //  Convage OpenTok Incoming Start
  //IncomingCallHandler incomingCallHandler;

  /*BaseCallEvent _lastEvent;
  CallEvent _lastCallEvent;
  HoldEvent _lastHoldEvent;
  MuteEvent _lastMuteEvent;
  DmtfEvent _lastDmtfEvent;
  AudioSessionEvent _lastAudioSessionEvent;*/

  var pubnub;
  var myChannel;
  var isOnCall = false;

  /*initIncoming() async {
    final sender_id = userData.userModel.id.toString();
    pubnub = PubNub(
        defaultKeyset: Keyset(
            subscribeKey: PubNubCfg.subscribeKeyIncomingApp,
            publishKey: PubNubCfg.publishKeyIncomingApp,
            uuid: UUID(sender_id)));

    var subscription = pubnub.subscribe(channels: {sender_id});
    /*subscription.messages.take(1).listen((envelope) async {
      print(envelope.payload);
      //await subscription.dispose();
    });*/

    //await Future.delayed(Duration(seconds: 3));

    // Publish a message
    //await pubnub.publish(
    //receiver_id.toString(), {'message': 'My message BY flutter app1!'});

    // Channel abstraction for easier usage
    //var channel = pubnub.channel('test');

    //await channel.publish({'message': 'Another message BY flutter app2'});

    // Work with channel History API
    //var history = channel.messages();
    //var count = await history.count();

    //print('Messages on test channel: $count');

    subscription.messages.listen((envelope) async {
      print('sent a message full : ${envelope.payload}');
      startIncoming("");
    });
  }

  void startIncoming(message) async {
    print(message);
    //if (incomingCallHandler == null) {
    incomingCallHandler = IncomingCallHandler();
    await FlutterIncomingCall.configure(
        appName: AppDefine.APP_NAME,
        duration: 30000,
        android: ConfigAndroid(
          vibration: true,
          ringtonePath: 'default',
          channelId: 'calls',
          channelName: 'Calls channel name',
          channelDescription: 'Calls channel description',
        ),
        ios: ConfigIOS(
          iconName: 'AppIcon40x40',
          ringtonePath: null,
          includesCallsInRecents: false,
          supportsVideo: true,
          maximumCallGroups: 2,
          maximumCallsPerCallGroup: 1,
        ));
    FlutterIncomingCall.onEvent.listen((event) {
      print("incoming event = " + event.toString());
      setState(() {
        _lastEvent = event;
      });
      if (event is CallEvent) {
        //setState(() async {
        _lastCallEvent = event;
        if (event.action == CallAction.accept) {
          OpenTokConfig().set(
              "T1==cGFydG5lcl9pZD00NzQzNzg1MSZzaWc9NWMwNTAwMGVhNjFjMDliNjY0ODExYmVjYmU5NjljZGE3MTA0M2ZiZDpzZXNzaW9uX2lkPTJfTVg0ME56UXpOemcxTVg1LU1UWTBNek0zTVRReU56QXlOSDVaWXpZNFJUZDBRVVEwZEZwRVdDOVNlV0l2ZVhWSFNrOS1mZyZjcmVhdGVfdGltZT0xNjQzODAyMTA1Jm5vbmNlPTAuMzc4NTI4MDIwNzA3NTQ2NzYmcm9sZT1wdWJsaXNoZXImZXhwaXJlX3RpbWU9MTY0NjM5NDEwNCZpbml0aWFsX2xheW91dF9jbGFzc19saXN0PQ==");
          if (Platform.isIOS) {
            // iOS-specific code
            final param = {
              'apiKey': OpenTokConfig.API_KEY,
              'sessionId': OpenTokConfig.SESSION_ID,
              'token': OpenTokConfig.TOKEN
            };
            platform.invokeMethod('start', param);
          } else {
            Get.to(() => CallWidget());
          }
        }
        //});
      } else if (event is HoldEvent) {
        setState(() {
          _lastHoldEvent = event;
        });
      } else if (event is MuteEvent) {
        setState(() {
          _lastMuteEvent = event;
        });
      } else if (event is DmtfEvent) {
        setState(() {
          _lastDmtfEvent = event;
        });
      } else if (event is AudioSessionEvent) {
        setState(() {
          _lastAudioSessionEvent = event;
        });
      }
    });
    await incomingCallHandler.invoke(message: message);
    //}
  }*/

  //  Convage OpenTok Incoming End

  @override
  onStateChanged(ObserverState state) async {
    int tabIndex = 0;
    if (state == ObserverState.STATE_CHANGED_logout) {
      currentTab = 0;
      await CookieMgr().delCookiee();
      await DBMgr.shared.delTable("User");
      await PrefMgr.shared.setPrefStr("accessToken", null);
      await PrefMgr.shared.setPrefStr("refreshToken", null);
      Get.offAll(() => LoginLandingScreen());
    } else if (state == ObserverState.STATE_CHANGED_tabbar1) {
      tabIndex = 0;
      if (!isDialogHelpOpenned) {
        isDialogHelpOpenned = true;
        viewController.isOpened.value = true;
        Get.dialog(HelpTutDialog(),
                barrierDismissible: true, barrierColor: Colors.transparent)
            .then((value) {
          viewController.isOpened.value = false;
          setState(() {
            isDialogHelpOpenned = false;
            selectTab(tabIndex);
          });
        });
      }
      setState(() {
        selectTab(tabIndex);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar2) {
      tabIndex = 1;
      setState(() {
        currentTab = tabIndex;
        selectTab(currentTab);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar3) {
      tabIndex = 2;
      setState(() {
        currentTab = tabIndex;
        selectTab(currentTab);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar4) {
      tabIndex = 3;
      setState(() {
        currentTab = tabIndex;
        selectTab(currentTab);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar5) {
      tabIndex = 4;
      setState(() {
        currentTab = tabIndex;
        selectTab(currentTab);
      });
    } else if (state == ObserverState.STATE_CHANGED_tabbar6) {
      tabIndex = 5;
      setState(() {
        currentTab = tabIndex;
        selectTab(currentTab);
      });
    }
  }

  //  Tabbar stuff start here...
  final List<TabItem> tabsItem = [
    TabItem(
      tabName: "Dashboard",
      icon: AssetImage("assets/images/tabbar/new_case.png"),
      page: DashBoardScreen(),
    ),
    TabItem(
      tabName: "My Cases",
      icon: AssetImage("assets/images/tabbar/my_cases_icon.png"),
      page: MyCaseTab(),
    ),
    TabItem(
      tabName: "Messages",
      icon: AssetImage("assets/images/tabbar/msg_icon.png"),
      page: TimeLineTab(),
    ),
    TabItem(
      tabName: "Notifications",
      icon: AssetImage("assets/images/tabbar/noti_icon.png"),
      page: NotiTab(),
    ),
    TabItem(
      tabName: "More",
      icon: AssetImage("assets/images/tabbar/more_icon.png"),
      page: MoreTab(),
    )
  ];

  MainCustomerScreenState() {
    tabsItem.asMap().forEach((index, details) {
      details.setIndex(index);
    });
  }

  // sets current tab index
  // and update state
  selectTab(int index) {
    if (mounted) {
      switch (index) {
        case 0:
          StateProvider()
              .notify(ObserverState.STATE_CHANGED_tabbar1_reload_case_api);
          break;
        case 1:
          StateProvider()
              .notify(ObserverState.STATE_CHANGED_tabbar2_reload_case_api);
          wsBadgeCounterAPI(index);
          break;
        case 2:
          StateProvider()
              .notify(ObserverState.STATE_CHANGED_tabbar3_reload_case_api);
          wsBadgeCounterAPI(index);
          break;
        case 3:
          StateProvider()
              .notify(ObserverState.STATE_CHANGED_tabbar4_reload_case_api);
          wsBadgeCounterAPI(index);
          break;
        default:
      }

      if (index == currentTab) {
        // pop to first route
        // if the user taps on the active tab
        print("Current State 3 = " +
            MainCustomerScreenState.currentTab.toString());
        print("Current State 33 = " + index.toString());

        tabsItem[index].key.currentState.popUntil((route) => route.isFirst);
        //setState(() {});
      } else {
        // update the state
        // in order to repaint
        print("Current State 4 = " + index.toString());

        if (mounted) {
          setState(() => currentTab = index);
        }
      }
    }
  }

  wsBadgeCounterAPI(int tabIndex) async {
    try {
      await APIViewModel().req<GetBadgeCounterAPIModel>(
          context: context,
          url: Server.GET_BADGE_COUNTER_URL
              .replaceAll("#userId#", userData.userModel.id.toString()),
          isLoading: false,
          reqType: ReqType.Get,
          callback: (model) async {
            if (model != null && mounted) {
              //appData.taskNotificationCountAndChatUnreadCountData = model
              //.responseData.taskNotificationCountAndChatUnreadCountData;

              final taskNotificationCountAndChatUnreadCountData = model
                  .responseData.taskNotificationCountAndChatUnreadCountData;

              // tmp
              /*taskNotificationCountAndChatUnreadCountData
                  .numberOfUnReadNotification = 10;
              taskNotificationCountAndChatUnreadCountData
                  .numberOfUnReadMessage = 2;
              taskNotificationCountAndChatUnreadCountData.numberOfPendingTask =
                  30;*/

              final unreadNotificationCount =
                  await PrefMgr.shared.getPrefInt("NumberOfUnReadNotification");
              final unreadMessageCount =
                  await PrefMgr.shared.getPrefInt("NumberOfUnReadMessage");
              final unreadTaskCount =
                  await PrefMgr.shared.getPrefInt("NumberOfPendingTask");
              try {
                appData.taskNotificationCountAndChatUnreadCountData =
                    TaskNotificationCountAndChatUnreadCountData.fromJson({
                  "NumberOfUnReadMessage": unreadMessageCount ==
                          taskNotificationCountAndChatUnreadCountData
                              .numberOfUnReadMessage
                      ? 0
                      : taskNotificationCountAndChatUnreadCountData
                          .numberOfUnReadMessage,
                  "NumberOfUnReadNotification": unreadNotificationCount ==
                          taskNotificationCountAndChatUnreadCountData
                              .numberOfUnReadNotification
                      ? 0
                      : taskNotificationCountAndChatUnreadCountData
                          .numberOfUnReadNotification,
                  "NumberOfPendingTask": unreadTaskCount ==
                          taskNotificationCountAndChatUnreadCountData
                              .numberOfPendingTask
                      ? 0
                      : taskNotificationCountAndChatUnreadCountData
                          .numberOfPendingTask,
                });
              } catch (e) {}
              switch (tabIndex) {
                case 1:
                  StateProvider().notifyWithData(
                      ObserverState.STATE_BADGE_TASK_COUNT, tabIndex);
                  break;
                case 2:
                  StateProvider().notifyWithData(
                      ObserverState.STATE_BADGE_MESSAGE_COUNT, tabIndex);
                  break;
                case 3:
                  StateProvider().notifyWithData(
                      ObserverState.STATE_BADGE_NOTIFICATION_COUNT, tabIndex);
                  break;
                default:
              }

              StateProvider().notifyWithData(ObserverState.STATE_BOTNAV, null);
            }
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
    //initIncoming();
  }

  @mustCallSuper
  @override
  void dispose() {
    try {
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    /*if (incomingCallHandler != null) {
      incomingCallHandler.endAllCalls();
    }*/
    super.dispose();
  }

  appInit() async {
    try {
      //SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
      //SystemChrome.setSystemUIOverlayStyle(
      //SystemUiOverlayStyle(statusBarColor: MyTheme.themeData.accentColor));
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        statusBarColor: MyTheme.titleColor,
      ));
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);

      DeviceInfoAPIMgr().wsFcmDeviceInfo(
        context: context,
        callback: (model) {
          if (model != null && mounted) {
            try {
              if (model.success) {}
            } catch (e) {
              myLog(e.toString());
            }
          } else {
            myLog("main customer screen new not in");
          }
        },
      );
      wsBadgeCounterAPI(0);
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        confirmDialog(
            context: context,
            title: "Logout",
            msg: "Are you sure, do you want to log out from the app?",
            callbackYes: () {
              StateProvider().notify(ObserverState.STATE_CHANGED_logout);
            });
        return;
      },
      child: SafeArea(
        child: Scaffold(
          key: _scaffoldKey,
          resizeToAvoidBottomInset: false,
          backgroundColor: MyTheme.bgColor2,
          body: IndexedStack(
            index: currentTab,
            children: tabsItem.map((e) => e.page).toList(),
          ),
          // Bottom navigation
          bottomNavigationBar: BottomNavigation(
            context: context,
            onSelectTab: selectTab,
            tabs: tabsItem,
            isHelpTut: isDialogHelpOpenned,
            totalMsg: 0,
            totalNoti: 0,
          ),
        ),
      ),
    );
  }
}
