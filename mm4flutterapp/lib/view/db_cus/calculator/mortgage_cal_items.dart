import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/view/widgets/btn/Btn.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/PriceBox.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:aitl/view_model/rx/mortgage_cal.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class MortgageCalItems extends StatefulWidget {
  const MortgageCalItems({Key key}) : super(key: key);

  @override
  _MortgageCalItemsState createState() => _MortgageCalItemsState();
}

class _MortgageCalItemsState extends State<MortgageCalItems> with Mixin {
  final mortgageCalController = Get.put(MortgageCalController());

  RegExp reg = RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))');
  String Function(Match) mathFunc = (Match match) => '${match[1]},';

  List<bool> lsitShowMore = [];
  final listItems = [
    {
      "totalCost": "Lowest total cost1",
      "img":
          "https://leappakistan.com/wp-content/uploads/2017/02/Royal-Bank-of-Scotland-Logo.jpg",
      "monthly_repayment": 1079,
      "initial_rate": 1.64,
      "deal_period": "2 years",
      "cost_over_2years": 26514,
      "term": "25 years",
      "rate_after_deal_ends": 3.74,
      "lender_incentives": 750,
      "lenders_fee": 1025,
    },
    {
      "totalCost": "",
      "img":
          "https://www.natwestgroup.com/content/dam/natwestgroup_com/natwestgroup/images/new-images-feb-2021/445x275/image.dim.180.natwest.png",
      "monthly_repayment": 1079,
      "initial_rate": 1.64,
      "deal_period": "2 years",
      "cost_over_2years": 26514,
      "term": "25 years",
      "rate_after_deal_ends": 3.74,
      "lender_incentives": 750,
      "lenders_fee": 1025,
    },
    {
      "totalCost": "",
      "img":
          "https://financialit.net/sites/default/files/yorkshire_building_society.jpg",
      "monthly_repayment": 1079,
      "initial_rate": 1.64,
      "deal_period": "2 years",
      "cost_over_2years": 26514,
      "term": "25 years",
      "rate_after_deal_ends": 3.74,
      "lender_incentives": 750,
      "lenders_fee": 1025,
    },
    {
      "totalCost": "Lowest total cost4",
      "img": Server.MISSING_IMG,
      "monthly_repayment": 1079,
      "initial_rate": 1.64,
      "deal_period": "2 years",
      "cost_over_2years": 26514,
      "term": "25 years",
      "rate_after_deal_ends": 3.74,
      "lender_incentives": 750,
      "lenders_fee": 1025,
    },
    {
      "totalCost": "Lowest total cost5",
      "img": Server.MISSING_IMG,
      "monthly_repayment": 1079,
      "initial_rate": 1.64,
      "deal_period": "2 years",
      "cost_over_2years": 26514,
      "term": "25 years",
      "rate_after_deal_ends": "3.74%",
      "lender_incentives": 750,
      "lenders_fee": 1025,
    },
  ];

  @override
  void initState() {
    super.initState();
    initPage();
  }

  initPage() {
    for (final v in listItems) {
      lsitShowMore.add(false);
    }
    if (mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: 0,
          automaticallyImplyLeading: false,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          title: UIHelper().drawAppbarTitle(title: "Mortgage Calculator"),
          centerTitle: false,
        ),
        bottomNavigationBar: Padding(
          padding:
              const EdgeInsets.only(left: 22, right: 22, top: 10, bottom: 5),
          child: MMBtn(
              txt: "EDIT DETAILS",
              height: getHP(context, 7),
              width: getW(context),
              radius: 5,
              callback: () {
                Get.back();
              }),
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.center,
                child: Container(
                  width: getW(context),
                  color: MyTheme.bgColor,
                  child: Padding(
                    padding: const EdgeInsets.all(10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Txt(
                          txt: "You may borrow maximum of",
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.center,
                          isBold: false,
                        ),
                        Txt(
                          txt: getCurSign() +
                              mortgageCalController.maxLoanAmount.value
                                  .toStringAsFixed(0)
                                  .replaceAllMapped(reg, mathFunc),
                          txtColor: MyTheme.brandColor,
                          txtSize: MyTheme.txtSize + .3,
                          txtAlign: TextAlign.center,
                          isBold: true,
                        ),
                        Txt(
                          txt:
                              "This means, with your deposit,\nyou can get a home worth",
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.center,
                          isBold: false,
                        ),
                        Txt(
                          txt: getCurSign() +
                              mortgageCalController.homeAmount.value
                                  .toStringAsFixed(0)
                                  .replaceAllMapped(reg, mathFunc),
                          txtColor: Colors.white,
                          txtSize: MyTheme.txtSize + .1,
                          txtAlign: TextAlign.center,
                          fontWeight: FontWeight.w500,
                          isBold: true,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(height: 10),
              drawItems(),
            ],
          ),
        ),
      ),
    );
  }

  drawItems() {
    return lsitShowMore.length > 0
        ? Container(
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                    width: 4, color: MyTheme.bgColor.withOpacity(.5)),
                left: BorderSide(
                    width: 4, color: MyTheme.bgColor.withOpacity(.5)),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.only(top: 2, left: 5),
              child: ListView.builder(
                primary: false,
                shrinkWrap: true,
                itemCount: listItems.length,
                itemBuilder: (context, i) {
                  final map = listItems[i];
                  return Padding(
                    padding: const EdgeInsets.only(top: 5),
                    child: Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey,
                          style: BorderStyle.solid,
                          width: 1.0,
                        ),
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            map['totalCost'] != ''
                                ? Padding(
                                    padding: const EdgeInsets.only(bottom: 10),
                                    child: Container(
                                      color: Color(0xFFFFDB58),
                                      child: Padding(
                                        padding: const EdgeInsets.all(5),
                                        child: Txt(
                                          txt: map['totalCost'],
                                          txtColor: Colors.black,
                                          txtSize: MyTheme.txtSize - .6,
                                          txtAlign: TextAlign.start,
                                          isBold: false,
                                        ),
                                      ),
                                    ),
                                  )
                                : SizedBox(),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: getWP(context, 30),
                                  height: getWP(context, 10),
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: MyNetworkImage.loadProfileImage(
                                          map['img']),
                                      fit: BoxFit.fill,
                                    ),
                                    shape: BoxShape.rectangle,
                                  ),
                                ),
                                ElevatedButton(
                                  onPressed: () {},
                                  child: Text('Apply'),
                                  style: ElevatedButton.styleFrom(
                                      //primary: MyTheme.dBlueAirColor,
                                      shape: StadiumBorder()),
                                )
                              ],
                            ),
                            SizedBox(height: 10),
                            /*Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          drawRow(
                              "Monthly repayment:",
                              getCurSign() +
                                  map['monthly_repayment'].toString()),
                          drawRow("Initial rate:",
                              map['initial_rate'].toString() + "%"),
                          drawRow(
                              "Deal period:", map['deal_period'].toString()),
                          drawRow(
                              "Cost over 2 years:",
                              getCurSign() +
                                  map['cost_over_2years'].toString()),
                          drawRow("Term:", map['term'].toString()),
                        ],
                      ),*/
                            Table(
                              //border: TableBorder.all(),

                              children: [
                                TableRow(children: [
                                  Txt(
                                    txt: "Monthly repayment",
                                    txtColor: MyTheme.dBlueAirColor,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.start,
                                    isBold: false,
                                  ),
                                  Txt(
                                    txt: "Initial rate",
                                    txtColor: MyTheme.dBlueAirColor,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.center,
                                    isBold: false,
                                  ),
                                  Txt(
                                    txt: "Deal period",
                                    txtColor: MyTheme.dBlueAirColor,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.center,
                                    isBold: false,
                                  ),
                                  Txt(
                                    txt: "Cost over 2 years",
                                    txtColor: MyTheme.dBlueAirColor,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.end,
                                    isBold: false,
                                  ),
                                ]),
                                TableRow(children: [
                                  SizedBox(height: 5),
                                  SizedBox(height: 5),
                                  SizedBox(height: 5),
                                  SizedBox(height: 5),
                                ]),
                                TableRow(children: [
                                  Txt(
                                    txt: getCurSign() +
                                        map['monthly_repayment'].toString(),
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.start,
                                    isBold: true,
                                  ),
                                  Txt(
                                    txt: map['initial_rate'].toString() + "%",
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.center,
                                    isBold: true,
                                  ),
                                  Txt(
                                    txt: map['deal_period'].toString(),
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.center,
                                    isBold: true,
                                  ),
                                  Txt(
                                    txt: getCurSign() +
                                        map['cost_over_2years'].toString(),
                                    txtColor: Colors.black,
                                    txtSize: MyTheme.txtSize - .6,
                                    txtAlign: TextAlign.end,
                                    isBold: true,
                                  ),
                                ]),
                              ],
                            ),
                            SizedBox(height: 10),
                            Align(
                              alignment: Alignment.center,
                              child: Theme(
                                data: Theme.of(context).copyWith(
                                  unselectedWidgetColor:
                                      Colors.grey, // here for close state
                                  colorScheme: ColorScheme.light(
                                    primary: Colors.black,
                                  ), // here for open state in replacement of deprecated accentColor
                                  dividerColor: Colors
                                      .transparent, // if you want to remove the border
                                ),
                                child: Container(
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        !lsitShowMore[i]
                                            ? Container(
                                                color:
                                                    Colors.grey.withOpacity(.1),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          top: 5, bottom: 5),
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      lsitShowMore[i] =
                                                          !lsitShowMore[i];
                                                      setState(() {});
                                                    },
                                                    child: Row(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: [
                                                          Text('Show more',
                                                              textAlign:
                                                                  TextAlign
                                                                      .center,
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .black)),
                                                          Icon(
                                                            lsitShowMore[i]
                                                                ? Icons
                                                                    .arrow_drop_up
                                                                : Icons
                                                                    .arrow_drop_down,
                                                            color: Colors.black,
                                                          )
                                                        ]),
                                                  ),
                                                ),
                                              )
                                            : SizedBox(),
                                        (lsitShowMore[i])
                                            ? Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 20, bottom: 20),
                                                child: Table(
                                                  //border: TableBorder.all(),
                                                  children: [
                                                    TableRow(children: [
                                                      Txt(
                                                        txt: "Term",
                                                        txtColor: MyTheme
                                                            .dBlueAirColor,
                                                        txtSize:
                                                            MyTheme.txtSize -
                                                                .6,
                                                        txtAlign:
                                                            TextAlign.start,
                                                        isBold: false,
                                                      ),
                                                      Txt(
                                                        txt:
                                                            "Rate after deal ends",
                                                        txtColor: MyTheme
                                                            .dBlueAirColor,
                                                        txtSize:
                                                            MyTheme.txtSize -
                                                                .6,
                                                        txtAlign:
                                                            TextAlign.center,
                                                        isBold: false,
                                                      ),
                                                      Txt(
                                                        txt:
                                                            "Lender incentives",
                                                        txtColor: MyTheme
                                                            .dBlueAirColor,
                                                        txtSize:
                                                            MyTheme.txtSize -
                                                                .6,
                                                        txtAlign:
                                                            TextAlign.center,
                                                        isBold: false,
                                                      ),
                                                      Txt(
                                                        txt: "Lender's fees",
                                                        txtColor: MyTheme
                                                            .dBlueAirColor,
                                                        txtSize:
                                                            MyTheme.txtSize -
                                                                .6,
                                                        txtAlign: TextAlign.end,
                                                        isBold: false,
                                                      ),
                                                    ]),
                                                    TableRow(children: [
                                                      SizedBox(height: 5),
                                                      SizedBox(height: 5),
                                                      SizedBox(height: 5),
                                                      SizedBox(height: 5),
                                                    ]),
                                                    TableRow(children: [
                                                      Txt(
                                                        txt: map['term']
                                                            .toString(),
                                                        txtColor: Colors.black,
                                                        txtSize:
                                                            MyTheme.txtSize -
                                                                .6,
                                                        txtAlign:
                                                            TextAlign.start,
                                                        isBold: true,
                                                      ),
                                                      Txt(
                                                        txt: map['rate_after_deal_ends']
                                                                .toString() +
                                                            "%",
                                                        txtColor: Colors.black,
                                                        txtSize:
                                                            MyTheme.txtSize -
                                                                .6,
                                                        txtAlign:
                                                            TextAlign.center,
                                                        isBold: true,
                                                      ),
                                                      Txt(
                                                        txt: getCurSign() +
                                                            map['lender_incentives']
                                                                .toString(),
                                                        txtColor: Colors.black,
                                                        txtSize:
                                                            MyTheme.txtSize -
                                                                .6,
                                                        txtAlign:
                                                            TextAlign.center,
                                                        isBold: true,
                                                      ),
                                                      Txt(
                                                        txt: getCurSign() +
                                                            map['lenders_fee']
                                                                .toString(),
                                                        txtColor: Colors.black,
                                                        txtSize:
                                                            MyTheme.txtSize -
                                                                .6,
                                                        txtAlign: TextAlign.end,
                                                        isBold: true,
                                                      ),
                                                    ]),
                                                  ],
                                                ),
                                              )
                                            : SizedBox(),
                                        SizedBox(height: 10),
                                        (lsitShowMore[i])
                                            ? Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Txt(
                                                    txt:
                                                        "Early repayment charges",
                                                    txtColor:
                                                        MyTheme.dBlueAirColor,
                                                    txtSize:
                                                        MyTheme.txtSize - .4,
                                                    txtAlign: TextAlign.center,
                                                    isBold: false,
                                                  ),
                                                  SizedBox(height: 5),
                                                  Txt(
                                                    txt:
                                                        "1.5% until April 2023, then 0.75% until April 2024",
                                                    txtColor: Colors.black,
                                                    txtSize:
                                                        MyTheme.txtSize - .4,
                                                    txtAlign: TextAlign.center,
                                                    isBold: true,
                                                  ),
                                                  SizedBox(height: 20),
                                                  Text.rich(
                                                    TextSpan(
                                                      children: [
                                                        TextSpan(
                                                            text:
                                                                'Your rate of ',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black54,
                                                                fontSize: 12)),
                                                        TextSpan(
                                                          text: '1.81% ',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 12),
                                                        ),
                                                        TextSpan(
                                                            text:
                                                                "will last for the first 2 years of your mortgage (the 'initial period'). After that, you'll switch to the lender's standard variable rate (SVR). For Royal Bank of Scotland that's ",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black54,
                                                                fontSize: 12)),
                                                        TextSpan(
                                                          text: '3.74%. ',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 12),
                                                        ),
                                                        TextSpan(
                                                            text:
                                                                "Based on what you've told us, your loan to value (LTV) is ",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black54,
                                                                fontSize: 12)),
                                                        TextSpan(
                                                          text: '87.8%. ',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 12),
                                                        ),
                                                        TextSpan(
                                                            text:
                                                                "The true cost of this mortgage is ",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black54,
                                                                fontSize: 12)),
                                                        TextSpan(
                                                          text: getCurSign() +
                                                              "36,102.",
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 12),
                                                        ),
                                                        TextSpan(
                                                            text:
                                                                "That's the total amount you'll pay over the initial period of 2 years, including all fees and payments.",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .black54,
                                                                fontSize: 12)),
                                                      ],
                                                    ),
                                                  )
                                                ],
                                              )
                                            : SizedBox(),
                                        lsitShowMore[i]
                                            ? Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 20),
                                                child: Container(
                                                  color: Colors.grey
                                                      .withOpacity(.1),
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 5, bottom: 5),
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        lsitShowMore[i] =
                                                            !lsitShowMore[i];
                                                        setState(() {});
                                                      },
                                                      child: Row(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .center,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Text('Show less',
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .black)),
                                                            Icon(
                                                              lsitShowMore[i]
                                                                  ? Icons
                                                                      .arrow_drop_up
                                                                  : Icons
                                                                      .arrow_drop_down,
                                                              color:
                                                                  Colors.black,
                                                            )
                                                          ]),
                                                    ),
                                                  ),
                                                ),
                                              )
                                            : SizedBox(),
                                      ]),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          )
        : SizedBox();
  }

  drawRow(String title, String val) {
    return Card(
      margin: EdgeInsets.all(1),
      elevation: 1,
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Row(
          children: [
            Txt(
              txt: title,
              txtColor: MyTheme.dBlueAirColor,
              txtSize: MyTheme.txtSize - .6,
              txtAlign: TextAlign.start,
              isBold: true,
            ),
            SizedBox(width: 20),
            Txt(
              txt: val,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .6,
              txtAlign: TextAlign.start,
              isBold: false,
            ),
          ],
        ),
      ),
    );
  }
}
