import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/db_cus/NewCaseCfg.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/view/AnimatedListItem.dart';
import 'package:aitl/view/db_cus/new_case/PostNewCaseScreen.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/ui_helper.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewCaseTab extends StatefulWidget {
  @override
  State createState() => NewCaseTabState();
}

class NewCaseTabState extends State<NewCaseTab> with Mixin {
  List<dynamic> listUserNotesModel = [];

  List selectedItemList = [];

//  dropdown title
  DropListModel caseDD = DropListModel([]);
  OptionItem caseOpt = OptionItem(id: null, title: "Action Required");
  Size caseDDSize;

  final GlobalKey _ddKey = GlobalKey();
  List<GlobalKey> listExpansionTileKey = [];

  getSizeAndPosition() {
    RenderBox _cardBox = _ddKey.currentContext.findRenderObject();
    caseDDSize = _cardBox.size;
    myLog(caseDDSize.height.toString());
    setState(() {});
  }

  void _scrollToSelectedContent({GlobalKey expansionTileKey}) {
    final keyContext = expansionTileKey.currentContext;
    if (keyContext != null) {
      Future.delayed(Duration(milliseconds: 200)).then((value) {
        Scrollable.ensureVisible(keyContext,
            duration: Duration(milliseconds: 200));
      });
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  appInit() async {
    try {
      for (final c in NewCaseCfg.listCreateNewCase) {
        listExpansionTileKey.add(new GlobalKey());
      }
      setState(() {});
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.bgColor2,
        appBar: AppBar(
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: MyTheme.statusBarColor,
          elevation: 0,
          automaticallyImplyLeading: false,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.arrow_back)),
          title: UIHelper().drawAppbarTitle(title: "New case"),
          centerTitle: false,
        ),
        body: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onPanDown: (detail) {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: listExpansionTileKey.length > 0 ? drawLayout() : SizedBox(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
        child: SingleChildScrollView(
            primary: true,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ListView.builder(
                    shrinkWrap: true,
                    primary: false,
                    scrollDirection: Axis.vertical,
                    itemCount: NewCaseCfg.listCreateNewCase.length,
                    itemBuilder: (context, index) {
                      final map = NewCaseCfg.listCreateNewCase[index];
                      final icon = map["url"];
                      final title = map["title"];
                      List subTitleList = map["subItem"];
                      var widget = Card(
                        elevation: 2,
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10),
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10),
                          ),
                          child: ListTileTheme(
                            tileColor: MyTheme.lGrayColor,
                            contentPadding: EdgeInsets.only(right: 0),
                            child: ExpansionTile(
                              key: listExpansionTileKey[index],
                              trailing: SizedBox.shrink(),
                              initiallyExpanded: false,
                              title: Padding(
                                padding:
                                    const EdgeInsets.only(top: 5, bottom: 5),
                                child: new Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(width: 7),
                                    Flexible(
                                      child: Container(
                                        //width: width * (isDashboard ? 0.13 : 0.09),
                                        height: getWP(context, 10),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          gradient: LinearGradient(
                                              colors: [
                                                HexColor.fromHex("#2B4564"),
                                                HexColor.fromHex("#112B4A"),
                                              ],
                                              begin: const FractionalOffset(
                                                  0.0, 0.0),
                                              end: const FractionalOffset(
                                                  0.0, 0.0),
                                              stops: [0.0, 1.0],
                                              tileMode: TileMode.clamp),
                                        ),
                                        child: Image.asset(icon),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 5,
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Container(
                                            width: getH(context),
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 15.0),
                                              child: Txt(
                                                txt: title,
                                                txtColor: Colors.black,
                                                txtSize: MyTheme.txtSize,
                                                txtAlign: TextAlign.left,
                                                maxLines: 4,
                                                isBold: false,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Flexible(
                                      child: subTitleList.isEmpty
                                          ? Container(
                                              child: Icon(
                                              selectedItemList.contains(index)
                                                  ? Icons.arrow_forward
                                                  : Icons.arrow_forward,
                                              size: 25.0,
                                              color: Colors.black,
                                            ))
                                          : Container(
                                              //color: Colors.black,
                                              child: Icon(
                                              selectedItemList.contains(index)
                                                  ? Icons.arrow_drop_up
                                                  : Icons.arrow_drop_down,
                                              size: 35.0,
                                              color: Colors.black,
                                            )),
                                    ),
                                  ],
                                ),
                              ),
                              onExpansionChanged: (value) {
                                if (value && subTitleList.isNotEmpty) {
                                  _scrollToSelectedContent(
                                      expansionTileKey:
                                          listExpansionTileKey[index]);

                                  setState(() {
                                    selectedItemList.add(index);
                                  });
                                } else {
                                  if (selectedItemList.contains(index)) {
                                    setState(() {
                                      selectedItemList.remove(index);
                                    });
                                  }
                                }
                                if (subTitleList.isEmpty) {
                                  Get.off(() =>
                                      PostNewCaseScreen(indexCase: index));
                                }
                              },
                              children: [
                                ListView.builder(
                                  shrinkWrap: true,
                                  primary: false,
                                  itemCount: subTitleList.length,
                                  itemBuilder: (context, subIndex) {
                                    final subTitle =
                                        subTitleList[subIndex]["title"];
                                    var widgitSubItem = GestureDetector(
                                      onTap: () {
                                        Get.off(() => PostNewCaseScreen(
                                              indexCase: index,
                                              subIndexCase: subIndex,
                                            ));
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.only(
                                            left: 18.0, top: 10, bottom: 10),
                                        child: Container(
                                          color: Colors.white,
                                          width: getWP(context, 100),
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Container(
                                                    width: 5,
                                                    height: 5,
                                                    color: Colors.black,
                                                  ),
                                                  SizedBox(
                                                    width: 10,
                                                  ),
                                                  Container(
                                                    child: Txt(
                                                      txt: subTitle,
                                                      txtColor: Colors.black,
                                                      txtSize: MyTheme.txtSize,
                                                      txtAlign: TextAlign.start,
                                                      isBold: true,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              SizedBox(height: 10),
                                              Container(
                                                margin:
                                                    EdgeInsets.only(right: 5),
                                                width: getWP(context, 100),
                                                height: 1,
                                                color: Colors.black,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                    return AnimatedListItem(
                                        index: index,
                                        itemDesign: widgitSubItem);
                                  },
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                      return AnimatedListItem(index: index, itemDesign: widget);
                    },
                  )
                ])));
  }
}
