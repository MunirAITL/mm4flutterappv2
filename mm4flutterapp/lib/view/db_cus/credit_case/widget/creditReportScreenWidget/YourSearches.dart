import 'package:aitl/Mixin.dart';
import 'package:aitl/config/AppDefine.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDashBoardReport.dart';
import 'package:aitl/view/db_cus/credit_case/AlertDialog/AlertDialogCreditCardDetails.dart';
import 'package:aitl/view/db_cus/credit_case/CreditDashboardTabController.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class YourSeaches extends StatefulWidget {
  @override
  State<YourSeaches> createState() => _YourSeachesState();
}

class _YourSeachesState extends State<YourSeaches> with Mixin {
  CreditDashBoardReport creditDashBoardReport;

  @override
  Widget build(BuildContext context) {
    creditDashBoardReport =
        CreditDashBoardTabControllerState.creditDashBoardReport;

    return Container(
      child: Column(
        children: [
          SizedBox(height: 15),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: getW(context),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Txt(
                        txt: "COMPANY",
                        txtColor: Colors.black87,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: true),
                  ),
                  Flexible(
                    child: Txt(
                        txt: "PURPOSE",
                        txtColor: Colors.black87,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: true),
                  ),
                  Flexible(
                    child: Txt(
                        txt: "DATE",
                        txtColor: Colors.black87,
                        txtSize: MyTheme.txtSize - .3,
                        txtAlign: TextAlign.center,
                        isBold: true),
                  ),
                  Flexible(child: SizedBox(width: getWP(context, 5))),
                ],
              ),
            ),
          ),
          drawAccountTypeItems(
              ["CurrentAddressUserSearches", "PreviousAddressUserSearches"],
              "Searches on your current address"),
        ],
      ),
    );
  }

  drawAccountTypeItems(List<String> listAccountType, String title) {
    if (creditDashBoardReport == null) return SizedBox();
    return Container(
      child: Column(
        children: [
          Container(
            width: getW(context),
            decoration: BoxDecoration(color: Colors.grey),
            child: Padding(
              padding: const EdgeInsets.all(5),
              child: Txt(
                  txt: title,
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .2,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ),
          ),
          ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            itemCount: creditDashBoardReport.cRSearchRecordList.length,
            itemBuilder: (context, index) {
              final model = creditDashBoardReport.cRSearchRecordList[index];

              bool isReturn = false;
              if (!listAccountType.contains(model.searchHistoryType)) {
                isReturn = true;
              }

              return (isReturn)
                  ? SizedBox()
                  : Container(
                      margin: EdgeInsets.only(top: 5, bottom: 5),
                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black87),
                          borderRadius: BorderRadius.all(
                            Radius.circular(7),
                          )),
                      child: ExpansionTile(
                        title: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Txt(
                                  txt: model.company ?? '',
                                  txtColor: Colors.black87,
                                  txtSize: MyTheme.txtSize - .4,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                            Flexible(
                              child: Txt(
                                  txt: model.searchPurpose ?? '',
                                  txtColor: Colors.black87,
                                  txtSize: MyTheme.txtSize - .3,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                            Flexible(
                              child: Txt(
                                  txt: DateFun.getDate(
                                      model.creationDate, "dd-MMM-yyyy"),
                                  txtColor: Colors.black87,
                                  txtSize: MyTheme.txtSize - .3,
                                  txtAlign: TextAlign.center,
                                  isBold: false),
                            ),
                          ],
                        ),
                        iconColor: Colors.black,
                        collapsedIconColor: Colors.black,
                        children: <Widget>[
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Container(
                              child: Column(
                                children: [],
                              ),
                            ),
                          )
                        ],
                      ),
                    );
            },
          ),
        ],
      ),
    );
  }

  userInfoItem({String title, String value}) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Row(
        children: [
          Txt(
              txt: "$title: ",
              txtColor: Colors.black87,
              txtSize: MyTheme.txtSize - .4,
              txtAlign: TextAlign.start,
              isBold: true),
          Expanded(
              child: Txt(
                  txt: "$value",
                  txtColor: Colors.black87,
                  txtSize: MyTheme.txtSize - .4,
                  txtAlign: TextAlign.start,
                  isBold: false))
        ],
      ),
    );
  }
}
