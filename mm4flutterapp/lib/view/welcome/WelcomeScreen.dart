import 'dart:async';

import 'package:aitl/Mixin.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/CookieMgr.dart';
import 'package:aitl/controller/observer/StateProvider.dart';
import 'package:aitl/model/data/AppData.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/model/db/DBMgr.dart';
import 'package:aitl/view/NoAccess.dart';
import 'package:aitl/view/auth/LoginLandingScreen.dart';
import 'package:aitl/view/auth/company_accounts/comp_acc_page.dart';
import 'package:aitl/view/auth/email/signin_page.dart';
import 'package:aitl/view/db_cus/MainCustomerScreen.dart';
import 'package:aitl/view/db_cus/more/settings/CaseAlertScreen.dart';
import 'package:cookie_jar/cookie_jar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

class WelcomeScreen extends StatefulWidget {
  @override
  State createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen>
    with Mixin, StateListener {
  bool isDoneCookieCheck = false;

  @override
  void onStateChanged(ObserverState state) async {
    try {} catch (e) {}
  }

  @override
  void onStateChangedWithData(ObserverState state, data) async {
    try {
      if (state == ObserverState.STATE_HREF_LOGIN) {
        //print(data);
        if (data == null) {
          appInit();
        } else {
          Get.to(() => SigninPage(hrefLoginData: data))
              .then((value) => appInit());
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
    /*final controller = FlutterMethodChannel().stringStream;
    controller.listen((data) {
      if (data == null) {
        //appInit();
      } else {
        Get.to(() => SigninPage(hrefLoginData: data))
            .then((value) => appInit());
      }
    });*/
  }

  //@mustCallSuper
  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    try {
      SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
      SystemChrome.setSystemUIOverlayStyle(
          SystemUiOverlayStyle(statusBarColor: MyTheme.brandColor));

      //  apns
      //  https://console.firebase.google.com/project/_/notification
      //  https://medium.com/comerge/implementing-push-notifications-in-flutter-apps-aef98451e8f1
      /*  final pushNotificationService = PushNotificationService(_firebaseMessaging);
      await pushNotificationService.initialise(callback: (enumFCM which, Map<String, dynamic> message) {
        switch (which) {
          case enumFCM.onMessage:
            fcmClickNoti(message);
            break;
          case enumFCM.onLaunch:
            break;
          case enumFCM.onResume:
            break;
          default:
        }
      });*/

      try {
        Future.delayed(Duration.zero, () {
          appData.getUDID(context);
        });

        //  cookie
        CookieJar cj = await CookieMgr().getCookiee();
        final listCookies = await cj.loadForRequest(Uri.parse(Server.BASE_URL));
        if (listCookies.length > 0 &&
            await DBMgr.shared.getTotalRow("User") > 0) {
          await userData.setUserModel();
          if (!Server.isOtp) {
            if (userData.userModel.communityID.toString() == "1") {
              //final isCompanyAccount =
              //await PrefMgr.shared.getPrefBool("isCompanyAccount");
              /*if (!isCompanyAccount) {
                //TEST
                Get.offAll(
                  () => CompAccScreen(),
                ).then((value) {
                  setState(() {
                    isDoneCookieCheck = true;
                  });
                });
              } else {*/
              Get.offAll(
                () => MainCustomerScreen(),
              ).then((value) {
                setState(() {
                  isDoneCookieCheck = true;
                });
              });
              //}
            } else {
              Get.to(() => NoAccess());
            }
          } //else if (userData.userModel.isEmailVerified) {
          else {
            //  live
            if (userData.userModel.communityID.toString() == "1") {
              //final isCompanyAccount =
              //await PrefMgr.shared.getPrefBool("isCompanyAccount");
              //if (!isCompanyAccount) {
              //Get.offAll(() => CompAccScreen());
              //} else {
              Get.offAll(() => MainCustomerScreen());
              //}
            } else {
              Get.to(() => NoAccess());
            }
          }
          //} else {
          //setState(() {
          //isDoneCookieCheck = true;
          //});
          //
        } else {
          setState(() {
            isDoneCookieCheck = true;
          });
        }
      } catch (e) {
        myLog(e.toString());
      }
    } catch (e) {}
  }

  fcmClickNoti(Map<String, dynamic> message) async {
    try {
      if (mounted) {
        if (userData.userModel.communityID.toString() == "1") {
          Get.to(() => CaseAlertScreen(
                message: message,
              )).then((value) async {
            await userData.setUserModel();
            Get.offAll(
              () => MainCustomerScreen(),
            ).then((value) {
              setState(() {
                isDoneCookieCheck = true;
              });
            });
          });
        } else {
          Get.to(() => NoAccess());
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: MyTheme.bgColor2,
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: (!isDoneCookieCheck)
                ? Container(
                    color: MyTheme.bgColor2,
                  )
                : callLoginPage(),
            /* : Container(
                    //decoration: MyTheme.boxDeco,
                    height: getH(context),

                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          drawTopLogo(),
                          drawCenterImage(),
                          drawMMText(),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 40, right: 40, top: 5),
                            child: Column(
                              children: [
                                MMBtn(
                                    txt: "Login",
                                    height: getHP(context, 6),
                                    width: getW(context),
                                    callback: () {
                                      navTo(
                                          context: context,
                                          page: () => LoginScreen());
                                    }),
                                SizedBox(height: 10,),
                                Txt(txt: "Or", txtColor: Colors.black,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.start,
                                  isBold: false,),
                                SizedBox(height: 10,),
                                MMBtn(
                                    txt: "Signup",
                                    height: getHP(context, 6),
                                    width: getW(context),
                                    callback: () {
                                      navTo(
                                          context: context,
                                          page: () => RegScreen());
                                    }),
                              ],
                            ),
                          ),
                          SizedBox(height: 20),
                        ],
                      ),
                    ),
                  ),*/
          )),
    );
  }

  callLoginPage() {
    Timer(Duration(seconds: 1), () {
      try {
        Get.off(() => LoginLandingScreen());
      } catch (e) {
        debugPrint("Welcome screen Error catch ");
      }
      // ignore: unnecessary_statements
    });

    return Container(
      child: Center(
        //color: MyTheme.themeData.accentColor,
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Container(
              //width: getWP(context, 70),
              child: Image.asset(
            'assets/images/logo/mm.png',
            fit: BoxFit.cover,
            width: getWP(context, 70),
          )),
        ),
      ),
    );
  }
}
