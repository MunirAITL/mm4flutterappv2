import 'package:aitl/model/json/web2native/case/misc/caseusertype/CaseTaskBiddings.dart';
import 'package:get/get.dart';

class CaseUserTypeCtrl extends GetxController {
  var listCaseTaskBiddings = List<CaseTaskBiddings>().obs;
}
