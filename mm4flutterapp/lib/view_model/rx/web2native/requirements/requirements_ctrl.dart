import 'package:aitl/model/json/web2native/case/requirements/equity_release/MortgageEquityReleasePowerOfAttorneyModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/gen_insurance/MortgageBuildingAndContentInsurance.dart';
import 'package:aitl/model/json/web2native/case/requirements/gen_insurance/MortgageBuildingAndContentInsuranceItem.dart';
import 'package:aitl/model/json/web2native/case/requirements/mortgage/MortgageUserRequirementModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/remortgage/reasons/MortgageLoanReasonsModel.dart';
import 'package:get/get.dart';

class RequirementsCtrl extends GetxController {
  //  Requirements Step 1:
  var requirementsModelAPIModel = MortgageUserRequirementModel().obs;
  var listMortgageReasons = List<MortgageLoanReasonsModel>().obs;
  var mortgageEquityReleasePowerOfAttorney =
      MortgageEquityReleasePowerOfAttorneyModel().obs;
  var mortgageBuildingAndContentInsurance =
      MortgageBuildingAndContentInsurance().obs;
  var listInsuranceItems = List<MortgageBuildingAndContentInsuranceItem>()
      .obs; //  general insurance p-> personal procession
}
