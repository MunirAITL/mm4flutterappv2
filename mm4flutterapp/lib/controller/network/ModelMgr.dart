import 'package:aitl/controller/api/db_cus/noti/DeleteNotification.dart';
import 'package:aitl/model/json/auth/ForgotAPIModel.dart';
import 'package:aitl/model/json/auth/LoginAPIModel.dart';
import 'package:aitl/model/json/auth/RegAPIModel.dart';
import 'package:aitl/model/json/auth/email/VerifyEmailAPIModel.dart';
import 'package:aitl/model/json/auth/hreflogin/LoginAccExistsByEmailApiModel.dart';
import 'package:aitl/model/json/auth/hreflogin/PostEmailOtpAPIModel.dart';
import 'package:aitl/model/json/auth/hreflogin/SendUserEmailOtpAPIModel.dart';
import 'package:aitl/model/json/auth/multiple_applicants/MultipleApplicantAPIModel.dart';
import 'package:aitl/model/json/auth/otp/LoginRegOtpFBAPIModel.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOtpPostAPIModel.dart';
import 'package:aitl/model/json/auth/otp/MobileUserOtpPutAPIModel.dart';
import 'package:aitl/model/json/auth/otp/SendOtpNotiAPIModel.dart';
import 'package:aitl/model/json/db_cus/action_req/GetMortgageCaseInfoLocTaskidUseridAPIModel.dart';
import 'package:aitl/model/json/db_cus/badge_counter/GetNotiBadgeAPIModel.dart';
import 'package:aitl/model/json/db_cus/case_lead/CaseLeadNegotiatorUserAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDertailsAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditInfoPostAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/GetSimulatedScoreUseridAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/GetUserValidationOldUserApiModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/PostKbaCrditQAAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/getKbaQuestionAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/getSummeryAPIModel.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostDocByBase64BitDataAPIModel.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostDocVerifyAPIModel.dart';
import 'package:aitl/model/json/db_cus/doc_scan/PostSelfieByBase64DataAPIModel.dart';
import 'package:aitl/model/json/db_cus/open_banking/GetForeCasts4PersonalAccountsAPIModel.dart';
import 'package:aitl/model/json/db_cus/open_banking/GetInsight4PersonnelAccountsAPIModel.dart';
import 'package:aitl/model/json/db_cus/review_rating/ReviewRatingAPIModel.dart';
import 'package:aitl/model/json/db_cus/review_rating/ReviewRatingPostAPIModel.dart';
import 'package:aitl/model/json/db_cus/submit_case/SubmitCaseAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_dash/PrivacyPolicyAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgeAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgeEmailAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgePhotoIDAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/badge/BadgePhotoIDDelAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/edit_profile/DeactivateProfileAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/edit_profile/UserProfileAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/noti/FcmTestNotiAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/noti/NotiSettingsAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/noti/NotiSettingsPostAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/settings/settings/ChangePwdAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_more/support/ResolutionAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_mycases/TaskInfoSearchAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/CaseInfosAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/CaseReviewAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/EditCaseAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/PostCaseAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNoteByEntityAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_newcase/UserNotePutAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_noti/NotiAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/GetCaseGroupChatAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/MessageTypeAdvisorAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TaskBiddingAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLineAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLineAdvisorAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLinePostAPIModel.dart';
import 'package:aitl/model/json/db_intr/caselead/CasePayInfoAPIModel.dart';
import 'package:aitl/model/json/db_intr/caselead/GetlLeadStatusIntrwiseReportDatabyIntrAPIModel.dart';
import 'package:aitl/model/json/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/model/json/misc/CommonAPIModel.dart';
import 'package:aitl/model/json/misc/FcmDeviceInfoAPIModel.dart';
import 'package:aitl/model/json/web2native/action_req/GetClientAgreementAcceptedEmailAPIModel.dart';
import 'package:aitl/model/json/web2native/action_req/GetMortCaseInfoByTaskIdAPIModel.dart';
import 'package:aitl/model/json/web2native/action_req/PostClientAgreementAcceptAPIModel.dart';
import 'package:aitl/model/json/web2native/case/application/GetMortCasePaymentAPIModel.dart';
import 'package:aitl/model/json/web2native/case/doc/GetCaseDocAPIModel.dart';
import 'package:aitl/model/json/web2native/case/doc/PostCaseDocAPIModel.dart';
import 'package:aitl/model/json/web2native/case/misc/caseusertype/GetCaseUserTypeAPIModel.dart';
import 'package:aitl/model/json/web2native/case/misc/caseusertype/PutIntroPermissionAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/10cr_commit/GetCreditCommitAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/10cr_commit/PostCreditCommitAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/GetCreditHisAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/GetCreditHisItemsAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/PostCreditHisAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/11cr_history/PostCreditHisItemsAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/GetAssessAffordAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/GetAssessAffordItemsAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/GetTaskAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/PostAssessAffordAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/12access_afford/PostAssessAffordItemAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/13btl_portfolio/GetBtlPortfolioAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/13btl_portfolio/PostBtlPortfolioAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/GetContactAutoSugAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/GetES1ApiModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/GetES2APIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/GetESBnkAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/PostContactAutoSugAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/PostES1APIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/PostES2APIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/2essentials/PostESBnkAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/4address_history/GetAddrHistoryAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/4address_history/PostAddHistoryAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/5dependents/GetDependentsAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/5dependents/PostDependentsAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/GetEmpAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/GetIncomeAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/PostEmpAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/7emp/PostIncomeAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/8existing_policy/GetExistingPolicyAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/8existing_policy/PostExistingPolicyAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/9saving_investment/GetSavingInvAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/9saving_investment/PostSavingInvAPIModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/cur_res_mort/GetCurResMortApiModel.dart';
import 'package:aitl/model/json/web2native/case/requirements/cur_res_mort/PostCurResMortAPIModel.dart';

import '../../model/json/db_cus/action_req/GetMortgageCaseInfoTaskIdAPIModel.dart';
import '../../model/json/web2native/case/requirements/equity_release/GetEquityReleasePowerAttorneyAPIModel.dart';
import '../../model/json/web2native/case/requirements/gen_insurance/GenInsAPIModel.dart';
import '../../model/json/web2native/case/requirements/gen_insurance/GetGenInsRequirementsAPIModel.dart';
import '../../model/json/web2native/case/requirements/gen_insurance/GetPersonalProcessionAPIModel.dart';
import '../../model/json/web2native/case/requirements/gen_insurance/PostPersonalProcessionAPIModel.dart';
import '../../model/json/web2native/case/requirements/mortgage/GetMortgageRequirementsAPIModel.dart';
import '../../model/json/web2native/case/requirements/mortgage/MortgageRequirementsAPIModel.dart';
import '../../model/json/web2native/case/requirements/remortgage/reasons/GetMortgageLoanReasonsAPIModel.dart';
import '../../model/json/web2native/case/requirements/remortgage/reasons/PostMortgageLoandReasonAPIModel.dart';

//  https://stackoverflow.com/questions/56271651/how-to-pass-a-generic-type-as-a-parameter-to-a-future-in-flutter
class ModelMgr {
  final listModels = [
    FcmDeviceInfoAPIModel,
    CommonAPIModel,
    LoginAPIModel,
    MobileUserOtpPostAPIModel,
    SendOtpNotiAPIModel,
    MobileUserOtpPutAPIModel,
    VerifyEmailAPIModel,
    LoginRegOtpFBAPIModel,
    ForgotAPIModel,
    ChangePwdAPIModel,
    RegAPIModel,
    DeactivateProfileAPIModel,
    UserProfileAPIModel,
    UserNoteByEntityAPIModel,
    UserNotePutAPIModel,
    TaskInfoSearchAPIModel,
    PostCaseAPIModel,
    EditCaseAPIModel,
    NotiAPIModel,
    GetCaseGroupChatAPIModel,
    TimeLineAdvisorAPIModel,
    TimeLineAPIModel,
    TimeLinePostAPIModel,
    TaskBiddingAPIModel,
    MediaUploadFilesAPIModel,
    ResolutionAPIModel,
    NotiSettingsAPIModel,
    NotiSettingsPostAPIModel,
    FcmTestNotiAPIModel,
    GetMortgageCaseInfoLocTaskidUseridAPIModel,
    BadgeAPIModel,
    BadgeEmailAPIModel,
    BadgePhotoIDAPIModel,
    BadgePhotoIDDelAPIModel,
    DeleteNotificationModel,
    CaseReviewAPIModel,
    MessageTypeAdvisorAPIModel,
    CreditDetailsAPIModel,
    GetUserValidationOldUserApiModel,
    CreditInfoPostAPIModel,
    GetKbaQuestionAPIModel,
    PostKbaCrditQAAPIModel,
    GetSummeryAPIModel,
    GetSimulatedScoreUseridAPIModel,
//  open banking
    GetInsight4PersonnelAccountsAPIModel,
    GetForeCasts4PersonalAccountsAPIModel,
//  db intr
    SubmitCaseAPIModel,
    CaseLeadNegotiatorUserAPIModel,
    CasePayInfoAPIModel,
    GetlLeadStatusIntrwiseReportDatabyIntrAPIModel,
    CaseInfosApiModel,
    PrivacyPolicyAPIModel,
//  eid
    PostSelfieByBase64DataAPIModel,
    PostDocByBase64BitAPIModel,
    PostDocVerifyAPIModel,
//  login by email
    LoginAccExistsByEmailApiModel,
    PostEmailOtpAPIModel,
    SendUserEmailOtpAPIModel,
//  review and rating
    ReviewRatingAPIModel,
    ReviewRatingPostAPIModel,
  ];

  /// If T is a List, K is the subtype of the list.
  Future<T> fromJson<T, K>(dynamic json) async {
    if (identical(T, FcmDeviceInfoAPIModel)) {
      return FcmDeviceInfoAPIModel.fromJson(json) as T;
    } else if (identical(T, CommonAPIModel)) {
      return CommonAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginAPIModel)) {
      return LoginAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPostAPIModel)) {
      return MobileUserOtpPostAPIModel.fromJson(json) as T;
    } else if (identical(T, SendOtpNotiAPIModel)) {
      return SendOtpNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, MobileUserOtpPutAPIModel)) {
      return MobileUserOtpPutAPIModel.fromJson(json) as T;
    } else if (identical(T, VerifyEmailAPIModel)) {
      return VerifyEmailAPIModel.fromJson(json) as T;
    } else if (identical(T, LoginRegOtpFBAPIModel)) {
      return LoginRegOtpFBAPIModel.fromJson(json) as T;
    } else if (identical(T, ForgotAPIModel)) {
      return ForgotAPIModel.fromJson(json) as T;
    } else if (identical(T, ChangePwdAPIModel)) {
      return ChangePwdAPIModel.fromJson(json) as T;
    } else if (identical(T, RegAPIModel)) {
      return RegAPIModel.fromJson(json) as T;
    } else if (identical(T, DeactivateProfileAPIModel)) {
      return DeactivateProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserProfileAPIModel)) {
      return UserProfileAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNoteByEntityAPIModel)) {
      return UserNoteByEntityAPIModel.fromJson(json) as T;
    } else if (identical(T, UserNotePutAPIModel)) {
      return UserNotePutAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskInfoSearchAPIModel)) {
      return TaskInfoSearchAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCaseAPIModel)) {
      return PostCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, EditCaseAPIModel)) {
      return EditCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiAPIModel)) {
      return NotiAPIModel.fromJson(json) as T;
    } else if (identical(T, GetCaseGroupChatAPIModel)) {
      return GetCaseGroupChatAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAdvisorAPIModel)) {
      return TimeLineAdvisorAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLineAPIModel)) {
      return TimeLineAPIModel.fromJson(json) as T;
    } else if (identical(T, TimeLinePostAPIModel)) {
      return TimeLinePostAPIModel.fromJson(json) as T;
    } else if (identical(T, TaskBiddingAPIModel)) {
      return TaskBiddingAPIModel.fromJson(json) as T;
    } else if (identical(T, MediaUploadFilesAPIModel)) {
      return MediaUploadFilesAPIModel.fromJson(json) as T;
    } else if (identical(T, ResolutionAPIModel)) {
      return ResolutionAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsAPIModel)) {
      return NotiSettingsAPIModel.fromJson(json) as T;
    } else if (identical(T, NotiSettingsPostAPIModel)) {
      return NotiSettingsPostAPIModel.fromJson(json) as T;
    } else if (identical(T, FcmTestNotiAPIModel)) {
      return FcmTestNotiAPIModel.fromJson(json) as T;
    } else if (identical(T, GetMortgageCaseInfoLocTaskidUseridAPIModel)) {
      return GetMortgageCaseInfoLocTaskidUseridAPIModel.fromJson(json) as T;
    } else if (identical(T, GetMortgageCaseInfoTaskIdAPIModel)) {
      return GetMortgageCaseInfoTaskIdAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgeAPIModel)) {
      return BadgeAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgeEmailAPIModel)) {
      return BadgeEmailAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgePhotoIDAPIModel)) {
      return BadgePhotoIDAPIModel.fromJson(json) as T;
    } else if (identical(T, BadgePhotoIDDelAPIModel)) {
      // return BadgePhotoIDDelAPIModel.fromJson(json) as T;
    } else if (identical(T, DeleteNotificationModel)) {
      return DeleteNotificationModel.fromJson(json) as T;
    } else if (identical(T, CaseReviewAPIModel)) {
      return CaseReviewAPIModel.fromJson(json) as T;
    } else if (identical(T, MessageTypeAdvisorAPIModel)) {
      return MessageTypeAdvisorAPIModel.fromJson(json) as T;
    } else if (identical(T, MultipleApplicantAPIModel)) {
      return MultipleApplicantAPIModel.fromJson(json) as T;
    }

    //  Credit Report
    else if (identical(T, CreditDetailsAPIModel)) {
      return CreditDetailsAPIModel.fromJson(json) as T;
    } else if (identical(T, GetUserValidationOldUserApiModel)) {
      return GetUserValidationOldUserApiModel.fromJson(json) as T;
    } else if (identical(T, CreditInfoPostAPIModel)) {
      return CreditInfoPostAPIModel.fromJson(json) as T;
    } else if (identical(T, GetKbaQuestionAPIModel)) {
      return GetKbaQuestionAPIModel.fromJson(json) as T;
    } else if (identical(T, PostKbaCrditQAAPIModel)) {
      return PostKbaCrditQAAPIModel.fromJson(json) as T;
    } else if (identical(T, GetSummeryAPIModel)) {
      return GetSummeryAPIModel.fromJson(json) as T;
    } else if (identical(T, GetSimulatedScoreUseridAPIModel)) {
      return GetSimulatedScoreUseridAPIModel.fromJson(json) as T;
    }

    //  Open Banking
    else if (identical(T, GetInsight4PersonnelAccountsAPIModel)) {
      return GetInsight4PersonnelAccountsAPIModel.fromJson(json) as T;
    } else if (identical(T, GetForeCasts4PersonalAccountsAPIModel)) {
      return GetForeCasts4PersonalAccountsAPIModel.fromJson(json) as T;
    }

    //  ********************************************  DB_INTR
    else if (identical(T, SubmitCaseAPIModel)) {
      return SubmitCaseAPIModel.fromJson(json) as T;
    } else if (identical(T, CaseLeadNegotiatorUserAPIModel)) {
      return CaseLeadNegotiatorUserAPIModel.fromJson(json) as T;
    } else if (identical(T, CasePayInfoAPIModel)) {
      return CasePayInfoAPIModel.fromJson(json) as T;
    } else if (identical(T, GetlLeadStatusIntrwiseReportDatabyIntrAPIModel)) {
      return GetlLeadStatusIntrwiseReportDatabyIntrAPIModel.fromJson(json) as T;
    } else if (identical(T, CaseInfosApiModel)) {
      return CaseInfosApiModel.fromJson(json) as T;
    } else if (identical(T, PrivacyPolicyAPIModel)) {
      return PrivacyPolicyAPIModel.fromJson(json) as T;
    } else if (identical(T, PostSelfieByBase64DataAPIModel)) {
      return PostSelfieByBase64DataAPIModel.fromJson(json) as T;
    } else if (identical(T, PostDocByBase64BitAPIModel)) {
      return PostDocByBase64BitAPIModel.fromJson(json) as T;
    } else if (identical(T, PostDocVerifyAPIModel)) {
      return PostDocVerifyAPIModel.fromJson(json) as T;
    }

    //  ******************************************* HREF LOGIN BY EMAIL
    else if (identical(T, LoginAccExistsByEmailApiModel)) {
      return LoginAccExistsByEmailApiModel.fromJson(json) as T;
    } else if (identical(T, PostEmailOtpAPIModel)) {
      return PostEmailOtpAPIModel.fromJson(json) as T;
    } else if (identical(T, SendUserEmailOtpAPIModel)) {
      return SendUserEmailOtpAPIModel.fromJson(json) as T;
    }
    //  ******************************************  REVIEW RATING
    else if (identical(T, ReviewRatingAPIModel)) {
      return ReviewRatingAPIModel.fromJson(json) as T;
    } else if (identical(T, ReviewRatingPostAPIModel)) {
      return ReviewRatingPostAPIModel.fromJson(json) as T;
    }

    //  Noti badge
    else if (identical(T, GetBadgeCounterAPIModel)) {
      return GetBadgeCounterAPIModel.fromJson(json) as T;
    }

    //  ******************************************* Web2NativeCase

    //  //  Case User Type -> Advisor, Introducer and Admin
    else if (identical(T, GetCaseUserTypeAPIModel)) {
      return GetCaseUserTypeAPIModel.fromJson(json) as T;
    } else if (identical(T, PutIntroPermissionAPIModel)) {
      return PutIntroPermissionAPIModel.fromJson(json) as T;
    }

    //  Requirements -> Step 1: Cases
    else if (identical(T, MortgageRequirementsAPIModel)) {
      return MortgageRequirementsAPIModel.fromJson(json) as T;
    } else if (identical(T, GetMortgageRequirementsAPIModel)) {
      return GetMortgageRequirementsAPIModel.fromJson(json) as T;
    } else if (identical(T, GetGenInsRequirementsAPIModel)) {
      return GetGenInsRequirementsAPIModel.fromJson(json) as T;
    } else if (identical(T, GetMortgageLoanReasonsAPIModel)) {
      return GetMortgageLoanReasonsAPIModel.fromJson(json) as T;
    } else if (identical(T, PostMortgageLoandReasonAPIModel)) {
      return PostMortgageLoandReasonAPIModel.fromJson(json) as T;
    } else if (identical(T, GetEquityReleasePowerAttorneyAPIModel)) {
      return GetEquityReleasePowerAttorneyAPIModel.fromJson(json) as T;
    } else if (identical(T, GetPersonalProcessionAPIModel)) {
      return GetPersonalProcessionAPIModel.fromJson(json) as T;
    } else if (identical(T, PostPersonalProcessionAPIModel)) {
      return PostPersonalProcessionAPIModel.fromJson(json) as T;
    } else if (identical(T, GenInsAPIModel)) {
      return GenInsAPIModel.fromJson(json) as T;
    }
    //  Requirements -> Step 2: Essential Contact Informations
    else if (identical(T, GetES1APIModel)) {
      return GetES1APIModel.fromJson(json) as T;
    } else if (identical(T, GetES2APIModel)) {
      return GetES2APIModel.fromJson(json) as T;
    } else if (identical(T, GetContactAutoSugAPIModel)) {
      return GetContactAutoSugAPIModel.fromJson(json) as T;
    } else if (identical(T, PostContactAutoSugAPIModel)) {
      return PostContactAutoSugAPIModel.fromJson(json) as T;
    } else if (identical(T, GetESBnkAPIModel)) {
      return GetESBnkAPIModel.fromJson(json) as T;
    } else if (identical(T, PostESBnkAPIModel)) {
      return PostESBnkAPIModel.fromJson(json) as T;
    } else if (identical(T, PostES1APIModel)) {
      return PostES1APIModel.fromJson(json) as T;
    } else if (identical(T, PostES2APIModel)) {
      return PostES2APIModel.fromJson(json) as T;
    }
    //  Requirements -> Step 4: Address History
    else if (identical(T, GetAddrHistoryAPIModel)) {
      return GetAddrHistoryAPIModel.fromJson(json) as T;
    } else if (identical(T, PostAddrHistoryAPIModel)) {
      return PostAddrHistoryAPIModel.fromJson(json) as T;
    }
    //   Requirements -> Step 5:  Dependents
    else if (identical(T, GetDependentsAPIModel)) {
      return GetDependentsAPIModel.fromJson(json) as T;
    } else if (identical(T, PostDependentsAPIModel)) {
      return PostDependentsAPIModel.fromJson(json) as T;
    }
    //  Requirements -> Step 6: Cur Residential Mortgage
    else if (identical(T, GetCurResMortApiModel)) {
      return GetCurResMortApiModel.fromJson(json) as T;
    } else if (identical(T, PostCurResMortAPIModel)) {
      return PostCurResMortAPIModel.fromJson(json) as T;
    }
    //  Requirements -> Step 7: Emp & Income
    else if (identical(T, PostEmpAPIModel)) {
      return PostEmpAPIModel.fromJson(json) as T;
    } else if (identical(T, PostIncomeAPIModel)) {
      return PostIncomeAPIModel.fromJson(json) as T;
    } else if (identical(T, GetIncomeAPIModel)) {
      return GetIncomeAPIModel.fromJson(json) as T;
    } else if (identical(T, GetEmpAPIModel)) {
      return GetEmpAPIModel.fromJson(json) as T;
    }

    //  Requirements -> Step 8: Existing Policy Item
    else if (identical(T, GetExistingPolicyAPIModel)) {
      return GetExistingPolicyAPIModel.fromJson(json) as T;
    } else if (identical(T, PostExistingPolicyAPIModel)) {
      return PostExistingPolicyAPIModel.fromJson(json) as T;
    }

    //  Requirements -> Step 9: Saving Investment
    else if (identical(T, GetSavingInvAPIModel)) {
      return GetSavingInvAPIModel.fromJson(json) as T;
    } else if (identical(T, PostSavingInvAPIModel)) {
      return PostSavingInvAPIModel.fromJson(json) as T;
    }

    //  Requirements -> Step 10: Credit Commitment
    else if (identical(T, GetCreditCommitAPIModel)) {
      return GetCreditCommitAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCreditCommitAPIModel)) {
      return PostCreditCommitAPIModel.fromJson(json) as T;
    }

    //  Requirements -> Step 11: Credit History
    else if (identical(T, GetCreditHisAPIModel)) {
      return GetCreditHisAPIModel.fromJson(json) as T;
    } else if (identical(T, GetCreditHisItemsAPIModel)) {
      return GetCreditHisItemsAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCreditHisAPIModel)) {
      return PostCreditHisAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCreditHisItemsAPIModel)) {
      return PostCreditHisItemsAPIModel.fromJson(json) as T;
    }

    //  Requirements -> Step 12: Assessment of Affordability
    else if (identical(T, GetTaskAPIModel)) {
      return GetTaskAPIModel.fromJson(json) as T;
    } else if (identical(T, GetAssessAffordAPIModel)) {
      return GetAssessAffordAPIModel.fromJson(json) as T;
    } else if (identical(T, PostAssessAffordAPIModel)) {
      return PostAssessAffordAPIModel.fromJson(json) as T;
    } else if (identical(T, GetAssessAffordItemsAPIModel)) {
      return GetAssessAffordItemsAPIModel.fromJson(json) as T;
    } else if (identical(T, PostAssessAffordItemAPIModel)) {
      return PostAssessAffordItemAPIModel.fromJson(json) as T;
    }

    //  Requirements -> Step 13: Btl Portfolio
    else if (identical(T, GetBtlPortfolioAPIModel)) {
      return GetBtlPortfolioAPIModel.fromJson(json) as T;
    } else if (identical(T, PostBtlPortfolioAPIModel)) {
      return PostBtlPortfolioAPIModel.fromJson(json) as T;
    }

    //  Documents ->
    else if (identical(T, GetCaseDocAPIModel)) {
      return GetCaseDocAPIModel.fromJson(json) as T;
    } else if (identical(T, PostCaseDocAPIModel)) {
      return PostCaseDocAPIModel.fromJson(json) as T;
    }

    //  Case Application ->
    else if (identical(T, GetMortCasePaymentAPIModel)) {
      return GetMortCasePaymentAPIModel.fromJson(json) as T;
    }

    //  Action Required ->  Customer Client Agreement
    else if (identical(T, GetMortCaseInfoByTaskIdAPIModel)) {
      return GetMortCaseInfoByTaskIdAPIModel.fromJson(json) as T;
    } else if (identical(T, GetClientAgreementAcceptedEmailAPIModel)) {
      return GetClientAgreementAcceptedEmailAPIModel.fromJson(json) as T;
    } else if (identical(T, PostClientAgreementAcceptAPIModel)) {
      return PostClientAgreementAcceptAPIModel.fromJson(json) as T;
    }

    /*if (json is Iterable) {
      return _fromJsonList<K>(json) as T;
    } else if (identical(T, LoginModel)) {
      return LoginModel.fromJson(json) as T;
    } else if (T == bool ||
        T == String ||
        T == int ||
        T == double ||
        T == Map) {
      // primitives
      return json;
    } else {
      throw Exception("Unknown class");
    }*/
  }

  /*List<K> _fromJsonList<K>(List<dynamic> jsonList) {
    return jsonList
        ?.map<K>((dynamic json) => fromJson<K, void>(json))
        ?.toList();
  }*/
}
