import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/data/PrefMgr.dart';
import 'package:aitl/model/data/UserData.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';
import 'package:aitl/config/MyTheme.dart';
import 'package:aitl/Mixin.dart';
import 'package:polygon_clipper/polygon_border.dart';

class ProfileHelper with Mixin {
  //static const String ProfilePicFG_Key = "ProfilePicFG";
  //static const String ProfilePicBG_Key = "ProfilePicBG";

  getStarsRow(int rate, Color colr) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        for (int i = 0; i < 5; i++)
          Icon(
            (i < rate) ? Icons.star : Icons.star_outline,
            color: colr,
            size: 20,
          ),
      ],
    );
  }

  getStarRatingView({int rate, int reviews}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          getStarsRow(rate, MyTheme.brandColor),
          SizedBox(height: 10),
          Txt(
              txt: "(" + reviews.toString() + " Reviews)",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .7,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  getCompletionText({int pa}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
            "assets/images/icons/ribbon_icon.png",
            width: 20,
            height: 20,
          ),
          Txt(
              txt: pa.toString() + "% Completion Rate",
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize - .7,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  getAvatorStarView({int rate}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: EdgeInsets.all(18.0),
            margin: EdgeInsets.only(right: 16.0),
            decoration: ShapeDecoration(
                shape: PolygonBorder(
                    sides: 6,
                    borderRadius: 1,
                    border: BorderSide(color: Colors.grey, width: 0.5))),
            child: Image.asset(
              "assets/images/icons/user_icon.png",
              width: 40,
              height: 40,
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              getStarsRow(rate, Colors.cyan),
              SizedBox(height: 10),
              Txt(
                  txt: DateFun.getTimeAgoTxt("2021-01-27T16:16:33.47Z"),
                  txtColor: Colors.black,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false),
            ],
          )
        ],
      ),
    );
  }
}
