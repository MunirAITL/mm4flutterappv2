import 'dart:convert';
import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLineAPIModel.dart';
import 'package:aitl/model/json/db_cus/tab_timeline/TimeLinePostAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';
import 'package:json_string/json_string.dart';

class TimelineChatAPIMgr with Mixin {
  static final TimelineChatAPIMgr _shared = TimelineChatAPIMgr._internal();

  factory TimelineChatAPIMgr() {
    return _shared;
  }

  TimelineChatAPIMgr._internal();

  wsPostTimelineAPI({
    BuildContext context,
    dynamic param,
    Function(TimeLinePostAPIModel) callback,
  }) async {
    try {
      final jsonString = JsonString(json.encode(param));
      myLog(jsonString.source);

      await NetworkMgr()
          .req<TimeLinePostAPIModel, Null>(
        context: context,
        url: Server.TIMELINE_POST_URL,
        isLoading: false,
        param: param,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  wsOnPageLoad({
    BuildContext context,
    String url,
    Function(TimeLineAPIModel) callback,
  }) async {
    try {
      myLog(url);
      await NetworkMgr()
          .req<TimeLineAPIModel, Null>(
        context: context,
        reqType: ReqType.Get,
        url: url,
        isLoading: false,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
