import 'dart:convert';

import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDertailsAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditInfoParms.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditInfoPostAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';
import 'package:json_string/json_string.dart';

class CreditInfoPostApiMgr with Mixin {
  static final CreditInfoPostApiMgr _shared = CreditInfoPostApiMgr._internal();

  factory CreditInfoPostApiMgr() {
    return _shared;
  }

  CreditInfoPostApiMgr._internal();

  creditUserInfoPost({
    BuildContext context,
    CreditInfoParms creditInfoParms,
    Function(CreditInfoPostAPIModel) callback,
  }) async {
    final jsonString = JsonString(json.encode(creditInfoParms.toJson()));
    myLog(jsonString.source);

    try {
      await NetworkMgr()
          .req<CreditInfoPostAPIModel, Null>(
        context: context,
        reqType: ReqType.Post,
        param: creditInfoParms.toJson(),
        url: Server.CREDIT_USER_INFO_POST_URL,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog("CREDIT_USER_INFO_POST_URL  ERROR = " + e.toString());
    }
  }
}
