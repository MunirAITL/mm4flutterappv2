import 'dart:convert';

import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditDertailsAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditInfoParms.dart';
import 'package:aitl/model/json/db_cus/credit_case/CreditInfoPostAPIModel.dart';
import 'package:aitl/model/json/db_cus/credit_case/getKbaQuestionAPIModel.dart';
import 'package:flutter/cupertino.dart';
import 'package:aitl/Mixin.dart';
import 'package:json_string/json_string.dart';

class GetKbaCreditQuestionApiMgr with Mixin {
  static final GetKbaCreditQuestionApiMgr _shared =
      GetKbaCreditQuestionApiMgr._internal();

  factory GetKbaCreditQuestionApiMgr() {
    return _shared;
  }

  GetKbaCreditQuestionApiMgr._internal();

  getKbaQuestionList({
    BuildContext context,
    String validationIdentifier,
    Function(GetKbaQuestionAPIModel) callback,
  }) async {
    var params = {"ValidationIdentifier": "$validationIdentifier"};
    //print("getQuestion bank is = $validationIdentifier");
    final jsonString = JsonString(json.encode(params));
    myLog(jsonString.source);
    try {
      await NetworkMgr()
          .req<GetKbaQuestionAPIModel, Null>(
        context: context,
        reqType: ReqType.Post,
        param: params,
        url: Server.GET_KBA_QUESTIONS_URL,
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog("GET_KBA_QUESTIONS_URL  ERROR = " + e.toString());
    }
  }
}
