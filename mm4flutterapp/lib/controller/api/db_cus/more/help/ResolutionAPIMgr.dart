import 'package:aitl/config/Server.dart';
import 'package:aitl/controller/helper/db_cus/tab_more/ResolutionHelper.dart';
import 'package:aitl/controller/network/NetworkMgr.dart';
import 'package:aitl/model/json/db_cus/tab_more/support/ResolutionAPIModel.dart';
import 'package:flutter/material.dart';
import 'package:aitl/Mixin.dart';

class ResolutionAPIMgr with Mixin {
  static final ResolutionAPIMgr _shared = ResolutionAPIMgr._internal();

  factory ResolutionAPIMgr() {
    return _shared;
  }

  ResolutionAPIMgr._internal();

  wsResolutionAPI({
    BuildContext context,
    String title,
    String desc,
    List<String> listFileUrl,
    Function(ResolutionAPIModel) callback,
  }) async {
    try {
      await NetworkMgr()
          .req<ResolutionAPIModel, Null>(
        context: context,
        url: Server.RESOLUTION_URL,
        param: ResolutionHelper().getParam(
          title: title,
          desc: desc,
          fileUrl: listFileUrl.join(','),
        ),
      )
          .then((model) async {
        callback(model);
      });
    } catch (e) {
      myLog(e.toString());
    }
  }
}
