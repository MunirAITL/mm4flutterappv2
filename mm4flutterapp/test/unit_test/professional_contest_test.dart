import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/view/db_cus/web2native/case/validate/professional_contact_validation.dart';
import 'package:test/test.dart';
//  https://pub.dev/packages/test

void main() {
  group('MM4 Group Testing: Professinal Contact Page', () {
    test('Checking data validation', () {
      var result = ProfessionalContactValidation.onValidate(
          modelContact: null, search: '', companyName: '');
      expect(
          result,
          allOf([
            "Please enter valid Contact Name Agent type already exist. Please select from contact drop-down.",
            "Please enter valid Company Name"
          ]));
    });
  });
}
